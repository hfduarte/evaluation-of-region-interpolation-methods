package jni_st_mesh;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;

import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.geometry.jts.LiteShape;
import org.jfree.ui.RefineryUtilities;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.locationtech.jts.util.GeometricShapeFactory;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JSlider;

import javax.swing.event.ChangeListener;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Font;
import javax.swing.border.BevelBorder;
import java.awt.SystemColor;
import javax.swing.JTextArea;

public class Characterization extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField png_sequence_filename_tx;
	private JButton save_interpolation_to_picture;
	private JButton play;
	private JSlider interpolation_slider;
	private JPanel viz_l_p;
	private Data d;
	private JFrame f;
	private JPanel jp_l;
	private JPanel jp_r;
	private JLabel status;
	private JPanel tools_p;
	private JButton minus;
	private JButton plus;
	private JLabel zoom_level;
	private JComboBox<String> metrics;
	
	public static final double DEFAULT_ZOOM_MULTIPLICATION_FACTOR = 0.25;
	public static final double DEFAULT_MIN_ZOOM_LEVEL = 0.25;
	public static final double DEFAULT_MAX_ZOOM_LEVEL = 100;
	
    private double zoomLevel = 1.0;
    private double minZoomLevel = DEFAULT_MIN_ZOOM_LEVEL;
    private double maxZoomLevel = DEFAULT_MAX_ZOOM_LEVEL;
    private double zoomMultiplicationFactor = DEFAULT_ZOOM_MULTIPLICATION_FACTOR;
    
    private double threshold;
    private JTextField rate_tx;
    private int geom_to_show_id = 0;
    private GeometryFactory geometryFactory;
    private WKTReader reader; 
    private String[] geometries_l;
    private String[] geometries_r;
    private int max;
    private TimerTask play_task;
	private int w_center;
	private int h_center;
	private double cx = 0;
	private double cy = 0;
    private Timer timer;
    private JTextField granularity_tx;
    private JCheckBox fill;
    private JSlider zoom_slider;
    private JButton show_metrics_in_chart_bt;
    private JLabel n_samples;
    private String ref_geom_wkt;
    private JLabel method_lbl;
    private JLabel method_info_lbl;
    private Geometry geometry;
    private Geometry aabb;
    private JButton visualize_metric_ev_bt;
    private JCheckBox align_geom_cb;
    
    private boolean show_l_info_about_interpolation_validity;
    private boolean show_l_info_about_geometry_validity;
    private boolean is_l_p_valid;
    private boolean is_l_q_valid;
    
    private boolean show_r_info_about_interpolation_validity;
    private boolean show_r_info_about_geometry_validity;
    private boolean is_r_p_valid;
    private boolean is_r_q_valid;

    private int granularity;
    
    private boolean is_l_interpolation_valid;
    private int num_l_invalid_geometries;
    private Boolean[] geometries_l_validity_info;
    
    private boolean is_r_interpolation_valid;
    private int num_r_invalid_geometries;
    private Boolean[] geometries_r_validity_info;
    
    private JButton save_curr_to_picture;
    
    private JCheckBox librip_cb;
    private JCheckBox pyspatiotemporalgeom_cb;
    private JCheckBox rigid_interpolation_lib_cb;
    
    private methods.PySpatiotemporalGeom pyspatiotemporalgeom_lib;
    private methods.Librip librip;
    private methods.RigidInterpolation rigid;
    private double db;
    private double de;
    
    private String w_p;
    private String w_q;
    
    private int dn;
    private JTextField zoom_factor_tx;
    private JTextField b_r_tx;
    private JTextField b_g_tx;
    private JTextField b_b_tx;
    private JTextField fill_r_tx;
    private JTextField fill_g_tx;
    private JTextField fill_b_tx;
    private JTextArea soa_dataset_wkt_tx;
    private JCheckBox use_ref_geom_cb;
    private JCheckBox use_guides_cb;
    private int horizontal_delta;
    private int vertical_delta;
    private JTextField horz_delta_tx;
    private JTextField vertical_delta_tx;
    private JButton guides_bt;
    private JButton show_points_bt;
    private boolean show_points;
    private JTextField maker_radius_tx;
    private JPanel viz_r_p;
    private JTextArea ref_wkt_tx;
    private JTextField unit_id_tx;
    private JButton char_eval_bt;
    private JButton viz_unit_bt;
    private String[] dataset_wkt;
    private String[] ref_wkt;
    private JTextField n_samples_tx;
    private JTextArea mrip_dataset_wkt_tx;
    
    private String RIGID = "Morphrip";
    private String LIBRIP = "Librip";
    private String PYGEOM = "PySpatiotemporalGeom";    
    private JButton check_input_geometries_valid_bt;
    private JButton check_interpolation_validity_bt;
    private JButton reset_bt;
    
	public Characterization(
				Data d, 
				methods.PySpatiotemporalGeom pyspatiotemporalgeom_lib, 
				methods.Librip librip, 
				methods.RigidInterpolation rigid
			) 
	{
		this.d = d;
		f = this;
		
		horizontal_delta = 5;
		vertical_delta = 5;
		show_points = false;
		
		this.pyspatiotemporalgeom_lib = pyspatiotemporalgeom_lib;
		this.librip = librip;
		this.rigid = rigid;
		
		geometries_l = null;
		geometries_r = null;
		
		dataset_wkt = null;
		ref_wkt = null;
		
		this.db = 1000;
		this.de = 2000;
		this.w_p = "";
		this.w_q = "";
		this.dn = 100;
		
	    show_l_info_about_interpolation_validity = false;
	    show_l_info_about_geometry_validity = false;
	    is_l_p_valid = false;
	    is_l_q_valid = false;
	    
	    show_r_info_about_interpolation_validity = false;
	    show_r_info_about_geometry_validity = false;
	    is_r_p_valid = false;
	    is_r_q_valid = false;
	    
	    is_l_interpolation_valid = false;
	    num_l_invalid_geometries = 0;
	    geometries_l_validity_info = null;
	    
	    is_r_interpolation_valid = false;
	    num_r_invalid_geometries = 0;
	    geometries_r_validity_info = null;
		
		geom_to_show_id = 0;
		
	    geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
	    reader = new WKTReader(geometryFactory);
			    
	    threshold = d.get_threshold();
	    
		draw_UI();
		
	    granularity = 0;
	    
		add_listeners();
		
		draw_geometry();
	}
	
	public void paint(Graphics g) 
	{
	    super.paint(g);
	    
	    jp_l.repaint();
	    jp_r.repaint();
	}

	public void draw_geometry()
	{
		viz_l_p.setLayout(new BorderLayout());
		jp_l = new DrawGraphs(1);
		viz_l_p.add(jp_l, BorderLayout.CENTER);
		
		viz_r_p.setLayout(new BorderLayout());
		jp_r = new DrawGraphs(2);
		viz_r_p.add(jp_r, BorderLayout.CENTER);
	}
	
    private class DrawGraphs extends JPanel
    {
    	private int dx = 0;
    	private int dy = 0;
    	
    	private double sx = 1;
    	private double sy = 1; 	
    	   	
    	private int xx = 0;
    	private int yy = 0;
    	
		private static final long serialVersionUID = 3203545490664689791L;
		private String[] geometries;
		private int id;
		private String method;
		
		public DrawGraphs(int id) 
		{
			setBackground(Color.white);
			
			setAlignmentY(CENTER_ALIGNMENT);
			setAlignmentX(CENTER_ALIGNMENT);
			
			this.id = id;
			
			method = "";
			
	        addMouseListener(new MouseAdapter() 
	        {
	            public void mousePressed(MouseEvent e) 
	            {
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });
	        
	        addMouseMotionListener(new MouseAdapter() 
	        {
	            public void mouseDragged(MouseEvent e) 
	            {        	
	            	int ddx = (e.getX() - xx);
	            	int ddy = (e.getY() - yy);
	            	
	            	translate(ddx, ddy);
	            	repaint();
	            	
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });
	        
	        addMouseWheelListener(new MouseAdapter()
	        {
	            public void mouseWheelMoved(MouseWheelEvent e) 
	            {
		            int notches = e.getWheelRotation();

					zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
					
		            if (notches > 0) 
		            {
		            	if (zoomLevel < maxZoomLevel) 
		            	{
		            		zoomLevel += zoomMultiplicationFactor; 
			            	scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
			            	translate(true);
		            	}
		            } else {
			           	if (zoomLevel > minZoomLevel) 
			           	{
			           		zoomLevel -= zoomMultiplicationFactor;
							scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
							translate(false);
			            }
		            }

		            repaint();
	            }
	        });       
		};
		
		@Override
        protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);

	        Graphics2D gr = (Graphics2D) g;
	        gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	        
		    gr.setStroke(new BasicStroke(1.0f));	        
		    gr.setPaint(Color.blue);

	        AffineTransform at = new AffineTransform();

	        at.translate(dx, dy);
	        at.scale(sx, -sy);
	        
	        zoom_level.setText("Zoom Level: " + sx);

	        int R = Integer.parseInt(b_r_tx.getText());
	        int G = Integer.parseInt(b_g_tx.getText());
	        int B = Integer.parseInt(b_b_tx.getText());
	        
	        int fill_R = Integer.parseInt(fill_r_tx.getText());
	        int fill_G = Integer.parseInt(fill_g_tx.getText());
	        int fill_B = Integer.parseInt(fill_b_tx.getText());
	        
			horizontal_delta = Integer.parseInt(horz_delta_tx.getText());
			vertical_delta = Integer.parseInt(vertical_delta_tx.getText());
	        
			double marker_radius = Double.parseDouble(maker_radius_tx.getText());
				        
			try 
			{				
				if(id == 1)
					geometries = geometries_l;
				else
					geometries = geometries_r;
			    
				if(geometries != null)
				{
					geometry = reader.read(geometries[geom_to_show_id]);
			    	
			    	gr.setPaint(new Color(R, G, B));
			    	gr.draw(new LiteShape(geometry, at, false));
			    	
   					if(fill.isSelected())
   					{
   						gr.setPaint(new Color(fill_R, fill_G, fill_B));
   						
		   		        for (int j = 0; j < geometry.getNumGeometries(); j++)
				   			gr.fill(new LiteShape(geometry.getGeometryN(j), at, false));
   					}
   					
					gr.setPaint(new Color(6, 40, 94));
					gr.setFont(new Font("Arial", Font.BOLD, 12));
					gr.drawString(method, 20, 30);
					
					// Guides
					
   					if(use_guides_cb.isSelected())
   					{
   						aabb = geometry.getEnvelope();
   						
   						Coordinate[] C = aabb.getCoordinates();
   						Coordinate min = new Coordinate();
   						Coordinate max = new Coordinate();
   						
   						min.setCoordinate(C[0]);
   						max.setCoordinate(C[0]);
   						
   						for(int j = 1; j < C.length; j++)
   						{
   							if(C[j].getX() < min.getX() && C[j].getY() < min.getY())
   								min.setCoordinate(C[j]);
   							
   							if(C[j].getX() > max.getX())
   								max.setCoordinate(C[j]);
   						}
   						
   						// Draw guides.
   						
   						String guide_wkt = "LINESTRING (" + (min.getX() - horizontal_delta) + " " + min.getY() + ", " + (min.getX() - horizontal_delta) + " " + (min.getY() + vertical_delta) + ")";
   						
   						LineString l = (LineString) reader.read(guide_wkt);
   			    		gr.setPaint(Color.black);
   			    		gr.draw(new LiteShape(l, at, false));
   			    		
   			    		guide_wkt = "LINESTRING (" + (max.getX() + horizontal_delta) + " " + min.getY() + ", " + (max.getX() + horizontal_delta) + " " + (min.getY() + vertical_delta) + ")";
   						
   						l = (LineString) reader.read(guide_wkt);
   			    		gr.setPaint(Color.black);
   			    		gr.draw(new LiteShape(l, at, false));
   					}
					
					// Points
					
					if(show_points)
   					{
						reset_panel_info();
						
						gr.setPaint(new Color(6, 40, 94));
						gr.setFont(new Font("Arial", Font.BOLD, 12));
						gr.drawString("N: " + String.valueOf(geometry.getNumPoints()), 20, 50); 	
						
   						Coordinate[] C = geometry.getCoordinates();
   						
   						String coordinate_wkt = "LINESTRING (" + C[0].getX() + " " + C[0].getY() + ", " + C[0].getX() + " " + C[0].getY() + ")";
   						
   						LineString l = (LineString) reader.read(coordinate_wkt);
   			    		gr.setPaint(Color.red);
   			    		gr.draw(new LiteShape(l, at, false));
   			    		
   				    	Coordinate c = l.getCoordinateN(0);
   						
   				    	// Draw marker.
   				    		
   				    	GeometricShapeFactory shape_factory = new GeometricShapeFactory();
   				    	shape_factory.setNumPoints(32);
   				    	shape_factory.setCentre(new Coordinate(c.x, c.y));
   				    	shape_factory.setSize(marker_radius);
   				    	Geometry circle = shape_factory.createCircle();
   				    		
   				    	gr.setPaint(Color.red);
   				    	gr.draw(new LiteShape(circle, at, false));
   			    		
   						coordinate_wkt = "LINESTRING (" + C[1].getX() + " " + C[1].getY() + ", " + C[1].getX() + " " + C[1].getY() + ")";
   						
   						l = (LineString) reader.read(coordinate_wkt);
   			    		gr.setPaint(Color.red);
   			    		gr.draw(new LiteShape(l, at, false));
   						
   				    	c = l.getCoordinateN(0);
   						
   				    	// Draw marker.
   				    		
   				    	shape_factory = new GeometricShapeFactory();
   				    	shape_factory.setNumPoints(32);
   				    	shape_factory.setCentre(new Coordinate(c.x, c.y));
   				    	shape_factory.setSize(marker_radius);
   				    	circle = shape_factory.createCircle();
   				    		
   				    	gr.setPaint(Color.black);
   				    	gr.draw(new LiteShape(circle, at, false));
   				    	
   						for(int j = 2; j < C.length - 1; j++)
   						{
   	   						coordinate_wkt = "LINESTRING (" + C[j].getX() + " " + C[j].getY() + ", " + C[j].getX() + " " + C[j].getY() + ")";
   	   						
   	   						l = (LineString) reader.read(coordinate_wkt);
   	   			    		gr.setPaint(Color.red);
   	   			    		gr.draw(new LiteShape(l, at, false));
   	   			    		
   	   				    	c = l.getCoordinateN(0);
   	   						
   	   				    	// Draw marker.
   	   				    		
   	   				    	shape_factory = new GeometricShapeFactory();
   	   				    	shape_factory.setNumPoints(32);
   	   				    	shape_factory.setCentre(new Coordinate(c.x, c.y));
   	   				    	shape_factory.setSize(marker_radius);
   	   				    	circle = shape_factory.createCircle();
   	   				    		
   	   				    	gr.setPaint(Color.blue);
   	   				    	gr.draw(new LiteShape(circle, at, false));
   						}
   					}
					
					// Input Validity check.
					
				    if(id == 1 && show_l_info_about_geometry_validity)
				    {
						if(is_l_p_valid && is_l_q_valid)
							gr.setPaint(new Color(6, 40, 94));
						else
							gr.setPaint(new Color(166, 10, 83));
						
						gr.setFont(new Font("Arial", Font.BOLD, 12));
						gr.drawString("Source is Valid: [" + is_l_p_valid + "] :: Target is Valid: [" + is_l_q_valid + "]", 20, 50); 				
				    }
				    else if(id == 2 && show_r_info_about_geometry_validity)
				    {
						if(is_r_p_valid && is_r_q_valid)
							gr.setPaint(new Color(6, 40, 94));
						else
							gr.setPaint(new Color(166, 10, 83));
						
						gr.setFont(new Font("Arial", Font.BOLD, 12));
						gr.drawString("Source is Valid: [" + is_r_p_valid + "] :: Target is Valid: [" + is_r_q_valid + "]", 20, 50); 				
				    }
					
					// Interpolation Validity check.
				    
				    if(id == 1 && show_l_info_about_interpolation_validity)
				    {
				    	boolean validity_info = geometries_l_validity_info[geom_to_show_id];
				    	
				    	if(validity_info)
				    		gr.setPaint(new Color(6, 40, 94));
				    	else
				    		gr.setPaint(new Color(166, 10, 83));
				    	
				    	gr.setFont(new Font("Arial", Font.BOLD, 12));
				    	gr.drawString("Observation " + (geom_to_show_id + 1) + " is Valid: [" + validity_info + "]", 20, 70);
				    	
						if(is_l_interpolation_valid)
							gr.setPaint(new Color(6, 40, 94));
						else
							gr.setPaint(new Color(166, 10, 83));
						
						gr.setFont(new Font("Arial", Font.BOLD, 12));
						gr.drawString("Interpolation is Valid: [" + is_l_interpolation_valid + "] :: N� Samples Observed: [" + granularity + "]", 20, 90);
						gr.drawString("N� Invalid Geometries: [" + num_l_invalid_geometries + "]", 20, 110);
				    }
				    else if(id == 2 && show_r_info_about_interpolation_validity)
				    {
				    	boolean validity_info = geometries_r_validity_info[geom_to_show_id];
				    	
				    	if(validity_info)
				    		gr.setPaint(new Color(6, 40, 94));
				    	else
				    		gr.setPaint(new Color(166, 10, 83));
				    	
				    	gr.setFont(new Font("Arial", Font.BOLD, 12));
				    	gr.drawString("Observation " + (geom_to_show_id + 1) + " is Valid: [" + validity_info + "]", 20, 70);
				    	
						if(is_r_interpolation_valid)
							gr.setPaint(new Color(6, 40, 94));
						else
							gr.setPaint(new Color(166, 10, 83));
						
						gr.setFont(new Font("Arial", Font.BOLD, 12));
						gr.drawString("Interpolation is Valid: [" + is_r_interpolation_valid + "] :: N� Samples Observed: [" + granularity + "]", 20, 90);
						gr.drawString("N� Invalid Geometries: [" + num_r_invalid_geometries + "]", 20, 110);
				    }
				}
			} 
			catch (ParseException e) 
			{
				e.printStackTrace();
			}
        };
		
        public void reset() 
        {
        	dx = 0;
        	dy = 0;
        	
        	sx = 1;
        	sy = 1; 	
        	   	
        	xx = 0;
        	yy = 0;
        	
        	geometries = null;
        	method = "";
        }
        
        public void reset_panel_info()
        {
		    show_l_info_about_interpolation_validity = false;
		    show_l_info_about_geometry_validity = false;
		    is_l_p_valid = false;
		    is_l_q_valid = false;
		    
		    show_r_info_about_interpolation_validity = false;
		    show_r_info_about_geometry_validity = false;
		    is_r_p_valid = false;
		    is_r_q_valid = false;
		    
		    is_l_interpolation_valid = false;
		    num_l_invalid_geometries = 0;
		    geometries_l_validity_info = null;
		    
		    is_r_interpolation_valid = false;
		    num_r_invalid_geometries = 0;
		    geometries_r_validity_info = null;
        }
        
        public void set_method(String method) 
        {
        	this.method = method;
        }
        
        public void translate(boolean sign) 
        {
        	zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
        	
        	double cx_star = cx * zoomMultiplicationFactor;
        	double cy_star = cy * zoomMultiplicationFactor;
        	
			if(sign) 
			{
				dx -= (int) cx_star;
				dy += (int) cy_star;				
			}
			else
			{
				dx += (int) cx_star;
				dy -= (int) cy_star;				
			}
        }
        
        public void translate(int x, int y) 
        {
        	dx += x;
        	dy += y;
        }
        
        public void scale(double x, double y) 
        {
        	sx += x;
        	sy += y;
        }
        
        public void adjust_screen_position()
        {
			if(id == 1)
				geometries = geometries_l;
			else
				geometries = geometries_r;
        	
        	if(geometries == null)
        		return;
        	
			try
			{
				geometry = reader.read(geometries[geom_to_show_id]);
				Point c = geometry.getCentroid();

				cx = c.getX();
				cy = c.getY();
			} 
			catch (ParseException e) {
				e.printStackTrace();
			}
		
			w_center = (int) (this.getParent().getWidth() / 2);
			h_center = (int) (this.getParent().getHeight() / 2);
			
			dx = (int) ((-cx + w_center));
			dy = (int) ((cy + h_center));
        }
    }
	
	public void draw_UI() 
	{
		setTitle("Characterization");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	    setSize(1296, 720);
	    setLocationRelativeTo(null);
	    setExtendedState(f.getExtendedState() | JFrame.MAXIMIZED_BOTH);
	    
		contentPane = new JPanel();
		contentPane.setToolTipText("");
		contentPane.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		viz_l_p = new JPanel();
		viz_l_p.setLocation(175, 10);
		
		viz_l_p.setSize(459, 590);
		
		viz_l_p.setBackground(Color.WHITE);
		viz_l_p.setBorder(null);
		contentPane.add(viz_l_p);
		
		tools_p = new JPanel();
		tools_p.setBorder(new LineBorder(new Color(0, 0, 0)));
		tools_p.setBounds(10, 616, 1260, 36);
		contentPane.add(tools_p);
		
		zoom_factor_tx = new JTextField();
		zoom_factor_tx.setBackground(SystemColor.inactiveCaptionBorder);
		zoom_factor_tx.setForeground(SystemColor.desktop);
		zoom_factor_tx.setText("0.25");
		zoom_factor_tx.setToolTipText("Zoom factor.");
		tools_p.add(zoom_factor_tx);
		zoom_factor_tx.setColumns(3);
		
		minus = new JButton("Zoom Out");
		tools_p.add(minus);
		
		plus = new JButton("Zoom In");
		tools_p.add(plus);
		
		save_curr_to_picture = new JButton("Save To Picture");
		tools_p.add(save_curr_to_picture);
	
		save_interpolation_to_picture = new JButton("Save All to PNG");
		save_interpolation_to_picture.setEnabled(false);
		save_interpolation_to_picture.setToolTipText("Save the interpolation to a sequence of png pictures.");
		tools_p.add(save_interpolation_to_picture);
		
		use_guides_cb = new JCheckBox("Use Guides");
		tools_p.add(use_guides_cb);
		
		png_sequence_filename_tx = new JTextField();
		png_sequence_filename_tx.setToolTipText("Name of the png pictures.");
		png_sequence_filename_tx.setText("d:\\\\char_eval_");
		tools_p.add(png_sequence_filename_tx);
		png_sequence_filename_tx.setColumns(16);
		
		rate_tx = new JTextField();
		rate_tx.setBackground(SystemColor.inactiveCaptionBorder);
		rate_tx.setToolTipText("Rate of the animation of the interpolation.");
		rate_tx.setText("16");
		tools_p.add(rate_tx);
		rate_tx.setColumns(2);
		
		play = new JButton("Interpolate");
		tools_p.add(play);
		
		interpolation_slider = new JSlider();
		interpolation_slider.setValue(0);
		tools_p.add(interpolation_slider);
				
		status = new JLabel("");
		tools_p.add(status);
		
		granularity_tx = new JTextField();
		granularity_tx.setToolTipText("Granularity.");
		granularity_tx.setText("1");
		granularity_tx.setBounds(11, 40, 56, 22);
		contentPane.add(granularity_tx);
		granularity_tx.setColumns(10);
		
		fill = new JCheckBox("Fill");
		fill.setBounds(105, 39, 56, 25);
		contentPane.add(fill);
		
		zoom_level = new JLabel("Zoom Level:");
		zoom_level.setBounds(1115, 10, 155, 14);
		contentPane.add(zoom_level);
		
		zoom_slider = new JSlider();
		zoom_slider.setEnabled(false);
		zoom_slider.setValue(1);
		zoom_slider.setMinimum(1);
		zoom_slider.setBounds(1115, 38, 155, 26);
		contentPane.add(zoom_slider);
		
		n_samples = new JLabel("N\u00BA Samples: ");
		n_samples.setBounds(1115, 73, 155, 14);
		contentPane.add(n_samples);
	    
	    metrics = new JComboBox<>();
	    metrics.setToolTipText("Metrics");
	    metrics.setModel(new DefaultComboBoxModel<>(new String[] {"Area", "Perimeter", "Hausdorff Distance", "Jaccard Index", "Jaccard Distance"}));
	    metrics.setBounds(11, 101, 154, 22);
	    contentPane.add(metrics);
	    
	    JLabel Metrics_lbl = new JLabel("Metrics:");
	    Metrics_lbl.setForeground(new Color(25, 25, 112));
	    Metrics_lbl.setFont(new Font("Tahoma", Font.BOLD, 12));
	    Metrics_lbl.setBounds(11, 78, 153, 16);
	    contentPane.add(Metrics_lbl);
	    
	    show_metrics_in_chart_bt = new JButton("Show Metric in Chart");
	    show_metrics_in_chart_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    show_metrics_in_chart_bt.setForeground(new Color(72, 61, 139));
	    show_metrics_in_chart_bt.setBounds(10, 130, 155, 25);
	    contentPane.add(show_metrics_in_chart_bt);
	    
	    method_lbl = new JLabel("Status:");
	    method_lbl.setForeground(new Color(0, 0, 128));
	    method_lbl.setFont(new Font("Tahoma", Font.BOLD, 12));
	    method_lbl.setBounds(1117, 100, 153, 27);
	    contentPane.add(method_lbl);
	    
	    method_info_lbl = new JLabel("");
	    method_info_lbl.setForeground(new Color(47, 79, 79));
	    method_info_lbl.setFont(new Font("Tahoma", Font.BOLD, 11));
	    method_info_lbl.setBounds(1117, 120, 153, 27);
	    contentPane.add(method_info_lbl);
	    
	    visualize_metric_ev_bt = new JButton("Visualize Metric");
	    visualize_metric_ev_bt.setForeground(new Color(47, 79, 79));
	    visualize_metric_ev_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    visualize_metric_ev_bt.setBounds(10, 161, 155, 23);
	    contentPane.add(visualize_metric_ev_bt);
	    
	    align_geom_cb = new JCheckBox("Align Geometries");
	    align_geom_cb.setSelected(true);
	    align_geom_cb.setBounds(10, 186, 155, 23);
	    contentPane.add(align_geom_cb);
	    
	    librip_cb = new JCheckBox("Librip");
	    librip_cb.setFont(new Font("Tahoma", Font.ITALIC, 11));
	    librip_cb.setForeground(new Color(25, 25, 112));
	    librip_cb.setSelected(true);
	    librip_cb.setBounds(10, 381, 153, 23);
	    contentPane.add(librip_cb);
	    
	    pyspatiotemporalgeom_cb = new JCheckBox("PySpatiotemporalGeom");
	    pyspatiotemporalgeom_cb.setFont(new Font("Tahoma", Font.ITALIC, 11));
	    pyspatiotemporalgeom_cb.setForeground(new Color(25, 25, 112));
	    pyspatiotemporalgeom_cb.setSelected(true);
	    pyspatiotemporalgeom_cb.setBounds(10, 401, 153, 23);
	    contentPane.add(pyspatiotemporalgeom_cb);
	    
	    rigid_interpolation_lib_cb = new JCheckBox("Morphrip");
	    rigid_interpolation_lib_cb.setFont(new Font("Tahoma", Font.ITALIC, 11));
	    rigid_interpolation_lib_cb.setForeground(new Color(25, 25, 112));
	    rigid_interpolation_lib_cb.setBounds(10, 421, 153, 23);
	    contentPane.add(rigid_interpolation_lib_cb);
	    
	    b_r_tx = new JTextField();
	    b_r_tx.setText("0");
	    b_r_tx.setBounds(32, 522, 31, 20);
	    contentPane.add(b_r_tx);
	    b_r_tx.setColumns(10);
	    
	    b_g_tx = new JTextField();
	    b_g_tx.setText("0");
	    b_g_tx.setBounds(73, 522, 31, 20);
	    contentPane.add(b_g_tx);
	    b_g_tx.setColumns(10);
	    
	    b_b_tx = new JTextField();
	    b_b_tx.setText("0");
	    b_b_tx.setBounds(114, 522, 31, 20);
	    contentPane.add(b_b_tx);
	    b_b_tx.setColumns(10);
	    
	    fill_r_tx = new JTextField();
	    fill_r_tx.setText("18");
	    fill_r_tx.setBounds(32, 547, 31, 20);
	    contentPane.add(fill_r_tx);
	    fill_r_tx.setColumns(10);
	    
	    fill_g_tx = new JTextField();
	    fill_g_tx.setText("21");
	    fill_g_tx.setBounds(73, 547, 31, 20);
	    contentPane.add(fill_g_tx);
	    fill_g_tx.setColumns(10);
	    
	    fill_b_tx = new JTextField();
	    fill_b_tx.setText("67");
	    fill_b_tx.setBounds(114, 547, 31, 20);
	    contentPane.add(fill_b_tx);
	    fill_b_tx.setColumns(10);
	    
	    use_ref_geom_cb = new JCheckBox("Use Reference Geometry");
	    use_ref_geom_cb.setEnabled(false);
	    use_ref_geom_cb.setSelected(true);
	    use_ref_geom_cb.setBounds(10, 206, 155, 23);
	    contentPane.add(use_ref_geom_cb);
	    
	    soa_dataset_wkt_tx = new JTextArea();
	    soa_dataset_wkt_tx.setText("POLYGON ((193.3 245.4, 192.3 243.2, 191 241.5, 189.2 241.5, 187.6 241.9, 185.8 239.6, 183.3 239.4, 183.2 237.1, 181.7 238.1, 181.2 238.1, 180 237.6, 176.6 241.6, 173.8 244, 173.2 244, 171.7 241, 167.3 236.8, 166.5 236.9, 163 233.5, 159.9 237.8, 159.7 238.5, 158.1 240.1, 157.4 240.2, 154.2 243.9, 154 244.7, 148.3 250.6, 145.3 254.9, 143 259.6, 143.1 260.7, 144.4 264.3, 145.2 266, 146.1 266.4, 146.1 267.6, 146.7 267.8, 146.7 268.9, 147.2 270.5, 149.4 271.6, 153.7 272.8, 160.2 273.5, 169.1 273.6, 170.4 271.8, 172.5 271.3, 176.2 268.4, 181 265.7, 185.3 260.8, 188.3 255.7, 192.6 247.4, 193.3 245.4));POLYGON ((175.2 270, 187 260.2, 191.5 251.1, 193.8 246.5, 194.8 242.4, 193.4 240, 192.4 239.7, 189.2 239.8, 188.5 238.5, 185.7 237.1, 185 236.8, 184.8 235.9, 184.4 235.7, 183.6 236.3, 181.5 236.4, 180 237.8, 177.4 241, 174.8 242.6, 173 239.2, 169.1 235.4, 168.1 235.3, 164.6 232, 164.3 231.9, 164.1 232.6, 161.8 236, 161 237.4, 159.3 238.8, 158.7 238.8, 156 242.5, 154.9 244.3, 149.9 249.3, 146.8 253.8, 144.8 258.3, 146 262.9, 150.2 268.4, 157.6 270.4, 164.8 272.2, 171.1 272.1, 172.2 270.8, 175.2 270));POLYGON ((194.5 247.4, 195.2 244.7, 194.2 242.2, 194 241, 193.2 240.1, 191.1 240.1, 190.3 240.5, 189.7 240.2, 189.4 239.1, 188.1 238, 186.1 236.9, 185.7 235.7, 184.1 236.4, 182.4 235.8, 180.1 237.9, 178.9 239.4, 175.6 241.8, 175.2 241.5, 174 238.5, 170.2 234.1, 169.2 234.1, 165.8 230.4, 164.4 232.4, 162.7 234.3, 162.3 235.2, 160.6 236.7, 159.7 236.7, 156.1 240.6, 155.3 241.9, 149.4 247.2, 148 248.9, 147 250.1, 144.2 254.8, 144.2 256.7, 144.7 257.3, 145 259.9, 146 262, 146.5 262.3, 146.5 263.4, 147.4 263.9, 147.6 266.2, 153.6 269.2, 160.4 270.8, 169.1 271.3, 170.9 269.7, 173.4 268.8, 179.5 265.4, 185.7 260.7, 193.8 248, 194.5 247.4));POLYGON ((186.4 263.1, 189.7 260.7, 192.8 257.7, 192.9 255.3, 192.4 253.3, 191.2 252.5, 189 252.3, 188.8 250, 187.6 248.5, 186.4 248.2, 186.8 246.2, 184.9 246.1, 183.6 245.4, 177.1 248.2, 175.3 248.5, 174.7 242.4, 172.8 238.9, 172.1 238.8, 170.4 234.4, 166.5 236.4, 164.8 237.8, 162 238.2, 154.4 242.4, 150.1 243.6, 145.5 246.3, 141.6 249.4, 141 250.7, 140.6 253.6, 140.6 256.3, 141 258.8, 141.8 261.2, 142.7 261.6, 143 263.2, 151.3 269.9, 159.1 273.4, 167.7 272.4, 170.9 271.5, 173.4 270.4, 175.7 269.1, 179.4 268.6, 186.4 263.1));POLYGON ((146 242.4, 141.2 245.2, 140.7 245.9, 140.2 248.1, 139.6 248.3, 139.4 251.2, 139.4 253.9, 139.8 255.2, 139.8 256.5, 140.5 257.4, 140.5 258.7, 146.25 264.75, 154.6 271.1, 160.8 271.7, 170.7 271.7, 176.7 269.8, 186.3 264.7, 190.9 261.5, 191.3 257.7, 190.1 256.3, 188 255.8, 187.8 252.6, 186.9 251.7, 186.2 251.4, 187 249.4, 185.3 249.3, 184.2 248.2, 182.6 248.2, 178.8 249.5, 175 249.6, 175.4 246.7, 175.2 244.6, 174.1 240.1, 173.6 239.8, 172.4 234.8, 170.8 235.8, 169.3 235.9, 166.8 237.4, 163.8 237.4, 159.2 238.7, 155.8 240.1, 151 240.7, 146 242.4));POLYGON ((153.8 235.6, 149.6 236.6, 144.4 238.9, 142.9 241.7, 141.9 245.1, 141.7 250.2, 145.3 256.8, 150.4 263, 154.1 266.1, 158.8 267.6, 167.8 268.6, 174.6 268.4, 180.6 266.5, 187.1 264, 190.9 262.2, 192.3 258.7, 192.3 257.4, 190.4 256.2, 189.3 255.7, 189.3 255, 189.6 252.5, 188.9 251.7, 188.1 251.2, 188.1 250.5, 188.2 250, 188.6 249.9, 189.1 249.6, 189.1 249.2, 188.6 249.1, 187.8 249.1, 186.5 247.5, 183.6 247.5, 180.4 248.1, 178.6 248, 177.4 247.7, 177.4 247.1, 178.3 244.5, 177.8 238.3, 177.1 237.6, 176.9 232.9, 174.6 233.5, 172.9 233.5, 171.2 234.4, 168.7 234.5, 167.8 234, 163.5 234.5, 160.9 235.5, 153.8 235.6));POLYGON ((181 234, 179 232.5, 177 230.1, 175.1 227.4, 171.6 224.2, 166.7 221.2, 165.1 221.2, 164.1 221.8, 162.5 221.8, 158.4 223.6, 156.2 225.3, 155.2 227.2, 154.6 227.2, 153.4 229.5, 150.9 237.6, 150.8 241.4, 149.9 246, 150.9 247.1, 150.9 248.9, 154.7 253.9, 155.6 256.2, 157.4 259.1, 159.6 262, 161.9 264.3, 174.7 272.3, 176.6 272.9, 177.6 272.1, 178.1 272.3, 178.5 271.6, 179.9 271.5, 180.4 269.4, 179.9 267.5, 181.5 267.2, 181.8 266.7, 182.6 266.5, 183 266.1, 182.8 263.7, 185.3 263.7, 185.1 263.4, 184.5 262.9, 184.4 262.1, 184.7 260.1, 182.7 258, 181.1 256.3, 179.4 253.9, 179.4 252.9, 183 251.4, 186.8 247.8, 186.9 247, 187.7 246.3, 190.3 243.9, 184.6 238.6, 184.4 237.2, 181 234));POLYGON ((189.8 227.8, 186.4 222.1, 181.3 220.7, 177.3 220.7, 176.9 221.2, 175.1 221.2, 170.6 224.7, 168 227.4, 164.7 230.8, 160.8 236.8, 160.9 238.9, 160.4 239.7, 160.4 242.6, 160.8 243.2, 161.4 246.3, 161.4 247.6, 161 248.7, 161.1 251.1, 161.7 255.1, 164.8 261.3, 167.2 264.4, 167.4 265.9, 169.7 269.4, 172.5 272.6, 176.4 272.7, 177.3 272.2, 178.2 270.9, 178.2 269.6, 181.2 269.4, 182.3 268.5, 182.4 267.8, 182.6 267.5, 183.4 267.6, 184 268, 184.4 268, 184.4 266.5, 185 265.6, 185.3 265.5, 185.5 265.2, 185.6 264.6, 183.9 260, 183.6 257.1, 183.8 256, 189.2 256, 192.6 254.9, 195.1 253.7, 198.2 253.1, 198.2 252.7, 197.3 252.3, 196.9 250.3, 196.9 249.5, 195.2 246.2, 195.3 244.3, 193.8 239.7, 192.4 236.9, 191.3 231.8, 189.8 227.8));POLYGON ((194.4 229.2, 191 222.8, 184.4 220.6, 181 220.8, 170 227.6, 164.4 233, 162.4 244.6, 162.4 252.8, 167 267.6, 169.6 271, 172.6 271.6, 175.2 268.8, 178.6 268.8, 179.8 267.4, 181.8 268.2, 183.4 265.6, 183.2 256.6, 192.4 257.6, 193.6 256.4, 198 256, 194.4 229.2));");
	    soa_dataset_wkt_tx.setToolTipText("Reference Geometry wkt.");
	    soa_dataset_wkt_tx.setBounds(10, 268, 155, 36);
	    contentPane.add(soa_dataset_wkt_tx);
	    
		guides_bt = new JButton("Show/Hide Guides");
		guides_bt.setForeground(new Color(47, 79, 79));
		guides_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
		guides_bt.setBounds(1113, 156, 157, 23);
		contentPane.add(guides_bt);
		
		horz_delta_tx = new JTextField();
		horz_delta_tx.setToolTipText("Horizontal delta.");
		horz_delta_tx.setText("10");
		horz_delta_tx.setBounds(1115, 180, 56, 20);
		contentPane.add(horz_delta_tx);
		horz_delta_tx.setColumns(3);
		
		vertical_delta_tx = new JTextField();
		vertical_delta_tx.setToolTipText("Vertical delta.");
		vertical_delta_tx.setText("10");
		vertical_delta_tx.setBounds(1214, 180, 56, 20);
		contentPane.add(vertical_delta_tx);
		vertical_delta_tx.setColumns(3);
		
		show_points_bt = new JButton("Show Points");
		show_points_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
		show_points_bt.setBounds(1114, 201, 156, 23);
		contentPane.add(show_points_bt);
		
		maker_radius_tx = new JTextField();
		maker_radius_tx.setToolTipText("Marker radius.");
		maker_radius_tx.setText("1");
		maker_radius_tx.setBounds(1115, 226, 56, 20);
		contentPane.add(maker_radius_tx);
		maker_radius_tx.setColumns(10);
		
		viz_r_p = new JPanel();
		viz_r_p.setBackground(Color.WHITE);
		viz_r_p.setBounds(644, 10, 461, 590);
		contentPane.add(viz_r_p);
		
		ref_wkt_tx = new JTextArea();
		ref_wkt_tx.setToolTipText("Ref. geometries.");
		ref_wkt_tx.setText("POLYGON ((193.3 245.4, 192.3 243.2, 191 241.5, 189.2 241.5, 187.6 241.9, 185.8 239.6, 183.3 239.4, 183.2 237.1, 181.7 238.1, 181.2 238.1, 180 237.6, 176.6 241.6, 173.8 244, 173.2 244, 171.7 241, 167.3 236.8, 166.5 236.9, 163 233.5, 159.9 237.8, 159.7 238.5, 158.1 240.1, 157.4 240.2, 154.2 243.9, 154 244.7, 148.3 250.6, 145.3 254.9, 143 259.6, 143.1 260.7, 144.4 264.3, 145.2 266, 146.1 266.4, 146.1 267.6, 146.7 267.8, 146.7 268.9, 147.2 270.5, 149.4 271.6, 153.7 272.8, 160.2 273.5, 169.1 273.6, 170.4 271.8, 172.5 271.3, 176.2 268.4, 181 265.7, 185.3 260.8, 188.3 255.7, 192.6 247.4, 193.3 245.4));POLYGON ((175.2 270, 187 260.2, 191.5 251.1, 193.8 246.5, 194.8 242.4, 193.4 240, 192.4 239.7, 189.2 239.8, 188.5 238.5, 185.7 237.1, 185 236.8, 184.8 235.9, 184.4 235.7, 183.6 236.3, 181.5 236.4, 180 237.8, 177.4 241, 174.8 242.6, 173 239.2, 169.1 235.4, 168.1 235.3, 164.6 232, 164.3 231.9, 164.1 232.6, 161.8 236, 161 237.4, 159.3 238.8, 158.7 238.8, 156 242.5, 154.9 244.3, 149.9 249.3, 146.8 253.8, 144.8 258.3, 146 262.9, 150.2 268.4, 157.6 270.4, 164.8 272.2, 171.1 272.1, 172.2 270.8, 175.2 270));POLYGON ((194.5 247.4, 195.2 244.7, 194.2 242.2, 194 241, 193.2 240.1, 191.1 240.1, 190.3 240.5, 189.7 240.2, 189.4 239.1, 188.1 238, 186.1 236.9, 185.7 235.7, 184.1 236.4, 182.4 235.8, 180.1 237.9, 178.9 239.4, 175.6 241.8, 175.2 241.5, 174 238.5, 170.2 234.1, 169.2 234.1, 165.8 230.4, 164.4 232.4, 162.7 234.3, 162.3 235.2, 160.6 236.7, 159.7 236.7, 156.1 240.6, 155.3 241.9, 149.4 247.2, 148 248.9, 147 250.1, 144.2 254.8, 144.2 256.7, 144.7 257.3, 145 259.9, 146 262, 146.5 262.3, 146.5 263.4, 147.4 263.9, 147.6 266.2, 153.6 269.2, 160.4 270.8, 169.1 271.3, 170.9 269.7, 173.4 268.8, 179.5 265.4, 185.7 260.7, 193.8 248, 194.5 247.4));POLYGON ((186.4 263.1, 189.7 260.7, 192.8 257.7, 192.9 255.3, 192.4 253.3, 191.2 252.5, 189 252.3, 188.8 250, 187.6 248.5, 186.4 248.2, 186.8 246.2, 184.9 246.1, 183.6 245.4, 177.1 248.2, 175.3 248.5, 174.7 242.4, 172.8 238.9, 172.1 238.8, 170.4 234.4, 166.5 236.4, 164.8 237.8, 162 238.2, 154.4 242.4, 150.1 243.6, 145.5 246.3, 141.6 249.4, 141 250.7, 140.6 253.6, 140.6 256.3, 141 258.8, 141.8 261.2, 142.7 261.6, 143 263.2, 151.3 269.9, 159.1 273.4, 167.7 272.4, 170.9 271.5, 173.4 270.4, 175.7 269.1, 179.4 268.6, 186.4 263.1));POLYGON ((146 242.4, 141.2 245.2, 140.7 245.9, 140.2 248.1, 139.6 248.3, 139.4 251.2, 139.4 253.9, 139.8 255.2, 139.8 256.5, 140.5 257.4, 140.5 258.7, 146.25 264.75, 154.6 271.1, 160.8 271.7, 170.7 271.7, 176.7 269.8, 186.3 264.7, 190.9 261.5, 191.3 257.7, 190.1 256.3, 188 255.8, 187.8 252.6, 186.9 251.7, 186.2 251.4, 187 249.4, 185.3 249.3, 184.2 248.2, 182.6 248.2, 178.8 249.5, 175 249.6, 175.4 246.7, 175.2 244.6, 174.1 240.1, 173.6 239.8, 172.4 234.8, 170.8 235.8, 169.3 235.9, 166.8 237.4, 163.8 237.4, 159.2 238.7, 155.8 240.1, 151 240.7, 146 242.4));POLYGON ((153.8 235.6, 149.6 236.6, 144.4 238.9, 142.9 241.7, 141.9 245.1, 141.7 250.2, 145.3 256.8, 150.4 263, 154.1 266.1, 158.8 267.6, 167.8 268.6, 174.6 268.4, 180.6 266.5, 187.1 264, 190.9 262.2, 192.3 258.7, 192.3 257.4, 190.4 256.2, 189.3 255.7, 189.3 255, 189.6 252.5, 188.9 251.7, 188.1 251.2, 188.1 250.5, 188.2 250, 188.6 249.9, 189.1 249.6, 189.1 249.2, 188.6 249.1, 187.8 249.1, 186.5 247.5, 183.6 247.5, 180.4 248.1, 178.6 248, 177.4 247.7, 177.4 247.1, 178.3 244.5, 177.8 238.3, 177.1 237.6, 176.9 232.9, 174.6 233.5, 172.9 233.5, 171.2 234.4, 168.7 234.5, 167.8 234, 163.5 234.5, 160.9 235.5, 153.8 235.6));POLYGON ((181 234, 179 232.5, 177 230.1, 175.1 227.4, 171.6 224.2, 166.7 221.2, 165.1 221.2, 164.1 221.8, 162.5 221.8, 158.4 223.6, 156.2 225.3, 155.2 227.2, 154.6 227.2, 153.4 229.5, 150.9 237.6, 150.8 241.4, 149.9 246, 150.9 247.1, 150.9 248.9, 154.7 253.9, 155.6 256.2, 157.4 259.1, 159.6 262, 161.9 264.3, 174.7 272.3, 176.6 272.9, 177.6 272.1, 178.1 272.3, 178.5 271.6, 179.9 271.5, 180.4 269.4, 179.9 267.5, 181.5 267.2, 181.8 266.7, 182.6 266.5, 183 266.1, 182.8 263.7, 185.3 263.7, 185.1 263.4, 184.5 262.9, 184.4 262.1, 184.7 260.1, 182.7 258, 181.1 256.3, 179.4 253.9, 179.4 252.9, 183 251.4, 186.8 247.8, 186.9 247, 187.7 246.3, 190.3 243.9, 184.6 238.6, 184.4 237.2, 181 234));POLYGON ((189.8 227.8, 186.4 222.1, 181.3 220.7, 177.3 220.7, 176.9 221.2, 175.1 221.2, 170.6 224.7, 168 227.4, 164.7 230.8, 160.8 236.8, 160.9 238.9, 160.4 239.7, 160.4 242.6, 160.8 243.2, 161.4 246.3, 161.4 247.6, 161 248.7, 161.1 251.1, 161.7 255.1, 164.8 261.3, 167.2 264.4, 167.4 265.9, 169.7 269.4, 172.5 272.6, 176.4 272.7, 177.3 272.2, 178.2 270.9, 178.2 269.6, 181.2 269.4, 182.3 268.5, 182.4 267.8, 182.6 267.5, 183.4 267.6, 184 268, 184.4 268, 184.4 266.5, 185 265.6, 185.3 265.5, 185.5 265.2, 185.6 264.6, 183.9 260, 183.6 257.1, 183.8 256, 189.2 256, 192.6 254.9, 195.1 253.7, 198.2 253.1, 198.2 252.7, 197.3 252.3, 196.9 250.3, 196.9 249.5, 195.2 246.2, 195.3 244.3, 193.8 239.7, 192.4 236.9, 191.3 231.8, 189.8 227.8));");
		ref_wkt_tx.setBounds(10, 348, 155, 22);
		contentPane.add(ref_wkt_tx);
		
	    char_eval_bt = new JButton("Evaluate");
	    char_eval_bt.setToolTipText("Chaacterization  Evaluation.");
	    char_eval_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    char_eval_bt.setForeground(new Color(47, 79, 79));
	    char_eval_bt.setBounds(10, 238, 155, 23);
	    contentPane.add(char_eval_bt);
	    
	    viz_unit_bt = new JButton("Show Unit");
	    viz_unit_bt.setForeground(new Color(47, 79, 79));
	    viz_unit_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    viz_unit_bt.setBounds(10, 451, 155, 23);
	    contentPane.add(viz_unit_bt);
	    
	    unit_id_tx = new JTextField();
	    unit_id_tx.setText("1");
	    unit_id_tx.setBounds(10, 478, 56, 20);
	    contentPane.add(unit_id_tx);
	    unit_id_tx.setColumns(10);
	    
	    n_samples_tx = new JTextField();
	    n_samples_tx.setToolTipText("N\u00BA samples.");
	    n_samples_tx.setText("100");
	    n_samples_tx.setBounds(105, 478, 60, 20);
	    contentPane.add(n_samples_tx);
	    n_samples_tx.setColumns(10);
	    
	    mrip_dataset_wkt_tx = new JTextArea();
	    mrip_dataset_wkt_tx.setText("POLYGON ((193.3 245.4, 192.3 243.2, 191.0 241.5, 189.2 241.5, 187.6 241.9, 186.7 240.75, 185.8 239.6, 183.3 239.4, 183.22 237.56, 183.2 237.1, 181.7 238.1, 181.2 238.1, 180.0 237.6, 178.64 239.2, 176.6 241.6, 173.8 244.0, 173.2 244.0, 171.7 241.0, 167.3 236.8, 166.5 236.9, 163.0 233.5, 162.69 233.93, 162.38 234.36, 159.9 237.8, 159.7 238.5, 158.1 240.1, 157.4 240.2, 154.2 243.9, 154.0 244.7, 148.3 250.6, 145.3 254.9, 143.0 259.6, 143.1 260.7, 144.4 264.3, 145.2 266.0, 146.1 266.4, 146.1 267.6, 146.7 267.8, 146.7 268.9, 147.2 270.5, 149.4 271.6, 153.7 272.8, 160.2 273.5, 169.1 273.6, 170.4 271.8, 172.5 271.3, 176.2 268.4, 181.0 265.7, 185.3 260.8, 188.3 255.7, 192.6 247.4, 193.3 245.4));POLYGON ((194.8 242.4, 194.1 241.2, 193.4 240.0, 192.4 239.7, 189.2 239.8, 188.5 238.5, 185.7 237.1, 185.0 236.8, 184.8 235.9, 184.4 235.7, 183.6 236.3, 183.285 236.315, 181.5 236.4, 180.0 237.8, 177.4 241.0, 174.8 242.6, 174.44 241.92, 173.0 239.2, 169.1 235.4, 168.1 235.3, 164.6 232.0, 164.3 231.9, 164.1 232.6, 161.8 236.0, 161.0 237.4, 159.3 238.8, 158.7 238.8, 156.0 242.5, 154.9 244.3, 149.9 249.3, 146.8 253.8, 144.8 258.3, 145.04 259.22, 146.0 262.9, 146.693 263.8075, 147.39 264.726, 148.0958 265.6445, 148.790186 266.55, 149.495093 267.48, 150.2 268.4, 153.16 269.2, 157.6 270.4, 164.8 272.2, 171.1 272.1, 172.2 270.8, 175.2 270.0, 178.74 267.06, 182.28 264.12, 187.0 260.2, 191.5 251.1, 193.8 246.5, 194.8 242.4));POLYGON ((194.8 242.4, 194.1 241.2, 193.4 240.0, 192.4 239.7, 190.8 239.75, 189.6 239.8, 189.2 239.8, 188.5 238.5, 185.7 237.1, 185.0 236.8, 184.8 235.9, 184.4 235.7, 183.6 236.3, 181.5 236.4, 180.0 237.8, 177.4 241.0, 174.8 242.6, 174.4 241.75, 173.0 239.2, 169.1 235.4, 168.1 235.3, 164.6 232.0, 164.3 231.9, 164.1 232.6, 161.8 236.0, 161.0 237.4, 159.3 238.8, 158.7 238.8, 156.0 242.5, 154.9 244.3, 149.9 249.3, 148.4 251.55, 146.8 253.8, 144.8 258.3, 145.1 259.5, 145.4 260.6, 145.7 261.75, 146.0 262.9, 147.05 264.275, 148.1 265.65, 149.15 267.025, 150.2 268.4, 157.6 270.4, 164.8 272.2, 171.1 272.1, 172.2 270.8, 175.2 270.0, 181.1 265.1, 187.0 260.2, 193.12 247.87, 193.8 246.5, 194.8 242.4));POLYGON ((195.2 244.7, 194.2 242.2, 194.0 241.0, 193.2 240.1, 191.1 240.1, 190.3 240.5, 189.7 240.2, 189.4 239.1, 188.1 238.0, 186.1 236.9, 185.7 235.7, 185.3 235.875, 184.1 236.4, 182.4 235.8, 180.1 237.9, 178.9 239.4, 175.6 241.8, 175.2 241.5, 174.0 238.5, 170.2 234.1, 169.2 234.1, 165.8 230.4, 165.5 230.9, 164.4 232.4, 162.7 234.3, 162.3 235.2, 160.6 236.7, 159.7 236.7, 156.1 240.6, 155.3 241.9, 149.4 247.2, 148.0 248.9, 147.0 250.1, 144.2 254.8, 144.2 256.7, 144.7 257.3, 145.0 259.9, 146.0 262.0, 146.5 262.3, 146.5 263.4, 147.4 263.9, 147.6 266.2, 153.6 269.2, 160.4 270.8, 169.1 271.3, 170.9 269.7, 173.4 268.8, 179.5 265.4, 185.7 260.7, 193.8 248.0, 194.5 247.4, 195.2 244.7));POLYGON ((195.2 244.7, 194.2 242.2, 194.0 241.0, 193.2 240.1, 191.1 240.1, 190.3 240.5, 189.7 240.2, 189.4 239.1, 188.1 238.0, 186.1 236.9, 185.7 235.7, 184.1 236.4, 182.4 235.8, 180.1 237.9, 178.9 239.4, 175.6 241.8, 175.2 241.5, 174.0 238.5, 170.2 234.1, 169.2 234.1, 165.8 230.4, 164.4 232.4, 162.7 234.3, 162.3 235.2, 160.6 236.7, 159.7 236.7, 156.1 240.6, 155.3 241.9, 149.4 247.2, 148.0 248.9, 147.0 250.1, 144.2 254.8, 144.2 256.7, 144.7 257.3, 145.0 259.9, 146.0 262.0, 146.5 262.3, 146.5 263.4, 147.4 263.9, 147.6 266.2, 153.6 269.2, 160.4 270.8, 169.1 271.3, 170.9 269.7, 173.4 268.8, 179.5 265.4, 182.6 263.1, 185.7 260.7, 193.8 248.0, 194.5 247.4, 195.2 244.7));POLYGON ((192.8 257.7, 192.9 255.3, 192.65 254.3, 192.4 253.3, 191.2 252.5, 189.0 252.3, 188.96 251.84, 188.8 250.0, 187.6 248.5, 186.4 248.2, 186.8 246.2, 184.9 246.1, 183.6 245.4, 180.35 246.8, 177.1 248.2, 175.3 248.5, 175.18 247.28, 174.7 242.4, 172.8 238.9, 172.1 238.8, 170.4 234.4, 168.45 235.4, 166.5 236.4, 164.8 237.8, 162.7 238.1, 162.0 238.2, 154.4 242.4, 150.1 243.6, 146.65 245.625, 145.5 246.3, 144.525 247.1, 141.6 249.4, 141.0 250.7, 140.6 253.6, 140.6 256.3, 141.0 258.8, 141.2 259.4, 141.8 261.2, 142.7 261.6, 143.0 263.2, 147.15 266.6, 151.3 269.9, 159.1 273.4, 167.7 272.4, 170.9 271.5, 173.4 270.4, 175.7 269.1, 179.4 268.6, 186.4 263.1, 189.7 260.7, 192.8 257.7));POLYGON ((192.8 257.7, 192.9 255.3, 192.4 253.3, 191.2 252.5, 189.0 252.3, 188.8 250.0, 187.6 248.5, 186.4 248.2, 186.8 246.2, 184.9 246.1, 183.6 245.4, 181.65 246.24, 177.1 248.2, 175.3 248.5, 175.0 245.45, 174.7 242.4, 172.8 238.9, 172.1 238.8, 170.4 234.4, 168.45 235.4, 166.5 236.4, 164.8 237.8, 162.0 238.2, 158.2 240.3, 154.4 242.4, 150.1 243.6, 145.5 246.3, 141.6 249.4, 141.0 250.7, 140.7 252.875, 140.6 253.6, 140.6 256.3, 141.0 258.8, 141.4 260.0, 141.8 261.2, 142.7 261.6, 143.0 263.2, 151.3 269.9, 159.1 273.4, 167.7 272.4, 170.9 271.5, 173.4 270.4, 175.7 269.1, 179.4 268.6, 186.4 263.1, 189.7 260.7, 192.8 257.7));POLYGON ((190.9 261.5, 191.1 259.6, 191.3 257.7, 190.1 256.3, 188.0 255.8, 187.8 252.6, 186.9 251.7, 186.2 251.4, 187.0 249.4, 185.3 249.3, 184.2 248.2, 182.6 248.2, 178.8 249.5, 175.0 249.6, 175.4 246.7, 175.2 244.6, 174.1 240.1, 173.6 239.8, 172.4 234.8, 170.8 235.8, 169.3 235.9, 166.8 237.4, 163.8 237.4, 159.2 238.7, 155.8 240.1, 151.0 240.7, 146.0 242.4, 141.2 245.2, 140.7 245.9, 140.2 248.1, 139.6 248.3, 139.4 251.2, 139.4 253.9, 139.8 255.2, 139.8 256.5, 140.5 257.4, 140.5 258.7, 146.25 264.75, 154.6 271.1, 160.8 271.7, 165.75 271.7, 170.7 271.7, 173.7 270.75, 176.7 269.8, 182.94 266.485, 186.3 264.7, 190.9 261.5));POLYGON ((190.9 261.5, 191.2 258.65, 191.3 257.7, 190.1 256.3, 188.0 255.8, 187.96 255.16, 187.8 252.6, 186.9 251.7, 186.2 251.4, 186.4 251.0, 186.488 250.68, 186.64 250.296, 186.7952 249.912, 187.0 249.4, 186.32 249.36, 185.3 249.3, 184.2 248.2, 182.6 248.2, 178.8 249.5, 176.9 249.55, 175.0 249.6, 175.4 246.7, 175.2 244.6, 174.1 240.1, 173.6 239.8, 172.4 234.8, 170.8 235.8, 169.3 235.9, 166.8 237.4, 164.7 237.4, 163.8 237.4, 159.2 238.7, 155.8 240.1, 151.0 240.7, 146.0 242.4, 141.2 245.2, 140.7 245.9, 140.2 248.1, 139.6 248.3, 139.4 251.2, 139.4 253.9, 139.8 255.2, 139.8 256.5, 140.5 257.4, 140.5 258.7, 146.25 264.75, 150.425 267.925, 154.6 271.1, 160.8 271.7, 170.7 271.7, 176.7 269.8, 180.54 267.76, 186.3 264.7, 190.9 261.5));POLYGON ((190.9 262.2, 192.3 258.7, 192.3 257.4, 190.4 256.2, 189.3 255.7, 189.3 255.0, 189.6 252.5, 188.9 251.7, 188.1 251.2, 188.1 250.5, 188.2 250.0, 188.6 249.9, 189.1 249.6, 189.1 249.2, 188.6 249.1, 187.8 249.1, 186.5 247.5, 183.6 247.5, 180.4 248.1, 178.6 248.0, 177.4 247.7, 177.4 247.1, 178.3 244.5, 177.8 238.3, 177.1 237.6, 176.9 232.9, 174.6 233.5, 172.9 233.5, 171.2 234.4, 168.7 234.5, 167.8 234.0, 163.5 234.5, 160.9 235.5, 153.8 235.6, 149.6 236.6, 144.4 238.9, 144.1 239.46, 142.9 241.7, 142.7 242.38, 141.9 245.1, 141.8 247.65, 141.75 248.9, 141.7 250.2, 142.42 251.5, 142.99 252.576, 145.3 256.8, 150.4 263.0, 154.1 266.1, 158.8 267.6, 167.8 268.6, 174.6 268.4, 180.6 266.5, 187.1 264.0, 190.9 262.2));POLYGON ((190.9 262.2, 191.32 261.15, 191.6 260.45, 191.88 259.75, 192.3 258.7, 192.3 257.4, 190.4 256.2, 189.3 255.7, 189.3 255.0, 189.375 254.375, 189.45 253.75, 189.6 252.5, 188.9 251.7, 188.1 251.2, 188.1 250.5, 188.2 250.0, 188.6 249.9, 189.1 249.6, 189.1 249.2, 188.6 249.1, 187.8 249.1, 186.5 247.5, 183.6 247.5, 180.4 248.1, 178.6 248.0, 177.4 247.7, 177.4 247.1, 178.3 244.5, 177.8 238.3, 177.1 237.6, 177.06 236.66, 176.9 232.9, 174.6 233.5, 172.9 233.5, 171.2 234.4, 168.7 234.5, 167.8 234.0, 163.5 234.5, 160.9 235.5, 153.8 235.6, 149.6 236.6, 147.728 237.428, 146.064 238.164, 144.4 238.9, 142.9 241.7, 142.15 244.25, 141.9 245.1, 141.7 250.2, 145.3 256.8, 147.85 259.9, 150.4 263.0, 152.25 264.55, 154.1 266.1, 158.8 267.6, 162.4 268.0, 167.8 268.6, 171.2 268.5, 174.6 268.4, 180.6 266.5, 187.1 264.0, 190.9 262.2));POLYGON ((174.7 272.3, 176.6 272.9, 177.6 272.1, 178.1 272.3, 178.5 271.6, 179.9 271.5, 180.4 269.4, 179.9 267.5, 181.5 267.2, 181.8 266.7, 182.6 266.5, 183.0 266.1, 182.936 265.332, 182.872 264.564, 182.84 264.18, 182.8 263.7, 183.8 263.7, 185.3 263.7, 185.1 263.4, 184.5 262.9, 184.4 262.1, 184.7 260.1, 182.7 258.0, 181.1 256.3, 180.25 255.1, 179.4 253.9, 179.4 252.9, 183.0 251.4, 186.8 247.8, 186.9 247.0, 187.7 246.3, 190.3 243.9, 186.595 240.45, 184.6 238.6, 184.4 237.2, 181.0 234.0, 179.0 232.5, 177.0 230.1, 175.1 227.4, 171.6 224.2, 166.7 221.2, 165.1 221.2, 164.1 221.8, 162.5 221.8, 158.4 223.6, 156.2 225.3, 155.2 227.2, 154.6 227.2, 153.4 229.5, 150.9 237.6, 150.8 241.4, 149.9 246.0, 150.9 247.1, 150.9 248.9, 154.7 253.9, 155.6 256.2, 157.4 259.1, 159.6 262.0, 161.9 264.3, 167.02 267.5, 174.7 272.3));POLYGON ((174.7 272.3, 176.6 272.9, 177.6 272.1, 178.1 272.3, 178.5 271.6, 179.9 271.5, 180.4 269.4, 179.9 267.5, 181.5 267.2, 181.8 266.7, 182.6 266.5, 183.0 266.1, 182.8 263.7, 184.05 263.7, 185.3 263.7, 185.1 263.4, 184.5 262.9, 184.4 262.1, 184.55 261.1, 184.625 260.6, 184.7 260.1, 182.7 258.0, 181.1 256.3, 179.4 253.9, 179.4 252.9, 183.0 251.4, 186.8 247.8, 186.9 247.0, 187.7 246.3, 190.3 243.9, 189.958 243.582, 189.38515 243.04935, 187.09375 240.91875, 186.595 240.45, 184.6 238.6, 184.4 237.2, 181.0 234.0, 179.0 232.5, 177.0 230.1, 175.1 227.4, 171.6 224.2, 166.7 221.2, 165.1 221.2, 164.1 221.8, 162.5 221.8, 158.4 223.6, 156.2 225.3, 155.2 227.2, 154.6 227.2, 153.4 229.5, 152.4 232.74, 150.9 237.6, 150.8 241.4, 149.9 246.0, 150.9 247.1, 150.9 248.9, 151.85 250.15, 152.8 251.4, 153.75 252.65, 154.7 253.9, 155.6 256.2, 157.4 259.1, 159.6 262.0, 161.9 264.3, 165.1 266.3, 167.5 267.8, 171.1 270.05, 174.7 272.3));POLYGON ((172.5 272.6, 173.475 272.625, 174.45 272.65, 175.425 272.67, 176.4 272.7, 177.3 272.2, 178.2 270.9, 178.2 269.6, 180.15 269.47, 181.2 269.4, 182.3 268.5, 182.4 267.8, 182.6 267.5, 183.4 267.6, 184.0 268.0, 184.4 268.0, 184.4 266.5, 185.0 265.6, 185.3 265.5, 185.5 265.2, 185.6 264.6, 184.75 262.3, 183.9 260.0, 183.6 257.1, 183.8 256.0, 189.2 256.0, 192.6 254.9, 193.85 254.3, 195.1 253.7, 198.2 253.1, 198.2 252.7, 197.3 252.3, 196.9 250.3, 196.9 249.5, 195.2 246.2, 195.3 244.3, 193.8 239.7, 192.4 236.9, 191.3 231.8, 189.8 227.8, 188.1 224.95, 186.4 222.1, 185.125 221.75, 183.85 221.4, 182.57 221.05, 181.3 220.7, 177.3 220.7, 176.9 221.2, 175.1 221.2, 170.6 224.7, 168.0 227.4, 164.7 230.8, 162.75 233.8, 160.8 236.8, 160.9 238.9, 160.4 239.7, 160.4 242.6, 160.8 243.2, 161.4 246.3, 161.4 247.6, 161.0 248.7, 161.1 251.1, 161.7 255.1, 164.8 261.3, 167.2 264.4, 167.4 265.9, 169.7 269.4, 172.5 272.6));POLYGON ((172.5 272.6, 176.4 272.7, 177.3 272.2, 178.2 270.9, 178.2 269.6, 181.2 269.4, 182.3 268.5, 182.4 267.8, 182.6 267.5, 183.4 267.6, 184.0 268.0, 184.4 268.0, 184.4 266.5, 185.0 265.6, 185.3 265.5, 185.5 265.2, 185.6 264.6, 183.9 260.0, 183.6 257.1, 183.8 256.0, 189.2 256.0, 192.6 254.9, 195.1 253.7, 198.2 253.1, 198.2 252.7, 197.3 252.3, 196.9 250.3, 196.9 249.5, 195.2 246.2, 195.3 244.3, 193.8 239.7, 192.4 236.9, 191.3 231.8, 189.8 227.8, 186.4 222.1, 181.3 220.7, 177.3 220.7, 176.9 221.2, 175.1 221.2, 170.6 224.7, 168.0 227.4, 164.7 230.8, 160.8 236.8, 160.9 238.9, 160.4 239.7, 160.4 242.6, 160.8 243.2, 161.4 246.3, 161.4 247.6, 161.0 248.7, 161.1 251.1, 161.7 255.1, 164.8 261.3, 167.2 264.4, 167.4 265.9, 169.7 269.4, 172.5 272.6));POLYGON ((169.6 271.0, 172.6 271.6, 173.25 270.9, 174.42 269.64, 175.2 268.8, 178.6 268.8, 179.2 268.1, 179.5 267.75, 179.8 267.4, 180.8 267.79, 181.3 268.0, 181.8 268.2, 182.772 266.6205, 183.096 266.094, 183.24 265.86, 183.4 265.6, 183.382 264.79, 183.28 260.2, 183.24 258.4, 183.2 256.6, 188.72 257.2, 192.4 257.6, 193.6 256.4, 198.0 256.0, 197.82 254.66, 197.64 253.32, 197.366625 251.284875, 197.23905 250.33515, 196.93 248.05581, 196.72875 246.53625, 196.182 242.466, 195.8175 239.7525, 195.21 235.23, 194.4 229.2, 191.0 222.8, 184.4 220.6, 181.0 220.8, 180.56 221.072, 178.8 222.16, 175.5 224.2, 173.025 225.73, 170.0 227.6, 164.4 233.0, 163.86 236.132, 163.608 237.5936, 163.2 239.9, 163.1 240.5168, 162.6 243.44, 162.4 244.6, 162.4 245.338, 162.4 248.29, 162.4 252.8, 164.7 260.2, 165.62 263.16, 165.89 264.048, 167.0 267.6, 169.6 271.0));");
	    mrip_dataset_wkt_tx.setBounds(10, 309, 155, 34);
	    contentPane.add(mrip_dataset_wkt_tx);
	    
	    check_input_geometries_valid_bt = new JButton("Input Validity");
	    check_input_geometries_valid_bt.setForeground(new Color(47, 79, 79));
	    check_input_geometries_valid_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    check_input_geometries_valid_bt.setBounds(1115, 312, 155, 23);
	    contentPane.add(check_input_geometries_valid_bt);
	    
	    check_interpolation_validity_bt = new JButton("Interpolation Validity");
	    check_interpolation_validity_bt.setForeground(new Color(47, 79, 79));
	    check_interpolation_validity_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    check_interpolation_validity_bt.setBounds(1115, 337, 155, 23);
	    contentPane.add(check_interpolation_validity_bt);
	    
	    reset_bt = new JButton("Reset");
	    reset_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    reset_bt.setBounds(1115, 381, 155, 23);
	    contentPane.add(reset_bt);
	}

	public void add_listeners()
	{
		fill.addItemListener(new ItemListener() 
		{
			@Override 
			public void itemStateChanged(ItemEvent e) 
			{
				jp_l.repaint();
				jp_r.repaint();
			}
		});
		
        addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(WindowEvent e) 
            {
                setVisible(false);
                dispose();
            }
        });
		
        viz_unit_bt.addActionListener(new ActionListener() 
        {
			public void actionPerformed(ActionEvent arg0) 
			{
				geometries_l = null;
				geometries_r = null;
				
				((DrawGraphs) jp_l).reset();
				((DrawGraphs) jp_r).reset();
				
				dataset_wkt = null;
				
				int s_unit_id = Integer.parseInt(unit_id_tx.getText());
				int t_unit_id = 0;
				
				if(s_unit_id < 1)
				{
					JOptionPane.showMessageDialog(null, "Unit id must be > 0!", "Viz", JOptionPane.INFORMATION_MESSAGE);
				}
				else
				{
					dn = Integer.parseInt(n_samples_tx.getText());
					s_unit_id -= 1;
						
					if(librip_cb.isSelected())
					{
						if(dataset_wkt == null)
						{
							String wkt = soa_dataset_wkt_tx.getText();
								
							if(wkt != null && wkt.length() > 0)
								dataset_wkt = wkt.split(";");
						}
							
						if(dataset_wkt == null || dataset_wkt.length == 0)
						{
							JOptionPane.showMessageDialog(null, "Invalid dataset!", "Viz", JOptionPane.INFORMATION_MESSAGE);
							return;
						}

						t_unit_id = s_unit_id + 1;
							
						if(t_unit_id > dataset_wkt.length - 1)
						{
							JOptionPane.showMessageDialog(null, "Invalid Unit id!", "Viz", JOptionPane.INFORMATION_MESSAGE);
							return;
						}
							
						w_p = dataset_wkt[s_unit_id];
						w_q = dataset_wkt[t_unit_id];
							
						method_info_lbl.setText("Using Secondo...");
						contentPane.update(contentPane.getGraphics());
							
						geometries_l = librip.during_period(w_p, w_q, db, de, dn, "overlap");
						((DrawGraphs) jp_l).set_method(LIBRIP);
					}

					if(pyspatiotemporalgeom_cb.isSelected())
					{
						if(dataset_wkt == null)
						{
							String wkt = soa_dataset_wkt_tx.getText();
								
							if(wkt != null && wkt.length() > 0)
								dataset_wkt = wkt.split(";");
						}
							
						if(dataset_wkt == null || dataset_wkt.length == 0)
						{
							JOptionPane.showMessageDialog(null, "Invalid dataset!", "Viz", JOptionPane.INFORMATION_MESSAGE);
							return;
						}

						t_unit_id = s_unit_id + 1;
							
						if(t_unit_id > dataset_wkt.length - 1)
						{
							JOptionPane.showMessageDialog(null, "Invalid Unit id!", "Viz", JOptionPane.INFORMATION_MESSAGE);
							return;
						}
							
						w_p = dataset_wkt[s_unit_id];
						w_q = dataset_wkt[t_unit_id];
						
						method_info_lbl.setText("Using PySptGeom...");
						contentPane.update(contentPane.getGraphics());
							
						if(geometries_l == null)
						{
							geometries_l = pyspatiotemporalgeom_lib.during_period((int) db, (int) de, dn, w_p, w_q, (int) db, (int) de);
							((DrawGraphs) jp_l).set_method(PYGEOM);
						}
						else
						{
							geometries_r = pyspatiotemporalgeom_lib.during_period((int) db, (int) de, dn, w_p, w_q, (int) db, (int) de);
							((DrawGraphs) jp_r).set_method(PYGEOM);
						}
					}
						
					if(rigid_interpolation_lib_cb.isSelected() && (geometries_l == null || geometries_r == null))
					{
						String wkt = mrip_dataset_wkt_tx.getText();
								
						if(wkt != null && wkt.length() > 0)
							dataset_wkt = wkt.split(";");
							
						if(dataset_wkt == null || dataset_wkt.length == 0)
						{
							JOptionPane.showMessageDialog(null, "Invalid dataset!", "Viz", JOptionPane.INFORMATION_MESSAGE);
							return;
						}
						
						if(s_unit_id > 0)
							s_unit_id *= 2;
							
						t_unit_id = s_unit_id + 1;
						
						if(t_unit_id > dataset_wkt.length - 1)
						{
							JOptionPane.showMessageDialog(null, "Invalid Unit id!", "Viz", JOptionPane.INFORMATION_MESSAGE);
							return;
						}
							
						w_p = dataset_wkt[s_unit_id];
						w_q = dataset_wkt[t_unit_id];						
						
						method_info_lbl.setText("Using Morphrip...");
						contentPane.update(contentPane.getGraphics());
							
						if(geometries_l == null)
						{
							geometries_l = rigid.during_period(db, de, w_p, w_q, db, de, dn, InterpolationMethod.EQUILATERAL.get_value(), d.get_cw(), d.get_threshold(), 0.0);
							((DrawGraphs) jp_l).set_method(RIGID);
						}
						else if(geometries_r == null)
						{
							geometries_r = rigid.during_period(db, de, w_p, w_q, db, de, dn, InterpolationMethod.EQUILATERAL.get_value(), d.get_cw(), d.get_threshold(), 0.0);
							((DrawGraphs) jp_r).set_method(RIGID);
						}
					}
						
					if(geometries_l != null || geometries_r != null)
					{
						n_samples.setText("N� Samples: " + dn + " :: " + (geom_to_show_id + 1));
							
					    max = dn - 1;
							
						interpolation_slider.setMinimum(0);
						interpolation_slider.setMaximum(max);
						    
						((DrawGraphs) jp_l).adjust_screen_position();
						((DrawGraphs) jp_r).adjust_screen_position();
					}
				}
				
				method_info_lbl.setText("Done.");
				contentPane.update(contentPane.getGraphics());
								
				jp_l.repaint();
				jp_r.repaint();
			}
		});
        
        guides_bt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				if(use_guides_cb.isSelected())
					use_guides_cb.setSelected(false);
				else
					use_guides_cb.setSelected(true);
				
				jp_l.repaint();
				jp_r.repaint();
			}
		});
        
        show_points_bt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				if(show_points)
					show_points = false;
				else
					show_points = true;
				
				jp_l.repaint();
				jp_r.repaint();
			}
		});
        
		// Zoom out.
		
    	minus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs l = (DrawGraphs) jp_l;
				DrawGraphs r = (DrawGraphs) jp_r;

				zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
				
	           	if (zoomLevel > minZoomLevel) 
	           	{
	           		zoomLevel -= zoomMultiplicationFactor;
	           		
					l.scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
					l.translate(false);
					
					r.scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
					r.translate(false);
	            }

	            jp_l.repaint();
	            jp_r.repaint();
			}
		});
    	
    	// Zoom in.
    	
    	plus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs l = (DrawGraphs) jp_l;
				DrawGraphs r = (DrawGraphs) jp_r;

				zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
				
            	if (zoomLevel < maxZoomLevel) 
            	{
            		zoomLevel += zoomMultiplicationFactor;
            		
	            	l.scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
	            	l.translate(true);
	            	
	            	r.scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
	            	r.translate(true);
            	}

	            jp_l.repaint();
	            jp_r.repaint();
			}
		});
        
		save_curr_to_picture.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{				
				status.setText("Saving!");
				String filename_l_panel = png_sequence_filename_tx.getText() + "_l_panel_" + geom_to_show_id + ".png";
				String filename_r_panel = png_sequence_filename_tx.getText() + "_r_panel_" + geom_to_show_id + ".png";
				
				BufferedImage panel_image = null;
				
				try
				{
					panel_image = ScreenImage.createImage(viz_l_p);
	    			ScreenImage.writeImage(panel_image, filename_l_panel);
	    			
	    			panel_image = ScreenImage.createImage(viz_r_p);
	    			ScreenImage.writeImage(panel_image, filename_r_panel);
    			} 
				catch (IOException ex) {
    				ex.printStackTrace();
    			}
				
				jp_l.repaint();
				jp_r.repaint();
				
				JOptionPane.showMessageDialog(null, "Saved to files: " + filename_l_panel + "; " + filename_r_panel + "!", "Save", JOptionPane.INFORMATION_MESSAGE);
				status.setText("");
			}
		});
    	
		play.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if(geometries_l == null && geometries_r == null)
					return;
				
				play.setEnabled(false);
				interpolation_slider.setEnabled(false);
				geom_to_show_id = 0;
				
			    play_task = new TimerTask() 
			    {
			    	int counter = 0;
			    	
			        public void run() 
			        {
			        	jp_l.repaint();
			        	jp_r.repaint();
			        	
			        	// To show the first frame.
			        	if(counter > 2)
			        	{
				    		if(geom_to_show_id < max) 
				    			geom_to_show_id++;	    			
				    		else 
				    		{
				    			timer.cancel();
				    			
								interpolation_slider.setValue(interpolation_slider.getMaximum());
								play.setEnabled(true);
								interpolation_slider.setEnabled(true);
				    		}			        		
			        	}
			        	
			        	counter++;
			        }
			    };
			    
				timer = new Timer();
				timer.scheduleAtFixedRate(play_task, 0, Long.parseLong(rate_tx.getText()));
			}
		});
		
		check_input_geometries_valid_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Main m = d.get_main();
				
				show_points = false;
				
				if(geometries_l != null && geometries_l.length > 0)
				{
					is_l_p_valid = m.is_valid_geometry(geometries_l[0]);
					is_l_q_valid = m.is_valid_geometry(geometries_l[geometries_l.length - 1]);
					
					show_l_info_about_geometry_validity = true;
				}
				
				if(geometries_r != null && geometries_r.length > 0)
				{
					is_r_p_valid = m.is_valid_geometry(geometries_r[0]);
					is_r_q_valid = m.is_valid_geometry(geometries_r[geometries_r.length - 1]);
					
					show_r_info_about_geometry_validity = true;
				}
				
			    jp_l.repaint();
			    jp_r.repaint();
			}
		});
		
		check_interpolation_validity_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Main m = d.get_main();
				
				show_points = false;
				
				if(geometries_l != null && geometries_l.length > 0)
				{
					is_l_interpolation_valid = true;
					num_l_invalid_geometries = 0;
					granularity = geometries_l.length;
					geometries_l_validity_info = new Boolean[granularity];
					
					for(int j = 0; j < geometries_l.length; j++)
					{
						geometries_l_validity_info[j] = m.is_valid_geometry(geometries_l[j]);
						
						if(!geometries_l_validity_info[j])
						{
							is_l_interpolation_valid = false;
							num_l_invalid_geometries++;
						}
					}
							
				    show_l_info_about_interpolation_validity = true;		
				}
				
				if(geometries_r != null && geometries_r.length > 0)
				{
					is_r_interpolation_valid = true;
					num_r_invalid_geometries = 0;
					granularity = geometries_r.length;
					geometries_r_validity_info = new Boolean[granularity];
					
					for(int j = 0; j < geometries_r.length; j++)
					{
						geometries_r_validity_info[j] = m.is_valid_geometry(geometries_r[j]);
						
						if(!geometries_r_validity_info[j])
						{
							is_r_interpolation_valid = false;
							num_r_invalid_geometries++;
						}
					}
							
				    show_r_info_about_interpolation_validity = true;		
				}

			    jp_l.repaint();
			    jp_r.repaint();
			}
		});
		
		reset_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
			    show_l_info_about_interpolation_validity = false;
			    show_l_info_about_geometry_validity = false;
			    is_l_p_valid = false;
			    is_l_q_valid = false;
			    
			    show_r_info_about_interpolation_validity = false;
			    show_r_info_about_geometry_validity = false;
			    is_r_p_valid = false;
			    is_r_q_valid = false;
			    
			    is_l_interpolation_valid = false;
			    num_l_invalid_geometries = 0;
			    geometries_l_validity_info = null;
			    
			    is_r_interpolation_valid = false;
			    num_r_invalid_geometries = 0;
			    geometries_r_validity_info = null;
				
			    jp_l.repaint();
			    jp_r.repaint();
			}
		});
				
		show_metrics_in_chart_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Main functions = d.get_main();
				String metric = String.valueOf(metrics.getSelectedItem());
				
				int n_series = 3;
				Double[] metrics_statistics = new Double[dn * n_series];
				String[] keys = new String[n_series];
				boolean[] show = new boolean[n_series];
				
				show[0] = false;
				show[1] = false;
				show[2] = false;
				
				keys[0] = "Librip";
				keys[1] = "PySpatiotemporalGeom";
				keys[2] = "Morphrip";
				
				String[] keys_to_show = new String[n_series];
				String[] arr = null;
				int n_series_to_show = 0;
				
				keys_to_show[0] = "";
				keys_to_show[1] = "";
				keys_to_show[2] = "";
				
				String rwkt = ref_wkt_tx.getText();
					
				if(rwkt != null && rwkt.length() > 0)
					ref_wkt = rwkt.split(";");

				if(ref_wkt == null || ref_wkt.length == 0)
				{
					JOptionPane.showMessageDialog(null, "Invalid reference geometries!", "Viz", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				int s_unit_id = Integer.parseInt(unit_id_tx.getText());
				int t_unit_id = 0;
				
				if(s_unit_id < 1 || s_unit_id > ref_wkt.length)
				{
					JOptionPane.showMessageDialog(null, "Unit id must be > 0!", "Viz", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				s_unit_id -= 1;
				
				int h = 0;
				
				if(librip_cb.isSelected())
				{
					String wkt = soa_dataset_wkt_tx.getText();
						
					if(wkt != null && wkt.length() > 0)
						dataset_wkt = wkt.split(";");
					
					if(dataset_wkt == null || dataset_wkt.length == 0)
					{
						JOptionPane.showMessageDialog(null, "Invalid dataset!", "Viz", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					
					t_unit_id = s_unit_id + 1;
					
					if(ref_wkt.length != dataset_wkt.length - 1)
					{
						JOptionPane.showMessageDialog(null, "Invalid number of reference geometries!", "Viz", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					
					if(t_unit_id > dataset_wkt.length - 1)
					{
						JOptionPane.showMessageDialog(null, "Invalid target id!", "Viz", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					
					w_p = dataset_wkt[s_unit_id];
					w_q = dataset_wkt[t_unit_id];
					ref_geom_wkt = ref_wkt[s_unit_id];
					
					method_info_lbl.setText("Using Secondo...");
					contentPane.update(contentPane.getGraphics());
					jp_l.repaint();
					jp_r.repaint();
					
    				arr = librip.during_period(w_p, w_q, db, de, dn, "overlap");
    				
    				if(arr != null && arr.length > 0)
    				{
        				keys_to_show[n_series_to_show] = "Librip";
        				n_series_to_show++;

        				switch(metric)
        				{
        					case "Area":
        						for(int j = 0; j < dn; j++)
        							metrics_statistics[j] = functions.numerical_metric(arr[j], Metrics.AREA.get_value());
        						break;
        					case "Perimeter":
        						for(int j = 0; j < dn; j++)
        							metrics_statistics[j] = functions.numerical_metric(arr[j], Metrics.PERIMETER.get_value());
        						break;
        					case "Hausdorff Distance":
        						if(use_ref_geom_cb.isSelected())
        						{
            						for(int j = 0; j < dn; j++)
            							metrics_statistics[j] = functions.compare_geometries(ref_geom_wkt, arr[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
        						}
        						else
        						{
        							metrics_statistics[0] = functions.compare_geometries(arr[0], arr[0], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
        							
            						for(int j = 1; j < dn; j++)
            							metrics_statistics[j] = functions.compare_geometries(arr[j - 1], arr[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
        						}
        						break;
        					case "Jaccard Index":
        						if(use_ref_geom_cb.isSelected())
        						{
            						for(int j = 0; j < dn; j++)
            							metrics_statistics[j] = functions.compare_geometries(ref_geom_wkt, arr[j], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());
        						}
        						else
        						{
        							metrics_statistics[0] = functions.compare_geometries(arr[0], arr[0], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());
        							
            						for(int j = 1; j < dn; j++)
            							metrics_statistics[j] = functions.compare_geometries(arr[j - 1], arr[j], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());
        						}
        						break;
        					case "Jaccard Distance":
        						if(use_ref_geom_cb.isSelected())
        						{
            						for(int j = 0; j < dn; j++)
            							metrics_statistics[j] = functions.compare_geometries(ref_geom_wkt, arr[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
        						}
        						else
        						{
        							metrics_statistics[0] = functions.compare_geometries(arr[0], arr[0], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
        							
            						for(int j = 1; j < dn; j++)
            							metrics_statistics[j] = functions.compare_geometries(arr[j - 1], arr[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
        						}
        						break;       						
        				}
        				
        				h += dn;
    				}
				}

				if(pyspatiotemporalgeom_cb.isSelected())
				{
					String wkt = soa_dataset_wkt_tx.getText();
						
					if(wkt != null && wkt.length() > 0)
						dataset_wkt = wkt.split(";");
					
					if(dataset_wkt == null || dataset_wkt.length == 0)
					{
						JOptionPane.showMessageDialog(null, "Invalid dataset!", "Viz", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					
					t_unit_id = s_unit_id + 1;
					
					if(ref_wkt.length != dataset_wkt.length - 1)
					{
						JOptionPane.showMessageDialog(null, "Invalid number of reference geometries!", "Viz", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					
					if(t_unit_id > dataset_wkt.length - 1)
					{
						JOptionPane.showMessageDialog(null, "Invalid target id!", "Viz", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					
					w_p = dataset_wkt[s_unit_id];
					w_q = dataset_wkt[t_unit_id];
					ref_geom_wkt = ref_wkt[s_unit_id];
					
					method_info_lbl.setText("Using PySptGeom...");
					contentPane.update(contentPane.getGraphics());
					jp_l.repaint();
					jp_r.repaint();
					
    				arr = pyspatiotemporalgeom_lib.during_period(
    						((Double) db).intValue(), 
    						((Double)de).intValue(), 
    						dn, 
    						w_p, 
    						w_q, 
    						((Double)db).intValue(), 
    						((Double)de).intValue());
    				
    				if(arr != null && arr.length > 0)
    				{
        				keys_to_show[n_series_to_show] = "PySpatiotemporalGeom";
        				n_series_to_show++;
        				
        				switch(metric)
        				{
        					case "Area":
        						for(int j = 0; j < dn; j++)
        							metrics_statistics[h + j] = functions.numerical_metric(arr[j], Metrics.AREA.get_value());
        						break;
        					case "Perimeter":
        						for(int j = 0; j < dn; j++)
        							metrics_statistics[h + j] = functions.numerical_metric(arr[j], Metrics.PERIMETER.get_value());
        						break;
        					case "Hausdorff Distance":
        						if(use_ref_geom_cb.isSelected())
        						{
            						for(int j = 0; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(ref_geom_wkt, arr[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
            					}
        						else
        						{
        							metrics_statistics[h] = functions.compare_geometries(arr[0], arr[0], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
        							
            						for(int j = 1; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(arr[j - 1], arr[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
        						}
        						break;
        					case "Jaccard Index":
        						if(use_ref_geom_cb.isSelected())
        						{
            						for(int j = 0; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(ref_geom_wkt, arr[j], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());
            					}
        						else
        						{
        							metrics_statistics[h] = functions.compare_geometries(arr[0], arr[0], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());
        							
            						for(int j = 1; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(arr[j - 1], arr[j], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());
            					}
        						break;
        					case "Jaccard Distance":
        						if(use_ref_geom_cb.isSelected())
        						{
            						for(int j = 0; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(ref_geom_wkt, arr[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
            					}
        						else
        						{
        							metrics_statistics[h] = functions.compare_geometries(arr[0], arr[0], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
        							
            						for(int j = 1; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(arr[j - 1], arr[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
            					}
        						break;
        				}
        				
        				h += dn;
    				}
				}
				
				if(rigid_interpolation_lib_cb.isSelected())
				{
					String wkt = mrip_dataset_wkt_tx.getText();
						
					if(wkt != null && wkt.length() > 0)
						dataset_wkt = wkt.split(";");
					
					if(dataset_wkt == null || dataset_wkt.length == 0)
					{
						JOptionPane.showMessageDialog(null, "Invalid dataset!", "Viz", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					
					ref_geom_wkt = ref_wkt[s_unit_id];
					
					if(s_unit_id > 0)
						s_unit_id *= 2;
					
					t_unit_id = s_unit_id + 1;
					
					if(ref_wkt.length != dataset_wkt.length / 2)
					{
						JOptionPane.showMessageDialog(null, "Invalid number of reference geometries!", "Viz", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					
					if(t_unit_id > dataset_wkt.length - 1)
					{
						JOptionPane.showMessageDialog(null, "Invalid target id!", "Viz", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					
					w_p = dataset_wkt[s_unit_id];
					w_q = dataset_wkt[t_unit_id];
					
					method_info_lbl.setText("Using Morphrip...");
					contentPane.update(contentPane.getGraphics());
					jp_l.repaint();
					jp_r.repaint();
					
        			arr = rigid.during_period(db, de, w_p, w_q, db, de, dn, InterpolationMethod.EQUILATERAL.get_value(), d.get_cw(), d.get_threshold(), 0);

    				if(arr != null && arr.length > 0)
    				{
        				keys_to_show[n_series_to_show] = "Morphrip";
        				n_series_to_show++;
        				
        				switch(metric)
        				{
        					case "Area":
        						for(int j = 0; j < dn; j++)
        							metrics_statistics[h + j] = functions.numerical_metric(arr[j], Metrics.AREA.get_value());
        						break;
        					case "Perimeter":
        						for(int j = 0; j < dn; j++)
        							metrics_statistics[h + j] = functions.numerical_metric(arr[j], Metrics.PERIMETER.get_value());
        						break;
        					case "Hausdorff Distance":
        						if(use_ref_geom_cb.isSelected())
        						{
            						for(int j = 0; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(ref_geom_wkt, arr[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
        						}
        						else
        						{
        							metrics_statistics[h] = functions.compare_geometries(arr[0], arr[0], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
        							
            						for(int j = 1; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(arr[j - 1], arr[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
        						}
        						break;
        					case "Jaccard Index":
        						if(use_ref_geom_cb.isSelected())
        						{
            						for(int j = 0; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(ref_geom_wkt, arr[j], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());        							
        						}
        						else
        						{
        							metrics_statistics[h] = functions.compare_geometries(arr[0], arr[0], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());
        							
            						for(int j = 1; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(arr[j - 1], arr[j], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());
        						}
        						break;
        					case "Jaccard Distance":
        						if(use_ref_geom_cb.isSelected())
        						{
            						for(int j = 0; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(ref_geom_wkt, arr[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());        							
        						}
        						else
        						{
        							metrics_statistics[h] = functions.compare_geometries(arr[0], arr[0], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
        							
            						for(int j = 1; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(arr[j - 1], arr[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
        						}
        						break;
        				}
        				
        				h += dn;
    				}
				}
				
				method_info_lbl.setText("Preparing chart...");
				contentPane.update(contentPane.getGraphics());
				
				if(n_series_to_show > 0)
				{
					String chart_title = "";
					String chart_x_axis_legend = "";
					String chart_y_axis_legend = "";
					
					switch(metric)
					{
						case "Area":
							chart_title = "Evolution of the Area";
							chart_x_axis_legend = "Observations";
							chart_y_axis_legend = "Area (Dimensionless)";
							break;
						case "Perimeter":
							chart_title = "Evolution of the Perimeter";
							chart_x_axis_legend = "Observations";
							chart_y_axis_legend = "Perimeter (Dimensionless)";
							break;
						case "Hausdorff Distance":
							chart_title = "Evolution of the Hausdorff Distance";
							chart_x_axis_legend = "Observations";
							chart_y_axis_legend = "Hausdorff Distance (Dimensionless)";
							break;
						case "Jaccard Index":
							chart_title = "Evolution of the Jaccard Index";
							chart_x_axis_legend = "Observations";
							chart_y_axis_legend = "[0, 1]";				
							break;
						case "Jaccard Distance":
							chart_title = "Evolution of the Jaccard Distance";
							chart_x_axis_legend = "Observations";
							chart_y_axis_legend = "[0, 1]";	
							
						break;
					}
					
					LineChart2 line_chart = new LineChart2(chart_title, chart_x_axis_legend, chart_y_axis_legend, metrics_statistics, keys_to_show, n_series_to_show, dn);
				    line_chart.pack();
				    
				    RefineryUtilities.centerFrameOnScreen(line_chart);   
				    line_chart.setVisible(true);
				}
				else
					JOptionPane.showMessageDialog(null, "Nothing to compare!", "Metrics", JOptionPane.INFORMATION_MESSAGE);
				
				method_info_lbl.setText("Done.");
				contentPane.update(contentPane.getGraphics());
				
				jp_l.repaint();
				jp_r.repaint();
			}
		});
		
		char_eval_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if(!librip_cb.isSelected() && !pyspatiotemporalgeom_cb.isSelected() && !rigid_interpolation_lib_cb.isSelected())
				{
					JOptionPane.showMessageDialog(null, "Choose at least one method to evaluate!", "Prediction", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				ref_wkt = null;
				
				String wkt = ref_wkt_tx.getText();
					
				if(wkt != null && wkt.length() > 0)
					ref_wkt = wkt.split(";");

				if(ref_wkt == null || ref_wkt.length == 0)
				{
					JOptionPane.showMessageDialog(null, "Invalid reference geometries!", "Viz", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				String title = "Method;" + "I Area;" + "F Area;" + "Min. Area;" + "At;" + "Max. Area;" + "At;" + "Dev. Min;" + "Dev. Max;";
				title += "I Per.;" + "F Per.;" + "Min. Per.;" + "At;" + "Max. Per.;" + "At;" + "Dev. Min;" + "Dev. Max;";
				title += "H.;" + "Max. H.;" + "At;" + "Dev.;";
				title += "J.;" + "Max. J.;" + "At;" + "Dev.;";
				title += "MAE A;MAE P;MAE H;MAE J;";
				
				ArrayList<String> eval = new ArrayList<>();
				
				String[] librip_geoms_wkt = null;
				String[] pygeom_geoms_wkt = null;
				String[] rigid_geoms_wkt = null;
				
				Double[] librip_avg = new Double[28];
				Double[] librip_min = new Double[28];
				Double[] librip_max = new Double[28];
				
				Double[] pygeom_avg = new Double[28];
				Double[] pygeom_min = new Double[28];
				Double[] pygeom_max = new Double[28];
				
				Double[] rigid_avg = new Double[28];
				Double[] rigid_min = new Double[28];
				Double[] rigid_max = new Double[28];
				
				Double[] values = new Double[28];
				
				for(int l = 0; l < values.length; l++)
					values[l] = 0.0;
				
				for(int l = 0; l < librip_avg.length; l++)
					librip_avg[l] = 0.0;
				
				for(int l = 0; l < librip_min.length; l++)
					librip_min[l] = Double.MAX_VALUE;
				
				for(int l = 0; l < librip_max.length; l++)
					librip_max[l] = -Double.MAX_VALUE;
				
				for(int l = 0; l < pygeom_avg.length; l++)
					pygeom_avg[l] = 0.0;
				
				for(int l = 0; l < pygeom_min.length; l++)
					pygeom_min[l] = Double.MAX_VALUE;
				
				for(int l = 0; l < pygeom_max.length; l++)
					pygeom_max[l] = -Double.MAX_VALUE;
				
				for(int l = 0; l < rigid_avg.length; l++)
					rigid_avg[l] = 0.0;
				
				for(int l = 0; l < rigid_min.length; l++)
					rigid_min[l] = Double.MAX_VALUE;
				
				for(int l = 0; l < rigid_max.length; l++)
					rigid_max[l] = -Double.MAX_VALUE;
				
				dataset_wkt = null;
				
				int n_obs = ref_wkt.length;
				
				if(librip_cb.isSelected() || pyspatiotemporalgeom_cb.isSelected())
				{
					String data = soa_dataset_wkt_tx.getText();
					
					if(data != null && data.length() > 0)
						dataset_wkt = data.split(";");
					
					if(dataset_wkt == null || dataset_wkt.length == 0)
					{
						JOptionPane.showMessageDialog(null, "Invalid dataset!", "Viz", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					
					if(dataset_wkt.length != ref_wkt.length + 1)
					{
						JOptionPane.showMessageDialog(null, "Invalid number of reference geometries!", "Viz", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					
					for(int k = 0; k < dataset_wkt.length - 1; k++)
					{
						method_lbl.setText("Status: " + (k + 1) + " :: " + (dataset_wkt.length - 1));
						contentPane.update(contentPane.getGraphics());
						jp_l.repaint();
						jp_r.repaint();
						
						w_p = dataset_wkt[k];
						w_q = dataset_wkt[k+1];
					
						if(librip_cb.isSelected())
						{
							method_info_lbl.setText("Using Secondo...");
							contentPane.update(contentPane.getGraphics());
							jp_l.repaint();
							jp_r.repaint();
							
							librip_geoms_wkt = librip.during_period(w_p, w_q, db, de, dn, "overlap");
						}
					
						if(pyspatiotemporalgeom_cb.isSelected())
						{
							method_info_lbl.setText("Using PySptGeom...");
							contentPane.update(contentPane.getGraphics());
							jp_l.repaint();
							jp_r.repaint();
							
							pygeom_geoms_wkt = pyspatiotemporalgeom_lib.during_period((int) db, (int) de, dn, w_p, w_q, (int) db, (int) de);
						}
					
						if(librip_geoms_wkt != null)
						{	
							method_info_lbl.setText("Evaluating Librip...");
							contentPane.update(contentPane.getGraphics());
							jp_l.repaint();
							jp_r.repaint();
							
							double i_area = 0;
							double f_area = 0;
							
							double i_p = 0;
							double f_p = 0;
							
							double hd = 0;
							double max_hd = 0;
							int max_hd_t = 0;
							
							double jd = 0;
							double max_jd = 0;
							int max_jd_t = 0;
							
							double min_area = Double.MAX_VALUE;
							double max_area = Double.MIN_VALUE;
							
							int min_area_t = 0;
							int max_area_t = 0;
							
							double min_p = Double.MAX_VALUE;
							double max_p = Double.MIN_VALUE;					
							
							int min_p_t = 0;
							int max_p_t = 0;
							
							double A = 0;
							double P = 0;
							double H = 0;
							double J = 0;
							
							Main m = d.get_main();
							
							System.out.println(dn - 1);
							System.out.println(dn);
							
							i_area = m.numerical_metric(librip_geoms_wkt[0], Metrics.AREA.get_value());
							f_area = m.numerical_metric(librip_geoms_wkt[dn - 1], Metrics.AREA.get_value());
							
							i_p = m.numerical_metric(librip_geoms_wkt[0], Metrics.PERIMETER.get_value());
							f_p = m.numerical_metric(librip_geoms_wkt[dn - 1], Metrics.PERIMETER.get_value());
							
							hd = m.compare_geometries(ref_wkt[k], librip_geoms_wkt[dn - 1], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
							jd = m.compare_geometries(ref_wkt[k], librip_geoms_wkt[dn - 1], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
							
							double mae_a = 0;
							double mae_p = 0;
							double mae_h = 0;
							double mae_j = 0;
							
							double delta_a = f_area - i_area;
							double delta_p = f_p - i_p;
							double delta_h = hd;
							double delta_j = jd;
							double t = 0;
							int n = dn - 1;
							
							for(int j = 0; j < dn; j++)
							{
								t = j / n;
								
								if(j == 0)
								{
									A = i_area;
									P = i_p;
									H = m.compare_geometries(ref_wkt[k], librip_geoms_wkt[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
									J = m.compare_geometries(ref_wkt[k], librip_geoms_wkt[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
								}
								else if(j == n)
								{
									A = f_area;
									P = f_p;
									H = hd;
									J = jd;
								}
								else
								{
									A = m.numerical_metric(librip_geoms_wkt[j], Metrics.AREA.get_value());
									P = m.numerical_metric(librip_geoms_wkt[j], Metrics.PERIMETER.get_value());
									H = m.compare_geometries(ref_wkt[k], librip_geoms_wkt[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
									J = m.compare_geometries(ref_wkt[k], librip_geoms_wkt[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
								}
								
								mae_a += Math.abs(A - (i_area + delta_a * t));
								mae_p += Math.abs(P - (i_p + delta_p * t));
								
								mae_h += Math.abs(H - (delta_h * t));
								mae_j += Math.abs(J - (delta_j * t));
								
								if(min_area > A)
								{
									min_area = A;
									min_area_t = j;
								}
									
								if(max_area < A)
								{
									max_area = A;
									max_area_t = j;
								}
									
								if(min_p > P)
								{
									min_p = P;
									min_p_t = j;
								}
									
								if(max_p < P)
								{
									max_p = P;
									max_p_t = j;
								}
									
								if(max_hd < H)
								{
									max_hd = H;
									max_hd_t = j;
								}
									
								if(max_jd < J)
								{
									max_jd = J;
									max_jd_t = j;
								}
							}
							
							mae_a /= dn;
							mae_p /= dn;
							mae_h /= dn;
							mae_j /= dn;
							
							double min = Math.min(i_area, f_area);
							double max = Math.max(i_area, f_area);
							
							values[0] = i_area;
							values[1] = f_area;
							values[2] = min_area;
							values[3] = min_area_t + 0.0;
							values[4] = max_area;
							values[5] = max_area_t + 0.0;
							values[6] = ((min_area - min) / (min + 0.0));
							values[7] = ((max_area - max) / (max + 0.0));
							
							min = Math.min(i_p, f_p);
							max = Math.max(i_p, f_p);
							
							values[8] = i_p;
							values[9] = f_p;
							values[10] = min_p;
							values[11] = min_p_t + 0.0;
							values[12] = max_p;
							values[13] = max_p_t + 0.0;
							values[14] = ((min_p - min) / (min + 0.0));
							values[15] = ((max_p - max) / (max + 0.0));
							values[16] = hd;
							values[17] = max_hd;
							values[18] = max_hd_t + 0.0;
							values[19] = ((max_hd - hd) / hd);
							values[20] = jd;
							values[21] = max_jd;
							values[22] = max_jd_t + 0.0;
							values[23] = ((max_jd - jd) / jd);
							values[24] = mae_a;
							values[25] = mae_p;
							values[26] = mae_h;
							values[27] = mae_j;				
							
							String r = "Librip;";
														
							for(int l = 0; l < values.length; l++)
							{
								r += values[l] + ";";
								
								librip_avg[l] += values[l];
								
								if(librip_min[l] > values[l])
									librip_min[l] = values[l];

								if(librip_max[l] < values[l])
									librip_max[l] = values[l];
							}

							eval.add(r);
						}
					
						if(pygeom_geoms_wkt != null)
						{
							method_info_lbl.setText("Evaluating PySprGeom...");
							contentPane.update(contentPane.getGraphics());
							jp_l.repaint();
							jp_r.repaint();
							
							double i_area = 0;
							double f_area = 0;
							
							double i_p = 0;
							double f_p = 0;
							
							double hd = 0;
							double max_hd = 0;
							int max_hd_t = 0;
							
							double jd = 0;
							double max_jd = 0;
							int max_jd_t = 0;
							
							double min_area = Double.MAX_VALUE;
							double max_area = Double.MIN_VALUE;
							
							int min_area_t = 0;
							int max_area_t = 0;
							
							double min_p = Double.MAX_VALUE;
							double max_p = Double.MIN_VALUE;					
							
							int min_p_t = 0;
							int max_p_t = 0;
							
							double A = 0;
							double P = 0;
							double H = 0;
							double J = 0;
							
							Main m = d.get_main();
							
							i_area = m.numerical_metric(pygeom_geoms_wkt[0], Metrics.AREA.get_value());
							f_area = m.numerical_metric(pygeom_geoms_wkt[dn - 1], Metrics.AREA.get_value());
							
							i_p = m.numerical_metric(pygeom_geoms_wkt[0], Metrics.PERIMETER.get_value());
							f_p = m.numerical_metric(pygeom_geoms_wkt[dn - 1], Metrics.PERIMETER.get_value());
							
							hd = m.compare_geometries(ref_wkt[k], pygeom_geoms_wkt[dn - 1], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
							jd = m.compare_geometries(ref_wkt[k], pygeom_geoms_wkt[dn - 1], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
							
							double mae_a = 0;
							double mae_p = 0;
							double mae_h = 0;
							double mae_j = 0;
							
							double delta_a = f_area - i_area;
							double delta_p = f_p - i_p;
							double delta_h = hd;
							double delta_j = jd;
							double t = 0;
							int n = dn - 1;
							
							for(int j = 0; j < dn; j++)
							{
								t = j / n;
								
								if(j == 0)
								{
									A = i_area;
									P = i_p;
									H = m.compare_geometries(ref_wkt[k], pygeom_geoms_wkt[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
									J = m.compare_geometries(ref_wkt[k], pygeom_geoms_wkt[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
								}
								else if(j == n)
								{
									A = f_area;
									P = f_p;
									H = hd;
									J = jd;
								}
								else
								{
									A = m.numerical_metric(pygeom_geoms_wkt[j], Metrics.AREA.get_value());
									P = m.numerical_metric(pygeom_geoms_wkt[j], Metrics.PERIMETER.get_value());
									H = m.compare_geometries(ref_wkt[k], pygeom_geoms_wkt[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
									J = m.compare_geometries(ref_wkt[k], pygeom_geoms_wkt[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
								}
								
								mae_a += Math.abs(A - (i_area + delta_a * t));
								mae_p += Math.abs(P - (i_p + delta_p * t));
								
								mae_h += Math.abs(H - (delta_h * t));
								mae_j += Math.abs(J - (delta_j * t));
								
								if(min_area > A)
								{
									min_area = A;
									min_area_t = j;
								}
									
								if(max_area < A)
								{
									max_area = A;
									max_area_t = j;
								}
									
								if(min_p > P)
								{
									min_p = P;
									min_p_t = j;
								}
									
								if(max_p < P)
								{
									max_p = P;
									max_p_t = j;
								}
									
								if(max_hd < H)
								{
									max_hd = H;
									max_hd_t = j;
								}							
									
								if(max_jd < J)
								{
									max_jd = J;
									max_jd_t = j;
								}
							}
							
							mae_a /= dn;
							mae_p /= dn;
							mae_h /= dn;
							mae_j /= dn;
							
							double min = Math.min(i_area, f_area);
							double max = Math.max(i_area, f_area);
							
							values[0] = i_area;
							values[1] = f_area;
							values[2] = min_area;
							values[3] = min_area_t + 0.0;
							values[4] = max_area;
							values[5] = max_area_t + 0.0;
							values[6] = ((min_area - min) / (min + 0.0));
							values[7] = ((max_area - max) / (max + 0.0));
							
							min = Math.min(i_p, f_p);
							max = Math.max(i_p, f_p);
							
							values[8] = i_p;
							values[9] = f_p;
							values[10] = min_p;
							values[11] = min_p_t + 0.0;
							values[12] = max_p;
							values[13] = max_p_t + 0.0;
							values[14] = ((min_p - min) / (min + 0.0));
							values[15] = ((max_p - max) / (max + 0.0));
							values[16] = hd;
							values[17] = max_hd;
							values[18] = max_hd_t + 0.0;
							values[19] = ((max_hd - hd) / hd);
							values[20] = jd;
							values[21] = max_jd;
							values[22] = max_jd_t + 0.0;
							values[23] = ((max_jd - jd) / jd);
							values[24] = mae_a;
							values[25] = mae_p;
							values[26] = mae_h;
							values[27] = mae_j;
							
							String r = "PySptGeom;";
							
							for(int l = 0; l < values.length; l++)
							{
								r += values[l] + ";";
								
								pygeom_avg[l] += values[l];
								
								if(pygeom_min[l] > values[l])
									pygeom_min[l] = values[l];
								
								if(pygeom_max[l] < values[l])
									pygeom_max[l] = values[l];
							}
							
							eval.add(r);
						}
					}
				}
				
				if(rigid_interpolation_lib_cb.isSelected())
				{
					dataset_wkt = null;
					
					String data = mrip_dataset_wkt_tx.getText();
					
					if(data != null && data.length() > 0)
						dataset_wkt = data.split(";");
					
					if(dataset_wkt == null || dataset_wkt.length == 0)
					{
						JOptionPane.showMessageDialog(null, "Invalid dataset!", "Viz", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					
					if(dataset_wkt.length != ref_wkt.length * 2)
					{
						JOptionPane.showMessageDialog(null, "Invalid number of reference geometries!", "Viz", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					
					int M = 0;
					
					for(int k = 0; k < dataset_wkt.length - 1; k = k + 2)
					{
						method_lbl.setText("Status: " + (M + 1) + " :: " + (dataset_wkt.length / 2));
						contentPane.update(contentPane.getGraphics());
						jp_l.repaint();
						jp_r.repaint();
												
						w_p = dataset_wkt[k];
						w_q = dataset_wkt[k+1];
					
						method_info_lbl.setText("Using Alexa...");
						contentPane.update(contentPane.getGraphics());
						jp_l.repaint();
						jp_r.repaint();
						
						rigid_geoms_wkt = rigid.during_period(db, de, w_p, w_q, db, de, dn, InterpolationMethod.EQUILATERAL.get_value(), d.get_cw(), d.get_threshold(), 0.0);

						if(rigid_geoms_wkt != null)
						{
							method_info_lbl.setText("Evaluating Rigid...");
							contentPane.update(contentPane.getGraphics());
							jp_l.repaint();
							jp_r.repaint();
							
							double i_area = 0;
							double f_area = 0;
							
							double i_p = 0;
							double f_p = 0;
							
							double hd = 0;
							double max_hd = 0;
							int max_hd_t = 0;
							
							double jd = 0;
							double max_jd = 0;
							int max_jd_t = 0;
							
							double min_area = Double.MAX_VALUE;
							double max_area = Double.MIN_VALUE;
							
							int min_area_t = 0;
							int max_area_t = 0;
							
							double min_p = Double.MAX_VALUE;
							double max_p = Double.MIN_VALUE;					
							
							int min_p_t = 0;
							int max_p_t = 0;
							
							double A = 0;
							double P = 0;
							double H = 0;
							double J = 0;
							
							Main m = d.get_main();
							
							i_area = m.numerical_metric(rigid_geoms_wkt[0], Metrics.AREA.get_value());
							f_area = m.numerical_metric(rigid_geoms_wkt[dn - 1], Metrics.AREA.get_value());
							
							i_p = m.numerical_metric(rigid_geoms_wkt[0], Metrics.PERIMETER.get_value());
							f_p = m.numerical_metric(rigid_geoms_wkt[dn - 1], Metrics.PERIMETER.get_value());
							
							hd = m.compare_geometries(ref_wkt[M], rigid_geoms_wkt[dn - 1], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
							jd = m.compare_geometries(ref_wkt[M], rigid_geoms_wkt[dn - 1], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
							
							double mae_a = 0;
							double mae_p = 0;
							double mae_h = 0;
							double mae_j = 0;
							
							double delta_a = f_area - i_area;
							double delta_p = f_p - i_p;
							double delta_h = hd;
							double delta_j = jd;
							double t = 0;
							int n = dn - 1;
							
							for(int j = 0; j < dn; j++)
							{
								t = j / n;
								
								if(j == 0)
								{
									A = i_area;
									P = i_p;
									H = m.compare_geometries(ref_wkt[M], rigid_geoms_wkt[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
									J = m.compare_geometries(ref_wkt[M], rigid_geoms_wkt[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
								}
								else if(j == n)
								{
									A = f_area;
									P = f_p;
									H = hd;
									J = jd;
								}
								else
								{
									A = m.numerical_metric(rigid_geoms_wkt[j], Metrics.AREA.get_value());
									P = m.numerical_metric(rigid_geoms_wkt[j], Metrics.PERIMETER.get_value());
									H = m.compare_geometries(ref_wkt[M], rigid_geoms_wkt[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
									J = m.compare_geometries(ref_wkt[M], rigid_geoms_wkt[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
								}
																
								mae_a += Math.abs(A - (i_area + delta_a * t));
								mae_p += Math.abs(P - (i_p + delta_p * t));
								
								mae_h += Math.abs(H - (delta_h * t));
								mae_j += Math.abs(J - (delta_j * t));
								
								if(min_area > A)
								{
									min_area = A;
									min_area_t = j;
								}
									
								if(max_area < A)
								{
									max_area = A;
									max_area_t = j;
								}
									
								if(min_p > P)
								{
									min_p = P;
									min_p_t = j;
								}
									
								if(max_p < P)
								{
									max_p = P;
									max_p_t = j;
								}
									
								if(max_hd < H)
								{
									max_hd = H;
									max_hd_t = j;
								}							
									
								if(max_jd < J)
								{
									max_jd = J;
									max_jd_t = j;
								}
							}
							
							mae_a /= dn;
							mae_p /= dn;
							mae_h /= dn;
							mae_j /= dn;
							
							double min = Math.min(i_area, f_area);
							double max = Math.max(i_area, f_area);
							
							values[0] = i_area;
							values[1] = f_area;
							values[2] = min_area;
							values[3] = min_area_t + 0.0;
							values[4] = max_area;
							values[5] = max_area_t + 0.0;
							values[6] = ((min_area - min) / (min + 0.0));
							values[7] = ((max_area - max) / (max + 0.0));
							
							min = Math.min(i_p, f_p);
							max = Math.max(i_p, f_p);
							
							values[8] = i_p;
							values[9] = f_p;
							values[10] = min_p;
							values[11] = min_p_t + 0.0;
							values[12] = max_p;
							values[13] = max_p_t + 0.0;
							values[14] = ((min_p - min) / (min + 0.0));
							values[15] = ((max_p - max) / (max + 0.0));
							values[16] = hd;
							values[17] = max_hd;
							values[18] = max_hd_t + 0.0;
							values[19] = ((max_hd - hd) / hd);
							values[20] = jd;
							values[21] = max_jd;
							values[22] = max_jd_t + 0.0;
							values[23] = ((max_jd - jd) / jd);
							values[24] = mae_a;
							values[25] = mae_p;
							values[26] = mae_h;
							values[27] = mae_j;
							
							String r = "Morphrip;";
							
							for(int l = 0; l < values.length; l++)
							{
								r += values[l] + ";";
								
								rigid_avg[l] += values[l];
								
								if(rigid_min[l] > values[l])
									rigid_min[l] = values[l];
								
								if(rigid_max[l] < values[l])
									rigid_max[l] = values[l];
							}
							
							eval.add(r);
						}
						
						M++;
					}
				}
								
				// Add final result: avg, min, max.
				
				ArrayList<String> res = new ArrayList<String>();
				
				int lr = 0;
				int py = 0;
				int mr = 0;
				
				int n_methods = eval.size() / n_obs;
				
				if((n_methods == 2 && rigid_interpolation_lib_cb.isSelected()) || n_methods == 3)
				{
					if(n_methods == 3)
					{
						for(int k = 0; k < n_obs; k++)
						{
							res.add(eval.get(lr));
							
							py = lr + 1;
							
							res.add(eval.get(py));
							
							mr = n_obs * 2 + k;
							
							res.add(eval.get(mr));
							
							lr += 2;
						}
					}
					else
					{
						for(int k = 0; k < n_obs; k++)
						{
							res.add(eval.get(k));
														
							mr = n_obs + k;
							
							res.add(eval.get(mr));
						}
					}
					
					eval = res;
				}
				
				if(librip_cb.isSelected())
				{
					String avg = "Lrip (Avg);";
					String min = "Lrip (Min);";
					String max = "Lrip (Max);";
					
					for(int l = 0; l < librip_avg.length; l++)
					{
						librip_avg[l] /= n_obs;
						
						avg += librip_avg[l] + ";";
						min += librip_min[l] + ";";
						max += librip_max[l] + ";";
					}

					eval.add(avg);
					eval.add(min);
					eval.add(max);
				}
				
				if(pyspatiotemporalgeom_cb.isSelected())
				{
					String avg = "PyG (Avg);";
					String min = "PyG (Min);";
					String max = "PyG (Max);";
					
					for(int l = 0; l < pygeom_avg.length; l++)
					{
						pygeom_avg[l] /= n_obs;
						
						avg += pygeom_avg[l] + ";";
						min += pygeom_min[l] + ";";
						max += pygeom_max[l] + ";";
					}

					eval.add(avg);
					eval.add(min);
					eval.add(max);
				}
				
				if(rigid_interpolation_lib_cb.isSelected())
				{
					String avg = "Mrip (Avg);";
					String min = "Mrip (Min);";
					String max = "Mrip (Max);";
					
					for(int l = 0; l < rigid_avg.length; l++)
					{
						rigid_avg[l] /= n_obs;
						
						avg += rigid_avg[l] + ";";
						min += rigid_min[l] + ";";
						max += rigid_max[l] + ";";
					}
					
					eval.add(avg);
					eval.add(min);
					eval.add(max);
				}
				
				ArrayList<String> dev = new ArrayList<>();
				ArrayList<String> mae = new ArrayList<>();
				
				for(String s : eval)
				{
					String[] vals = s.split(";");
					String d = vals[0] + ";";
					String m = vals[0] + ";";
					
					for(int l = 1; l < vals.length; l++)
					{
						if(l < vals.length - 4)
							d += vals[l] + ";";
						else
							m += vals[l] + ";";
					}
					
					dev.add(d);
					mae.add(m);
				}

				String[] col_names = title.split(";");
				
				String[] dev_col_names = new String[col_names.length - 4];
				String[] mae_col_names = new String[5];
				
				int g = 1;
				
				mae_col_names[0] = col_names[0];
				
				for(int l = 0; l < col_names.length; l++)
				{
					if(l < col_names.length - 4)
						dev_col_names[l] = col_names[l];
					else
					{
						mae_col_names[g] = col_names[l];
						g++;
					}
				}
				
				Table dev_table = new Table("Prediction Evaluation", dev_col_names, dev, 1280, 600);
				dev_table.setVisible(true);
				
				Table mae_table = new Table("Prediction Evaluation", mae_col_names, mae, 800, 600);
				mae_table.setVisible(true);
								
				Table tab = new Table("Prediction Evaluation", col_names, eval, 1280, 600);
				tab.setVisible(true);
				
				method_info_lbl.setText("Done!");
				method_lbl.setText("Status:");
				contentPane.update(contentPane.getGraphics());
				
				jp_l.repaint();
				jp_r.repaint();
			}
		});

		visualize_metric_ev_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				JOptionPane.showMessageDialog(null, "TODO!", "Metrics", JOptionPane.INFORMATION_MESSAGE);
				
				/*
				Main functions = d.get_main();
				
				// Metric.
				
				String metric = String.valueOf(metrics.getSelectedItem());
				
				String[] arr = new String[geometries.length * 2];
				String[] res;
				String[] metrics = new String[geometries.length];
				
				int n = geometries.length;
				
				//int counter = 0;
				
				SimilarityMetrics geom_sim_metric = SimilarityMetrics.JACCARD_INDEX;
				
				if(metric == "Hausdorff Distance")
					geom_sim_metric = SimilarityMetrics.HAUSDORFF_DISTANCE;
				else if(metric == "Jaccard Distance")
					geom_sim_metric = SimilarityMetrics.JACCARD_DISTANCE;
				
				String[] ref_objects_wkt = null;
				
				// Compute metric.
				
				switch(metric)
				{
					case "Area":
						for(int j = 0; j < n; j++)
							metrics[j] = String.valueOf(functions.numerical_metric(geometries[j], Metrics.AREA.get_value()));
						//arr = geometries;
						break;
					case "Perimeter":
						for(int j = 0; j < n; j++)
							metrics[j] = String.valueOf(functions.numerical_metric(geometries[j], Metrics.PERIMETER.get_value()));
						//arr = geometries;
						break;
					case "Hausdorff Distance":
					case "Jaccard Index":
					case "Jaccard Distance":
						
						if(!dataset_wkt_tx.getText().isEmpty())
							ref_geom_wkt = dataset_wkt_tx.getText();
						else
							ref_geom_wkt = geometries[0];
						
						
						if(use_ref_geom_cb.isSelected())
						{
							for(int j = 0; j < n; j++)
							{
								res = functions.compare_geometries_2(ref_geom_wkt, geometries[j], geom_sim_metric.get_value(), align_geom_cb.isSelected());
								arr[j] = res[0];
								metrics[j] = res[1];
							}							
						}
						else
						{
							ref_objects_wkt = new String[n];
							
							res = functions.compare_geometries_2(geometries[0], geometries[0], geom_sim_metric.get_value(), align_geom_cb.isSelected());
							arr[0] = res[0];
							metrics[0] = res[1];
							
							ref_objects_wkt[0] = geometries[0];
														
							for(int j = 1; j < n; j++)
							{
								res = functions.compare_geometries_2(geometries[j - 1], geometries[j], geom_sim_metric.get_value(), align_geom_cb.isSelected());
								arr[j] = res[0];
								metrics[j] = res[1];
								
								ref_objects_wkt[j] = geometries[j - 1];
							}
						}
						
						for(int j = 0; j < metrics.length; j++)
							arr[n + j] = metrics[j];
						
						break;
				}
				
				int type = 1;
				String ref_geom = null;
				
				switch(metric)
				{
					case "Perimeter":
						type = 2;
						break;
					case "Hausdorff Distance":
						type = 3;
						ref_geom = ref_geom_wkt;
						break;
					case "Jaccard Index":
						type = 4;
						ref_geom = ref_geom_wkt;
						break;
					case "Jaccard Distance":
						type = 5;
						ref_geom = ref_geom_wkt;
						break;
				}

				if(type < 3)
				{
					for(int j = 0; j < n; j++)
						arr[j] = geometries[j];
					
					for(int j = 0; j < metrics.length; j++)
						arr[n + j] = metrics[j];
				}
				
    			Data _d = new Data(type, arr, ref_geom, d.get_main(), d.get_interpolation_method(), d.get_threshold(), d.get_cw(), ref_objects_wkt); 
    			final Data in = _d;
    			
    			MetricViz mviz = new MetricViz(in);
    			mviz.setVisible(true);
    			*/
			}
		});

		interpolation_slider.addChangeListener(new ChangeListener() 
		{
		    public void stateChanged(ChangeEvent e) 
		    {
		    	if(geometries_l == null && geometries_r == null)
		    		return;

		        JSlider s = (JSlider) e.getSource();
		        int i = (int) s.getValue();

		    	if(i <= max)
		    		geom_to_show_id = i;
		    	
		    	n_samples.setText("N� Samples: " + dn + " :: " + (geom_to_show_id + 1));
		    	
		    	jp_l.repaint();
		    	jp_r.repaint();
		    }
		});
	}
}