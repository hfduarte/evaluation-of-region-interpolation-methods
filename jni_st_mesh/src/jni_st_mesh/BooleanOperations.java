package jni_st_mesh;

public enum BooleanOperations 
{
	DISJOINT(1), TOUCHES(2), INTERSECTS(3), CROSSES(4), WITHIN(5), CONTAINS(6), OVERLAPS(7), EQUALS(8), COVERS(9), COVEREDBY(10);
	
	private int value;    

	private BooleanOperations(int value) {
		this.value = value;
	}

	public int get_value() {
		return value;
	}	
}