package jni_st_mesh;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.geometry.jts.LiteShape;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.locationtech.jts.util.GeometricShapeFactory;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JSlider;

import javax.swing.JLabel;
import javax.swing.JCheckBox;
import java.awt.Font;
import javax.swing.border.BevelBorder;
import java.awt.SystemColor;
import javax.swing.JTextArea;

public class Corr extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField png_sequence_filename_tx;
	private JButton play;
	private JSlider interpolation_slider;
	private JPanel viz_l_p;
	private Data d;
	private JFrame f;
	private JPanel jp_l;
	private JPanel jp_r;
	private JPanel jp_a;
	private JLabel status;
	private JPanel tools_p;
	private JButton minus;
	private JButton plus;
	private JLabel zoom_level;
	
	public static final double DEFAULT_ZOOM_MULTIPLICATION_FACTOR = 0.25;
	public static final double DEFAULT_MIN_ZOOM_LEVEL = 0.25;
	public static final double DEFAULT_MAX_ZOOM_LEVEL = 100;
	
    private double zoomLevel = 1.0;
    private double minZoomLevel = DEFAULT_MIN_ZOOM_LEVEL;
    private double maxZoomLevel = DEFAULT_MAX_ZOOM_LEVEL;
    private double zoomMultiplicationFactor = DEFAULT_ZOOM_MULTIPLICATION_FACTOR;
    
    private double threshold;
    private JTextField rate_tx;
    private int geom_to_show_id = 0;
    private GeometryFactory geometryFactory;
    private WKTReader reader; 
    private int w_center;
	private int h_center;
	private double cx = 0;
	private double cy = 0;
    private Geometry geometry;
    private boolean show_info_about_geometry_validity;
    private boolean is_p_valid;
    private boolean is_q_valid;

    private JButton save_curr_to_picture;
    
    private methods.PySpatiotemporalGeom pyspatiotemporalgeom_lib;
    private methods.Librip librip;
    private methods.RigidInterpolation rigid;
    private double db;
    private double de;
    
    private int dn;
    private JTextField zoom_factor_tx;
    private JTextArea p_wkt_tx;
    private JButton show_points_bt;
    private boolean show_points;
    private JTextField maker_radius_tx;
    private JPanel viz_r_p;
    private JTextArea q_wkt_tx;
    private JPanel aligned_panel;
    
    private String P;
    private String Q;
    private JButton load_geoms_bt;
    private JButton corr_to_wkt_save_bt;
    private JTextField wkt_save_in_file;
    private JTextField b_r_tx;
    private JTextField b_g_tx;
    private JTextField b_b_tx;
    private JTextField fill_b_tx;
    private JTextField fill_g_tx;
    private JTextField fill_r_tx;
    private JCheckBox fill;
    
    private Seg seg;
    private int panel_id;
    
    private JTextField coll_p_f_tx;
    private JButton add_coll_p_bt;
    private JButton clear_bt;
    private JTextField p_i_id_tx;
    private JTextField q_i_id_tx;
    private JButton save_bt;
    private JTextField corr_filename_tx;
    private JButton load_bt;
    private JButton interpolate_bt;
    private ArrayList<String> undo;
    private JButton undo_bt;
    private JButton redo_bt;
    private int undo_id;
    private JButton del_bt;
    private JButton reset_undo_bt;
    private JButton norm_geom_bt;
    private JButton input_validity_bt;
    
	public Corr(
				Data d, 
				methods.PySpatiotemporalGeom pyspatiotemporalgeom_lib, 
				methods.Librip librip, 
				methods.RigidInterpolation rigid
			) 
	{
		this.d = d;
		f = this;
		
		show_points = true;
		
		this.pyspatiotemporalgeom_lib = pyspatiotemporalgeom_lib;
		this.librip = librip;
		this.rigid = rigid;
		
		P = null;
		Q = null;
		
		seg = null;
		
		this.db = 1000;
		this.de = 2000;
		//this.w_p = "";
		//this.w_q = "";
		this.dn = 100;
		
		geom_to_show_id = 0;
		
	    geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
	    reader = new WKTReader(geometryFactory);
			    
	    threshold = d.get_threshold();
	    
	    undo_id = 0;
	    
	    undo = new ArrayList<String>();
	    
		draw_UI();
		
		add_listeners();
		
		draw_geometry();
	}
	
	public void paint(Graphics g) 
	{
	    super.paint(g);
	    
	    jp_l.repaint();
	    jp_r.repaint();
	    jp_a.repaint();
	}

	public void draw_geometry()
	{
		viz_l_p.setLayout(new BorderLayout());
		jp_l = new DrawGraphs(1);
		viz_l_p.add(jp_l, BorderLayout.CENTER);
		
		viz_r_p.setLayout(new BorderLayout());
		jp_r = new DrawGraphs(2);
		viz_r_p.add(jp_r, BorderLayout.CENTER);
		
		aligned_panel.setLayout(new BorderLayout());
		jp_a = new DrawGraphs(3);
		aligned_panel.add(jp_a, BorderLayout.CENTER);
	}
	
    private class DrawGraphs extends JPanel //implements MouseListener, MouseMotionListener
    {
    	private int dx = 0;
    	private int dy = 0;
    	
    	private double sx = 1;
    	private double sy = 1; 	
    	   	
    	private int xx = 0;
    	private int yy = 0;
    	
		private static final long serialVersionUID = 3203545490664689791L;

		private int id;
		private String title;
		public DrawGraphs(int id) 
		{
			setBackground(Color.white);
			
			setAlignmentY(CENTER_ALIGNMENT);
			setAlignmentX(CENTER_ALIGNMENT);
					
			this.id = id;
			
			if(this.id == 1)
				title = "P";
			else if(this.id == 2)
				title = "Q";
			else if(this.id == 3)
				title = "";
			
	        addMouseListener(new MouseAdapter() 
	        {
	            public void mousePressed(MouseEvent e) 
	            {
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });

	        addMouseListener(new MouseAdapter()
	        {
	            @Override
	            public void mouseClicked(MouseEvent e)
	            {
	                if(e.getClickCount() == 2)
	                {
	                	seg = null;
	                
	                	int x = e.getX();
	                	int y = e.getY();
	                	
	        	        AffineTransform af = new AffineTransform();
	        	        af.translate(dx, dy);
	        	        af.scale(sx, -sy);
	                	
	                	y *= -1;
	                	y += dy;
	                	y *= -1;
	                	y += dy;
	        	        
	                	java.awt.Point pp = new java.awt.Point(x, y);

	        	        java.awt.Point transformed = new java.awt.Point(0, 0);
	        	        
	                	try {
							af.inverseTransform(pp, transformed);
						} catch (NoninvertibleTransformException e2) 
	                	{
							e2.printStackTrace();
						}
		            	
		            	x = (int) transformed.getX();
		            	y = (int) transformed.getY();
		            	
		            	Coordinate[] C = null;
		            	double min = Double.MAX_VALUE;
		            	double d = 0;

		            	Coordinate curr = null;
		            	Coordinate next = null;
		            	
		            	if((P == null || P.isEmpty()) && (Q == null || Q.isEmpty()))
		            	{
	    					JOptionPane.showMessageDialog(null, "Load the geometries!", "Load Geometries", JOptionPane.INFORMATION_MESSAGE);
	    					return;
		            	}
		            	
	                	if(id == 1)
	                	{
	                		try {
								geometry = reader.read(P);
							} catch (ParseException e1) 
	                		{
								e1.printStackTrace();
							}	                		
	                	}
	                	else if(id == 2)
	                	{
	                		try {
								geometry = reader.read(Q);
							} catch (ParseException e1) 
	                		{
								e1.printStackTrace();
							}
	                	}
	                	
	                	panel_id = id;
	                	
	                	//.
	                	
                		C = geometry.getCoordinates();
                		
                		for(int h = 0; h < C.length; h++)
                		{
                			Seg s = null;
                			curr = C[h];
                			
                			if(h < C.length - 1)
                				next = C[h + 1];
                			else
                				next = C[0];
                			                			
            				s = new Seg(curr.x, curr.y, next.x, next.y);
                			d = s.distance(x, y);
                			
                			if(min > d)
                			{	
                				min = d;
                				seg = s;
                			}
                		}
                		
        				jp_l.repaint();
        				jp_r.repaint();
        				jp_a.repaint();
	                }
	            }
	        });

	        addMouseMotionListener(new MouseAdapter() 
	        {
	            public void mouseDragged(MouseEvent e) 
	            {        	
	            	int ddx = (e.getX() - xx);
	            	int ddy = (e.getY() - yy);
	            	
	            	translate(ddx, ddy);
	            	repaint();
	            	
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });

	        addMouseWheelListener(new MouseAdapter()
	        {
	            public void mouseWheelMoved(MouseWheelEvent e) 
	            {
		            int notches = e.getWheelRotation();

					zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
					
		            if (notches > 0) 
		            {
		            	if (zoomLevel < maxZoomLevel) 
		            	{
		            		zoomLevel += zoomMultiplicationFactor; 
			            	scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
			            	translate(true);
		            	}
		            } else {
			           	if (zoomLevel > minZoomLevel) 
			           	{
			           		zoomLevel -= zoomMultiplicationFactor;
							scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
							translate(false);
			            }
		            }

		            repaint();
	            }
	        });       
		};
		
		@Override
        protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);

	        Graphics2D gr = (Graphics2D) g;
	        gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	        gr.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
	        
		    gr.setStroke(new BasicStroke(1.0f));	        
		    gr.setPaint(Color.blue);

	        AffineTransform at = new AffineTransform();
	        
	        at.translate(dx, dy);
	        at.scale(sx, -sy);
	        
	        zoom_level.setText("Zoom Level: " + sx);

	        int R = Integer.parseInt(b_r_tx.getText());
	        int G = Integer.parseInt(b_g_tx.getText());
	        int B = Integer.parseInt(b_b_tx.getText());
	        
	        int fill_R = Integer.parseInt(fill_r_tx.getText());
	        int fill_G = Integer.parseInt(fill_g_tx.getText());
	        int fill_B = Integer.parseInt(fill_b_tx.getText());
	        
			double marker_radius = Double.parseDouble(maker_radius_tx.getText());
			
			try 
			{			
				if(id == 1)
				{
					if(P == null)
						return;
					
					geometry = reader.read(P);
				}
				else if(id == 2)
				{
					if(Q == null)
						return;
					
					geometry = reader.read(Q);
				}
				else if(id == 3)
				{
					if(P == null || Q == null)
						return;
					
					geometry = reader.read(P);
					
			    	gr.setPaint(new Color(R, G, B));
			    	gr.draw(new LiteShape(geometry, at, false));
			    	
   					if(fill.isSelected())
   					{
   						gr.setPaint(new Color(fill_R, fill_G, fill_B));
   						
		   		        for (int j = 0; j < geometry.getNumGeometries(); j++)
				   			gr.fill(new LiteShape(geometry.getGeometryN(j), at, false));
   					}
   					
   					if(show_points)
   						show_points(gr, geometry, at, marker_radius);
   					
					geometry = reader.read(Q);
					
			    	gr.setPaint(new Color(R, G, B));
			    	gr.draw(new LiteShape(geometry, at, false));
			    	
   					if(fill.isSelected())
   					{
   						gr.setPaint(new Color(fill_R, fill_G, fill_B));
   						
		   		        for (int j = 0; j < geometry.getNumGeometries(); j++)
				   			gr.fill(new LiteShape(geometry.getGeometryN(j), at, false));
   					}
   					
   					if(show_points)
   						show_points(gr, geometry, at, marker_radius);
				}

				//...
				
				if(id == 1 || id == 2)
				{
			    	gr.setPaint(new Color(R, G, B));
			    	gr.draw(new LiteShape(geometry, at, false));
			    	
			    	new LiteShape(geometry, at, false);

   					if(fill.isSelected())
   					{
   						gr.setPaint(new Color(fill_R, fill_G, fill_B));
   						
		   		        for (int j = 0; j < geometry.getNumGeometries(); j++)
				   			gr.fill(new LiteShape(geometry.getGeometryN(j), at, false));
   					}
   					
   					if(show_points)
   						show_points(gr, geometry, at, marker_radius);
   					   					
   					if(panel_id == id && seg != null)
   					{
   						String coordinate_wkt = "LINESTRING (" + seg.get_ix() + " " + seg.get_iy() + ", " + seg.get_fx() + " " + seg.get_fy() + ")";
   						
   						LineString l = (LineString) reader.read(coordinate_wkt);
   			    		gr.setPaint(Color.red);
   			    		gr.draw(new LiteShape(l, at, false));
   					}
   					
   				    if(show_info_about_geometry_validity)
   				    {
   				    	if(id == 1)
   				    	{
   	   						if(is_p_valid)
   	   							gr.setPaint(new Color(6, 40, 94));
   	   						else
   	   							gr.setPaint(new Color(166, 10, 83));
   	   						
   	   						gr.setFont(new Font("Arial", Font.BOLD, 12));
   	   						gr.drawString("Geometry is Valid: [" + is_p_valid + "]", 20, 70); 		
   				    	}
   				    	else
   				    	{
   	   						if(is_q_valid)
   	   							gr.setPaint(new Color(6, 40, 94));
   	   						else
   	   							gr.setPaint(new Color(166, 10, 83));
   	   						
   	   						gr.setFont(new Font("Arial", Font.BOLD, 12));
   	   						gr.drawString("Geometry is Valid: [" + is_q_valid + "]", 20, 70); 		
   				    	}			
   				    }
				}
				
				//...
				
				gr.setPaint(new Color(6, 40, 94));
				gr.setFont(new Font("Arial", Font.BOLD, 12));
				gr.drawString(title, 20, 30);
			} 
			catch (ParseException e) 
			{
				e.printStackTrace();
			}
        };
		
        public void show_points(Graphics2D gr, Geometry geometry, AffineTransform at, double marker_radius) throws ParseException 
        {
        	if(id != 3)
        	{
    			gr.setPaint(new Color(6, 40, 94));
    			gr.setFont(new Font("Arial", Font.BOLD, 12));
    			gr.drawString("N: " + String.valueOf(geometry.getNumPoints() - 1), 20, 50);
        	}

			Coordinate[] C = geometry.getCoordinates();
				
			String coordinate_wkt = "LINESTRING (" + C[0].getX() + " " + C[0].getY() + ", " + C[0].getX() + " " + C[0].getY() + ")";
				
			LineString l = (LineString) reader.read(coordinate_wkt);
    		gr.setPaint(Color.red);
    		gr.draw(new LiteShape(l, at, false));
	    		
	    	Coordinate c = l.getCoordinateN(0);
				
	    	// Draw marker.
		    		
	    	GeometricShapeFactory shape_factory = new GeometricShapeFactory();
	    	shape_factory.setNumPoints(32);
	    	shape_factory.setCentre(new Coordinate(c.x, c.y));
	    	shape_factory.setSize(marker_radius);
	    	Geometry circle = shape_factory.createCircle();
		    		
	    	gr.setPaint(Color.red);
	    	gr.draw(new LiteShape(circle, at, false));
	    		
			coordinate_wkt = "LINESTRING (" + C[1].getX() + " " + C[1].getY() + ", " + C[1].getX() + " " + C[1].getY() + ")";
				
			l = (LineString) reader.read(coordinate_wkt);
    		gr.setPaint(Color.red);
    		gr.draw(new LiteShape(l, at, false));
				
	    	c = l.getCoordinateN(0);
				
	    	// Draw marker.
		    		
	    	shape_factory = new GeometricShapeFactory();
	    	shape_factory.setNumPoints(32);
	    	shape_factory.setCentre(new Coordinate(c.x, c.y));
	    	shape_factory.setSize(marker_radius);
	    	circle = shape_factory.createCircle();
		    		
	    	gr.setPaint(Color.black);
	    	gr.draw(new LiteShape(circle, at, false));
	    	
	    	boolean on = true;
	    	
			for(int j = 2; j < C.length - 1; j++)
			{
				coordinate_wkt = "LINESTRING (" + C[j].getX() + " " + C[j].getY() + ", " + C[j].getX() + " " + C[j].getY() + ")";
						
				l = (LineString) reader.read(coordinate_wkt);
	    		gr.setPaint(Color.red);
	    		gr.draw(new LiteShape(l, at, false));
			    		
		    	c = l.getCoordinateN(0);
						
		    	// Draw marker.
				    		
		    	shape_factory = new GeometricShapeFactory();
		    	shape_factory.setNumPoints(32);
		    	shape_factory.setCentre(new Coordinate(c.x, c.y));
		    	shape_factory.setSize(marker_radius);
		    	circle = shape_factory.createCircle();
				
		    	if(on)
		    	{
		    		gr.setPaint(Color.blue);
		    		on = false;
		    	}
		    	else
		    	{
		    		gr.setPaint(Color.orange);
		    		on = true;
		    	}

		    	gr.draw(new LiteShape(circle, at, false));
			}
		}
    
        public void reset() 
        {
        	dx = 0;
        	dy = 0;
        	
        	sx = 1;
        	sy = 1; 	
        	   	
        	xx = 0;
        	yy = 0;
        }

        public void translate(boolean sign) 
        {
        	zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
        	
        	double cx_star = cx * zoomMultiplicationFactor;
        	double cy_star = cy * zoomMultiplicationFactor;
        	
			if(sign) 
			{
				dx -= (int) cx_star;
				dy += (int) cy_star;				
			}
			else
			{
				dx += (int) cx_star;
				dy -= (int) cy_star;				
			}
        }
        
        public void translate(int x, int y) 
        {
        	dx += x;
        	dy += y;
        }
        
        public void scale(double x, double y) 
        {
        	sx += x;
        	sy += y;
        }
        
        public void adjust_screen_position()
        {
			if(id == 1 || id == 3)
			{
				if(P == null)
					return;
				
				try {
					geometry = reader.read(P);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			else if(id == 2)
			{
				if(P == null)
					return;
				
				try {
					geometry = reader.read(Q);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

        	if(geometry == null)
        		return;
        	
			Point c = geometry.getCentroid();

			cx = c.getX();
			cy = c.getY();

		
			w_center = (int) (this.getParent().getWidth() / 2);
			h_center = (int) (this.getParent().getHeight() / 2);
			
			dx = (int) ((-cx + w_center));
			dy = (int) ((cy + h_center));
        }
    }
	
	public void draw_UI() 
	{
		setTitle("Correspondences");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	    setSize(1296, 720);
	    setLocationRelativeTo(null);
	    setExtendedState(f.getExtendedState() | JFrame.MAXIMIZED_BOTH);
	    
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setToolTipText("");
		contentPane.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		viz_l_p = new JPanel();
		viz_l_p.setLocation(10, 10);
		
		viz_l_p.setSize(459, 449);
		
		viz_l_p.setBackground(Color.WHITE);
		viz_l_p.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(viz_l_p);
		
		tools_p = new JPanel();
		tools_p.setBorder(new LineBorder(new Color(0, 0, 0)));
		tools_p.setBounds(10, 616, 1260, 36);
		contentPane.add(tools_p);
		
		zoom_factor_tx = new JTextField();
		zoom_factor_tx.setBackground(SystemColor.inactiveCaptionBorder);
		zoom_factor_tx.setForeground(SystemColor.desktop);
		zoom_factor_tx.setText("0.25");
		zoom_factor_tx.setToolTipText("Zoom factor.");
		tools_p.add(zoom_factor_tx);
		zoom_factor_tx.setColumns(3);
		
		minus = new JButton("Zoom Out");
		tools_p.add(minus);
		
		plus = new JButton("Zoom In");
		tools_p.add(plus);
		
		save_curr_to_picture = new JButton("Save to Picture");
		tools_p.add(save_curr_to_picture);
		
		png_sequence_filename_tx = new JTextField();
		png_sequence_filename_tx.setToolTipText("Name of the png pictures.");
		png_sequence_filename_tx.setText("d:\\\\interpolation_viz");
		tools_p.add(png_sequence_filename_tx);
		png_sequence_filename_tx.setColumns(16);
		
		rate_tx = new JTextField();
		rate_tx.setEnabled(false);
		rate_tx.setBackground(SystemColor.inactiveCaptionBorder);
		rate_tx.setToolTipText("Rate of the animation of the interpolation.");
		rate_tx.setText("16");
		tools_p.add(rate_tx);
		rate_tx.setColumns(2);
		
		play = new JButton("Interpolate");
		play.setEnabled(false);
		tools_p.add(play);
		
		interpolation_slider = new JSlider();
		interpolation_slider.setEnabled(false);
		interpolation_slider.setValue(0);
		tools_p.add(interpolation_slider);
		
		status = new JLabel("");
		tools_p.add(status);
		
		zoom_level = new JLabel("Zoom Level:");
		zoom_level.setForeground(new Color(0, 0, 128));
		zoom_level.setBounds(950, 11, 320, 20);
		contentPane.add(zoom_level);
	    
	    p_wkt_tx = new JTextArea();
	    p_wkt_tx.setBackground(new Color(240, 255, 240));
	    p_wkt_tx.setFont(new Font("Monospaced", Font.PLAIN, 10));
	    p_wkt_tx.setLineWrap(true);
	    p_wkt_tx.setText("POLYGON ((193.3 245.4, 192.3 243.2, 191 241.5, 189.2 241.5, 187.6 241.9, 185.8 239.6, 183.3 239.4, 183.2 237.1, 181.7 238.1, 181.2 238.1, 180 237.6, 176.6 241.6, 173.8 244, 173.2 244, 171.7 241, 167.3 236.8, 166.5 236.9, 163 233.5, 159.9 237.8, 159.7 238.5, 158.1 240.1, 157.4 240.2, 154.2 243.9, 154 244.7, 148.3 250.6, 145.3 254.9, 143 259.6, 143.1 260.7, 144.4 264.3, 145.2 266, 146.1 266.4, 146.1 267.6, 146.7 267.8, 146.7 268.9, 147.2 270.5, 149.4 271.6, 153.7 272.8, 160.2 273.5, 169.1 273.6, 170.4 271.8, 172.5 271.3, 176.2 268.4, 181 265.7, 185.3 260.8, 188.3 255.7, 192.6 247.4, 193.3 245.4))");
	    p_wkt_tx.setToolTipText("P");
	    p_wkt_tx.setBounds(10, 467, 1260, 66);
	    contentPane.add(p_wkt_tx);
		
		show_points_bt = new JButton("Show/Hide Vertices");
		show_points_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
		show_points_bt.setBounds(950, 60, 155, 23);
		contentPane.add(show_points_bt);
		
		maker_radius_tx = new JTextField();
		maker_radius_tx.setToolTipText("Marker radius.");
		maker_radius_tx.setText("1");
		maker_radius_tx.setBounds(950, 87, 43, 20);
		contentPane.add(maker_radius_tx);
		maker_radius_tx.setColumns(10);
		
		viz_r_p = new JPanel();
		viz_r_p.setBorder(new LineBorder(new Color(0, 0, 0)));
		viz_r_p.setBackground(Color.WHITE);
		viz_r_p.setBounds(479, 10, 461, 449);
		contentPane.add(viz_r_p);
		
		q_wkt_tx = new JTextArea();
		q_wkt_tx.setFont(new Font("Monospaced", Font.PLAIN, 10));
		q_wkt_tx.setLineWrap(true);
		q_wkt_tx.setToolTipText("Q");
		q_wkt_tx.setText("POLYGON ((194.5 247.4, 195.2 244.7, 194.2 242.2, 194 241, 193.2 240.1, 191.1 240.1, 190.3 240.5, 189.7 240.2, 189.4 239.1, 188.1 238, 186.1 236.9, 185.7 235.7, 184.1 236.4, 182.4 235.8, 180.1 237.9, 178.9 239.4, 175.6 241.8, 175.2 241.5, 174 238.5, 170.2 234.1, 169.2 234.1, 165.8 230.4, 164.4 232.4, 162.7 234.3, 162.3 235.2, 160.6 236.7, 159.7 236.7, 156.1 240.6, 155.3 241.9, 149.4 247.2, 148 248.9, 147 250.1, 144.2 254.8, 144.2 256.7, 144.7 257.3, 145 259.9, 146 262, 146.5 262.3, 146.5 263.4, 147.4 263.9, 147.6 266.2, 153.6 269.2, 160.4 270.8, 169.1 271.3, 170.9 269.7, 173.4 268.8, 179.5 265.4, 185.7 260.7, 193.8 248, 194.5 247.4))");
		q_wkt_tx.setBounds(10, 539, 1260, 60);
		contentPane.add(q_wkt_tx);
	    
	    aligned_panel = new JPanel();
	    aligned_panel.setBorder(new LineBorder(new Color(0, 0, 0)));
	    aligned_panel.setBackground(new Color(255, 255, 255));
	    aligned_panel.setBounds(950, 164, 320, 295);
	    
	    load_geoms_bt = new JButton("Load Geometries");
	    load_geoms_bt.setToolTipText("Chaacterization  Evaluation.");
	    load_geoms_bt.setForeground(new Color(47, 79, 79));
	    load_geoms_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    load_geoms_bt.setBounds(950, 36, 155, 23);
	    contentPane.add(load_geoms_bt);
	    
	    corr_to_wkt_save_bt = new JButton("Save");
	    corr_to_wkt_save_bt.setEnabled(false);
	    corr_to_wkt_save_bt.setForeground(new Color(0, 0, 128));
	    corr_to_wkt_save_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    corr_to_wkt_save_bt.setBounds(1115, 36, 156, 23);
	    contentPane.add(corr_to_wkt_save_bt);
	    
	    wkt_save_in_file = new JTextField();
	    wkt_save_in_file.setEnabled(false);
	    wkt_save_in_file.setText("d:\\\\1-3-corr.txt");
	    wkt_save_in_file.setForeground(new Color(25, 25, 112));
	    wkt_save_in_file.setColumns(1);
	    wkt_save_in_file.setBackground(new Color(240, 255, 240));
	    wkt_save_in_file.setBounds(1116, 61, 155, 21);
	    contentPane.add(wkt_save_in_file);
	    
	    fill = new JCheckBox("Fill");
	    fill.setEnabled(false);
	    fill.setBackground(new Color(255, 255, 255));
	    fill.setBounds(1041, 84, 62, 25);
	    contentPane.add(fill);
	    
	    b_r_tx = new JTextField();
	    b_r_tx.setText("0");
	    b_r_tx.setColumns(10);
	    b_r_tx.setBounds(1136, 102, 31, 20);
	    contentPane.add(b_r_tx);
	    
	    b_g_tx = new JTextField();
	    b_g_tx.setText("0");
	    b_g_tx.setColumns(10);
	    b_g_tx.setBounds(1179, 102, 31, 20);
	    contentPane.add(b_g_tx);
	    
	    b_b_tx = new JTextField();
	    b_b_tx.setText("0");
	    b_b_tx.setColumns(10);
	    b_b_tx.setBounds(1220, 102, 31, 20);
	    contentPane.add(b_b_tx);
	    
	    fill_b_tx = new JTextField();
	    fill_b_tx.setText("67");
	    fill_b_tx.setColumns(10);
	    fill_b_tx.setBounds(1220, 127, 31, 20);
	    contentPane.add(fill_b_tx);
	    
	    fill_g_tx = new JTextField();
	    fill_g_tx.setText("21");
	    fill_g_tx.setColumns(10);
	    fill_g_tx.setBounds(1179, 127, 31, 20);
	    contentPane.add(fill_g_tx);
	    
	    fill_r_tx = new JTextField();
	    fill_r_tx.setText("18");
	    fill_r_tx.setColumns(10);
	    fill_r_tx.setBounds(1136, 127, 31, 20);
	    contentPane.add(fill_r_tx);
	    
	    coll_p_f_tx = new JTextField();
	    coll_p_f_tx.setToolTipText("Collinearity factor.");
	    coll_p_f_tx.setText("0.5");
	    coll_p_f_tx.setBounds(950, 256, 43, 20);
	    contentPane.add(coll_p_f_tx);
	    coll_p_f_tx.setColumns(10);
	    
	    add_coll_p_bt = new JButton("Add Point");
	    add_coll_p_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    add_coll_p_bt.setBounds(1000, 255, 89, 23);
	    contentPane.add(add_coll_p_bt);
	    
	    clear_bt = new JButton("Clear");
	    clear_bt.setForeground(new Color(128, 0, 128));
	    clear_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    clear_bt.setBounds(1090, 303, 89, 23);
	    contentPane.add(clear_bt);
	    
	    p_i_id_tx = new JTextField();
	    p_i_id_tx.setToolTipText("P initial vertex id [0, N].");
	    p_i_id_tx.setBackground(new Color(240, 255, 240));
	    p_i_id_tx.setText("0");
	    p_i_id_tx.setBounds(950, 280, 43, 20);
	    contentPane.add(p_i_id_tx);
	    p_i_id_tx.setColumns(10);
	    
	    q_i_id_tx = new JTextField();
	    q_i_id_tx.setToolTipText("Q initial vertex id [0, N].");
	    q_i_id_tx.setBackground(new Color(240, 255, 240));
	    q_i_id_tx.setText("1");
	    q_i_id_tx.setBounds(950, 304, 43, 20);
	    contentPane.add(q_i_id_tx);
	    q_i_id_tx.setColumns(10);
	    
	    save_bt = new JButton("Save");
	    save_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    save_bt.setBounds(1000, 411, 179, 23);
	    contentPane.add(save_bt);
	    
	    corr_filename_tx = new JTextField();
	    corr_filename_tx.setBackground(new Color(240, 255, 240));
	    corr_filename_tx.setText("d:\\\\corr_file");
	    corr_filename_tx.setBounds(1000, 436, 180, 21);
	    contentPane.add(corr_filename_tx);
	    corr_filename_tx.setColumns(10);
	    
	    load_bt = new JButton("Load");
	    load_bt.setForeground(new Color(0, 0, 128));
	    load_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    load_bt.setBounds(1000, 303, 89, 23);
	    contentPane.add(load_bt);
	    
	    interpolate_bt = new JButton("Interpolate");
	    interpolate_bt.setToolTipText("Interpolate.");
	    interpolate_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    interpolate_bt.setBounds(1000, 387, 179, 23);
	    contentPane.add(interpolate_bt);
	    
	    undo_bt = new JButton("Undo");
	    undo_bt.setForeground(new Color(47, 79, 79));
	    undo_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    undo_bt.setBounds(1090, 255, 89, 23);
	    contentPane.add(undo_bt);
	    
	    redo_bt = new JButton("Redo");
	    redo_bt.setForeground(new Color(47, 79, 79));
	    redo_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    redo_bt.setBounds(1090, 279, 89, 23);
	    contentPane.add(redo_bt);
	    
	    del_bt = new JButton("Del Point");
	    del_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    del_bt.setBounds(1000, 279, 89, 23);
	    contentPane.add(del_bt);
	    
	    reset_undo_bt = new JButton("Reset");
	    reset_undo_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    reset_undo_bt.setToolTipText("Reset undo history.");
	    reset_undo_bt.setBounds(1090, 327, 89, 23);
	    contentPane.add(reset_undo_bt);
	    
	    norm_geom_bt = new JButton("Normalize Geometries");
	    norm_geom_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    norm_geom_bt.setBounds(1000, 363, 179, 23);
	    contentPane.add(norm_geom_bt);
	    
	    input_validity_bt = new JButton("Check Validity");
	    input_validity_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    input_validity_bt.setBounds(950, 118, 155, 23);
	    contentPane.add(input_validity_bt);
	}

	public void add_listeners()
	{
        addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(WindowEvent e) 
            {
                setVisible(false);
                dispose();
            }
        });
        
   		norm_geom_bt.addActionListener(new ActionListener() {
   			public void actionPerformed(ActionEvent arg0) 
   			{
				P = p_wkt_tx.getText();
				Q = q_wkt_tx.getText();
   				
				if(P == null || P.isEmpty() || Q == null || Q.isEmpty())
				{
					JOptionPane.showMessageDialog(null, "Invalid geometries!", "Load Geometries", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				try 
				{
					geometry = reader.read(P);
				} catch (ParseException e) 
				{
					e.printStackTrace();
				}
				
				if(geometry != null && !geometry.isEmpty())
				{			
					p_wkt_tx.setText(geometry.norm().toText());
				}

				try 
				{
					geometry = reader.read(Q);
				} catch (ParseException e) 
				{
					e.printStackTrace();
				}

				if(geometry != null && !geometry.isEmpty())
				{
					q_wkt_tx.setText(geometry.norm().toText());
				}
				
    			P = p_wkt_tx.getText();
    			Q = q_wkt_tx.getText();

    			if(P == null || P.isEmpty() || Q == null || Q.isEmpty())
    			{
    				JOptionPane.showMessageDialog(null, "Invalid geometries!", "Load Geometries", JOptionPane.INFORMATION_MESSAGE);
    				return;
    			}
    				
    			((DrawGraphs) jp_l).reset();
    			((DrawGraphs) jp_r).reset();
    			((DrawGraphs) jp_a).reset();
    				
    			((DrawGraphs) jp_l).adjust_screen_position();
    			((DrawGraphs) jp_r).adjust_screen_position();
    			((DrawGraphs) jp_a).adjust_screen_position();

    	        jp_l.repaint();
    	        jp_r.repaint();
    			jp_a.repaint();
            }
        });
        
   		reset_undo_bt.addActionListener(new ActionListener() {
   			public void actionPerformed(ActionEvent arg0) 
   			{
   				undo.clear();
   				undo_id = -1;
   				
				jp_l.repaint();
				jp_r.repaint();
				jp_a.repaint();
            }
        });

        del_bt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				if(seg == null)
					return;
				
				if(panel_id == 1)
				{
					try {
						geometry = reader.read(p_wkt_tx.getText());
					} catch (ParseException e) 
					{
						e.printStackTrace();
					}
				}
				else
				{
					try {
						geometry = reader.read(q_wkt_tx.getText());
					} catch (ParseException e) 
					{
						e.printStackTrace();
					}
				}
				
				Coordinate[] C = geometry.getCoordinates();
				boolean found = false;
				int id = 0;//, e = 0;
				Coordinate c = null;
				
				for(int k = 0; k < C.length - 1; k++)
				{
					c = C[k];
					
					if(seg.i_x == c.x && seg.i_y == c.y)
					{
						id = k;
						found = true;
						break;
					}
				}

				if(found)
				{
					String wkt = "POLYGON ((";
					
					// First
					if(id == 0)
					{
						for(int k = 1; k < C.length - 1; k++)
						{
							c = C[k];
							wkt += c.x + " " + c.y + ", ";
						}
						
						wkt += C[1].x + " " + C[1].y;
					}
					// Last
					else if(id ==  C.length - 2)
					{
						for(int k = 0; k < C.length - 2; k++)
						{
							c = C[k];
							wkt += c.x + " " + c.y + ", ";
						}
						
						wkt += C[0].x + " " + C[0].y;
					}
					// Other point.
					else
					{
						for(int k = 0; k < C.length - 1; k++)
						{
							c = C[k];
							
							if(id != k)
								wkt += c.x + " " + c.y + ", ";
						}
						
						wkt += C[0].x + " " + C[0].y;
					}
					
					//...

					wkt += "))";
					
					if(undo.isEmpty())
					{
						undo.add(p_wkt_tx.getText());
						undo.add(q_wkt_tx.getText());
					}

					if(panel_id == 1)
						p_wkt_tx.setText(wkt);
					else
						q_wkt_tx.setText(wkt);
					
					seg = null;
					
    				P = p_wkt_tx.getText();
    				Q = q_wkt_tx.getText();
    				
					undo.add(p_wkt_tx.getText());
					undo.add(q_wkt_tx.getText());
					
					undo_id = undo.size() - 1;

    				if(P == null || P.isEmpty() || Q == null || Q.isEmpty())
    				{
    					JOptionPane.showMessageDialog(null, "Invalid geometries!", "Load Geometries", JOptionPane.INFORMATION_MESSAGE);
    					return;
    				}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Seg not found!", "Add Point", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				jp_l.repaint();
				jp_r.repaint();
				jp_a.repaint();
			}
		});

        add_coll_p_bt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				if(seg == null)
					return;
				
				double f = Double.parseDouble(coll_p_f_tx.getText());
				
				if(f <= 0 || f >= 1)
				{
					JOptionPane.showMessageDialog(null, "Invalid factor!", "Add Point", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				double x = seg.i_x + f * (seg.f_x - seg.i_x);
				double y = seg.i_y + f * (seg.f_y - seg.i_y);
				
				if(panel_id == 1)
				{
					try {
						geometry = reader.read(p_wkt_tx.getText());
					} catch (ParseException e) 
					{
						e.printStackTrace();
					}
				}
				else
				{
					try {
						geometry = reader.read(q_wkt_tx.getText());
					} catch (ParseException e) 
					{
						e.printStackTrace();
					}
				}
				
				Coordinate[] C = geometry.getCoordinates();
				int found = 0;
				int b = 0, e = 0;
				Coordinate c = null;
				
				for(int k = 0; k < C.length - 1; k++)
				{
					c = C[k];
					
					if(seg.i_x == c.x && seg.i_y == c.y)
					{
						b = k;
						found++;
					}

					if(seg.f_x == c.x && seg.f_y == c.y)
					{
						e = k;
						found++;
					}
					
					if(found == 2)
						break;
				}
				
				if(found == 2)
				{
					int min = Math.min(b, e);
					int max = Math.max(b, e);
					
					String wkt = "POLYGON ((";
					
					if(min == 0 && max == C.length - 2)
					{
						for(int k = 0; k < C.length - 1; k++)
						{
							c = C[k];
							wkt += c.x + " " + c.y + ", ";
						}
						
						wkt += x + " " + y + ", " + C[C.length - 1].x + " " + C[C.length - 1].y;
					}
					else
					{
						for(int k = 0; k < C.length - 1; k++)
						{
							c = C[k];
							wkt += c.x + " " + c.y + ", ";
							
							if(k == min )
							{
								if(k + 1 == max)
								{
									wkt += x + " " + y + ", ";
								}
								else
								{
									JOptionPane.showMessageDialog(null, "Wrong ids!", "Add Point", JOptionPane.INFORMATION_MESSAGE);
									return;
								}
							}
						}
						
						wkt += C[C.length - 1].x + " " + C[C.length - 1].y;
					}
					
					//...

					wkt += "))";
					
					if(undo.isEmpty())
					{
						undo.add(p_wkt_tx.getText());
						undo.add(q_wkt_tx.getText());
					}

					if(panel_id == 1)
						p_wkt_tx.setText(wkt);
					else
						q_wkt_tx.setText(wkt);
					
					seg = null;
					
    				P = p_wkt_tx.getText();
    				Q = q_wkt_tx.getText();
    				
					undo.add(p_wkt_tx.getText());
					undo.add(q_wkt_tx.getText());
					
					undo_id = undo.size() - 1;

    				if(P == null || P.isEmpty() || Q == null || Q.isEmpty())
    				{
    					JOptionPane.showMessageDialog(null, "Invalid geometries!", "Load Geometries", JOptionPane.INFORMATION_MESSAGE);
    					return;
    				}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Seg not found!", "Add Point", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				jp_l.repaint();
				jp_r.repaint();
				jp_a.repaint();
			}
		});
        
        undo_bt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				if(undo.isEmpty() || undo_id < 1)
				{
					JOptionPane.showMessageDialog(null, "Nothing to undo!", "Undo", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				seg = null;

				if(undo_id == 1)
				{
	    			P = undo.get(undo_id - 1);
	    			Q = undo.get(undo_id);
				}
				else
				{
	    			P = undo.get(undo_id - 3);
	    			Q = undo.get(undo_id - 2);
				}
				
    			p_wkt_tx.setText(P);
    			q_wkt_tx.setText(Q);
    			
    			undo_id -= 2;
    			
    			if(P == null || P.isEmpty() || Q == null || Q.isEmpty())
    			{
    				JOptionPane.showMessageDialog(null, "Invalid geometries!", "Load Geometries", JOptionPane.INFORMATION_MESSAGE);
    				return;
    			}
				
				jp_l.repaint();
				jp_r.repaint();
				jp_a.repaint();
			}
		});
        
        redo_bt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				if(undo.isEmpty() || undo_id == undo.size() - 1)
				{
					JOptionPane.showMessageDialog(null, "Nothing to redo!", "Undo", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				seg = null;

    			P = undo.get(undo_id + 1);
    			Q = undo.get(undo_id + 2);

    			p_wkt_tx.setText(P);
    			q_wkt_tx.setText(Q);
    			
    			undo_id += 2;

    			if(P == null || P.isEmpty() || Q == null || Q.isEmpty())
    			{
    				JOptionPane.showMessageDialog(null, "Invalid geometries!", "Load Geometries", JOptionPane.INFORMATION_MESSAGE);
    				return;
    			}
				
				jp_l.repaint();
				jp_r.repaint();
				jp_a.repaint();
			}
		});
        
        clear_bt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				seg = null;
				
				jp_l.repaint();
				jp_r.repaint();
				jp_a.repaint();
			}
		});
        
        show_points_bt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				if(show_points)
					show_points = false;
				else
					show_points = true;
				
				jp_l.repaint();
				jp_r.repaint();
				jp_a.repaint();
			}
		});
                
		// Zoom out.
		
    	minus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs l = (DrawGraphs) jp_l;
				DrawGraphs r = (DrawGraphs) jp_r;

				zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
				
	           	if (zoomLevel > minZoomLevel) 
	           	{
	           		zoomLevel -= zoomMultiplicationFactor;
	           		
					l.scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
					l.translate(false);
					
					r.scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
					r.translate(false);
	            }

	            jp_l.repaint();
	            jp_r.repaint();
				jp_a.repaint();
			}
		});
    	
    	load_geoms_bt.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent arg0) 
    		{
    			P = p_wkt_tx.getText();
    			Q = q_wkt_tx.getText();

    			if(P == null || P.isEmpty() || Q == null || Q.isEmpty())
    			{
    				JOptionPane.showMessageDialog(null, "Invalid geometries!", "Load Geometries", JOptionPane.INFORMATION_MESSAGE);
    				return;
    			}
    				
    			((DrawGraphs) jp_l).reset();
    			((DrawGraphs) jp_r).reset();
    			((DrawGraphs) jp_a).reset();
    				
    			((DrawGraphs) jp_l).adjust_screen_position();
    			((DrawGraphs) jp_r).adjust_screen_position();
    			((DrawGraphs) jp_a).adjust_screen_position();

    	        jp_l.repaint();
    	        jp_r.repaint();
    			jp_a.repaint();
    		}
    	});
    	
    	// Zoom in.
    	
    	plus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs l = (DrawGraphs) jp_l;
				DrawGraphs r = (DrawGraphs) jp_r;

				zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
				
            	if (zoomLevel < maxZoomLevel) 
            	{
            		zoomLevel += zoomMultiplicationFactor;
            		
	            	l.scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
	            	l.translate(true);
	            	
	            	r.scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
	            	r.translate(true);
            	}

	            jp_l.repaint();
	            jp_r.repaint();
				jp_a.repaint();
			}
		});
            	
		save_curr_to_picture.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{			
				status.setText("Saving!");
				String filename_l_panel = png_sequence_filename_tx.getText() + "_l_panel_" + geom_to_show_id + ".png";
				String filename_r_panel = png_sequence_filename_tx.getText() + "_r_panel_" + geom_to_show_id + ".png";
				
				BufferedImage panel_image = null;
				
				try
				{
					panel_image = ScreenImage.createImage(viz_l_p);
	    			ScreenImage.writeImage(panel_image, filename_l_panel);
	    			
	    			panel_image = ScreenImage.createImage(viz_r_p);
	    			ScreenImage.writeImage(panel_image, filename_r_panel);
    			} 
				catch (IOException ex) {
    				ex.printStackTrace();
    			}
				
				jp_l.repaint();
				jp_r.repaint();
				jp_a.repaint();
				
				JOptionPane.showMessageDialog(null, "Saved to files: " + filename_l_panel + "; " + filename_r_panel + "!", "Save", JOptionPane.INFORMATION_MESSAGE);	

				status.setText("");
			}
		});

		save_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
		    {
				int p_id = Integer.parseInt(p_i_id_tx.getText());
				int q_id = Integer.parseInt(q_i_id_tx.getText());
				
				P = p_wkt_tx.getText();
				Q = q_wkt_tx.getText();

				if(P == null || P.isEmpty() || Q == null || Q.isEmpty())
				{
					JOptionPane.showMessageDialog(null, "Invalid geometries!", "Save Corr", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				Geometry p = null;
				Geometry q = null;
				
				try {
					p = reader.read(P);
					q = reader.read(Q);
				} catch (ParseException e1) 
				{
					JOptionPane.showMessageDialog(null, "Invalid geometries!", "Save Corr", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				if(p.getCoordinates().length != q.getCoordinates().length)
				{
					JOptionPane.showMessageDialog(null, "Different number of coordinates!", "Save Corr", JOptionPane.INFORMATION_MESSAGE);
					return;					
				}
				
				//,,,
				
				String filename = corr_filename_tx.getText();
				PrintWriter writer = null;
				
				try {
					writer = new PrintWriter(filename, "UTF-8");
				} catch (FileNotFoundException | UnsupportedEncodingException e) 
				{
					JOptionPane.showMessageDialog(null, e.getMessage(), "Save Corr", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				String s = null;
				int pid = p_id;
				int qid = q_id;
				
				Coordinate[] pc = p.getCoordinates();
				Coordinate[] qc = q.getCoordinates();
				
				String np = "POLYGON ((";
				String nq = "POLYGON ((";
				
		    	for(int i = p_id; i < pc.length - 1; i++) 
		    	{
		    		if(q_id == qc.length - 1)
		    			q_id = 0;
		    		
		    		s = pc[p_id].x + " " + pc[p_id].y + " " + qc[q_id].x + " " + qc[q_id].y;
		    		
		    		np += pc[p_id].x + " " + pc[p_id].y + ", ";
		    		nq += qc[q_id].x + " " + qc[q_id].y + ", ";
		    		
		    		p_id++;
		    		q_id++;
		    		
		    		writer.println(s);
		    	}
				
		    	if(pid != 0)
		    	{
			    	for(int i = 0; i < pid; i++) 
			    	{	    		
			    		s = pc[i].x + " " + pc[i].y + " " + qc[q_id].x + " " + qc[q_id].y;
			    		
			    		np += pc[i].x + " " + pc[i].y + ", ";
			    		nq += qc[q_id].x + " " + qc[q_id].y + ", ";
			    		
			    		q_id++;
			    		writer.println(s);
			    	}
		    	}
		    	
	    		s = pc[pid].x + " " + pc[pid].y + " " + qc[qid].x + " " + qc[qid].y;
	    		writer.println(s);
		    	
	    		np += pc[pid].x + " " + pc[pid].y;
	    		nq += qc[qid].x + " " + qc[qid].y;
	    		
				np += "))";
				nq += "))";
				
				writer.println(np);
				writer.println(nq);
	    		
				writer.close();
				JOptionPane.showMessageDialog(null, "Saved to " + filename + "!", "Save Corr", JOptionPane.INFORMATION_MESSAGE);
		    }
		});
		
		interpolate_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
		    {		
				P = p_wkt_tx.getText();
				Q = q_wkt_tx.getText();

				if(P == null || P.isEmpty() || Q == null || Q.isEmpty())
				{
					JOptionPane.showMessageDialog(null, "Invalid geometries!", "Save Corr", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				Geometry p = null;
				Geometry q = null;
				
				try {
					p = reader.read(P);
					q = reader.read(Q);
				} catch (ParseException e1) 
				{
					JOptionPane.showMessageDialog(null, "Invalid geometries!", "Save Corr", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				if(p.getCoordinates().length != q.getCoordinates().length)
				{
					JOptionPane.showMessageDialog(null, "Different number of coordinates!", "Save Corr", JOptionPane.INFORMATION_MESSAGE);
					return;					
				}
				
				//,,,
				
				String[] arr = rigid.during_period(db, de, P, Q, db, de, dn, InterpolationMethod.EQUILATERAL.get_value(), d.get_cw(), d.get_threshold(), 0);
				
				Data out = new Data(2, arr, null, d.get_main(), InterpolationMethod.EQUILATERAL, d.get_threshold(), d.get_cw(), null); 
				
				final Data in = out;
				
        		Vce vce = new Vce(in, pyspatiotemporalgeom_lib, librip, rigid, db, de, P, Q, db, de, dn);
        		vce.setVisible(true);
				
				jp_l.repaint();
				jp_r.repaint();
				jp_a.repaint();
		    }
		});
		
		load_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
		    {
				int p_id = Integer.parseInt(p_i_id_tx.getText());
				int q_id = Integer.parseInt(q_i_id_tx.getText());
				
				P = p_wkt_tx.getText();
				Q = q_wkt_tx.getText();

				if(P == null || P.isEmpty() || Q == null || Q.isEmpty())
				{
					JOptionPane.showMessageDialog(null, "Invalid geometries!", "Save Corr", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				Geometry p = null;
				Geometry q = null;
				
				try {
					p = reader.read(P);
					q = reader.read(Q);
				} catch (ParseException e1) 
				{
					JOptionPane.showMessageDialog(null, "Invalid geometries!", "Save Corr", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				if(p.getCoordinates().length != q.getCoordinates().length)
				{
					JOptionPane.showMessageDialog(null, "Different number of coordinates!", "Save Corr", JOptionPane.INFORMATION_MESSAGE);
					return;					
				}
				
				if(p_id > p.getCoordinates().length - 1 || q_id > q.getCoordinates().length - 1)
				{
					JOptionPane.showMessageDialog(null, "Invalid coordinate id!", "Save Corr", JOptionPane.INFORMATION_MESSAGE);
					return;					
				}
				
				//,,,
				
				int pid = p_id;
				int qid = q_id;
				
				Coordinate[] pc = p.getCoordinates();
				Coordinate[] qc = q.getCoordinates();
				
				String np = "POLYGON ((";
				String nq = "POLYGON ((";
				
		    	for(int i = p_id; i < pc.length - 1; i++) 
		    	{
		    		if(q_id == qc.length - 1)
		    			q_id = 0;
		    		
		    		np += pc[p_id].x + " " + pc[p_id].y + ", ";
		    		nq += qc[q_id].x + " " + qc[q_id].y + ", ";
		    		
		    		p_id++;
		    		q_id++;
		    	}
				
		    	if(pid != 0)
		    	{
			    	for(int i = 0; i < pid; i++) 
			    	{	    		
			    		if(q_id == qc.length - 1)
			    			q_id = 0;
			    		
			    		np += pc[i].x + " " + pc[i].y + ", ";
			    		nq += qc[q_id].x + " " + qc[q_id].y + ", ";
			    		
			    		q_id++;
			    	}
		    	}
		    	
	    		np += pc[pid].x + " " + pc[pid].y;
	    		nq += qc[qid].x + " " + qc[qid].y;
	    		
				np += "))";
				nq += "))";
				
				p_wkt_tx.setText(np);
				q_wkt_tx.setText(nq);
				
				seg = null;
				
				P = p_wkt_tx.getText();
				Q = q_wkt_tx.getText();

				if(P == null || P.isEmpty() || Q == null || Q.isEmpty())
				{
					JOptionPane.showMessageDialog(null, "Invalid geometries!", "Load Geometries", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				jp_l.repaint();
				jp_r.repaint();
				jp_a.repaint();
		    }
		});
		
		input_validity_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if(show_info_about_geometry_validity)
				{
					show_info_about_geometry_validity = false;
					
					jp_l.repaint();
					jp_r.repaint();
					jp_a.repaint();
					
					return;
				}
				
				Main m = d.get_main();
				
				P = p_wkt_tx.getText();
				Q = q_wkt_tx.getText();

				if(P == null || P.isEmpty() || Q == null || Q.isEmpty())
				{
					JOptionPane.showMessageDialog(null, "Invalid geometries!", "Load Geometries", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				is_p_valid = m.is_valid_geometry(P);
				is_q_valid = m.is_valid_geometry(Q);
				
			    show_info_about_geometry_validity = true;		
			    
				jp_l.repaint();
				jp_r.repaint();
				jp_a.repaint();
			}
		});
		
		corr_to_wkt_save_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
		    {
				/*
				String[] p_corr = p_corr_tx.getText().split(" ");
				String[] q_corr = q_corr_tx.getText().split(" ");
				
				if(p_corr.length != q_corr.length)
				{
					JOptionPane.showMessageDialog(null, "Invalid corr!", "Save Corr", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				if(P == null || P.isEmpty() || Q == null || Q.isEmpty())
				{
					JOptionPane.showMessageDialog(null, "Load geometries first!", "Save Corr", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				Geometry p = null;
				Geometry q = null;
				
				try {
					p = reader.read(P);
					q = reader.read(Q);
				} catch (ParseException e1) {
					e1.printStackTrace();
				}

				String filename = wkt_save_in_file.getText();
				PrintWriter writer = null;
				
				try {
					writer = new PrintWriter(filename, "UTF-8");
				} catch (FileNotFoundException | UnsupportedEncodingException e) 
				{
					e.printStackTrace();
				}

				String s;
				int pid;
				int qid;
				
				Coordinate[] pc = p.getCoordinates();
				Coordinate[] qc = q.getCoordinates();
				
		    	for(int i = 0; i < p_corr.length; i++) 
		    	{
		    		pid = Integer.parseInt(p_corr[i]) - 1;
		    		qid = Integer.parseInt(q_corr[i]) - 1;
		    		
		    		s = pc[pid].x + " " + pc[pid].y + " " + qc[qid].x + " " + qc[qid].y;
		    		writer.println(s);
		    	}
				
				writer.close();
				JOptionPane.showMessageDialog(null, "Saved to " + filename + "!", "Save Corr", JOptionPane.INFORMATION_MESSAGE);
				*/	
		    }
		});
	}
}