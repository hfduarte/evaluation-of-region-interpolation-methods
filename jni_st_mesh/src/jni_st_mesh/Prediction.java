package jni_st_mesh;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;

import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.geometry.jts.LiteShape;
import org.jfree.ui.RefineryUtilities;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.locationtech.jts.util.GeometricShapeFactory;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JSlider;

import javax.swing.event.ChangeListener;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Font;
import javax.swing.border.BevelBorder;
import java.awt.SystemColor;
import javax.swing.JTextArea;

import java.awt.Toolkit;
import javax.swing.JSeparator;

public class Prediction extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField png_sequence_filename_tx;
	private JButton play;
	private JSlider interpolation_slider;
	private JPanel viz_l_p;
	private Data d;
	private JFrame f;
	private JPanel jp_l;
	private JPanel jp_r;
	private JLabel status;
	private JPanel tools_p;
	private JButton minus;
	private JButton plus;
	private JLabel zoom_level;
	private JComboBox<String> metrics;
	
	public static final double DEFAULT_ZOOM_MULTIPLICATION_FACTOR = 0.25;
	public static final double DEFAULT_MIN_ZOOM_LEVEL = 0.25;
	public static final double DEFAULT_MAX_ZOOM_LEVEL = 100;
	
    private double zoomLevel = 1.0;
    private double minZoomLevel = DEFAULT_MIN_ZOOM_LEVEL;
    private double maxZoomLevel = DEFAULT_MAX_ZOOM_LEVEL;
    private double zoomMultiplicationFactor = DEFAULT_ZOOM_MULTIPLICATION_FACTOR;
    
    private double threshold;
    private JTextField rate_tx;
    private int geom_to_show_id = 0;
    private int bbox_to_show_id = 0;
    private GeometryFactory geometryFactory;
    private WKTReader reader; 
    private String[] geometries_l;
    private String[] geometries_r;
    private int max;
    private TimerTask play_task;
	private int w_center;
	private int h_center;
	private double cx = 0;
	private double cy = 0;
    private Timer timer;
    private JTextField granularity_tx;
    private JCheckBox fill;
    private int show_granularity;
    private boolean show_footprint;
    private JSlider zoom_slider;
    private JButton show_metrics_in_chart_bt;
    private JLabel n_samples;
    private String ref_geom_wkt;
    private boolean show_bbox;
    private JLabel method_lbl;
    private JLabel method_info_lbl;
    private Geometry geometry;
    private Geometry aabb;
    private JButton visualize_metric_ev_bt;
    private JCheckBox align_geom_cb;
    private boolean show_info_about_interpolation_validity;
    private boolean show_info_about_geometry_validity;
    private boolean is_p_valid;
    private boolean is_q_valid;

    private boolean is_interpolation_valid;
    private int granularity;
    private int num_invalid_geometries;
    private Boolean[] geometries_validity_info;
    private JButton save_curr_to_picture;
    
    private JCheckBox librip_cb;
    private JCheckBox pyspatiotemporalgeom_cb;
    private JCheckBox rigid_interpolation_lib_cb;
    
    private methods.PySpatiotemporalGeom pyspatiotemporalgeom_lib;
    private methods.Librip librip;
    private methods.RigidInterpolation rigid;
    private double db;
    private double de;
    
    private String w_p;
    private String w_q;
    
    private double db_i;
    private double de_i;
    
    private int dn;
    private JTextField zoom_factor_tx;
    private JTextField b_r_tx;
    private JTextField b_g_tx;
    private JTextField b_b_tx;
    private JTextField fill_r_tx;
    private JTextField fill_g_tx;
    private JTextField fill_b_tx;
    private JTextArea mrip_dataset_wkt_tx;
    private JCheckBox use_ref_geom_cb;
    private JCheckBox use_guides_cb;
    private int horizontal_delta;
    private int vertical_delta;
    private double y;
    private boolean show_points;
    private JPanel viz_r_p;
    private JTextArea at_ref_t_tx;
    private JTextField unit_id_tx;
    private JButton pred_eval_bt;
    private JButton viz_unit_bt;
    private String[] dataset_wkt;
    private double[] ref_at_t;
    private JTextField n_samples_tx;
    private JTextArea soa_dataset_wkt_tx;
    
    private String RIGID = "Morphrip";
    private String LIBRIP = "Librip";
    private String PYGEOM = "PySpatiotemporalGeom";    
    
	public Prediction(
				Data d, 
				methods.PySpatiotemporalGeom pyspatiotemporalgeom_lib, 
				methods.Librip librip, 
				methods.RigidInterpolation rigid
			) 
	{
		this.d = d;
		f = this;
		
		horizontal_delta = 5;
		vertical_delta = 5;
		show_points = false;
		
		this.pyspatiotemporalgeom_lib = pyspatiotemporalgeom_lib;
		this.librip = librip;
		this.rigid = rigid;
		
		geometries_l = null;
		geometries_r = null;
		
		dataset_wkt = null;
		ref_at_t = null;
		
		this.db = 1000;
		this.de = 2000;
		this.w_p = "";
		this.w_q = "";
		this.dn = 100;
		
		geom_to_show_id = 0;
		
	    geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
	    reader = new WKTReader(geometryFactory);
	    
	    threshold = d.get_threshold();
	    
		draw_UI();

	    granularity = 0;
	    
		add_listeners();
		
		draw_geometry();
	}
	
	public void paint(Graphics g) 
	{
	    super.paint(g);
	    
	    jp_l.repaint();
	    jp_r.repaint();
	}

	public void draw_geometry()
	{
		viz_l_p.setLayout(new BorderLayout());
		jp_l = new DrawGraphs(1);
		viz_l_p.add(jp_l, BorderLayout.CENTER);
		
		viz_r_p.setLayout(new BorderLayout());
		jp_r = new DrawGraphs(2);
		viz_r_p.add(jp_r, BorderLayout.CENTER);
	}
	
    private class DrawGraphs extends JPanel
    {
    	private int dx = 0;
    	private int dy = 0;
    	
    	private double sx = 1;
    	private double sy = 1; 	
    	   	
    	private int xx = 0;
    	private int yy = 0;
    	
		private static final long serialVersionUID = 3203545490664689791L;
		private String[] geometries;
		private int id;
		private String method;
		
		public DrawGraphs(int id) 
		{
			setBackground(Color.white);
			
			setAlignmentY(CENTER_ALIGNMENT);
			setAlignmentX(CENTER_ALIGNMENT);
			
			this.id = id;
			
			method = "";
			
	        addMouseListener(new MouseAdapter() 
	        {
	            public void mousePressed(MouseEvent e) 
	            {
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });

	        addMouseMotionListener(new MouseAdapter() 
	        {
	            public void mouseDragged(MouseEvent e) 
	            {        	
	            	int ddx = (e.getX() - xx);
	            	int ddy = (e.getY() - yy);
	            	
	            	translate(ddx, ddy);
	            	repaint();
	            	
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });
	        
	        addMouseWheelListener(new MouseAdapter()
	        {
	            public void mouseWheelMoved(MouseWheelEvent e) 
	            {
		            int notches = e.getWheelRotation();

					zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
					
		            if (notches > 0) 
		            {
		            	if (zoomLevel < maxZoomLevel) 
		            	{
		            		zoomLevel += zoomMultiplicationFactor; 
			            	scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
			            	translate(true);
		            	}
		            } else {
			           	if (zoomLevel > minZoomLevel) 
			           	{
			           		zoomLevel -= zoomMultiplicationFactor;
							scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
							translate(false);
			            }
		            }

		            repaint();
	            }
	        });       
		};
		
		@Override
        protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);

	        Graphics2D gr = (Graphics2D) g;
	        gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	        
		    gr.setStroke(new BasicStroke(1.0f));	        
		    gr.setPaint(Color.blue);

	        AffineTransform at = new AffineTransform();

	        at.translate(dx, dy);
	        at.scale(sx, -sy);
	        
	        zoom_level.setText("Zoom Level: " + sx);

	        int R = Integer.parseInt(b_r_tx.getText());
	        int G = Integer.parseInt(b_g_tx.getText());
	        int B = Integer.parseInt(b_b_tx.getText());
	        
	        int fill_R = Integer.parseInt(fill_r_tx.getText());
	        int fill_G = Integer.parseInt(fill_g_tx.getText());
	        int fill_B = Integer.parseInt(fill_b_tx.getText());
	        
			try 
			{			
				if(id == 1)
					geometries = geometries_l;
				else
					geometries = geometries_r;
			    
				if(geometries != null)
				{
					geometry = reader.read(geometries[geom_to_show_id]);
			    	
			    	gr.setPaint(new Color(R, G, B));
			    	gr.draw(new LiteShape(geometry, at, false));
			    	
   					if(fill.isSelected())
   					{
   						gr.setPaint(new Color(fill_R, fill_G, fill_B));
   						
		   		        for (int j = 0; j < geometry.getNumGeometries(); j++)
				   			gr.fill(new LiteShape(geometry.getGeometryN(j), at, false));
   					}
   					
					gr.setPaint(new Color(6, 40, 94));
					gr.setFont(new Font("Arial", Font.BOLD, 12));
					gr.drawString(method, 20, 30);
				}
			} 
			catch (ParseException e) 
			{
				e.printStackTrace();
			}
        };
		
        public void reset() 
        {
        	dx = 0;
        	dy = 0;
        	
        	sx = 1;
        	sy = 1; 	
        	   	
        	xx = 0;
        	yy = 0;
        	
        	geometries = null;
        	method = "";
        }
        
        public void set_method(String method) 
        {
        	this.method = method;
        }
        
        public void translate(boolean sign) 
        {
        	zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
        	
        	double cx_star = cx * zoomMultiplicationFactor;
        	double cy_star = cy * zoomMultiplicationFactor;
        	
			if(sign) 
			{
				dx -= (int) cx_star;
				dy += (int) cy_star;				
			}
			else
			{
				dx += (int) cx_star;
				dy -= (int) cy_star;				
			}
        }
        
        public void translate(int x, int y) 
        {
        	dx += x;
        	dy += y;
        }
        
        public void scale(double x, double y) 
        {
        	sx += x;
        	sy += y;
        }
        
        public void adjust_screen_position()
        {
			if(id == 1)
				geometries = geometries_l;
			else
				geometries = geometries_r;
        	
        	if(geometries == null)
        		return;
        	
			try
			{
				geometry = reader.read(geometries[geom_to_show_id]);
				Point c = geometry.getCentroid();

				cx = c.getX();
				cy = c.getY();
			} 
			catch (ParseException e) {
				e.printStackTrace();
			}
		
			w_center = (int) (this.getParent().getWidth() / 2);
			h_center = (int) (this.getParent().getHeight() / 2);
			
			dx = (int) ((-cx + w_center));
			dy = (int) ((cy + h_center));
        }
    }
	
	public void draw_UI() 
	{
		setTitle("Prediction Evaluation");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	    setSize(1296, 720);
	    setLocationRelativeTo(null);
	    setExtendedState(f.getExtendedState() | JFrame.MAXIMIZED_BOTH);
	    
		contentPane = new JPanel();
		contentPane.setToolTipText("");
		contentPane.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		viz_l_p = new JPanel();
		viz_l_p.setLocation(175, 10);
		
		viz_l_p.setSize(459, 590);
		
		viz_l_p.setBackground(Color.WHITE);
		viz_l_p.setBorder(null);
		contentPane.add(viz_l_p);
		
		tools_p = new JPanel();
		tools_p.setBorder(new LineBorder(new Color(0, 0, 0)));
		tools_p.setBounds(10, 616, 1260, 36);
		contentPane.add(tools_p);
		
		zoom_factor_tx = new JTextField();
		zoom_factor_tx.setBackground(SystemColor.inactiveCaptionBorder);
		zoom_factor_tx.setForeground(SystemColor.desktop);
		zoom_factor_tx.setText("0.25");
		zoom_factor_tx.setToolTipText("Zoom factor.");
		tools_p.add(zoom_factor_tx);
		zoom_factor_tx.setColumns(3);
		
		minus = new JButton("Zoom Out");
		tools_p.add(minus);
		
		plus = new JButton("Zoom In");
		tools_p.add(plus);
		
		save_curr_to_picture = new JButton("Save To Picture");
		save_curr_to_picture.setEnabled(false);
		tools_p.add(save_curr_to_picture);
		
		use_guides_cb = new JCheckBox("Use Guides");
		use_guides_cb.setEnabled(false);
		tools_p.add(use_guides_cb);
		
		png_sequence_filename_tx = new JTextField();
		png_sequence_filename_tx.setEnabled(false);
		png_sequence_filename_tx.setToolTipText("Name of the png pictures.");
		png_sequence_filename_tx.setText("d:\\\\pred_eval_viz");
		tools_p.add(png_sequence_filename_tx);
		png_sequence_filename_tx.setColumns(16);
		
		rate_tx = new JTextField();
		rate_tx.setBackground(SystemColor.inactiveCaptionBorder);
		rate_tx.setToolTipText("Rate of the animation of the interpolation.");
		rate_tx.setText("16");
		tools_p.add(rate_tx);
		rate_tx.setColumns(2);
		
		play = new JButton("Interpolate");
		tools_p.add(play);
		
		interpolation_slider = new JSlider();
		interpolation_slider.setValue(0);
		tools_p.add(interpolation_slider);

		status = new JLabel("");
		tools_p.add(status);
		
		granularity_tx = new JTextField();
		granularity_tx.setToolTipText("Granularity.");
		granularity_tx.setText("1");
		granularity_tx.setBounds(11, 40, 56, 22);
		contentPane.add(granularity_tx);
		granularity_tx.setColumns(10);
		
		fill = new JCheckBox("Fill");
		fill.setBounds(105, 39, 60, 25);
		contentPane.add(fill);
		
		zoom_level = new JLabel("Zoom Level:");
		zoom_level.setBounds(1115, 10, 155, 14);
		contentPane.add(zoom_level);
		
		zoom_slider = new JSlider();
		zoom_slider.setEnabled(false);
		zoom_slider.setValue(1);
		zoom_slider.setMinimum(1);
		zoom_slider.setBounds(1115, 38, 155, 26);
		contentPane.add(zoom_slider);
		
		n_samples = new JLabel("N\u00BA Samples: ");
		n_samples.setBounds(1115, 73, 155, 14);
		contentPane.add(n_samples);
	    
	    metrics = new JComboBox<>();
	    metrics.setEnabled(false);
	    metrics.setToolTipText("Metrics");
	    metrics.setModel(new DefaultComboBoxModel<>(new String[] {"Hausdorff Distance", "Jaccard Distance"}));
	    metrics.setBounds(10, 101, 155, 22);
	    contentPane.add(metrics);
	    
	    JLabel Metrics_lbl = new JLabel("Metrics:");
	    Metrics_lbl.setForeground(new Color(25, 25, 112));
	    Metrics_lbl.setFont(new Font("Tahoma", Font.BOLD, 12));
	    Metrics_lbl.setBounds(11, 78, 154, 16);
	    contentPane.add(Metrics_lbl);
	    
	    show_metrics_in_chart_bt = new JButton("Show Metric in Chart");
	    show_metrics_in_chart_bt.setEnabled(false);
	    show_metrics_in_chart_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    show_metrics_in_chart_bt.setForeground(new Color(72, 61, 139));
	    show_metrics_in_chart_bt.setBounds(10, 130, 155, 25);
	    contentPane.add(show_metrics_in_chart_bt);
	    
	    method_lbl = new JLabel("Status:");
	    method_lbl.setForeground(new Color(0, 0, 128));
	    method_lbl.setFont(new Font("Tahoma", Font.BOLD, 12));
	    method_lbl.setBounds(1115, 90, 155, 27);
	    contentPane.add(method_lbl);
	    
	    method_info_lbl = new JLabel("");
	    method_info_lbl.setForeground(new Color(47, 79, 79));
	    method_info_lbl.setFont(new Font("Tahoma", Font.BOLD, 11));
	    method_info_lbl.setBounds(1115, 110, 155, 27);
	    contentPane.add(method_info_lbl);
	    
	    visualize_metric_ev_bt = new JButton("Visualize Metric");
	    visualize_metric_ev_bt.setEnabled(false);
	    visualize_metric_ev_bt.setForeground(new Color(47, 79, 79));
	    visualize_metric_ev_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    visualize_metric_ev_bt.setBounds(10, 161, 155, 23);
	    contentPane.add(visualize_metric_ev_bt);
	    
	    align_geom_cb = new JCheckBox("Align Geometries");
	    align_geom_cb.setEnabled(false);
	    align_geom_cb.setSelected(true);
	    align_geom_cb.setBounds(10, 186, 155, 23);
	    contentPane.add(align_geom_cb);
	    
	    librip_cb = new JCheckBox("Librip");
	    librip_cb.setFont(new Font("Tahoma", Font.ITALIC, 11));
	    librip_cb.setForeground(new Color(25, 25, 112));
	    librip_cb.setSelected(true);
	    librip_cb.setBounds(10, 381, 155, 23);
	    contentPane.add(librip_cb);
	    
	    pyspatiotemporalgeom_cb = new JCheckBox("PySpatiotemporalGeom");
	    pyspatiotemporalgeom_cb.setFont(new Font("Tahoma", Font.ITALIC, 11));
	    pyspatiotemporalgeom_cb.setForeground(new Color(25, 25, 112));
	    pyspatiotemporalgeom_cb.setSelected(true);
	    pyspatiotemporalgeom_cb.setBounds(10, 401, 155, 23);
	    contentPane.add(pyspatiotemporalgeom_cb);
	    
	    rigid_interpolation_lib_cb = new JCheckBox("Morphrip");
	    rigid_interpolation_lib_cb.setFont(new Font("Tahoma", Font.ITALIC, 11));
	    rigid_interpolation_lib_cb.setForeground(new Color(25, 25, 112));
	    rigid_interpolation_lib_cb.setBounds(10, 421, 155, 23);
	    contentPane.add(rigid_interpolation_lib_cb);
	    
	    b_r_tx = new JTextField();
	    b_r_tx.setText("0");
	    b_r_tx.setBounds(32, 522, 31, 20);
	    contentPane.add(b_r_tx);
	    b_r_tx.setColumns(10);
	    
	    b_g_tx = new JTextField();
	    b_g_tx.setText("0");
	    b_g_tx.setBounds(73, 522, 31, 20);
	    contentPane.add(b_g_tx);
	    b_g_tx.setColumns(10);
	    
	    b_b_tx = new JTextField();
	    b_b_tx.setText("0");
	    b_b_tx.setBounds(114, 522, 31, 20);
	    contentPane.add(b_b_tx);
	    b_b_tx.setColumns(10);
	    
	    fill_r_tx = new JTextField();
	    fill_r_tx.setText("18");
	    fill_r_tx.setBounds(32, 547, 31, 20);
	    contentPane.add(fill_r_tx);
	    fill_r_tx.setColumns(10);
	    
	    fill_g_tx = new JTextField();
	    fill_g_tx.setText("21");
	    fill_g_tx.setBounds(73, 547, 31, 20);
	    contentPane.add(fill_g_tx);
	    fill_g_tx.setColumns(10);
	    
	    fill_b_tx = new JTextField();
	    fill_b_tx.setText("67");
	    fill_b_tx.setBounds(114, 547, 31, 20);
	    contentPane.add(fill_b_tx);
	    fill_b_tx.setColumns(10);
	    
	    use_ref_geom_cb = new JCheckBox("Use Reference Geometry");
	    use_ref_geom_cb.setEnabled(false);
	    use_ref_geom_cb.setSelected(true);
	    use_ref_geom_cb.setBounds(10, 206, 155, 23);
	    contentPane.add(use_ref_geom_cb);
	    
	    mrip_dataset_wkt_tx = new JTextArea();
	    mrip_dataset_wkt_tx.setText("POLYGON ((193.3 245.4, 192.3 243.2, 191.65 242.35, 191 241.5, 189.2 241.5, 187.6 241.9, 187 241.133, 186.4 240.367, 185.8 239.6, 183.3 239.4, 183.2 237.1, 181.7 238.1, 181.2 238.1, 180 237.6, 178.3 239.6, 176.6 241.6, 173.8 244, 173.2 244, 171.7 241, 167.3 236.8, 166.5 236.9, 163 233.5, 161.45 235.65, 159.9 237.8, 159.7 238.5, 158.1 240.1, 157.4 240.2, 154.2 243.9, 154 244.7, 148.3 250.6, 146.8 252.75, 145.3 254.9, 143 259.6, 143.1 260.7, 143.75 262.5, 144.4 264.3, 145.2 266, 146.1 266.4, 146.1 267.6, 146.7 267.8, 146.7 268.9, 147.2 270.5, 149.4 271.6, 153.7 272.8, 160.2 273.5, 169.1 273.6, 170.4 271.8, 172.5 271.3, 176.2 268.4, 181 265.7, 185.3 260.8, 188.3 255.7, 192.6 247.4, 192.95 246.4, 193.3 245.4));POLYGON ((175.2 270, 187 260.2, 191.5 251.1, 193.8 246.5, 194.8 242.4, 193.4 240, 192.4 239.7, 189.2 239.8, 188.5 238.5, 185.7 237.1, 185 236.8, 184.8 235.9, 184.4 235.7, 183.6 236.3, 181.5 236.4, 180 237.8, 177.4 241, 174.8 242.6, 173 239.2, 169.1 235.4, 168.1 235.3, 164.6 232, 164.3 231.9, 164.1 232.6, 161.8 236, 161 237.4, 159.3 238.8, 158.7 238.8, 156 242.5, 154.9 244.3, 149.9 249.3, 146.8 253.8, 144.8 258.3, 146 262.9, 150.2 268.4, 157.6 270.4, 164.8 272.2, 171.1 272.1, 172.2 270.8, 175.2 270));POLYGON((195.2 244.7, 194.2 242.2, 194 241, 193.2 240.1, 191.1 240.1, 190.3 240.5, 189.7 240.2, 189.4 239.1, 188.1 238, 186.1 236.9, 185.7 235.7, 184.1 236.4, 183.25 236.1, 182.4 235.8, 180.1 237.9, 178.9 239.4, 175.6 241.8, 175.2 241.5, 174 238.5, 170.2 234.1, 169.2 234.1, 165.8 230.4, 164.4 232.4, 162.7 234.3, 162.3 235.2, 160.6 236.7, 159.7 236.7, 156.1 240.6, 155.3 241.9, 149.4 247.2, 148 248.9, 147 250.1, 144.2 254.8, 144.2 256.7, 144.7 257.3, 145 259.9, 146 262, 146.5 262.3, 146.5 263.4, 147.4 263.9, 147.5 265.05, 147.6 266.2, 150.6 267.7, 153.6 269.2, 160.4 270.8, 169.1 271.3, 170.9 269.7, 173.4 268.8, 176.45 267.1, 179.5 265.4, 185.7 260.7, 189.75 254.35, 193.8 248, 194.5 247.4, 195.2 244.7));POLYGON((194.8 242.4, 194.1 241.2, 193.4 240, 192.4 239.7, 189.2 239.8, 188.5 238.5, 185.7 237.1, 185 236.8, 184.8 235.9, 184.4 235.7, 183.6 236.3, 181.5 236.4, 180 237.8, 177.4 241, 174.8 242.6, 173 239.2, 169.1 235.4, 168.1 235.3, 164.6 232, 164.3 231.9, 164.1 232.6, 161.8 236, 161 237.4, 159.3 238.8, 158.7 238.8, 156 242.5, 154.9 244.3, 149.9 249.3, 146.8 253.8, 144.8 258.3, 145.4 260.6, 146 262.9, 148.1 265.65, 150.2 268.4, 153.9 269.4, 157.6 270.4, 164.8 272.2, 171.1 272.1, 172.2 270.8, 175.2 270, 179.133 266.733, 183.067 263.467, 187 260.2, 191.5 251.1, 193.8 246.5, 194.8 242.4));POLYGON ((194.5 247.4, 195.2 244.7, 194.2 242.2, 194 241, 193.2 240.1, 191.1 240.1, 190.3 240.5, 189.7 240.2, 189.4 239.1, 188.1 238, 186.1 236.9, 185.7 235.7, 184.1 236.4, 182.4 235.8, 180.1 237.9, 178.9 239.4, 175.6 241.8, 175.2 241.5, 174 238.5, 170.2 234.1, 169.2 234.1, 165.8 230.4, 164.4 232.4, 162.7 234.3, 162.3 235.2, 160.6 236.7, 159.7 236.7, 156.1 240.6, 155.3 241.9, 149.4 247.2, 148 248.9, 147 250.1, 144.2 254.8, 144.2 256.7, 144.7 257.3, 145 259.9, 146 262, 146.5 262.3, 146.5 263.4, 147.4 263.9, 147.6 266.2, 153.6 269.2, 160.4 270.8, 169.1 271.3, 170.9 269.7, 173.4 268.8, 179.5 265.4, 185.7 260.7, 193.8 248, 194.5 247.4));POLYGON((192.8 257.7, 192.9 255.3, 192.4 253.3, 191.2 252.5, 189 252.3, 188.8 250, 187.6 248.5, 186.4 248.2, 186.6 247.2, 186.8 246.2, 184.9 246.1, 183.6 245.4, 180.35 246.8, 177.1 248.2, 175.3 248.5, 174.7 242.4, 172.8 238.9, 172.1 238.8, 171.25 236.6, 170.4 234.4, 168.45 235.4, 166.5 236.4, 164.8 237.8, 162 238.2, 158.2 240.3, 154.4 242.4, 150.1 243.6, 145.5 246.3, 141.6 249.4, 141 250.7, 140.6 253.6, 140.6 256.3, 141 258.8, 141.8 261.2, 142.7 261.6, 143 263.2, 151.3 269.9, 159.1 273.4, 167.7 272.4, 170.9 271.5, 173.4 270.4, 175.7 269.1, 179.4 268.6, 186.4 263.1, 189.7 260.7, 192.8 257.7));POLYGON((195.2 244.7, 194.2 242.2, 194 241, 193.2 240.1, 191.1 240.1, 190.3 240.5, 189.7 240.2, 189.4 239.1, 188.1 238, 186.1 236.9, 185.7 235.7, 184.1 236.4, 182.4 235.8, 180.1 237.9, 178.9 239.4, 175.6 241.8, 175.2 241.5, 174 238.5, 170.2 234.1, 169.2 234.1, 165.8 230.4, 164.4 232.4, 162.7 234.3, 162.3 235.2, 160.6 236.7, 159.7 236.7, 156.1 240.6, 155.3 241.9, 149.4 247.2, 148 248.9, 147 250.1, 144.2 254.8, 144.2 256.7, 144.7 257.3, 145 259.9, 146 262, 146.5 262.3, 146.5 263.4, 147.4 263.9, 147.6 266.2, 153.6 269.2, 160.4 270.8, 169.1 271.3, 170.9 269.7, 173.4 268.8, 179.5 265.4, 185.7 260.7, 193.8 248, 194.5 247.4, 195.2 244.7));POLYGON ((186.4 263.1, 189.7 260.7, 192.8 257.7, 192.9 255.3, 192.4 253.3, 191.2 252.5, 189 252.3, 188.8 250, 187.6 248.5, 186.4 248.2, 186.8 246.2, 184.9 246.1, 183.6 245.4, 177.1 248.2, 175.3 248.5, 174.7 242.4, 172.8 238.9, 172.1 238.8, 170.4 234.4, 166.5 236.4, 164.8 237.8, 162 238.2, 154.4 242.4, 150.1 243.6, 145.5 246.3, 141.6 249.4, 141 250.7, 140.6 253.6, 140.6 256.3, 141 258.8, 141.8 261.2, 142.7 261.6, 143 263.2, 151.3 269.9, 159.1 273.4, 167.7 272.4, 170.9 271.5, 173.4 270.4, 175.7 269.1, 179.4 268.6, 186.4 263.1));POLYGON((190.9 261.5, 191.3 257.7, 190.7 257, 190.1 256.3, 189.05 256.05, 188 255.8, 187.9 254.2, 187.8 252.6, 186.9 251.7, 186.2 251.4, 187 249.4, 185.3 249.3, 184.2 248.2, 182.6 248.2, 178.8 249.5, 175 249.6, 175.4 246.7, 175.2 244.6, 174.1 240.1, 173.6 239.8, 172.4 234.8, 170.8 235.8, 169.3 235.9, 168.05 236.65, 166.8 237.4, 163.8 237.4, 159.2 238.7, 155.8 240.1, 151 240.7, 146 242.4, 141.2 245.2, 140.7 245.9, 140.2 248.1, 139.6 248.3, 139.4 251.2, 139.4 253.9, 139.8 255.2, 139.8 256.5, 140.5 257.4, 140.5 258.7, 146.25 264.75, 150.425 267.925, 154.6 271.1, 157.7 271.4, 160.8 271.7, 170.7 271.7, 176.7 269.8, 186.3 264.7, 188.6 263.1, 190.9 261.5));POLYGON((192.8 257.7, 192.9 255.3, 192.4 253.3, 191.2 252.5, 189 252.3, 188.9 251.15, 188.8 250, 188.2 249.25, 187.6 248.5, 187 248.35, 186.4 248.2, 186.533 247.533, 186.667 246.867, 186.8 246.2, 185.85 246.15, 184.9 246.1, 183.6 245.4, 181.433 246.333, 179.267 247.267, 177.1 248.2, 175.3 248.5, 175 245.45, 174.7 242.4, 172.8 238.9, 172.1 238.8, 170.4 234.4, 168.45 235.4, 166.5 236.4, 164.8 237.8, 163.4 238, 162 238.2, 154.4 242.4, 150.1 243.6, 145.5 246.3, 141.6 249.4, 141 250.7, 140.6 253.6, 140.6 256.3, 141 258.8, 141.8 261.2, 142.7 261.6, 143 263.2, 151.3 269.9, 159.1 273.4, 167.7 272.4, 170.9 271.5, 173.4 270.4, 175.7 269.1, 179.4 268.6, 186.4 263.1, 189.7 260.7, 192.8 257.7));POLYGON ((146 242.4, 141.2 245.2, 140.7 245.9, 140.2 248.1, 139.6 248.3, 139.4 251.2, 139.4 253.9, 139.8 255.2, 139.8 256.5, 140.5 257.4, 140.5 258.7, 146.25 264.75, 154.6 271.1, 160.8 271.7, 170.7 271.7, 176.7 269.8, 186.3 264.7, 190.9 261.5, 191.3 257.7, 190.1 256.3, 188 255.8, 187.8 252.6, 186.9 251.7, 186.2 251.4, 187 249.4, 185.3 249.3, 184.2 248.2, 182.6 248.2, 178.8 249.5, 175 249.6, 175.4 246.7, 175.2 244.6, 174.1 240.1, 173.6 239.8, 172.4 234.8, 170.8 235.8, 169.3 235.9, 166.8 237.4, 163.8 237.4, 159.2 238.7, 155.8 240.1, 151 240.7, 146 242.4));POLYGON((190.9 262.2, 192.3 258.7, 192.3 257.4, 190.4 256.2, 189.3 255.7, 189.3 255, 189.6 252.5, 188.9 251.7, 188.1 251.2, 188.1 250.5, 188.2 250, 188.6 249.9, 189.1 249.6, 189.1 249.2, 188.6 249.1, 187.8 249.1, 186.5 247.5, 183.6 247.5, 180.4 248.1, 178.6 248, 177.4 247.7, 177.4 247.1, 178.3 244.5, 177.8 238.3, 177.1 237.6, 176.9 232.9, 174.6 233.5, 172.9 233.5, 171.2 234.4, 168.7 234.5, 167.8 234, 163.5 234.5, 160.9 235.5, 153.8 235.6, 149.6 236.6, 144.4 238.9, 142.9 241.7, 141.9 245.1, 141.7 250.2, 142.9 252.4, 144.1 254.6, 145.3 256.8, 150.4 263, 154.1 266.1, 158.8 267.6, 163.3 268.1, 167.8 268.6, 171.2 268.5, 174.6 268.4, 180.6 266.5, 187.1 264, 190.9 262.2));POLYGON((146 242.4, 141.2 245.2, 140.7 245.9, 140.2 248.1, 139.6 248.3, 139.4 251.2, 139.4 253.9, 139.8 255.2, 139.8 256.5, 140.5 257.4, 140.5 258.7, 146.25 264.75, 154.6 271.1, 157.7 271.4, 160.8 271.7, 164.1 271.7, 167.4 271.7, 170.7 271.7, 173.7 270.75, 176.7 269.8, 186.3 264.7, 190.9 261.5, 191.1 259.6, 191.3 257.7, 190.7 257, 190.1 256.3, 189.05 256.05, 188 255.8, 187.9 254.2, 187.8 252.6, 187.35 252.15, 186.9 251.7, 186.2 251.4, 186.6 250.4, 187 249.4, 186.15 249.35, 185.3 249.3, 184.2 248.2, 182.6 248.2, 178.8 249.5, 175 249.6, 175.4 246.7, 175.2 244.6, 174.1 240.1, 173.6 239.8, 173 237.3, 172.4 234.8, 170.8 235.8, 169.3 235.9, 166.8 237.4, 163.8 237.4, 159.2 238.7, 155.8 240.1, 151 240.7, 146 242.4));POLYGON ((153.8 235.6, 149.6 236.6, 144.4 238.9, 142.9 241.7, 141.9 245.1, 141.7 250.2, 145.3 256.8, 150.4 263, 154.1 266.1, 158.8 267.6, 167.8 268.6, 174.6 268.4, 180.6 266.5, 187.1 264, 190.9 262.2, 192.3 258.7, 192.3 257.4, 190.4 256.2, 189.3 255.7, 189.3 255, 189.6 252.5, 188.9 251.7, 188.1 251.2, 188.1 250.5, 188.2 250, 188.6 249.9, 189.1 249.6, 189.1 249.2, 188.6 249.1, 187.8 249.1, 186.5 247.5, 183.6 247.5, 180.4 248.1, 178.6 248, 177.4 247.7, 177.4 247.1, 178.3 244.5, 177.8 238.3, 177.1 237.6, 176.9 232.9, 174.6 233.5, 172.9 233.5, 171.2 234.4, 168.7 234.5, 167.8 234, 163.5 234.5, 160.9 235.5, 153.8 235.6));POLYGON((171.6 224.2, 166.7 221.2, 165.1 221.2, 164.1 221.8, 162.5 221.8, 158.4 223.6, 156.2 225.3, 155.2 227.2, 154.6 227.2, 153.4 229.5, 150.9 237.6, 150.8 241.4, 149.9 246, 150.9 247.1, 150.9 248.9, 154.7 253.9, 155.6 256.2, 157.4 259.1, 159.6 262, 161.9 264.3, 174.7 272.3, 176.6 272.9, 177.6 272.1, 178.1 272.3, 178.5 271.6, 179.9 271.5, 180.4 269.4, 179.9 267.5, 181.5 267.2, 181.8 266.7, 182.6 266.5, 183 266.1, 182.8 263.7, 185.3 263.7, 185.1 263.4, 184.5 262.9, 184.4 262.1, 184.7 260.1, 182.7 258, 181.1 256.3, 179.4 253.9, 179.4 252.9, 183 251.4, 186.8 247.8, 186.9 247, 187.7 246.3, 190.3 243.9, 187.45 241.25, 184.6 238.6, 184.4 237.2, 181 234, 179 232.5, 177 230.1, 175.1 227.4, 171.6 224.2));POLYGON((144.4 238.9, 142.9 241.7, 141.9 245.1, 141.8 247.65, 141.7 250.2, 143.5 253.5, 145.3 256.8, 147.85 259.9, 150.4 263, 151.633 264.033, 152.867 265.067, 154.1 266.1, 155.667 266.6, 157.233 267.1, 158.8 267.6, 161.8 267.933, 164.8 268.267, 167.8 268.6, 174.6 268.4, 177.6 267.45, 180.6 266.5, 187.1 264, 190.9 262.2, 192.3 258.7, 192.3 257.4, 190.4 256.2, 189.3 255.7, 189.3 255, 189.6 252.5, 188.9 251.7, 188.1 251.2, 188.1 250.5, 188.2 250, 188.6 249.9, 189.1 249.6, 189.1 249.2, 188.6 249.1, 187.8 249.1, 187.475 248.7, 187.15 248.3, 186.825 247.9, 186.5 247.5, 183.6 247.5, 180.4 248.1, 178.6 248, 177.4 247.7, 177.4 247.1, 178.3 244.5, 177.8 238.3, 177.1 237.6, 176.9 232.9, 175.75 233.2, 174.6 233.5, 172.9 233.5, 171.2 234.4, 168.7 234.5, 167.8 234, 165.65 234.25, 163.5 234.5, 160.9 235.5, 153.8 235.6, 149.6 236.6, 144.4 238.9));POLYGON ((181 234, 179 232.5, 177 230.1, 175.1 227.4, 171.6 224.2, 166.7 221.2, 165.1 221.2, 164.1 221.8, 162.5 221.8, 158.4 223.6, 156.2 225.3, 155.2 227.2, 154.6 227.2, 153.4 229.5, 150.9 237.6, 150.8 241.4, 149.9 246, 150.9 247.1, 150.9 248.9, 154.7 253.9, 155.6 256.2, 157.4 259.1, 159.6 262, 161.9 264.3, 174.7 272.3, 176.6 272.9, 177.6 272.1, 178.1 272.3, 178.5 271.6, 179.9 271.5, 180.4 269.4, 179.9 267.5, 181.5 267.2, 181.8 266.7, 182.6 266.5, 183 266.1, 182.8 263.7, 185.3 263.7, 185.1 263.4, 184.5 262.9, 184.4 262.1, 184.7 260.1, 182.7 258, 181.1 256.3, 179.4 253.9, 179.4 252.9, 183 251.4, 186.8 247.8, 186.9 247, 187.7 246.3, 190.3 243.9, 184.6 238.6, 184.4 237.2, 181 234));POLYGON((186.4 222.1, 181.3 220.7, 177.3 220.7, 176.9 221.2, 175.1 221.2, 170.6 224.7, 168 227.4, 164.7 230.8, 160.8 236.8, 160.9 238.9, 160.4 239.7, 160.4 242.6, 160.8 243.2, 161.4 246.3, 161.4 247.6, 161 248.7, 161.1 251.1, 161.7 255.1, 164.8 261.3, 167.2 264.4, 167.4 265.9, 169.7 269.4, 172.5 272.6, 176.4 272.7, 177.3 272.2, 178.2 270.9, 178.2 269.6, 179.7 269.5, 181.2 269.4, 182.3 268.5, 182.35 268.15, 182.4 267.8, 182.6 267.5, 183.4 267.6, 184 268, 184.4 268, 184.4 267.25, 184.4 266.5, 185 265.6, 185.3 265.5, 185.5 265.2, 185.6 264.6, 184.75 262.3, 183.9 260, 183.6 257.1, 183.8 256, 186.5 256, 189.2 256, 192.6 254.9, 195.1 253.7, 198.2 253.1, 198.2 252.7, 197.3 252.3, 196.9 250.3, 196.9 249.5, 195.2 246.2, 195.3 244.3, 193.8 239.7, 192.4 236.9, 191.3 231.8, 189.8 227.8, 188.1 224.95, 186.4 222.1));POLYGON((175.1 227.4, 171.6 224.2, 166.7 221.2, 165.1 221.2, 164.1 221.8, 162.5 221.8, 158.4 223.6, 156.2 225.3, 155.2 227.2, 154.6 227.2, 153.4 229.5, 150.9 237.6, 150.8 241.4, 149.9 246, 150.9 247.1, 150.9 248.9, 154.7 253.9, 155.6 256.2, 157.4 259.1, 159.6 262, 161.9 264.3, 174.7 272.3, 176.6 272.9, 177.6 272.1, 178.1 272.3, 178.5 271.6, 179.9 271.5, 180.4 269.4, 179.9 267.5, 181.5 267.2, 181.8 266.7, 182.6 266.5, 183 266.1, 182.8 263.7, 185.3 263.7, 185.1 263.4, 184.5 262.9, 184.4 262.1, 184.7 260.1, 182.7 258, 181.1 256.3, 179.4 253.9, 179.4 252.9, 183 251.4, 186.8 247.8, 186.9 247, 187.7 246.3, 190.3 243.9, 184.6 238.6, 184.4 237.2, 181 234, 179 232.5, 177 230.1, 175.1 227.4));POLYGON ((189.8 227.8, 186.4 222.1, 181.3 220.7, 177.3 220.7, 176.9 221.2, 175.1 221.2, 170.6 224.7, 168 227.4, 164.7 230.8, 160.8 236.8, 160.9 238.9, 160.4 239.7, 160.4 242.6, 160.8 243.2, 161.4 246.3, 161.4 247.6, 161 248.7, 161.1 251.1, 161.7 255.1, 164.8 261.3, 167.2 264.4, 167.4 265.9, 169.7 269.4, 172.5 272.6, 176.4 272.7, 177.3 272.2, 178.2 270.9, 178.2 269.6, 181.2 269.4, 182.3 268.5, 182.4 267.8, 182.6 267.5, 183.4 267.6, 184 268, 184.4 268, 184.4 266.5, 185 265.6, 185.3 265.5, 185.5 265.2, 185.6 264.6, 183.9 260, 183.6 257.1, 183.8 256, 189.2 256, 192.6 254.9, 195.1 253.7, 198.2 253.1, 198.2 252.7, 197.3 252.3, 196.9 250.3, 196.9 249.5, 195.2 246.2, 195.3 244.3, 193.8 239.7, 192.4 236.9, 191.3 231.8, 189.8 227.8));POLYGON((194.4 229.2, 192.7 226, 191 222.8, 188.8 222.067, 186.6 221.333, 184.4 220.6, 181 220.8, 178.25 222.5, 175.5 224.2, 172.75 225.9, 170 227.6, 168.133 229.4, 166.267 231.2, 164.4 233, 163.733 236.867, 163.067 240.733, 162.4 244.6, 162.4 247.333, 162.4 250.067, 162.4 252.8, 164.7 260.2, 167 267.6, 169.6 271, 170.6 271.2, 171.6 271.4, 172.6 271.6, 173.467 270.667, 174.333 269.733, 175.2 268.8, 176.333 268.8, 177.467 268.8, 178.6 268.8, 179.2 268.1, 179.8 267.4, 180.8 267.8, 181.8 268.2, 182.333 267.333, 182.867 266.467, 183.4 265.6, 183.333 262.6, 183.267 259.6, 183.2 256.6, 186.267 256.933, 189.333 257.267, 192.4 257.6, 193.6 256.4, 195.8 256.2, 198 256, 197.4 251.533, 196.8 247.067, 196.2 242.6, 195.6 238.133, 195 233.667, 194.4 229.2));");
	    mrip_dataset_wkt_tx.setToolTipText("Mrip ground-truth.");
	    mrip_dataset_wkt_tx.setBounds(10, 268, 155, 36);
	    contentPane.add(mrip_dataset_wkt_tx);
		
		viz_r_p = new JPanel();
		viz_r_p.setBackground(Color.WHITE);
		viz_r_p.setBounds(644, 10, 461, 590);
		contentPane.add(viz_r_p);
		
		at_ref_t_tx = new JTextArea();
		at_ref_t_tx.setToolTipText("Instant at which the reference geometry is taken.");
		at_ref_t_tx.setText("0.5;0.5;0.5;0.5;0.5;0.5;0.5;");
		at_ref_t_tx.setBounds(10, 352, 155, 22);
		contentPane.add(at_ref_t_tx);
		
	    pred_eval_bt = new JButton("Evaluate");
	    pred_eval_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    pred_eval_bt.setForeground(new Color(47, 79, 79));
	    pred_eval_bt.setBounds(10, 238, 155, 23);
	    contentPane.add(pred_eval_bt);
	    
	    viz_unit_bt = new JButton("Show Unit");
	    viz_unit_bt.setForeground(new Color(47, 79, 79));
	    viz_unit_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    viz_unit_bt.setBounds(10, 451, 155, 23);
	    contentPane.add(viz_unit_bt);
	    
	    unit_id_tx = new JTextField();
	    unit_id_tx.setText("1");
	    unit_id_tx.setBounds(10, 478, 56, 20);
	    contentPane.add(unit_id_tx);
	    unit_id_tx.setColumns(10);
	    
	    n_samples_tx = new JTextField();
	    n_samples_tx.setToolTipText("N\u00BA samples.");
	    n_samples_tx.setText("100");
	    n_samples_tx.setBounds(105, 478, 60, 20);
	    contentPane.add(n_samples_tx);
	    n_samples_tx.setColumns(10);
	    
	    soa_dataset_wkt_tx = new JTextArea();
	    soa_dataset_wkt_tx.setToolTipText("Soa ground-truth.");
	    soa_dataset_wkt_tx.setText("POLYGON ((193.3 245.4, 192.3 243.2, 191 241.5, 189.2 241.5, 187.6 241.9, 185.8 239.6, 183.3 239.4, 183.2 237.1, 181.7 238.1, 181.2 238.1, 180 237.6, 176.6 241.6, 173.8 244, 173.2 244, 171.7 241, 167.3 236.8, 166.5 236.9, 163 233.5, 159.9 237.8, 159.7 238.5, 158.1 240.1, 157.4 240.2, 154.2 243.9, 154 244.7, 148.3 250.6, 145.3 254.9, 143 259.6, 143.1 260.7, 144.4 264.3, 145.2 266, 146.1 266.4, 146.1 267.6, 146.7 267.8, 146.7 268.9, 147.2 270.5, 149.4 271.6, 153.7 272.8, 160.2 273.5, 169.1 273.6, 170.4 271.8, 172.5 271.3, 176.2 268.4, 181 265.7, 185.3 260.8, 188.3 255.7, 192.6 247.4, 193.3 245.4));POLYGON ((175.2 270, 187 260.2, 191.5 251.1, 193.8 246.5, 194.8 242.4, 193.4 240, 192.4 239.7, 189.2 239.8, 188.5 238.5, 185.7 237.1, 185 236.8, 184.8 235.9, 184.4 235.7, 183.6 236.3, 181.5 236.4, 180 237.8, 177.4 241, 174.8 242.6, 173 239.2, 169.1 235.4, 168.1 235.3, 164.6 232, 164.3 231.9, 164.1 232.6, 161.8 236, 161 237.4, 159.3 238.8, 158.7 238.8, 156 242.5, 154.9 244.3, 149.9 249.3, 146.8 253.8, 144.8 258.3, 146 262.9, 150.2 268.4, 157.6 270.4, 164.8 272.2, 171.1 272.1, 172.2 270.8, 175.2 270));POLYGON ((194.5 247.4, 195.2 244.7, 194.2 242.2, 194 241, 193.2 240.1, 191.1 240.1, 190.3 240.5, 189.7 240.2, 189.4 239.1, 188.1 238, 186.1 236.9, 185.7 235.7, 184.1 236.4, 182.4 235.8, 180.1 237.9, 178.9 239.4, 175.6 241.8, 175.2 241.5, 174 238.5, 170.2 234.1, 169.2 234.1, 165.8 230.4, 164.4 232.4, 162.7 234.3, 162.3 235.2, 160.6 236.7, 159.7 236.7, 156.1 240.6, 155.3 241.9, 149.4 247.2, 148 248.9, 147 250.1, 144.2 254.8, 144.2 256.7, 144.7 257.3, 145 259.9, 146 262, 146.5 262.3, 146.5 263.4, 147.4 263.9, 147.6 266.2, 153.6 269.2, 160.4 270.8, 169.1 271.3, 170.9 269.7, 173.4 268.8, 179.5 265.4, 185.7 260.7, 193.8 248, 194.5 247.4));POLYGON ((186.4 263.1, 189.7 260.7, 192.8 257.7, 192.9 255.3, 192.4 253.3, 191.2 252.5, 189 252.3, 188.8 250, 187.6 248.5, 186.4 248.2, 186.8 246.2, 184.9 246.1, 183.6 245.4, 177.1 248.2, 175.3 248.5, 174.7 242.4, 172.8 238.9, 172.1 238.8, 170.4 234.4, 166.5 236.4, 164.8 237.8, 162 238.2, 154.4 242.4, 150.1 243.6, 145.5 246.3, 141.6 249.4, 141 250.7, 140.6 253.6, 140.6 256.3, 141 258.8, 141.8 261.2, 142.7 261.6, 143 263.2, 151.3 269.9, 159.1 273.4, 167.7 272.4, 170.9 271.5, 173.4 270.4, 175.7 269.1, 179.4 268.6, 186.4 263.1));POLYGON ((146 242.4, 141.2 245.2, 140.7 245.9, 140.2 248.1, 139.6 248.3, 139.4 251.2, 139.4 253.9, 139.8 255.2, 139.8 256.5, 140.5 257.4, 140.5 258.7, 146.25 264.75, 154.6 271.1, 160.8 271.7, 170.7 271.7, 176.7 269.8, 186.3 264.7, 190.9 261.5, 191.3 257.7, 190.1 256.3, 188 255.8, 187.8 252.6, 186.9 251.7, 186.2 251.4, 187 249.4, 185.3 249.3, 184.2 248.2, 182.6 248.2, 178.8 249.5, 175 249.6, 175.4 246.7, 175.2 244.6, 174.1 240.1, 173.6 239.8, 172.4 234.8, 170.8 235.8, 169.3 235.9, 166.8 237.4, 163.8 237.4, 159.2 238.7, 155.8 240.1, 151 240.7, 146 242.4));POLYGON ((153.8 235.6, 149.6 236.6, 144.4 238.9, 142.9 241.7, 141.9 245.1, 141.7 250.2, 145.3 256.8, 150.4 263, 154.1 266.1, 158.8 267.6, 167.8 268.6, 174.6 268.4, 180.6 266.5, 187.1 264, 190.9 262.2, 192.3 258.7, 192.3 257.4, 190.4 256.2, 189.3 255.7, 189.3 255, 189.6 252.5, 188.9 251.7, 188.1 251.2, 188.1 250.5, 188.2 250, 188.6 249.9, 189.1 249.6, 189.1 249.2, 188.6 249.1, 187.8 249.1, 186.5 247.5, 183.6 247.5, 180.4 248.1, 178.6 248, 177.4 247.7, 177.4 247.1, 178.3 244.5, 177.8 238.3, 177.1 237.6, 176.9 232.9, 174.6 233.5, 172.9 233.5, 171.2 234.4, 168.7 234.5, 167.8 234, 163.5 234.5, 160.9 235.5, 153.8 235.6));POLYGON ((181 234, 179 232.5, 177 230.1, 175.1 227.4, 171.6 224.2, 166.7 221.2, 165.1 221.2, 164.1 221.8, 162.5 221.8, 158.4 223.6, 156.2 225.3, 155.2 227.2, 154.6 227.2, 153.4 229.5, 150.9 237.6, 150.8 241.4, 149.9 246, 150.9 247.1, 150.9 248.9, 154.7 253.9, 155.6 256.2, 157.4 259.1, 159.6 262, 161.9 264.3, 174.7 272.3, 176.6 272.9, 177.6 272.1, 178.1 272.3, 178.5 271.6, 179.9 271.5, 180.4 269.4, 179.9 267.5, 181.5 267.2, 181.8 266.7, 182.6 266.5, 183 266.1, 182.8 263.7, 185.3 263.7, 185.1 263.4, 184.5 262.9, 184.4 262.1, 184.7 260.1, 182.7 258, 181.1 256.3, 179.4 253.9, 179.4 252.9, 183 251.4, 186.8 247.8, 186.9 247, 187.7 246.3, 190.3 243.9, 184.6 238.6, 184.4 237.2, 181 234));POLYGON ((189.8 227.8, 186.4 222.1, 181.3 220.7, 177.3 220.7, 176.9 221.2, 175.1 221.2, 170.6 224.7, 168 227.4, 164.7 230.8, 160.8 236.8, 160.9 238.9, 160.4 239.7, 160.4 242.6, 160.8 243.2, 161.4 246.3, 161.4 247.6, 161 248.7, 161.1 251.1, 161.7 255.1, 164.8 261.3, 167.2 264.4, 167.4 265.9, 169.7 269.4, 172.5 272.6, 176.4 272.7, 177.3 272.2, 178.2 270.9, 178.2 269.6, 181.2 269.4, 182.3 268.5, 182.4 267.8, 182.6 267.5, 183.4 267.6, 184 268, 184.4 268, 184.4 266.5, 185 265.6, 185.3 265.5, 185.5 265.2, 185.6 264.6, 183.9 260, 183.6 257.1, 183.8 256, 189.2 256, 192.6 254.9, 195.1 253.7, 198.2 253.1, 198.2 252.7, 197.3 252.3, 196.9 250.3, 196.9 249.5, 195.2 246.2, 195.3 244.3, 193.8 239.7, 192.4 236.9, 191.3 231.8, 189.8 227.8));POLYGON ((194.4 229.2, 191 222.8, 184.4 220.6, 181 220.8, 170 227.6, 164.4 233, 162.4 244.6, 162.4 252.8, 167 267.6, 169.6 271, 172.6 271.6, 175.2 268.8, 178.6 268.8, 179.8 267.4, 181.8 268.2, 183.4 265.6, 183.2 256.6, 192.4 257.6, 193.6 256.4, 198 256, 194.4 229.2));");
	    soa_dataset_wkt_tx.setBounds(10, 310, 155, 36);
	    contentPane.add(soa_dataset_wkt_tx);
	}

	public void add_listeners()
	{
		fill.addItemListener(new ItemListener() 
		{
			@Override 
			public void itemStateChanged(ItemEvent e) 
			{
				jp_l.repaint();
				jp_r.repaint();
			}
		});
		
        addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(WindowEvent e) 
            {
                setVisible(false);
                dispose();
            }
        });
		
        viz_unit_bt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{			
				geometries_l = null;
				geometries_r = null;
				
				((DrawGraphs) jp_l).reset();
				((DrawGraphs) jp_r).reset();
				
				dataset_wkt = null;
				
				int s_unit_id = Integer.parseInt(unit_id_tx.getText());
				int t_unit_id;
				
				if(s_unit_id < 1)
				{
					JOptionPane.showMessageDialog(null, "Unit id must be > 0!", "Viz", JOptionPane.INFORMATION_MESSAGE);
				}
				else
				{
					dn = Integer.parseInt(n_samples_tx.getText());
					s_unit_id -= 1;
						
					if(librip_cb.isSelected())
					{
						if(dataset_wkt == null)
						{
							String wkt = soa_dataset_wkt_tx.getText();
								
							if(wkt != null && wkt.length() > 0)
								dataset_wkt = wkt.split(";");
						}
							
						if(dataset_wkt == null || dataset_wkt.length == 0)
						{
							JOptionPane.showMessageDialog(null, "Invalid dataset!", "Viz", JOptionPane.INFORMATION_MESSAGE);
							return;
						}

						t_unit_id = s_unit_id + 2;
							
						if(t_unit_id > dataset_wkt.length - 1)
						{
							JOptionPane.showMessageDialog(null, "Invalid Unit id!", "Viz", JOptionPane.INFORMATION_MESSAGE);
							return;
						}
							
						w_p = dataset_wkt[s_unit_id];
						w_q = dataset_wkt[t_unit_id];
							
						method_info_lbl.setText("Using Secondo...");
						contentPane.update(contentPane.getGraphics());
							
						geometries_l = librip.during_period(w_p, w_q, db, de, dn, "overlap");
						((DrawGraphs) jp_l).set_method(LIBRIP);
					}

					if(pyspatiotemporalgeom_cb.isSelected())
					{
						if(dataset_wkt == null)
						{
							String wkt = soa_dataset_wkt_tx.getText();
								
							if(wkt != null && wkt.length() > 0)
								dataset_wkt = wkt.split(";");
						}
							
						if(dataset_wkt == null || dataset_wkt.length == 0)
						{
							JOptionPane.showMessageDialog(null, "Invalid dataset!", "Viz", JOptionPane.INFORMATION_MESSAGE);
							return;
						}

						t_unit_id = s_unit_id + 2;
							
						if(t_unit_id > dataset_wkt.length - 1)
						{
							JOptionPane.showMessageDialog(null, "Invalid Unit id!", "Viz", JOptionPane.INFORMATION_MESSAGE);
							return;
						}
							
						w_p = dataset_wkt[s_unit_id];
						w_q = dataset_wkt[t_unit_id];
						
						method_info_lbl.setText("Using PySptGeom...");
						contentPane.update(contentPane.getGraphics());
							
						if(geometries_l == null)
						{
							geometries_l = pyspatiotemporalgeom_lib.during_period((int) db, (int) de, dn, w_p, w_q, (int) db, (int) de);
							((DrawGraphs) jp_l).set_method(PYGEOM);
						}
						else
						{
							geometries_r = pyspatiotemporalgeom_lib.during_period((int) db, (int) de, dn, w_p, w_q, (int) db, (int) de);
							((DrawGraphs) jp_r).set_method(PYGEOM);
						}
					}
						
					if(rigid_interpolation_lib_cb.isSelected() && (geometries_l == null || geometries_r == null))
					{
						String wkt = mrip_dataset_wkt_tx.getText();
								
						if(wkt != null && wkt.length() > 0)
							dataset_wkt = wkt.split(";");
							
						if(dataset_wkt == null || dataset_wkt.length == 0)
						{
							JOptionPane.showMessageDialog(null, "Invalid dataset!", "Viz", JOptionPane.INFORMATION_MESSAGE);
							return;
						}
							
						if(s_unit_id > 0)
							s_unit_id *= 3;
							
						t_unit_id = s_unit_id + 2;
						
						if(t_unit_id > dataset_wkt.length - 1)
						{
							JOptionPane.showMessageDialog(null, "Invalid Unit id!", "Viz", JOptionPane.INFORMATION_MESSAGE);
							return;
						}
							
						w_p = dataset_wkt[s_unit_id];
						w_q = dataset_wkt[t_unit_id];						
						
						method_info_lbl.setText("Using Morphrip...");
						contentPane.update(contentPane.getGraphics());
							
						if(geometries_l == null)
						{
							geometries_l = rigid.during_period(db, de, w_p, w_q, db, de, dn, InterpolationMethod.EQUILATERAL.get_value(), d.get_cw(), d.get_threshold(), 0.0);
							((DrawGraphs) jp_l).set_method(RIGID);
						}
						else if(geometries_r == null)
						{
							geometries_r = rigid.during_period(db, de, w_p, w_q, db, de, dn, InterpolationMethod.EQUILATERAL.get_value(), d.get_cw(), d.get_threshold(), 0.0);
							((DrawGraphs) jp_r).set_method(RIGID);
						}
					}
						
					if(geometries_l != null || geometries_r != null)
					{
						n_samples.setText("N� Samples: " + dn + " :: " + (geom_to_show_id + 1));
							
					    max = dn - 1;
							
						interpolation_slider.setMinimum(0);
						interpolation_slider.setMaximum(max);
						    
						((DrawGraphs) jp_l).adjust_screen_position();
						((DrawGraphs) jp_r).adjust_screen_position();
					}
				}
				
				method_info_lbl.setText("Done.");
				contentPane.update(contentPane.getGraphics());
								
				jp_l.repaint();
				jp_r.repaint();
			}
		});
        
        pred_eval_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if(!librip_cb.isSelected() && !pyspatiotemporalgeom_cb.isSelected() && !rigid_interpolation_lib_cb.isSelected())
				{
					JOptionPane.showMessageDialog(null, "Choose at least one method to evaluate!", "Prediction", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
				String wkt = at_ref_t_tx.getText();
				String[] v = null;
					
				if(wkt != null && wkt.length() > 0)
				{
					v = wkt.split(";");
						
					if(v != null && v.length > 0)
					{
						ref_at_t = new double[v.length];
							
						for(int j = 0; j < v.length; j++)
							ref_at_t[j] = Double.parseDouble(v[j]);
					}
				}

				if(ref_at_t == null || ref_at_t.length == 0)
				{
					JOptionPane.showMessageDialog(null, "Invalid reference instants!", "Viz", JOptionPane.INFORMATION_MESSAGE);
					return;
				}

				// Do Work.
				
				String title = "Method;" + /*"Formula;" +*/ "Position;" + "Area;" + "Perimeter;" + "H.;" + "J.;" + "J. I.;" + "N� Faces;" + "N� Holes;";

				ArrayList<String> _MAE = new ArrayList<>();
				ArrayList<String> _RMSE = new ArrayList<>();
				
				double[] librip_mae = new double [8];
				double[] pygeom_mae = new double [8];
				double[] mrip_mae = new double [8];
				
				for(int g = 0; g < librip_mae.length; g++)
					librip_mae[g] = 0;
				
				for(int g = 0; g < pygeom_mae.length; g++)
					pygeom_mae[g] = 0;
				
				for(int g = 0; g < mrip_mae.length; g++)
					mrip_mae[g] = 0;
				
				int n_obs = 0;
				
				int t = 0;
				Polygon p = null;
				
				int N = 0;
				
				n_obs = ref_at_t.length;
				
				if(librip_cb.isSelected() || pyspatiotemporalgeom_cb.isSelected())
				{
					//N -= 2;
					
					String data = soa_dataset_wkt_tx.getText();

					if(data != null && data.length() > 0)
						dataset_wkt = data.split(";");
					
					if(dataset_wkt == null || dataset_wkt.length == 0)
					{
						JOptionPane.showMessageDialog(null, "Invalid dataset!", "Viz", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					
					if(dataset_wkt.length != ref_at_t.length + 2)
					{
						JOptionPane.showMessageDialog(null, "Invalid number of reference geometries!", "Viz", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					
					N = dataset_wkt.length - 2;
					
					for(int k = 0; k < N; k++)
					{
						method_lbl.setText("Status: " + (k + 1) + " :: " + N);
						contentPane.update(contentPane.getGraphics());
						jp_l.repaint();
						jp_r.repaint();
						
						int idb = (int) db;
						int ide = (int) de;
						
						w_p = dataset_wkt[k];
						w_q = dataset_wkt[k+2];
						
						double at = (idb + (ide - idb) * ref_at_t[t]);
						t++;
						
						Main functions = d.get_main();
						 
						String r_g_wkt = dataset_wkt[k+1];
						
						double r_g_area = 0;
						double r_g_perimeter = 0;
						Point r_g_position = null;
						int r_n_faces = 0;
						int r_n_holes = 0;
						
						r_g_area = functions.numerical_metric(r_g_wkt, Metrics.AREA.get_value());
						r_g_perimeter = functions.numerical_metric(r_g_wkt, Metrics.PERIMETER.get_value());
						
						try {
							geometry = reader.read(r_g_wkt);
							r_g_position = geometry.getCentroid();
							
							r_n_faces = geometry.getNumGeometries();
							
							for (int j = 0; j < geometry.getNumGeometries(); j++)
							{
								p = (Polygon) geometry.getGeometryN(0);
								r_n_holes += p.getNumInteriorRing();
							}						
						} catch (ParseException e) {
							e.printStackTrace();
						}
						
						// get geometry at.
						
						String[] librip_g_wkt_at = null;
						String[] pygeom_g_wkt_at = null;
						
						if(librip_cb.isSelected())
						{
							double librip_g_area = 0;
							double librip_g_perimeter = 0;
							Point librip_g_position = null;
							int librip_n_faces = 0;
							int librip_n_holes = 0;
							
							double librip_g_h = 0;
							double librip_g_c = 0;
							double librip_g_ji = 0;
							double librip_g_jd = 0;
							
							method_info_lbl.setText("Using Secondo...");
							contentPane.update(contentPane.getGraphics());
							jp_l.repaint();
							jp_r.repaint();
							
							librip_g_wkt_at = librip.at(w_p, w_q, db, de, at, "overlap");
							
							librip_g_area = functions.numerical_metric(librip_g_wkt_at[0], Metrics.AREA.get_value());
							librip_g_perimeter = functions.numerical_metric(librip_g_wkt_at[0], Metrics.PERIMETER.get_value());
							
							try
							{
								geometry = reader.read(librip_g_wkt_at[0]);
								librip_g_position = geometry.getCentroid();
								
								librip_n_faces = geometry.getNumGeometries();	
								for (int j = 0; j < geometry.getNumGeometries(); j++)
								{
									p = (Polygon) geometry.getGeometryN(0);
									librip_n_holes += p.getNumInteriorRing();
								}							
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
							librip_g_h = functions.compare_geometries(r_g_wkt, librip_g_wkt_at[0], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
							librip_g_ji = functions.compare_geometries(r_g_wkt, librip_g_wkt_at[0], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());
							librip_g_jd = functions.compare_geometries(r_g_wkt, librip_g_wkt_at[0], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
							
							double delta_x =  librip_g_position.getX() - r_g_position.getX();
							double delta_y =  librip_g_position.getY() - r_g_position.getY();
							
							double d = Math.sqrt(delta_x * delta_x + delta_y * delta_y);
							double delta_area = librip_g_area - r_g_area;
							double delta_perimeter = librip_g_perimeter - r_g_perimeter;
							double delta_h = librip_g_h;
							double delta_jd = librip_g_jd;
							double delta_ji = librip_g_ji - 1;
							
							int delta_n_faces = librip_n_faces - r_n_faces;
							int delta_n_holes = librip_n_holes - r_n_holes;
							
							librip_mae[0] += Math.abs(d);
							librip_mae[1] += Math.abs(delta_area);
							librip_mae[2] += Math.abs(delta_perimeter);
							librip_mae[3] += Math.abs(delta_h);
							librip_mae[4] += Math.abs(delta_jd);
							librip_mae[5] += Math.abs(delta_ji);
							librip_mae[6] += Math.abs(delta_n_faces);
							librip_mae[7] += Math.abs(delta_n_holes);
							
							String mae = LIBRIP + ";"
										//+ MAE + ";"
										+ Math.abs(d) + ";" 
										+ Math.abs(delta_area) + ";" 
										+ Math.abs(delta_perimeter) + ";" 
										+ Math.abs(delta_h) + ";" 
										+ Math.abs(delta_jd) + ";" 
										+ Math.abs(delta_ji) + ";" 
										+ Math.abs(delta_n_faces) + ";"
										+ Math.abs(delta_n_holes) + ";";
							
							_MAE.add(mae);
							
							String rmse = LIBRIP + ";"
										//+ RMSE + ";"
										+ (d * d) + ";" 
										+ (delta_area * delta_area) + ";" 
										+ (delta_perimeter * delta_perimeter) + ";" 
										+ (delta_h * delta_h) + ";" 
										+ (delta_jd * delta_jd) + ";" 
										+ (delta_ji * delta_ji) + ";" 
										+ (delta_n_faces * delta_n_faces) + ";"
										+ (delta_n_holes * delta_n_holes) + ";";
							
							_RMSE.add(rmse);
						}
						
						if(pyspatiotemporalgeom_cb.isSelected())
						{
							double pygeom_g_area = 0;
							double pygeom_g_perimeter = 0;
							Point pygeom_g_position = null;
							int pygeom_n_faces = 0;
							int pygeom_n_holes = 0;
												
							double pygeom_g_h = 0;
							double pygeom_g_c = 0;
							double pygeom_g_ji = 0;
							double pygeom_g_jd = 0;
							
							method_info_lbl.setText("Using PySptGeom...");
							contentPane.update(contentPane.getGraphics());
							jp_l.repaint();
							jp_r.repaint();
							
							pygeom_g_wkt_at = pyspatiotemporalgeom_lib.at(w_p, w_q, idb, ide, (int) at);
							
							pygeom_g_area = functions.numerical_metric(pygeom_g_wkt_at[0], Metrics.AREA.get_value());
							pygeom_g_perimeter = functions.numerical_metric(pygeom_g_wkt_at[0], Metrics.PERIMETER.get_value());
							
							try 
							{						
								geometry = reader.read(pygeom_g_wkt_at[0]);
								pygeom_g_position = geometry.getCentroid();
								
								pygeom_n_faces = geometry.getNumGeometries();	
								for (int j = 0; j < geometry.getNumGeometries(); j++)
								{
									p = (Polygon) geometry.getGeometryN(0);
									pygeom_n_holes += p.getNumInteriorRing();
								}

							} catch (ParseException e) {
								e.printStackTrace();
							}
							
							pygeom_g_h = functions.compare_geometries(r_g_wkt, pygeom_g_wkt_at[0], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
							pygeom_g_ji = functions.compare_geometries(r_g_wkt, pygeom_g_wkt_at[0], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());
							pygeom_g_jd = functions.compare_geometries(r_g_wkt, pygeom_g_wkt_at[0], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
							
							double delta_x =  pygeom_g_position.getX() - r_g_position.getX();
							double delta_y =  pygeom_g_position.getY() - r_g_position.getY();
							
							double d = Math.sqrt(delta_x * delta_x + delta_y * delta_y);
							double delta_area = pygeom_g_area - r_g_area;
							double delta_perimeter = pygeom_g_perimeter - r_g_perimeter;
							double delta_h = pygeom_g_h;
							double delta_jd = pygeom_g_jd;
							double delta_ji = pygeom_g_ji - 1;
							
							int delta_n_faces = pygeom_n_faces - r_n_faces;
							int delta_n_holes = pygeom_n_holes - r_n_holes;

							pygeom_mae[0] += Math.abs(d);
							pygeom_mae[1] += Math.abs(delta_area);
							pygeom_mae[2] += Math.abs(delta_perimeter);
							pygeom_mae[3] += Math.abs(delta_h);
							pygeom_mae[4] += Math.abs(delta_jd);
							pygeom_mae[5] += Math.abs(delta_ji);
							pygeom_mae[6] += Math.abs(delta_n_faces);
							pygeom_mae[7] += Math.abs(delta_n_holes);
							
							String mae = PYGEOM + ";"
									//+ MAE + ";"
									+ Math.abs(d) + ";" 
									+ Math.abs(delta_area) + ";" 
									+ Math.abs(delta_perimeter) + ";" 
									+ Math.abs(delta_h) + ";" 
									+ Math.abs(delta_jd) + ";" 
									+ Math.abs(delta_ji) + ";" 
									+ Math.abs(delta_n_faces) + ";"
									+ Math.abs(delta_n_holes) + ";";
							
							_MAE.add(mae);
							
							String rmse = PYGEOM + ";"
									//+ RMSE + ";"
									+ (d * d) + ";" 
									+ (delta_area * delta_area) + ";" 
									+ (delta_perimeter * delta_perimeter) + ";" 
									+ (delta_h * delta_h) + ";" 
									+ (delta_jd * delta_jd) + ";" 
									+ (delta_ji * delta_ji) + ";" 
									+ (delta_n_faces * delta_n_faces) + ";"
									+ (delta_n_holes * delta_n_holes) + ";";
							
							_RMSE.add(rmse);
						}
					}
				}
				
				if(rigid_interpolation_lib_cb.isSelected())
				{
					String data = mrip_dataset_wkt_tx.getText();
					
					if(data != null && data.length() > 0)
						dataset_wkt = data.split(";");
					
					if(dataset_wkt == null || dataset_wkt.length == 0)
					{
						JOptionPane.showMessageDialog(null, "Invalid dataset!", "Viz", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					
					if(dataset_wkt.length / 3 != ref_at_t.length)
					{
						JOptionPane.showMessageDialog(null, "Invalid number of reference geometries!", "Viz", JOptionPane.INFORMATION_MESSAGE);
						return;
					}	
					
					t = 0;
					N = dataset_wkt.length;
					
					double dt = de - db;
					
					for(int k = 0; k < N; k = k + 3)
					{
						method_lbl.setText("Status: " + (k + 1) + " :: " + (dataset_wkt.length));
						contentPane.update(contentPane.getGraphics());
						jp_l.repaint();
						jp_r.repaint();

						w_p = dataset_wkt[k];
						w_q = dataset_wkt[k+2];
						
						double at = db + dt * ref_at_t[t];
						t++;
						
						Main functions = d.get_main();
						 
						String r_g_wkt = dataset_wkt[k+1];
						
						double r_g_area = 0;
						double r_g_perimeter = 0;
						Point r_g_position = null;
						int r_n_faces = 0;
						int r_n_holes = 0;
						
						r_g_area = functions.numerical_metric(r_g_wkt, Metrics.AREA.get_value());
						r_g_perimeter = functions.numerical_metric(r_g_wkt, Metrics.PERIMETER.get_value());
						
						try {
							geometry = reader.read(r_g_wkt);
							r_g_position = geometry.getCentroid();
							
							r_n_faces = geometry.getNumGeometries();
							
							for (int j = 0; j < geometry.getNumGeometries(); j++)
							{
								p = (Polygon) geometry.getGeometryN(0);
								r_n_holes += p.getNumInteriorRing();
							}						
						} catch (ParseException e) {
							e.printStackTrace();
						}
						
						// get geometry at.
						
						String[] morphrip_g_wkt_at = null;
						
							double mrip_g_area = 0;
							double mrip_g_perimeter = 0;
							Point mrip_g_position = null;
							int mrip_n_faces = 0;
							int mrip_n_holes = 0;
							
							double mrip_g_h = 0;
							double mrip_g_c = 0;
							double mrip_g_ji = 0;
							double mrip_g_jd = 0;
							
							method_info_lbl.setText("Using Morphrip...");
							contentPane.update(contentPane.getGraphics());
							jp_l.repaint();
							jp_r.repaint();
							
							morphrip_g_wkt_at = rigid.at(db, de, w_p, w_q, at, InterpolationMethod.EQUILATERAL.get_value(), d.get_cw(), d.get_threshold()/*, 0*/);
													
							mrip_g_area = functions.numerical_metric(morphrip_g_wkt_at[0], Metrics.AREA.get_value());
							mrip_g_perimeter = functions.numerical_metric(morphrip_g_wkt_at[0], Metrics.PERIMETER.get_value());

							try 
							{
								geometry = reader.read(morphrip_g_wkt_at[0]);
								mrip_g_position = geometry.getCentroid();
								
								mrip_n_faces = geometry.getNumGeometries();
								
								for (int j = 0; j < geometry.getNumGeometries(); j++)
								{
									p = (Polygon) geometry.getGeometryN(0);
									mrip_n_holes += p.getNumInteriorRing();
								}
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
							mrip_g_h = functions.compare_geometries(r_g_wkt, morphrip_g_wkt_at[0], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
							mrip_g_ji = functions.compare_geometries(r_g_wkt, morphrip_g_wkt_at[0], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());
							mrip_g_jd = functions.compare_geometries(r_g_wkt, morphrip_g_wkt_at[0], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());

							double delta_x =  mrip_g_position.getX() - r_g_position.getX();
							double delta_y =  mrip_g_position.getY() - r_g_position.getY();
							
							double d = Math.sqrt(delta_x * delta_x + delta_y * delta_y);
							double delta_area = mrip_g_area - r_g_area;
							double delta_perimeter = mrip_g_perimeter - r_g_perimeter;
							double delta_h = mrip_g_h;
							double delta_jd = mrip_g_jd;
							double delta_ji = mrip_g_ji - 1;
							
							int delta_n_faces = mrip_n_faces - r_n_faces;
							int delta_n_holes = mrip_n_holes - r_n_holes;
							
							mrip_mae[0] += Math.abs(d);
							mrip_mae[1] += Math.abs(delta_area);
							mrip_mae[2] += Math.abs(delta_perimeter);
							mrip_mae[3] += Math.abs(delta_h);
							mrip_mae[4] += Math.abs(delta_jd);
							mrip_mae[5] += Math.abs(delta_ji);
							mrip_mae[6] += Math.abs(delta_n_faces);
							mrip_mae[7] += Math.abs(delta_n_holes);
							
							String mae = RIGID + ";"
										//+ MAE + ";"
										+ Math.abs(d) + ";" 
										+ Math.abs(delta_area) + ";" 
										+ Math.abs(delta_perimeter) + ";" 
										+ Math.abs(delta_h) + ";" 
										+ Math.abs(delta_jd) + ";" 
										+ Math.abs(delta_ji) + ";" 
										+ Math.abs(delta_n_faces) + ";"
										+ Math.abs(delta_n_holes) + ";";
							
							_MAE.add(mae);
							
							String rmse = RIGID + ";"
									//+ RMSE + ";"
									+ (d * d) + ";" 
									+ (delta_area * delta_area) + ";" 
									+ (delta_perimeter * delta_perimeter) + ";" 
									+ (delta_h * delta_h) + ";" 
									+ (delta_jd * delta_jd) + ";" 
									+ (delta_ji * delta_ji) + ";" 
									+ (delta_n_faces * delta_n_faces) + ";"
									+ (delta_n_holes * delta_n_holes) + ";";
							
							_RMSE.add(rmse);
					}
				}
				
				// Output.
				
				ArrayList<String> res = new ArrayList<String>();
				
				int lr = 0;
				int py = 0;
				int mr = 0;
				
				int n_methods = _MAE.size() / n_obs;
				
				if((n_methods == 2 && rigid_interpolation_lib_cb.isSelected()) || n_methods == 3)
				{
					if(n_methods == 3)
					{
						for(int k = 0; k < n_obs; k++)
						{
							res.add(_MAE.get(lr));
							
							py = lr + 1;
							
							res.add(_MAE.get(py));
							
							mr = n_obs * 2 + k;
							
							res.add(_MAE.get(mr));
							
							lr += 2;
						}
					}
					else
					{
						for(int k = 0; k < n_obs; k++)
						{
							res.add(_MAE.get(k));
														
							mr = n_obs + k;
							
							res.add(_MAE.get(mr));
						}
					}
					
					_MAE = res;
				}
				
				if(librip_cb.isSelected())
				{
					String mae = LIBRIP + " (MAE);";
					
					for(int g = 0; g < librip_mae.length; g++)
					{
						librip_mae[g] /= n_obs;
						mae += librip_mae[g] + ";";
					}
				
					_MAE.add(mae);
				}
				
				if(pyspatiotemporalgeom_cb.isSelected())
				{
					String mae = PYGEOM + " (MAE);";
					
					for(int g = 0; g < pygeom_mae.length; g++)
					{
						pygeom_mae[g] /= n_obs;
						mae += pygeom_mae[g] + ";";
					}
				
					_MAE.add(mae);
				}
				
				if(rigid_interpolation_lib_cb.isSelected())
				{
					String mae = RIGID + " (MAE);";
					
					for(int g = 0; g < mrip_mae.length; g++)
					{
						mrip_mae[g] /= n_obs;
						mae += mrip_mae[g] + ";";
					}
				
					_MAE.add(mae);
				}

				// CSV.
				
				/*
				System.out.println();
				
				System.out.println(title);
				for(int j = 0; j < _MAE.size(); j++)
					System.out.println(_MAE.get(j));
				
				System.out.println();
				
				System.out.println(title);
				for(int j = 0; j < _RMSE.size(); j++)
					System.out.println(_RMSE.get(j));
				
				System.out.println();
				*/
				
				// Table.
				
				String[] col_names = title.split(";");
				
				Table tab = new Table("Prediction Evaluation", col_names, _MAE, 850, 600);
				tab.setVisible(true);
				
				method_info_lbl.setText("Done!");
				method_lbl.setText("Status:");
				contentPane.update(contentPane.getGraphics());
				
				jp_l.repaint();
				jp_r.repaint();
			}
		});
                
		// Zoom out.
		
    	minus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs l = (DrawGraphs) jp_l;
				DrawGraphs r = (DrawGraphs) jp_r;

				zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
				
	           	if (zoomLevel > minZoomLevel) 
	           	{
	           		zoomLevel -= zoomMultiplicationFactor;
	           		
					l.scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
					l.translate(false);
					
					r.scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
					r.translate(false);
	            }

	            jp_l.repaint();
	            jp_r.repaint();
			}
		});
    	
    	// Zoom in.
    	
    	plus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs l = (DrawGraphs) jp_l;
				DrawGraphs r = (DrawGraphs) jp_r;

				zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
				
            	if (zoomLevel < maxZoomLevel) 
            	{
            		zoomLevel += zoomMultiplicationFactor;
            		
	            	l.scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
	            	l.translate(true);
	            	
	            	r.scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
	            	r.translate(true);
            	}

	            jp_l.repaint();
	            jp_r.repaint();
			}
		});
    	
		play.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if(geometries_l == null && geometries_r == null)
					return;
				
				play.setEnabled(false);
				interpolation_slider.setEnabled(false);
				show_footprint = false;
				
				geom_to_show_id = 0;
				
			    play_task = new TimerTask() 
			    {
			    	int counter = 0;
			    	
			        public void run() 
			        {
			        	jp_l.repaint();
			        	jp_r.repaint();
			        	
			        	// To show the first frame.
			        	if(counter > 2)
			        	{
				    		if(geom_to_show_id < max) 
				    			geom_to_show_id++;	    			
				    		else 
				    		{
				    			timer.cancel();
				    			
								interpolation_slider.setValue(interpolation_slider.getMaximum());
								play.setEnabled(true);
								interpolation_slider.setEnabled(true);
				    		}			        		
			        	}
			        	
			        	counter++;
			        }
			    };
			    
				timer = new Timer();
				timer.scheduleAtFixedRate(play_task, 0, Long.parseLong(rate_tx.getText()));
			}
		});
		
		interpolation_slider.addChangeListener(new ChangeListener() 
		{
		    public void stateChanged(ChangeEvent e) 
		    {
		    	if(geometries_l == null && geometries_r == null)
		    		return;

		        JSlider s = (JSlider) e.getSource();
		        int i = (int) s.getValue();

		    	if(i <= max)
		    		geom_to_show_id = i;
		    	
		    	n_samples.setText("N� Samples: " + dn + " :: " + (geom_to_show_id + 1));
		    	
		    	jp_l.repaint();
		    	jp_r.repaint();
		    }
		});
	}
}