package jni_st_mesh;

public enum SimilarityMetrics 
{
	HAUSDORFF_DISTANCE(1), JACCARD_INDEX(2), JACCARD_DISTANCE(3);
			
	private int value;    

	private SimilarityMetrics(int value) {
		this.value = value;
	}

	public int get_value() {
		return value;
	}
}