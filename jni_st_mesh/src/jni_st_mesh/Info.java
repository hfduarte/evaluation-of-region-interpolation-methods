package jni_st_mesh;

import javax.swing.JFrame;
import javax.swing.JTable;
import java.awt.BorderLayout;
import javax.swing.table.DefaultTableModel;

public class Info extends JFrame
{
	private static final long serialVersionUID = 1L;
	private String[] data;
	private JTable table;
	
	public Info(String[] data)
	{
		this.data = data;
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Instant (t)", "X", "Y"
			}
		) 
		{
			private static final long serialVersionUID = 1L;
			
			Class[] columnTypes = new Class[] {
				Double.class, Double.class, Double.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		getContentPane().add(table, BorderLayout.CENTER);
	}
	
}
