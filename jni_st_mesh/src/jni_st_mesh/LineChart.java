package jni_st_mesh;

import java.awt.Color;   
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import javax.swing.JFrame;
import org.jfree.chart.*;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PlotOrientation;   
import org.jfree.chart.plot.XYPlot;   
import org.jfree.data.xy.*;

public class LineChart extends JFrame
{
	private static final long serialVersionUID = 1328795438858727411L;
	private HashMap<String, Double> dic_xy_values;
	private String title;
	private String x_leg;
	private String y_leg;
	private ChartPanel chartpanel;
	private boolean natural_sort;
	private double lower_range;
	private double upper_range;
	private double step;
	private boolean set_range;

	public LineChart(
						String title, 
						String x_legend, 
						String y_legend, 
						HashMap<String, Double> values, 
						boolean natural_sort,
						double lower_range,
						double upper_range,
						double step,
						boolean set_range)   
	{   
		super("2D Chart");   
		
		this.dic_xy_values = values;
		this.title = title;
		this.x_leg = x_legend;
		this.y_leg = y_legend;
		this.natural_sort = natural_sort;
		
		this.lower_range = lower_range;
		this.upper_range = upper_range;
		this.step = step;
		this.set_range = set_range;
		
	    XYDataset xydataset = createDataset();   
	    JFreeChart jfreechart = createChart(xydataset);   
	    chartpanel = new ChartPanel(jfreechart);   
	    chartpanel.setPreferredSize(new Dimension(800, 600));   
	    setContentPane(chartpanel);   
	    
        addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(WindowEvent e) 
            {
                setVisible(false);
                dispose();
            }
        });
	}
	
	private XYDataset createDataset()   
	{   
		XYSeries xyseries = new XYSeries(title);   
        
		List<String> sortedKeys=new ArrayList<>(dic_xy_values.keySet());
		
		if(natural_sort)
			Collections.sort(sortedKeys, new Comparator<String>()
		    {
		      public int compare(String s1, String s2)
		      {
		        return Integer.valueOf(s1).compareTo(Integer.valueOf(s2));
		      }
		    });
		else
			Collections.sort(sortedKeys);
		
		for(String k : sortedKeys)
			xyseries.add(Integer.parseInt(k), dic_xy_values.get(k));

	    XYSeriesCollection xyseriescollection = new XYSeriesCollection();   
	    xyseriescollection.addSeries(xyseries);   
	        
	    return xyseriescollection;   
	}   
	   
	private JFreeChart createChart(XYDataset xydataset)   
	{   
		// Does not show the legend
		JFreeChart jfreechart = ChartFactory.createXYLineChart(title, x_leg, y_leg, xydataset, PlotOrientation.VERTICAL, false, true, false);   
	    
	    jfreechart.setBackgroundPaint(Color.white);   
	    
	    XYPlot xyplot = (XYPlot) jfreechart.getPlot();
	    xyplot.setBackgroundPaint(Color.white);   
	    xyplot.setDomainGridlinePaint(Color.black);   
	    xyplot.setRangeGridlinePaint(Color.black);
	    
	    xyplot.getDomainAxis().setLowerMargin(0.0);
	    xyplot.getDomainAxis().setUpperMargin(0.0);
	    
	    if(set_range)
	    {
		    NumberAxis rangeAxis = (NumberAxis) xyplot.getRangeAxis();
		    rangeAxis.setRange(lower_range, upper_range);
		    rangeAxis.setTickUnit(new NumberTickUnit(step));
	    }
	    
	    return jfreechart;   
	}
}