package jni_st_mesh;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;

import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.geometry.jts.LiteShape;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.locationtech.jts.util.GeometricShapeFactory;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JSlider;

import javax.swing.event.ChangeListener;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.SystemColor;
import javax.swing.JTextPane;

public class Agile extends JFrame 
{
	private static final long serialVersionUID = 6237268416941231840L;
	
	private JPanel contentPane;
	
	private JTextField save_picture_to_tx;

	private JButton save_to_picture_bt;
	
	private JButton show_m_region_evolution_bt;
	
	private JSlider show_m_region_evolution_slider;
	
	private JPanel viz_p;
	
	private JFrame f;
	
	private JPanel jp;
	
	private JLabel status;
	
	private JPanel tools_p;
	
	private JButton zoom_out_bt;
	
	private JButton zoom_in_bt;
	
	private JButton show_footprint_bt;
	
	private JLabel zoom_level;
	
	public static final double DEFAULT_ZOOM_MULTIPLICATION_FACTOR = 0.25;
	public static final double DEFAULT_MIN_ZOOM_LEVEL = 0.25;
	public static final double DEFAULT_MAX_ZOOM_LEVEL = 100;
	
    private double zoomLevel = 1.0;
    private double minZoomLevel = DEFAULT_MIN_ZOOM_LEVEL;
    private double maxZoomLevel = DEFAULT_MAX_ZOOM_LEVEL;
    private double zoomMultiplicationFactor = DEFAULT_ZOOM_MULTIPLICATION_FACTOR;
    
    private JTextField rate;
    
    private int geom_to_show_id = 0;
    
    private GeometryFactory geometryFactory;
    private WKTReader reader;
    
    private int Nr;
    
    private String[] mr_p_i;
    private int mr_p_i_len;
    
    private int max;
    
    private TimerTask play_task;
    
	private int w_center;
	private int h_center;
    
	private double cx = 0;
	private double cy = 0;

    private Timer timer;
    private JTextField step;
    private JCheckBox fill;

    private JSlider zoom;
    
    private JLabel n_samples;
    
    private JTextField n_samples_tx;
    private JButton reset_bt;
    private JTextField i_e_tx;
    private Geometry geometry;
    private JTextField i_b_tx;
    private JButton mr_p_i_bt;
    private String intersection_info;
    private String solver_execution_time;
    private JTextField p_radius_tx;
    private JTextArea mra_data_tx;
    private JTextArea range_data_tx;
    private JComboBox<String> tests_cb;
    private JButton load_test_bt;
    
    private String[] titles;
    private String[] mss_data;
    private String[] mps_data;
    private String[] intervals;
    private String[] tests_description;
	private JTextPane test_desc_tp;
	private JTextField s_precision_tx;
	private JTextField n_dec_dig_tx;
	private JComboBox<String> op_cb;
	private String operation;
	private JButton load_mr1_bt;
	private JButton load_mr2_bt;
	private JButton load_mp_bt;
	private JTextField at_tx;
	private JTextField t_gran_tx;
	private JButton exec_op_bt;
	private Data data;
	private String footprint;
	private String envelope;
	private JTextArea mp_data_tx;
	private String intersection;
	private JButton mreg_r_bt;
	private String[] aabb;
	private String[] geoms;
	private String static_region_wkt;
	private String python_script_path;
	private JTextArea mrb_data_tx;
	private JButton load_mr_bt;
	private JButton boolean_ops_bt;
	private JButton real_ops_bt;
	private JButton geom_ops_bt;
	
	private String mo_union;
	private String mo_intersection;
	private String mo_difference;
	private JButton mr_r_i_bt;

	public Agile(Data d) 
	{	
		mr_p_i = null;
		Nr = 0;
		f = this;
		data = d;
		
		boolean load_tests_information = true;
		
		// Load tests data from hd.
		
		String tests_dir = "D:\\java\\tests\\msmp";
		
		File folder = new File(tests_dir);
		File[] listOfFiles = folder.listFiles();

		int n = listOfFiles.length;
		
		python_script_path = "D:\\\\java\\\\agile.py";
		
		operation = "";
		
		footprint = null;
		envelope = null;
		intersection = null;
		aabb = null;
		geoms = null;
		
		mo_union = null;
		mo_intersection = null;
		mo_difference = null;
		
		if(n > 0)
		{
			titles = new String[n];
			mss_data = new String[n];
			mps_data = new String[n];
			intervals = new String[n];
			tests_description = new String[n];
		}
		
    	BufferedReader br;
    	String l;
    	int j = 0;
    	int k = 0;
		
		for (File file : listOfFiles) 
		{	
		    if (file.isFile()) 
		    {    	
				try 
				{
					br = new BufferedReader(new FileReader(file));
					k = 0;
					
					while ((l = br.readLine()) != null) 
					{
						if(k == 0)
							titles[j] = l;
						else if(k == 1)
							tests_description[j] = l;
						else if(k == 2)
							mss_data[j] = l;
						else if(k == 3)
							mps_data[j] = l;
						else if(k == 4)
							intervals[j] = l;
						else
						{
							JOptionPane.showMessageDialog(null, "ERR: Invalid tests information!", "Tests", JOptionPane.INFORMATION_MESSAGE);
							load_tests_information = false;
						}
						
						k++;
					}
					
				} catch (FileNotFoundException e1) 
				{
					e1.printStackTrace();
				} catch (IOException e) 
				{
					e.printStackTrace();
				}
				
				j++;
		    }
		}
		
		// End Tests data.
		
	    intersection_info = null;
	    solver_execution_time = null;
				
		geom_to_show_id = 0;
		
		geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
	    reader = new WKTReader(geometryFactory);
			    
	    max = 0;
	    	    
		draw_UI();
			
		w_center = (int) (viz_p.getWidth() / 2);
		h_center = (int) (viz_p.getHeight() / 2);
			
		add_listeners();
		draw_geometry();
		
		if(load_tests_information)
		{
			n = titles.length;
			String[] model = new String[n];
			
			for (int i = 0; i < n; i++)
				model[i] = titles[i];

			tests_cb.setModel(new DefaultComboBoxModel<>(model));
			
        	SimpleAttributeSet just = new SimpleAttributeSet();
        	StyleConstants.setAlignment(just, StyleConstants.ALIGN_JUSTIFIED);
        	
        	StyledDocument doc = test_desc_tp.getStyledDocument();
        	doc.setParagraphAttributes(20, 1,just, false);
        	
        	test_desc_tp.setText(tests_description[tests_cb.getSelectedIndex()]);
		}
	}
	
	public void paint(Graphics g) 
	{
	    super.paint(g);
	    jp.repaint();
	}

	public void draw_geometry()
	{
		viz_p.setLayout(new BorderLayout());
		jp = new DrawGraphs();
		viz_p.add(jp, BorderLayout.CENTER);
	}
	
    private class DrawGraphs extends JPanel
    {
    	private int dx = 0;
    	private int dy = 0;
    	
    	private double sx = 1;
    	private double sy = 1; 	
    	   	
    	private int xx = 0;
    	private int yy = 0;
    	
		private static final long serialVersionUID = 3203545490664689791L;
		
		public DrawGraphs() 
		{
			setBackground(Color.white);
			setAlignmentY(CENTER_ALIGNMENT);
			setAlignmentX(CENTER_ALIGNMENT);
			
			cx = 0;
			cy = 0;

			dx = (int) ((-cx + w_center));
			dy = (int) ((cy + h_center));
			
	        addMouseListener(new MouseAdapter() 
	        {
	            public void mousePressed(MouseEvent e) 
	            {
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });

	        addMouseMotionListener(new MouseAdapter() 
	        {
	            public void mouseDragged(MouseEvent e) 
	            {        	
	            	int ddx = (e.getX() - xx);
	            	int ddy = (e.getY() - yy);
	            	
	            	translate(ddx, ddy);
	            	repaint();
	            	
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });
	        
	        addMouseWheelListener(new MouseAdapter(){
	            public void mouseWheelMoved(MouseWheelEvent e) 
	            {
    	        
		            int notches = e.getWheelRotation();
					DrawGraphs c = (DrawGraphs) jp;
					
		            if (notches > 0) 
		            {
		            	if (zoomLevel < maxZoomLevel) 
		            	{
		            		zoomLevel += zoomMultiplicationFactor; 
			            	c.scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
			            	c.translate(true);
		            	}
		            } else {
			           	if (zoomLevel > minZoomLevel) 
			           	{
			           		zoomLevel -= zoomMultiplicationFactor;
							c.scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
							c.translate(false);
			            }
		            }

		            jp.repaint();
	            }
	        });       
		};
		
        public void reset() 
        {
        	dx = 0;
        	dy = 0;
        	
        	sx = 1;
        	sy = 1; 	
        	   	
        	xx = 0;
        	yy = 0;
        }
		
        public void adjust_screen_position()
        {
			if(mr_p_i == null)
				return;
			
			try {
				geometry = reader.read(mr_p_i[0]);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
        	if(geometry == null)
        		return;
        	
			Point c = geometry.getCentroid();

			cx = c.getX();
			cy = c.getY();

			w_center = (int) (this.getParent().getWidth() / 2);
			h_center = (int) (this.getParent().getHeight() / 2);
			
			dx = (int) ((-cx + w_center));
			dy = (int) ((cy + h_center));
        }
        
		@Override
        protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);

	        Graphics2D gr = (Graphics2D) g;
	        gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		    gr.setStroke(new BasicStroke(1.0f));	        
		    gr.setPaint(Color.blue);

	        AffineTransform at = new AffineTransform();

	        at.translate(dx, dy);        
	        at.scale(sx, -sy);
	        
	        zoom_level.setText("Zoom Level: " + sx);
	                
			try 
			{
	    		double marker_radius = Double.parseDouble(p_radius_tx.getText());

	    		if(footprint != null)
	    		{
	    			Geometry geom = reader.read(footprint);
	    			
	    			if(fill.isSelected())
	    			{
	    		        //int fill_R = Integer.parseInt(fill_r_tx.getText());
	    		        //int fill_G = Integer.parseInt(fill_g_tx.getText());
	    		        //int fill_B = Integer.parseInt(fill_b_tx.getText());
	    				
   						gr.setPaint(new Color(18, 21, 67));
   						
		   		        for (int j = 0; j < geom.getNumGeometries(); j++)
				   			gr.fill(new LiteShape(geom.getGeometryN(j), at, false));
	    			}
	    			else
	    				gr.draw(new LiteShape(geom, at, false));
	    		}
	    		
	    		if(envelope != null)
	    		{
	    			Geometry geom = reader.read(envelope);
	    			
	    			/*
	    			if(fill.isSelected())
	    			{
   						gr.setPaint(new Color(18, 21, 67));
   						
		   		        for (int j = 0; j < geom.getNumGeometries(); j++)
				   			gr.fill(new LiteShape(geom.getGeometryN(j), at, false));
	    			}
	    			else
	    			*/
	    			
	    			gr.setPaint(Color.black);
	    			gr.draw(new LiteShape(geom, at, false));
	    		}
	    		
	    		if(intersection != null)
	    		{
	    			Geometry geom = reader.read(intersection);
	    			gr.setPaint(Color.red);
	    			gr.draw(new LiteShape(geom, at, false));
	    		}
	    		
	    		if(mo_union != null)
	    		{
	    			Geometry geom = reader.read(mo_union);
	    			gr.setPaint(Color.blue);
	    			gr.draw(new LiteShape(geom, at, false));
	    		}
	    		
	    		if(mo_intersection != null)
	    		{
	    			Geometry geom = reader.read(mo_intersection);
	    			gr.setPaint(Color.red);
	    			gr.draw(new LiteShape(geom, at, false));
	    		}
	    		
	    		if(mo_difference != null)
	    		{
	    			/*
	    			Geometry geom = reader.read(mo_difference);
	    			gr.setPaint(Color.black);
	    			gr.draw(new LiteShape(geom, at, false));
	    			*/
	    		}
	    		
	    		if(aabb != null)
	    		{
	    			Geometry geom = reader.read(aabb[geom_to_show_id]);
	    			gr.setPaint(Color.blue);
	    			gr.draw(new LiteShape(geom, at, false));
	    			
	    			geom = reader.read(geoms[geom_to_show_id]);
	    			gr.setPaint(Color.black);
	    			gr.draw(new LiteShape(geom, at, false));
	    			
	    			geom = reader.read(static_region_wkt);
	    			gr.setPaint(Color.magenta);
	    			gr.draw(new LiteShape(geom, at, false));
	    		}
	    		
	    		gr.setPaint(Color.blue);
	    		
	    		if(mr_p_i != null)
	    		{
	    			Polygon p;
	    			String v = mr_p_i[geom_to_show_id * Nr + (Nr - 1)];
	    			String info = "O: " + String.valueOf(geom_to_show_id + 1) + " :: ";
	    			
	    			for(int h = 0; h < Nr - 2; h++)
	    			{
		    			p = (Polygon) reader.read(mr_p_i[geom_to_show_id * Nr + h]);
		    			
		    			if(v.equalsIgnoreCase("0"))
		    			{
		    				info += "False";
		    				gr.setPaint(Color.green);
		    			}
		    			else
		    			{
		    				info += "True";
		    				gr.setPaint(Color.red);
		    			}
		    			
			    		gr.draw(new LiteShape(p, at, false));
	    			}  			
	    			
		    		LineString l = (LineString) reader.read(mr_p_i[geom_to_show_id * Nr + (Nr - 2)]);
		    		
		    		if(!l.isEmpty())
		    		{
		    			if(v.equalsIgnoreCase("0"))
		    				gr.setPaint(Color.red);
		    			else
		    				gr.setPaint(Color.black);
		    			
	   					//gr.setPaint(Color.red);
			    		gr.draw(new LiteShape(l, at, false));

			    		// Draw marker.
			    		
		    			Point c;
		    			GeometricShapeFactory shape_factory;
		    			Geometry circle;
			    		
			    		c = l.getEnvelope().getCentroid();
			    		
			    		shape_factory = new GeometricShapeFactory();
			    		shape_factory.setNumPoints(32);
			    		shape_factory.setCentre(new Coordinate(c.getX(), c.getY()));
			    		shape_factory.setSize(marker_radius);
			    		circle = shape_factory.createCircle();
			    		
		    			if(v.equalsIgnoreCase("0"))
		    				gr.setPaint(Color.green);
		    			else
		    				gr.setPaint(Color.magenta);
			    		
			    		gr.draw(new LiteShape(circle, at, false));
		    		}
		    		
		    		Color c = null;
		    		c = new Color(140, 87, 29);
		    				
	    		    gr.setPaint(c);
	    		    gr.setFont(new Font("Arial", Font.BOLD, 14));
	    		    gr.drawString(operation, 20, 30);
		    		
	    		    c = new Color(19, 25, 66);
	    		    gr.setPaint(c);
	    		    gr.setFont(new Font("Arial", Font.BOLD, 14));
	    		    gr.drawString(intersection_info, 20, 50);
	    		    
	    			if(v.equalsIgnoreCase("0"))
	    				gr.setPaint(Color.black);
	    			else
	    				gr.setPaint(Color.magenta);
	    		    
	    		    gr.setFont(new Font("Arial", Font.BOLD, 14));
	    		    gr.drawString(info, 20, 70);
	    		    
	    		    if(solver_execution_time != null)
	    		    {
		    		    gr.setPaint(Color.gray);
		    		    gr.setFont(new Font("Arial", Font.BOLD, 14));
		    		    gr.drawString(solver_execution_time, 20, 90);
	    		    }
	    		}
			} 
			catch (ParseException e) 
			{
				e.printStackTrace();
			}
        };
		
        public void translate(boolean sign) 
        {
        	double cx_star = cx * zoomMultiplicationFactor;
        	double cy_star = cy * zoomMultiplicationFactor;
        	
			if(sign) 
			{
				dx -= (int) cx_star;
				dy += (int) cy_star;				
			}
			else
			{
				dx += (int) cx_star;
				dy -= (int) cy_star;				
			}
        }
        
        public void translate(int x, int y) 
        {
        	dx += x;
        	dy += y;
        }
        
        public void scale(double x, double y) 
        {
        	sx += x;
        	sy += y;
        }
    }
	
	public void draw_UI() 
	{
		setTitle("Study of Discrete Solutions");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	    setSize(1296, 720);
	    setLocationRelativeTo(null);
	    setExtendedState(f.getExtendedState() | JFrame.MAXIMIZED_BOTH);
	    
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		viz_p = new JPanel();
		viz_p.setLocation(175, 10);
		
		viz_p.setSize(930, 590);
		
		viz_p.setBackground(Color.WHITE);
		viz_p.setBorder(new LineBorder(Color.black, 1));
		contentPane.add(viz_p);
		
		tools_p = new JPanel();
		tools_p.setBorder(new LineBorder(new Color(0, 0, 0)));
		tools_p.setBounds(10, 616, 1260, 36);
		contentPane.add(tools_p);
		
		zoom_out_bt = new JButton("Zoom Out");
		tools_p.add(zoom_out_bt);
		
		zoom_in_bt = new JButton("Zoom In");
		tools_p.add(zoom_in_bt);
	
		save_to_picture_bt = new JButton("Save to Picture");
		tools_p.add(save_to_picture_bt);
		
		save_picture_to_tx = new JTextField();
		save_picture_to_tx.setText("d:\\\\curve_app_viz_1");
		tools_p.add(save_picture_to_tx);
		save_picture_to_tx.setColumns(16);
		
		rate = new JTextField();
		rate.setText("16");
		tools_p.add(rate);
		rate.setColumns(2);
		
		show_m_region_evolution_bt = new JButton("Interpolate");
		tools_p.add(show_m_region_evolution_bt);
		
		show_m_region_evolution_slider = new JSlider();
		show_m_region_evolution_slider.setValue(0);
		tools_p.add(show_m_region_evolution_slider);
		
		show_m_region_evolution_slider.setMinimum(0);
		show_m_region_evolution_slider.setMaximum(max);
		
		status = new JLabel("");
		tools_p.add(status);
		
		show_footprint_bt = new JButton("Show Footprint");
		show_footprint_bt.setEnabled(false);
		show_footprint_bt.setBounds(12, 10, 153, 25);
		contentPane.add(show_footprint_bt);
		
		step = new JTextField();
		step.setEnabled(false);
		step.setText("1");
		step.setBounds(12, 37, 56, 22);
		contentPane.add(step);
		step.setColumns(10);
		
		fill = new JCheckBox("Fill");
		fill.setBounds(107, 36, 56, 25);
		contentPane.add(fill);
		
		zoom_level = new JLabel("Zoom Level:");
		zoom_level.setBounds(1115, 10, 155, 14);
		contentPane.add(zoom_level);
		
		zoom = new JSlider();
		zoom.setEnabled(false);
		zoom.setValue(1);
		zoom.setMinimum(1);
		zoom.setBounds(1115, 38, 155, 26);
		contentPane.add(zoom);
		
		n_samples = new JLabel("N\u00BA Samples: ");
		n_samples.setBounds(1115, 73, 155, 14);
		contentPane.add(n_samples);
		
	    n_samples.setText("N� Samples: 0");
	    
	    n_samples_tx = new JTextField();
	    n_samples_tx.setToolTipText("Number of samples for vizualisation.");
	    n_samples_tx.setText("100");
	    n_samples_tx.setBounds(10, 441, 65, 20);
	    contentPane.add(n_samples_tx);
	    n_samples_tx.setColumns(10);
	    
	    reset_bt = new JButton("Reset");
	    reset_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    reset_bt.setForeground(new Color(0, 0, 128));
	    reset_bt.setBounds(10, 578, 155, 23);
	    contentPane.add(reset_bt);
	    
	    i_e_tx = new JTextField();
	    i_e_tx.setToolTipText("Final instant of the interval considered.");
	    i_e_tx.setBackground(SystemColor.inactiveCaption);
	    i_e_tx.setText("2000");
	    i_e_tx.setBounds(100, 419, 64, 20);
	    contentPane.add(i_e_tx);
	    i_e_tx.setColumns(10);
	    
	    i_b_tx = new JTextField();
	    i_b_tx.setToolTipText("Initial instant of the interval considered.");
	    i_b_tx.setBackground(SystemColor.inactiveCaption);
	    i_b_tx.setForeground(Color.BLACK);
	    i_b_tx.setText("1000");
	    i_b_tx.setBounds(10, 419, 65, 20);
	    contentPane.add(i_b_tx);
	    i_b_tx.setColumns(10);
	    
	    mr_p_i_bt = new JButton("Load MOs");
	    mr_p_i_bt.setToolTipText("Load the moving objects.");
	    mr_p_i_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    mr_p_i_bt.setBounds(10, 170, 155, 23);
	    contentPane.add(mr_p_i_bt);
	    
	    mra_data_tx = new JTextArea();
	    mra_data_tx.setToolTipText("Moving segment data.");
	    mra_data_tx.setBackground(new Color(245, 245, 220));
	    mra_data_tx.setText("POLYGON((2 2, 3.5 6, 5 1, 2 2));POLYGON((8 11, 11 8, 9 6.5, 8 11));1000;2000#POLYGON((8 11, 11 8, 9 6.5, 8 11));POLYGON((16 7, 14.5 3, 12.5 8, 16 7));2000;3000");
	    mra_data_tx.setBounds(10, 216, 155, 36);
	    contentPane.add(mra_data_tx);
	    
	    range_data_tx = new JTextArea();
	    range_data_tx.setToolTipText("Moving point data.");
	    range_data_tx.setBackground(new Color(230, 230, 250));
	    range_data_tx.setText("1;1000;2000;0#1;2000;3000;0");
	    range_data_tx.setBounds(10, 298, 155, 36);
	    contentPane.add(range_data_tx);
	    
	    tests_cb = new JComboBox<String>();
	    tests_cb.setToolTipText("Tests found on disk.");
	    tests_cb.setModel(new DefaultComboBoxModel<>(new String[] {}));
	    tests_cb.setBounds(10, 93, 155, 20);
	    contentPane.add(tests_cb);
	    
	    p_radius_tx = new JTextField();
	    p_radius_tx.setToolTipText("Moving point marker radius.");
	    p_radius_tx.setBackground(new Color(240, 255, 240));
	    p_radius_tx.setText("0.25");
	    p_radius_tx.setBounds(10, 497, 65, 20);
	    contentPane.add(p_radius_tx);
	    p_radius_tx.setColumns(10);
	    
	    load_test_bt = new JButton("Load Test");
	    load_test_bt.setToolTipText("Load selected test.");
	    load_test_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    load_test_bt.setBounds(10, 114, 155, 23);
	    contentPane.add(load_test_bt);
	    
	    JLabel tests_lb = new JLabel("Tests:");
	    tests_lb.setForeground(new Color(25, 25, 112));
	    tests_lb.setFont(new Font("Tahoma", Font.BOLD, 12));
	    tests_lb.setBounds(10, 66, 155, 25);
	    contentPane.add(tests_lb);
	    
	    JLabel test_desc_lb = new JLabel("Test Description:");
	    test_desc_lb.setFont(new Font("Tahoma", Font.BOLD, 12));
	    test_desc_lb.setBounds(1115, 96, 155, 22);
	    contentPane.add(test_desc_lb);
	    
	    test_desc_tp = new JTextPane();
	    test_desc_tp.setBackground(new Color(245, 255, 250));
	    test_desc_tp.setBounds(1115, 123, 155, 222);
	    contentPane.add(test_desc_tp);
	    
	    s_precision_tx = new JTextField();
	    s_precision_tx.setBackground(new Color(245, 245, 220));
	    s_precision_tx.setToolTipText("Spatial precision.");
	    s_precision_tx.setText("0.0000001");
	    s_precision_tx.setBounds(10, 518, 65, 20);
	    contentPane.add(s_precision_tx);
	    s_precision_tx.setColumns(10);
	    
	    n_dec_dig_tx = new JTextField();
	    n_dec_dig_tx.setBackground(new Color(245, 255, 250));
	    n_dec_dig_tx.setToolTipText("Number of decimal  digits.");
	    n_dec_dig_tx.setText("2");
	    n_dec_dig_tx.setBounds(100, 518, 65, 20);
	    contentPane.add(n_dec_dig_tx);
	    n_dec_dig_tx.setColumns(10);
	    
	    op_cb = new JComboBox<>();
	    op_cb.setModel(new DefaultComboBoxModel<>(new String[] {"footprint", "intersects", "touches", "equals", "disjoint", "contains", "within"}));
	    op_cb.setSelectedIndex(0);
	    op_cb.setBounds(10, 149, 155, 20);
	    contentPane.add(op_cb);
	    
	    load_mr1_bt = new JButton("Load MR1");
	    load_mr1_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    load_mr1_bt.setBounds(10, 346, 155, 23);
	    contentPane.add(load_mr1_bt);
	    
	    load_mr2_bt = new JButton("Load MR2");
	    load_mr2_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    load_mr2_bt.setBounds(10, 370, 155, 23);
	    contentPane.add(load_mr2_bt);
	    
	    load_mp_bt = new JButton("Load MP");
	    load_mp_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    load_mp_bt.setBounds(10, 394, 155, 23);
	    contentPane.add(load_mp_bt);
	    
	    at_tx = new JTextField();
	    at_tx.setBackground(Color.PINK);
	    at_tx.setToolTipText("At");
	    at_tx.setText("1100");
	    at_tx.setColumns(10);
	    at_tx.setBounds(100, 441, 65, 20);
	    contentPane.add(at_tx);
	    
	    t_gran_tx = new JTextField();
	    t_gran_tx.setBackground(new Color(245, 245, 220));
	    t_gran_tx.setForeground(Color.BLACK);
	    t_gran_tx.setToolTipText("Number of samples for vizualisation.");
	    t_gran_tx.setText("10");
	    t_gran_tx.setColumns(10);
	    t_gran_tx.setBounds(10, 463, 65, 20);
	    contentPane.add(t_gran_tx);
	    
	    exec_op_bt = new JButton("Apply Operation");
	    exec_op_bt.setForeground(new Color(0, 0, 0));
	    exec_op_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    exec_op_bt.setBounds(10, 554, 155, 23);
	    contentPane.add(exec_op_bt);
	    
	    mp_data_tx = new JTextArea();
	    mp_data_tx.setText("1;17;1;17;1000;3000");
	    mp_data_tx.setBackground(new Color(216, 191, 216));
	    mp_data_tx.setBounds(10, 257, 155, 36);
	    contentPane.add(mp_data_tx);
	    
	    mreg_r_bt = new JButton("New button");
	    mreg_r_bt.setBounds(1115, 356, 155, 23);
	    contentPane.add(mreg_r_bt);
	    
	    mrb_data_tx = new JTextArea();
	    mrb_data_tx.setToolTipText("Moving segment data.");
	    mrb_data_tx.setText("POLYGON((8 11, 11 8, 9 6.5, 8 11));POLYGON((16 7, 14.5 3, 12.5 8, 16 7));1000;2000#POLYGON((2 2, 3.5 6, 5 1, 2 2));POLYGON((8 11, 11 8, 9 6.5, 8 11));2000;3000");
	    mrb_data_tx.setBackground(new Color(245, 245, 220));
	    mrb_data_tx.setBounds(1115, 393, 155, 36);
	    contentPane.add(mrb_data_tx);
	    
	    load_mr_bt = new JButton("New button");
	    load_mr_bt.setBounds(1115, 431, 155, 23);
	    contentPane.add(load_mr_bt);
	    
	    boolean_ops_bt = new JButton("Boolean Ops");
	    boolean_ops_bt.setBounds(1115, 454, 155, 23);
	    contentPane.add(boolean_ops_bt);
	    
	    real_ops_bt = new JButton("Real Ops");
	    real_ops_bt.setBounds(1115, 477, 155, 23);
	    contentPane.add(real_ops_bt);
	    
	    geom_ops_bt = new JButton("Geometric Ops");
	    geom_ops_bt.setBounds(1115, 500, 155, 23);
	    contentPane.add(geom_ops_bt);
	    
	    mr_r_i_bt = new JButton("MR. R");
	    mr_r_i_bt.setBounds(1115, 529, 155, 23);
	    contentPane.add(mr_r_i_bt);
	}

	public void add_listeners()
	{
        addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(WindowEvent e) 
            {
                setVisible(false);
            }
        });
        
        tests_cb.addActionListener (new ActionListener () 
        {
            public void actionPerformed(ActionEvent e) 
            {
            	test_desc_tp.setText(tests_description[tests_cb.getSelectedIndex()]);
            }
        });
		
		show_footprint_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				/*
				if(show_footprint) {
					show_footprint = false;
					return;
				}
				
				show_m_region_evolution_bt.setEnabled(false);
				show_m_region_evolution_slider.setEnabled(false);
				save_to_picture_bt.setEnabled(false);
				
			    show_granularity = Integer.parseInt(step.getText());
			    show_footprint = true;
				
				geom_to_show_id = 0;

			    play_task = new TimerTask() 
			    {
			        public void run() 
			        {
			        	jp.repaint();
			        	
			    		if(geom_to_show_id < max) 
			    			geom_to_show_id += show_granularity;	    			
			    		else 
			    		{
			    			timer.cancel();
			    			
							show_m_region_evolution_slider.setValue(show_m_region_evolution_slider.getMaximum());
							show_m_region_evolution_bt.setEnabled(true);
							show_m_region_evolution_slider.setEnabled(true);
							save_to_picture_bt.setEnabled(true);
			    		}
			        }
			    };
			    
				timer = new Timer();
				timer.scheduleAtFixedRate(play_task, 0, Long.parseLong(rate.getText()));
				*/
			}
		});
        
    	zoom_out_bt.addActionListener(new ActionListener() 
    	{
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs c = (DrawGraphs) jp;

	           	if (zoomLevel > minZoomLevel) 
	           	{
	           		zoomLevel -= zoomMultiplicationFactor;
					c.scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
					c.translate(false);
	            }

	            jp.repaint();
			}
		});
    	
    	mr_r_i_bt.addActionListener(new ActionListener() 
    	{
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs c = (DrawGraphs) jp;

				String mrA = "POLYGON((-5 1, -3 2, -2 1, -1 3, -2 5, -2 3, -4 3, -5 1));POLYGON((19 1, 21 1.5, 22.5 1, 23 3.5, 21 6, 19 2.5, 17 2.5, 19 1));1000;2000";
				//String r = "POLYGON((6 -1, 10 -1, 9 1, 6 -1))";
				String r = "POLYGON((6 -6, 10 -6, 9 -4, 6 -6))";
				double tg = Double.parseDouble(t_gran_tx.getText());

				String mbool = data.get_main().intersect(mrA, r, tg);
				System.out.println("i: " + mbool);

	            jp.repaint();
			}
		});
    	
    	load_mr_bt.addActionListener(new ActionListener() 
    	{
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs c = (DrawGraphs) jp;
				

				//...


	            jp.repaint();
			}
		});
    	
    	geom_ops_bt.addActionListener(new ActionListener() 
    	{
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs c = (DrawGraphs) jp;

				double t = Double.parseDouble(at_tx.getText());
				
				mo_union = data.get_main().geometric_operations(mra_data_tx.getText(), mrb_data_tx.getText(), GeometricOperations.UNION.get_value(), t);
				mo_intersection = data.get_main().geometric_operations(mra_data_tx.getText(), mrb_data_tx.getText(), GeometricOperations.INTERSECTION.get_value(), t);
				mo_difference = data.get_main().geometric_operations(mra_data_tx.getText(), mrb_data_tx.getText(), GeometricOperations.DIFFERENCE.get_value(), t);

				System.out.println("At: " + t);
				System.out.println("union: " + mo_union);
				System.out.println("intersection: " + mo_intersection);
				System.out.println("difference: " + mo_difference);				
				
	            jp.repaint();
			}
		});
    	
    	real_ops_bt.addActionListener(new ActionListener() 
    	{
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs c = (DrawGraphs) jp;

				double t = Double.parseDouble(at_tx.getText());
				
				double d = data.get_main().real_operations(mra_data_tx.getText(), mrb_data_tx.getText(), RealOperations.DISTANCE.get_value(), t);
				double h = data.get_main().real_operations(mra_data_tx.getText(), mrb_data_tx.getText(), RealOperations.HAUSDORFF.get_value(), t);
		
				System.out.println("At: " + t);
				System.out.println("distance: " + d);
				System.out.println("hausdorff: " + h);

	            jp.repaint();
			}
		});
    	
    	boolean_ops_bt.addActionListener(new ActionListener() 
    	{
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs c = (DrawGraphs) jp;

				double t = Double.parseDouble(at_tx.getText());
				
				boolean disjoint = data.get_main().boolean_operations(mra_data_tx.getText(), mrb_data_tx.getText(), BooleanOperations.DISJOINT.get_value(), t);
				boolean touches = data.get_main().boolean_operations(mra_data_tx.getText(), mrb_data_tx.getText(), BooleanOperations.TOUCHES.get_value(), t);
				boolean intersects = data.get_main().boolean_operations(mra_data_tx.getText(), mrb_data_tx.getText(), BooleanOperations.INTERSECTS.get_value(), t);
				boolean crosses = data.get_main().boolean_operations(mra_data_tx.getText(), mrb_data_tx.getText(), BooleanOperations.CROSSES.get_value(), t);
				boolean within = data.get_main().boolean_operations(mra_data_tx.getText(), mrb_data_tx.getText(), BooleanOperations.WITHIN.get_value(), t);
				boolean contains = data.get_main().boolean_operations(mra_data_tx.getText(), mrb_data_tx.getText(), BooleanOperations.CONTAINS.get_value(), t);
				boolean overlaps = data.get_main().boolean_operations(mra_data_tx.getText(), mrb_data_tx.getText(), BooleanOperations.OVERLAPS.get_value(), t);
				boolean equals = data.get_main().boolean_operations(mra_data_tx.getText(), mrb_data_tx.getText(), BooleanOperations.EQUALS.get_value(), t);
				boolean covers = data.get_main().boolean_operations(mra_data_tx.getText(), mrb_data_tx.getText(), BooleanOperations.COVERS.get_value(), t);
				boolean coveredBy = data.get_main().boolean_operations(mra_data_tx.getText(), mrb_data_tx.getText(), BooleanOperations.COVEREDBY.get_value(), t);

				System.out.println("disjoint: " + disjoint);
				System.out.println("touches: " + touches);
				System.out.println("intersects: " + intersects);
				System.out.println("crosses: " + crosses);
				System.out.println("within: " + within);
				System.out.println("contains: " + contains);
				System.out.println("overlaps: " + overlaps);
				System.out.println("equals: " + equals);
				System.out.println("covers: " + covers);
				System.out.println("coveredBy: " + coveredBy);
				
				//...

	            jp.repaint();
			}
		});
    	  	
    	mreg_r_bt.addActionListener(new ActionListener() 
    	{
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs c = (DrawGraphs) jp;

				String p_wkt = "POLYGON((-5 1, -3 2, -2 1, -1 3, -2 5, -2 3, -4 3, -5 1))";
				String q_wkt = "POLYGON((19 1, 21 1.5, 22.5 1, 23 3.5, 21 6, 19 2.5, 17 2.5, 19 1))";
				static_region_wkt = "POLYGON((6 -1, 10 -1, 9 1, 6 -1))";
				
				Geometry p = null;
				Geometry q = null;
				Geometry r = null;
				
				try {
					p = reader.read(p_wkt);
					q = reader.read(q_wkt);;
					r = reader.read(static_region_wkt);;
					
					p.normalize();
					q.normalize();
					r.normalize();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				};

				Geometry p_env = p.getEnvelope();
				Geometry q_env = q.getEnvelope();
				Geometry r_env = r.getEnvelope();
				
				Point p_env_c = p_env.getCentroid();
				Point q_env_c = q_env.getCentroid();
				Point r_env_c = r_env.getCentroid();
				
				double dx = q_env_c.getX() - p_env_c.getX();
				double dy = q_env_c.getY() - p_env_c.getY();			
				
				int n = 100;
				
				double d1 = Math.sqrt((p_env_c.getX() - p_env.getCoordinates()[0].x) * (p_env_c.getX() - p_env.getCoordinates()[0].x) + (p_env_c.getY() - p_env.getCoordinates()[0].y) * (p_env_c.getY() - p_env.getCoordinates()[0].y));
				double d2 = Math.sqrt((q_env_c.getX() - q_env.getCoordinates()[0].x) * (q_env_c.getX() - q_env.getCoordinates()[0].x) + (q_env_c.getY() - q_env.getCoordinates()[0].y) * (q_env_c.getY() - q_env.getCoordinates()[0].y));
				double d3 = Math.sqrt((r_env_c.getX() - r_env.getCoordinates()[0].x) * (r_env_c.getX() - r_env.getCoordinates()[0].x) + (r_env_c.getY() - r_env.getCoordinates()[0].y) * (r_env_c.getY() - r_env.getCoordinates()[0].y));
				
				double f = 0.5;
				double d = Math.max(d1, d2);
				
				double add = d * f;
				d += add + d3;
				//add /= 2;
				
				String[] cmd = new String[9];
				
				cmd[0] = "python";
				cmd[1] = python_script_path;			
				cmd[2] = Double.toString(p_env_c.getX());
				cmd[3] = Double.toString(p_env_c.getY());
				cmd[4] = Double.toString(r_env_c.getX());
				cmd[5] = Double.toString(r_env_c.getY());
				cmd[6] = Double.toString(dx);
				cmd[7] = Double.toString(dy);
				cmd[8] = Double.toString(d);
				
				for(int k = 0; k < cmd.length; k++)
					System.out.print(cmd[k] + " ");
				
				System.out.println("");
				
				
				Runtime rt = Runtime.getRuntime();
				Process pr = null;
				
				try
				{
					pr = rt.exec(cmd);
				}
				catch (IOException e1) 
				{
					e1.printStackTrace();
				}
				
				BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
				String line = "";
				
				//String[] arr = new String[n_samples];
				String res = "-1";
				int i = 0;
				
				try
				{
					// Build array of geometries in wkt representation.
					
					while((line = bfr.readLine()) != null)
					{
						//System.out.println(line);
						res = line;
						i++;
					}
				}
				catch (IOException e1)
				{
					e1.printStackTrace();
				}
				
				System.out.println(res);
				
				/*
				double llx, lly;
				double ulx, uly;
				double rrx, rry;
				double urx, ury;
				*/
				
				boolean to_origion = false;
				
				Geometry g = p_env;
				
				if(d1 < d2)
				{
					g = q_env;
					to_origion = true;
				}
				
				//g.getEnvelope().m
				//Envelope e ) 
				
				System.out.println(g);

				Coordinate[] coords = g.getCoordinates().clone();
				
				for(int k = 0; k < coords.length; k++)
				{
					if(to_origion)
					{
						coords[k].x -= dx;
						coords[k].y -= dy;
					}
					
					if(k < 2)
						coords[k].x -= add;
					else
						coords[k].x += add;
					
					
					if(k == 0 || k == 3)
						coords[k].y -= add;
					else
						coords[k].y += add;
				}
							
				aabb = new String[100];
				
				double t = 0;
				double x, y;
				
				for(int k = 0; k < n; k++)
				{
					t = (double)k / (n - 1);
					x = t * dx;
					y = t * dy;			
					
					String l = "POLYGON((" + (coords[0].x + x) + " " + (coords[0].y + y) + ", "
										   + (coords[1].x + x) + " " + (coords[1].y + y) + ", "
										   + (coords[2].x + x) + " " + (coords[2].y + y) + ", "
										   + (coords[3].x + x) + " " + (coords[3].y + y) + ", "
										   + (coords[0].x + x) + " " + (coords[0].y + y) + "))";
					aabb[k] = l;
					
					//System.out.println(aabb[k]);
					//System.out.println(t);
				}
				
				show_m_region_evolution_slider.setMinimum(0);
				show_m_region_evolution_slider.setMaximum(n);

				double rot = 2 * Math.PI;
				rot = 0;
				geoms = data.get_main().during_period_poly(1000, 2000, p_wkt, q_wkt, 1000, 2000, 100, 2, false, data.get_threshold(), rot);
				
				
				/*
	           	if (zoomLevel > minZoomLevel) 
	           	{
	           		zoomLevel -= zoomMultiplicationFactor;
					c.scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
					c.translate(false);
	            }
	            */

	            jp.repaint();
			}
		});
    	
    	
    	zoom_in_bt.addActionListener(new ActionListener() 
    	{
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs c = (DrawGraphs) jp;

            	if (zoomLevel < maxZoomLevel) 
            	{
            		zoomLevel += zoomMultiplicationFactor; 
	            	c.scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
	            	c.translate(true);
            	}

	            jp.repaint();

			}
		});
    	
    	exec_op_bt.addActionListener(new ActionListener() 
    	{
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs c = (DrawGraphs) jp;
				
				//String mr = "POLYGON((2 2, 3.5 6, 5 1, 2 2));POLYGON((8 11, 11 8, 9 6.5, 8 11));1000;2000#POLYGON((8 11, 11 8, 9 6.5, 8 11));POLYGON((16 7, 14.5 3, 12.5 8, 16 7));2000;3000";
				//String range = "1;1000;2000;0#1;2000;3000;0";
				
				String mr =mra_data_tx.getText();
				String range = range_data_tx.getText();
				String mp = mp_data_tx.getText();
				
				double i = Double.parseDouble(i_b_tx.getText());
				double f = Double.parseDouble(i_e_tx.getText());
				
				double dt = Double.parseDouble(t_gran_tx.getText());
				
				//footprint = data.get_main().footprint(mr, range, dt);
				
				footprint = data.get_main().geometric_period(mr, range, 1, dt);
				
				envelope = data.get_main().geometric_period(mr, range, 2, dt);
				
				intersection = data.get_main().intersect(mr, mp, range, dt);
				
				//System.out.println("<");
				//System.out.println(footprint);
				//System.out.println(">");
				
	            jp.repaint();

			}
		});
    	
    	/*
    	mr_p_i_bt.addActionListener(new ActionListener() 
    	{
			public void actionPerformed(ActionEvent arg0) 
			{
				mr_p_i = null;

				String ms_data = mr_data_tx.getText();
				String mp_data = mp_data_tx.getText();
				
				boolean pass_through_control_points = pass_through_control_points_cb.isSelected();
				boolean show_intersection_information = print_intersection_information_cb.isSelected();
				boolean show_solver_exec_time = print_solver_execution_time_cb.isSelected();
				
				int num_samples = Integer.parseInt(n_samples_tx.getText());
				String interval = i_b_tx.getText() + "," + i_e_tx.getText();

				String prec = s_precision_tx.getText();
				String n_dec_dig = n_dec_dig_tx.getText();
				
				int op_id = op_cb.getSelectedIndex();
				
				String op = String.valueOf(op_id + 1);

				if(op_id == 0)
					operation = "Intersect(MR, MP, I)";
				else if(op_id == 1)
					operation = "Touch(MR, MP, I)";
				else if(op_id == 2)
					operation = "Equal(MR, MP, I)";
				else if(op_id == 3)
					operation = "Disjoint(MR, MP, I)";
				else if(op_id == 4)
					operation = "Contains(MR, MP, I)";
				else if(op_id == 5)
					operation = "Within(MP, MR, I)";
				else {
					JOptionPane.showMessageDialog(null, "Operation unknown or mot implemented/applicable.", "Operation", JOptionPane.INFORMATION_MESSAGE);
					jp.repaint();
					return;
				}
				
				String[] cmd = new String[12];
				
				cmd[0] = "python";
				cmd[1] = "D:\\java\\mrmp.py";
				cmd[2] = ms_data;
				cmd[3] = mp_data;
				
				if(pass_through_control_points)
					cmd[4] = "1";
				else
					cmd[4] = "0";
				
				if(show_intersection_information)
					cmd[5] = "1";
				else
					cmd[5] = "0";
				
				if (show_solver_exec_time)
					cmd[6] = "1";
				else
					cmd[6] = "0";
				
				cmd[7] = String.valueOf(num_samples);
				cmd[8] = interval;
				cmd[9] = prec;
				cmd[10] = n_dec_dig;
				cmd[11] = op;
				
				Runtime rt = Runtime.getRuntime();
				Process pr = null;
				
				try
				{
					pr = rt.exec(cmd);
				}
				catch (IOException e1) 
				{
					e1.printStackTrace();
				}
				
				BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
				String line = "";
				
				String[] arr = new String[1000];
				int i = 0;
				
				try
				{
					while((line = bfr.readLine()) != null)
					{
						arr[i] = line;
						i++;
					}
				}
				catch (IOException e1)
				{
					e1.printStackTrace();
				}
				
				if(i == 1)
				{
					JOptionPane.showMessageDialog(null, arr[0], "Intersection", JOptionPane.INFORMATION_MESSAGE);
					jp.repaint();
					return;
				}
				
				solver_execution_time = null;
				mr_p_i_len = i;
				
				int m = 1;
				
				if (show_solver_exec_time)
				    m = 2;
				
				mr_p_i = new String[mr_p_i_len - m];
				
				for(int k = 0; k < mr_p_i_len - m; k++)
					mr_p_i[k] = arr[k];
				
				if (show_solver_exec_time)
					solver_execution_time = arr[mr_p_i_len - 1];
				
				intersection_info = arr[mr_p_i_len - m];
				
				max = mr_p_i.length / 3;
				
				n_samples.setText("N� Samples: " + String.valueOf(max));
				
				show_m_region_evolution_slider.setMinimum(0);
				show_m_region_evolution_slider.setMaximum(max);
				
				Nr = 3;
				
    			((DrawGraphs) jp).reset();
    			((DrawGraphs) jp).adjust_screen_position();		
	            jp.repaint();
			}
		});
		*/
    	
    	/*
    	load_test_bt.addActionListener(new ActionListener() 
    	{
			public void actionPerformed(ActionEvent arg0) 
			{
				mr_p_i = null;

				int idx = tests_cb.getSelectedIndex();
								
				boolean pass_through_control_points = pass_through_control_points_cb.isSelected();
				boolean show_intersection_information = print_intersection_information_cb.isSelected();
				boolean show_solver_exec_time = print_solver_execution_time_cb.isSelected();
				
				int num_samples = Integer.parseInt(n_samples_tx.getText());
				//String interval = i_b_tx.getText() + "," + i_e_tx.getText();

				String prec = s_precision_tx.getText();
				String n_dec_dig = n_dec_dig_tx.getText();
				
				int op_id = op_cb.getSelectedIndex();
				
				String op = String.valueOf(op_id + 1);

				if(op_id == 0)
					operation = "Intersect(MS, MP, I)";
				else if(op_id == 1)
					operation = "Touch(MS, MP, I)";
				else if(op_id == 2)
					operation = "Equal(MS, MP, I)";
				else if(op_id == 3)
					operation = "Disjoint(MS, MP, I)";
				else if(op_id == 4)
					operation = "Contains(MS, MP, I)";
				else if(op_id == 5)
					operation = "Within(MP, MS, I)";
				else {
					JOptionPane.showMessageDialog(null, "Operation mot implemented.", "Operation", JOptionPane.INFORMATION_MESSAGE);
					jp.repaint();
					return;
				}
				
				String[] cmd = new String[12];
				
				cmd[0] = "python";
				cmd[1] = "D:\\java\\tests.py";	
				cmd[2] = mss_data[idx];
				cmd[3] = mps_data[idx];
				
				if(pass_through_control_points)
					cmd[4] = "1";
				else
					cmd[4] = "0";
				
				if(show_intersection_information)
					cmd[5] = "1";
				else
					cmd[5] = "0";
				
				if (show_solver_exec_time)
					cmd[6] = "1";
				else
					cmd[6] = "0";
				
				cmd[7] = String.valueOf(num_samples);
				cmd[8] = intervals[idx];
				//cmd[8] = interval;
				cmd[9] = prec;
				cmd[10] = n_dec_dig;
				cmd[11] = op;
				
				Runtime rt = Runtime.getRuntime();
				Process pr = null;
				
				try
				{
					pr = rt.exec(cmd);
				}
				catch (IOException e1) 
				{
					e1.printStackTrace();
				}
				
				BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
				String line = "";
				
				String[] arr = new String[1000];
				int i = 0;
				
				try
				{
					while((line = bfr.readLine()) != null)
					{
						arr[i] = line;
						i++;
					}
				}
				catch (IOException e1)
				{
					e1.printStackTrace();
				}
				
				if(i == 1)
				{
					JOptionPane.showMessageDialog(null, arr[0], "Intersection", JOptionPane.INFORMATION_MESSAGE);
					jp.repaint();
					return;
				}
				
				solver_execution_time = null;
				mr_p_i_len = i;
				
				int m = 1;
				
				if (show_solver_exec_time)
				    m = 2;
				
				mr_p_i = new String[mr_p_i_len - m];
				
				for(int k = 0; k < mr_p_i_len - m; k++)
					mr_p_i[k] = arr[k];
				
				if (show_solver_exec_time)
					solver_execution_time = arr[mr_p_i_len - 1];
				
				intersection_info = arr[mr_p_i_len - m];		
				max = mr_p_i.length / 3;
								
				n_samples.setText("N� Samples: " + String.valueOf(max));
				
				show_m_region_evolution_slider.setMinimum(0);
				show_m_region_evolution_slider.setMaximum(max);
				
				Nr = 3;
				
    			((DrawGraphs) jp).reset();
    			((DrawGraphs) jp).adjust_screen_position();		
	            jp.repaint();
			}
		});
    	*/
    	
 		save_to_picture_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				String filename = save_picture_to_tx.getText() + ".png";
				
				try
				{
					BufferedImage bi = ScreenImage.createImage(viz_p);
    				ScreenImage.writeImage(bi, filename);
    			} 
				catch (IOException ex) {
    				ex.printStackTrace();
    			}
							
				JOptionPane.showMessageDialog(null, "Saved to " + filename + ".", "Save", JOptionPane.INFORMATION_MESSAGE);	
			}
		});
		
		show_m_region_evolution_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				 if(mr_p_i == null)
					 return;
				
				show_m_region_evolution_bt.setEnabled(false);
				show_m_region_evolution_slider.setEnabled(false);
				save_to_picture_bt.setEnabled(false);		
				geom_to_show_id = 0;
				
			    play_task = new TimerTask() 
			    {
			        public void run() 
			        {
			        	jp.repaint();
			        	
			    		if(geom_to_show_id < max - 1) 
			    			geom_to_show_id++;	    			
			    		else {
			    			timer.cancel();
			    			
							show_m_region_evolution_slider.setValue(show_m_region_evolution_slider.getMaximum());
							show_m_region_evolution_bt.setEnabled(true);
							show_m_region_evolution_slider.setEnabled(true);
							save_to_picture_bt.setEnabled(true);
			    		}
			    		
			        	//System.out.println(geom_to_show_id);
			        	//System.out.println(max);
			        }
			    };
			    
				timer = new Timer();
				timer.scheduleAtFixedRate(play_task, 0, Long.parseLong(rate.getText()));
			}
		});
		
		show_m_region_evolution_slider.addChangeListener(new ChangeListener() 
		{
		    public void stateChanged(ChangeEvent e) 
		    {
		        JSlider s = (JSlider) e.getSource();
		        int i = (int) s.getValue();
		        
		        if(geoms != null)
		        {
			    	if(i < geoms.length)
			    		geom_to_show_id = i;
			    	else if(i == geoms.length)
			    		geom_to_show_id = i - 1;
			    	
		        	/*
			        int n = mr_p_i.length / 3;
			        
			    	if(i < n)
			    		geom_to_show_id = i;
			    	else if(i == n)
			    		geom_to_show_id = i - 1;
			    	*/
		        }
		        
		    	jp.repaint();
		    }
		});
		
		reset_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
		    {
				mr_p_i = null;
				footprint = null;
				envelope = null;
				intersection = null;
				aabb = null;
				geoms = null;
				mo_union = null;
				mo_intersection = null;
				mo_difference = null;
				
		    	jp.repaint();
		    }
		});
	}
}