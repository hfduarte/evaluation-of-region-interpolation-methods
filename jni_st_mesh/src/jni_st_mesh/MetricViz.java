package jni_st_mesh;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;

import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.geometry.jts.LiteShape;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JSlider;

import javax.swing.event.ChangeListener;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import java.awt.Font;
import javax.swing.border.BevelBorder;

public class MetricViz extends JFrame 
{
	private static final long serialVersionUID = 1L;

	private JPanel contentPane;
	
	private JTextField png_sequence_filename_tx;

	private JButton save_to_picture;
	
	private JButton play;
	
	private JSlider interpolation_slider;
	
	private JPanel viz_p;
	
	private Data d;
	
	private JFrame f;
	
	private JPanel jp;
	
	private JLabel status;
	
	private JPanel tools_p;
	
	private JButton minus;
	
	private JButton plus;
	
	private JLabel zoom_level;
	
	public static final double DEFAULT_ZOOM_MULTIPLICATION_FACTOR = 0.25;
	public static final double DEFAULT_MIN_ZOOM_LEVEL = 0.25;
	public static final double DEFAULT_MAX_ZOOM_LEVEL = 100;
	
    private double zoomLevel = 1.0;
    private double minZoomLevel = DEFAULT_MIN_ZOOM_LEVEL;
    private double maxZoomLevel = DEFAULT_MAX_ZOOM_LEVEL;
    private double zoomMultiplicationFactor = DEFAULT_ZOOM_MULTIPLICATION_FACTOR;
    
    private double threshold;
    
    private JTextField rate_tx;
    
    private int geom_to_show_id = 0;
    private int bbox_to_show_id = 0;

    private GeometryFactory geometryFactory;
    private WKTReader reader;
    
    private String[] geometries;
    private String[] bboxes;
    
    private String wkt;
    private int max;
    
    private TimerTask play_task;
    
	private int w_center;
	private int h_center;
    
	private double cx = 0;
	private double cy = 0;

    private Timer timer;
    private JCheckBox fill;

    private int show_granularity;
    private boolean show_footprint;
    
    private JSlider zoom_slider;
    
    private JButton wkt_save_bt;
    
    private JLabel n_samples;
    
    private boolean show_bbox;
    
    private JTextField wkt_save_in_file;
    private JLabel method_lbl;
    private JLabel method_info_lbl;

    private Geometry geometry;
    private Geometry ref_geometry;
    private String metric_name;
    private boolean multi_ref_objects;
    private String[] ref_objects_wkt;
    private JTextField ref_R_tx;
    private JTextField ref_G_tx;
    private JTextField ref_B_tx;
    private JTextField alg_R_tx;
    private JTextField alg_G_tx;
    private JTextField alg_B_tx;
    private JTextField alg_A_tx;
    private JTextField ref_A_tx;
    
	public MetricViz(Data d) 
	{
		this.d = d;
		f = this;
		
		geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
		
		reader = new WKTReader(geometryFactory);
		
		try 
		{
			if(d.get_ref_obj_wkt() != null)
				ref_geometry = reader.read(d.get_ref_obj_wkt());
			else
				ref_geometry = null;
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
		
		geom_to_show_id = 0;
		
		bbox_to_show_id = 0;

	    geometries = (String[]) d.get_data();

	    multi_ref_objects = false;
	    ref_objects_wkt = d.get_ref_objects_wkt();
	    
	    if(ref_objects_wkt != null)
	    	multi_ref_objects = true;
	    	    
	    wkt = null;
	    
	    max = (geometries.length / 2) - 1;
	    
	    show_bbox = false;
	    
	    threshold = d.get_threshold();
	    
		draw_UI();
			
		w_center = (int) (viz_p.getWidth() / 2);
		h_center = (int) (viz_p.getHeight() / 2);
		
		metric_name = "Area";
		
		if(d.get_type() == 2)
			metric_name = "Perimeter";
		else if(d.get_type() == 3)
			metric_name = "Hausdorff Distance";
		else if(d.get_type() == 4)
			metric_name = "Jaccard Index";
		else if(d.get_type() == 5)
			metric_name = "Jaccard Distance";
				
		// Show the name of the method being used.
		
		switch(d.get_interpolation_method())
		{
			case COMPATIBLE:
				method_info_lbl.setText("Alexa (Compatible)");
				break;
			case EQUILATERAL:
				method_info_lbl.setText("Alexa (Equilateral)");
				break;
			case PY_SPATIOTEMPORAL_GEOM:
				method_info_lbl.setText("PySpatiotemporalGeom");
				break;
			case LIBRIP:
				method_info_lbl.setText("Librip");
				break;
		default:
			break;
		}
	    
		add_listeners();
		
		draw_geometry();
	}
	
	public void paint(Graphics g) 
	{
	    super.paint(g);
	    jp.repaint();
	}

	public void draw_geometry()
	{
		viz_p.setLayout(new BorderLayout());
		jp = new DrawGraphs();
		viz_p.add(jp, BorderLayout.CENTER);
	}
	
    private class DrawGraphs extends JPanel
    {
    	private int dx = 0;
    	private int dy = 0;
    	
    	private double sx = 1;
    	private double sy = 1; 	
    	   	
    	private int xx = 0;
    	private int yy = 0;
    	
		private static final long serialVersionUID = 3203545490664689791L;
		
		public DrawGraphs() 
		{
			setBackground(Color.white);
			
			setAlignmentY(CENTER_ALIGNMENT);
			setAlignmentX(CENTER_ALIGNMENT);
			
			adjust_screen_position();
			
	        addMouseListener(new MouseAdapter() 
	        {
	            public void mousePressed(MouseEvent e) 
	            {
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });

	        addMouseMotionListener(new MouseAdapter() 
	        {
	            public void mouseDragged(MouseEvent e) 
	            {        	
	            	int ddx = (e.getX() - xx);
	            	int ddy = (e.getY() - yy);
	            	
	            	translate(ddx, ddy);
	            	repaint();
	            	
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });
	        
	        addMouseWheelListener(new MouseAdapter()
	        {
	            public void mouseWheelMoved(MouseWheelEvent e) 
	            {
    	        
		            int notches = e.getWheelRotation();
					DrawGraphs c = (DrawGraphs) jp;
					
		            if (notches > 0) 
		            {
		            	if (zoomLevel < maxZoomLevel) 
		            	{
		            		zoomLevel += zoomMultiplicationFactor; 
			            	c.scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
			            	c.translate(true);
		            	}
		            } else {
			           	if (zoomLevel > minZoomLevel) 
			           	{
			           		zoomLevel -= zoomMultiplicationFactor;
							c.scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
							c.translate(false);
			            }
		            }

		            jp.repaint();
	            }
	        });       
		};
		
		@Override
        protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);

	        Graphics2D gr = (Graphics2D) g;
	        gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	        
		    gr.setStroke(new BasicStroke(1.0f));	        

	        AffineTransform at = new AffineTransform();

	        at.translate(dx, dy);
	        at.scale(sx, -sy);
	        
	        zoom_level.setText("Zoom Level: " + sx);

			try 
			{
				Float R = Float.parseFloat(ref_R_tx.getText());
				Float G = Float.parseFloat(ref_G_tx.getText());
				Float B = Float.parseFloat(ref_B_tx.getText());
		        float A = Float.parseFloat(ref_A_tx.getText());
				
				if(multi_ref_objects)
				{
				    geometry = reader.read(ref_objects_wkt[geom_to_show_id]);
			    	
			    	gr.setPaint(Color.black);
			    	gr.draw(new LiteShape(geometry, at, false));
			    		
			    	gr.setPaint(new Color(R / 255, G / 255, B / 255, A));
							
		   		    for (int j = 0; j < geometry.getNumGeometries(); j += 1)
						gr.fill(new LiteShape(geometry.getGeometryN(j), at, false));
				}
				else
				{
					if(ref_geometry != null)
					{
						gr.setPaint(Color.black);
						gr.draw(new LiteShape(ref_geometry, at, false));
						
						gr.setPaint(new Color(R / 255, G / 255, B / 255, A));
						
		   		        for (int i = 0; i < ref_geometry.getNumGeometries(); i += 1) 
				   			gr.fill(new LiteShape(ref_geometry.getGeometryN(i), at, false));
					}					
				}
		    
				// Aligned geometry.
			    	
			    int n = geometries.length;
			    n /= 2;
			    				    	
			    geometry = reader.read(geometries[geom_to_show_id]);
			    	
		    	gr.setPaint(Color.black);
		    	gr.draw(new LiteShape(geometry, at, false));
		    		
				if(fill.isSelected())
				{
			        R = Float.parseFloat(alg_R_tx.getText());
			        G = Float.parseFloat(alg_G_tx.getText());
			        B = Float.parseFloat(alg_B_tx.getText());
			        A = Float.parseFloat(alg_A_tx.getText());
			        
					gr.setPaint(new Color(R / 255, G / 255, B / 255, A));
						
		   		    for (int j = 0; j < geometry.getNumGeometries(); j += 1)
						gr.fill(new LiteShape(geometry.getGeometryN(j), at, false));
				}
		    		
				// Metric
		    	
				gr.setPaint(new Color(6, 40, 94));
				gr.setFont(new Font("Arial", Font.BOLD, 12));
		    	gr.drawString(metric_name + ": " + String.format("%.02f", Float.parseFloat(geometries[n + geom_to_show_id])), 20, 30);	
			} 
			catch (ParseException e) 
			{
				e.printStackTrace();
			}
        };
		
        public void translate(boolean sign) 
        {
        	double cx_star = cx * zoomMultiplicationFactor;
        	double cy_star = cy * zoomMultiplicationFactor;
        	
			if(sign) 
			{
				dx -= (int) cx_star;
				dy += (int) cy_star;				
			}
			else
			{
				dx += (int) cx_star;
				dy -= (int) cy_star;				
			}
        }
        
        public void translate(int x, int y) 
        {
        	dx += x;
        	dy += y;
        }
        
        public void scale(double x, double y) 
        {
        	sx += x;
        	sy += y;
        }
        
        public void adjust_screen_position()
        {
			try 
			{				
	    		geometry = reader.read(geometries[geom_to_show_id]);
				Point c = geometry.getCentroid();

				cx = c.getX();
				cy = c.getY();
			} 
			catch (ParseException e) {
				e.printStackTrace();
			}

			w_center = (int) (viz_p.getWidth() / 2);
			h_center = (int) (viz_p.getHeight() / 2);
			
			dx = (int) ((-cx + w_center));
			dy = (int) ((cy + h_center));
        }
    }
	
	public void draw_UI() 
	{
		setTitle("Metrics Visualization");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	    setSize(1296, 720);
	    setLocationRelativeTo(null);
	    setExtendedState(f.getExtendedState() | JFrame.MAXIMIZED_BOTH);
	    
		contentPane = new JPanel();
		contentPane.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		viz_p = new JPanel();
		viz_p.setLocation(175, 10);
		
		viz_p.setSize(930, 590);
		
		viz_p.setBackground(Color.WHITE);
		viz_p.setBorder(new LineBorder(Color.black, 1));
		contentPane.add(viz_p);
		
		tools_p = new JPanel();
		tools_p.setBorder(new LineBorder(new Color(0, 0, 0)));
		tools_p.setBounds(10, 616, 1260, 36);
		contentPane.add(tools_p);
		
		minus = new JButton("Zoom Out");
		tools_p.add(minus);
		
		plus = new JButton("Zoom In");
		tools_p.add(plus);
	
		save_to_picture = new JButton("Save to PNG");
		save_to_picture.setToolTipText("Save the interpolation to a sequence of png pictures.");
		tools_p.add(save_to_picture);
		
		png_sequence_filename_tx = new JTextField();
		png_sequence_filename_tx.setToolTipText("Name of the png pictures.");
		png_sequence_filename_tx.setText("d:\\\\interpolation_viz");
		tools_p.add(png_sequence_filename_tx);
		png_sequence_filename_tx.setColumns(16);
		
		rate_tx = new JTextField();
		rate_tx.setToolTipText("Rate of the animation of the interpolation.");
		rate_tx.setText("16");
		tools_p.add(rate_tx);
		rate_tx.setColumns(2);
		
		play = new JButton("Interpolate");
		tools_p.add(play);
		
		interpolation_slider = new JSlider();
		interpolation_slider.setValue(0);
		tools_p.add(interpolation_slider);
		
		interpolation_slider.setMinimum(0);
		interpolation_slider.setMaximum(max);
		
		status = new JLabel("");
		tools_p.add(status);
		
		fill = new JCheckBox("Fill Geometry");
		fill.setForeground(new Color(25, 25, 112));
		fill.setSelected(true);
		fill.setBounds(10, 23, 156, 25);
		contentPane.add(fill);
		
		zoom_level = new JLabel("Zoom Level:");
		zoom_level.setBounds(1115, 10, 155, 14);
		contentPane.add(zoom_level);
		
		zoom_slider = new JSlider();
		zoom_slider.setEnabled(false);
		zoom_slider.setValue(1);
		zoom_slider.setMinimum(1);
		zoom_slider.setBounds(1115, 38, 155, 26);
		contentPane.add(zoom_slider);
		
		n_samples = new JLabel("N\u00BA Samples: ");
		n_samples.setFont(new Font("Tahoma", Font.BOLD, 11));
		n_samples.setBounds(1115, 73, 155, 14);
		contentPane.add(n_samples);
		
		n_samples.setText("N� Samples: " + (geometries.length / 2) + " :: " +  (geom_to_show_id + 1));
	    
	    wkt_save_in_file = new JTextField();
	    wkt_save_in_file.setEnabled(false);
	    wkt_save_in_file.setText("d:\\\\wkt.txt");
	    wkt_save_in_file.setBounds(11, 580, 155, 20);
	    contentPane.add(wkt_save_in_file);
	    wkt_save_in_file.setColumns(1);
	    
	    wkt_save_bt = new JButton("Save WKT to File");
	    wkt_save_bt.setEnabled(false);
	    wkt_save_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    wkt_save_bt.setBounds(10, 551, 156, 25);
	    contentPane.add(wkt_save_bt);
	    
	    method_lbl = new JLabel("Method:");
	    method_lbl.setForeground(new Color(0, 0, 128));
	    method_lbl.setFont(new Font("Tahoma", Font.BOLD, 12));
	    method_lbl.setBounds(1117, 92, 145, 27);
	    contentPane.add(method_lbl);
	    
	    method_info_lbl = new JLabel("");
	    method_info_lbl.setForeground(new Color(47, 79, 79));
	    method_info_lbl.setFont(new Font("Tahoma", Font.BOLD, 11));
	    method_info_lbl.setBounds(1117, 112, 145, 27);
	    contentPane.add(method_info_lbl);
	    
	    ref_R_tx = new JTextField();
	    ref_R_tx.setText("0");
	    ref_R_tx.setBounds(13, 391, 31, 20);
	    contentPane.add(ref_R_tx);
	    ref_R_tx.setColumns(10);
	    
	    ref_G_tx = new JTextField();
	    ref_G_tx.setText("64");
	    ref_G_tx.setBounds(52, 391, 31, 20);
	    contentPane.add(ref_G_tx);
	    ref_G_tx.setColumns(10);
	    
	    ref_B_tx = new JTextField();
	    ref_B_tx.setText("128");
	    ref_B_tx.setBounds(93, 391, 30, 20);
	    contentPane.add(ref_B_tx);
	    ref_B_tx.setColumns(10);
	    
	    alg_R_tx = new JTextField();
	    alg_R_tx.setText("255");
	    alg_R_tx.setBounds(13, 443, 31, 20);
	    contentPane.add(alg_R_tx);
	    alg_R_tx.setColumns(10);
	    
	    alg_G_tx = new JTextField();
	    alg_G_tx.setText("255");
	    alg_G_tx.setBounds(52, 443, 31, 20);
	    contentPane.add(alg_G_tx);
	    alg_G_tx.setColumns(10);
	    
	    alg_B_tx = new JTextField();
	    alg_B_tx.setText("255");
	    alg_B_tx.setBounds(92, 443, 31, 20);
	    contentPane.add(alg_B_tx);
	    alg_B_tx.setColumns(10);
	    
	    alg_A_tx = new JTextField();
	    alg_A_tx.setText("0.5");
	    alg_A_tx.setBounds(131, 443, 31, 20);
	    contentPane.add(alg_A_tx);
	    alg_A_tx.setColumns(10);
	    
	    ref_A_tx = new JTextField();
	    ref_A_tx.setText("1");
	    ref_A_tx.setBounds(131, 391, 31, 20);
	    contentPane.add(ref_A_tx);
	    ref_A_tx.setColumns(10);
	    
	    JLabel lblNewLabel = new JLabel("Ref. Geometry:");
	    lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
	    lblNewLabel.setForeground(new Color(0, 0, 0));
	    lblNewLabel.setBounds(10, 365, 155, 25);
	    contentPane.add(lblNewLabel);
	    
	    JLabel lblNewLabel_1 = new JLabel("Aligned Geometry:");
	    lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
	    lblNewLabel_1.setBounds(10, 417, 155, 25);
	    contentPane.add(lblNewLabel_1);
	}

	public void add_listeners()
	{
        addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(WindowEvent e) 
            {
                setVisible(false);
                dispose();
            }
        });
        
		// Zoom out.
		
    	minus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs c = (DrawGraphs) jp;

	           	if (zoomLevel > minZoomLevel) 
	           	{
	           		zoomLevel -= zoomMultiplicationFactor;
					c.scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
					c.translate(false);
	            }

	            jp.repaint();
			}
		});
    	
    	// Zoom in.
    	
    	plus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs c = (DrawGraphs) jp;

            	if (zoomLevel < maxZoomLevel) 
            	{
            		zoomLevel += zoomMultiplicationFactor; 
	            	c.scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
	            	c.translate(true);
            	}

	            jp.repaint();

			}
		});
        
		save_to_picture.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				interpolation_slider.setEnabled(false);
				play.setEnabled(false);
				
				int v = geom_to_show_id;
				int N = geometries.length / 2;
				
				status.setText("Saving!");
				
				JOptionPane.showMessageDialog(null, "Click to start!", "Save", JOptionPane.INFORMATION_MESSAGE);
				
				try
				{
					show_granularity = 1;
					
					for(int i = 0; i < N; i = i + show_granularity) 
					{
						geom_to_show_id = i;
			    		jp.repaint();
			    		
						BufferedImage bi = ScreenImage.createImage(viz_p);
	    				ScreenImage.writeImage(bi, png_sequence_filename_tx.getText() + "_" + (N + i) + ".png");
					}
    			} 
				catch (IOException ex) {
    				ex.printStackTrace();
    			}
				
				geom_to_show_id = v;
				jp.repaint();
				
				JOptionPane.showMessageDialog(null, "Saved!", "Save", JOptionPane.INFORMATION_MESSAGE);	
				
				interpolation_slider.setEnabled(true);
				play.setEnabled(true);
				status.setText("");
			}
		});
		
		play.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				play.setEnabled(false);
				interpolation_slider.setEnabled(false);
				save_to_picture.setEnabled(false);
				show_footprint = false;
				
				geom_to_show_id = 0;
				
			    play_task = new TimerTask() 
			    {
			    	int counter = 0;
			    	
			        public void run() 
			        {
			        	jp.repaint();
			        	
			        	// To show the first frame.
			        	if(counter > 2)
			        	{
				    		if(geom_to_show_id < max) 
				    			geom_to_show_id++;	    			
				    		else 
				    		{
				    			timer.cancel();
				    			
								interpolation_slider.setValue(interpolation_slider.getMaximum());
								play.setEnabled(true);
								interpolation_slider.setEnabled(true);
								save_to_picture.setEnabled(true);
				    		}			        		
			        	}
			        	
			        	counter++;
			        }
			    };
			    
				timer = new Timer(/*true*/);
				timer.scheduleAtFixedRate(play_task, 0, Long.parseLong(rate_tx.getText()));
			}
		});
		
		interpolation_slider.addChangeListener(new ChangeListener() 
		{
		    public void stateChanged(ChangeEvent e) 
		    {
		        JSlider s = (JSlider) e.getSource();
		        int i = (int) s.getValue();

		        // Geometries.
		        
		    	if(i < geometries.length)
		    		geom_to_show_id = i;
		    	
		    	n_samples.setText("N� Samples: " + (geometries.length / 2) + " :: " +  (geom_to_show_id + 1));
					    	
		    	jp.repaint();
		    }
		});
		
		zoom_slider.addChangeListener(new ChangeListener() 
		{
		    public void stateChanged(ChangeEvent e) 
		    {
		        JSlider s = (JSlider) e.getSource();
		        int desired_zoom_level = (int) s.getValue();
		        DrawGraphs c = (DrawGraphs) jp;
		        double diff = desired_zoom_level - zoomLevel;
		        
		        if(diff > 0 && desired_zoom_level < maxZoomLevel)
		        {
		        	zoomLevel = desired_zoom_level;
            		c.scale(diff, diff);
            		c.translate(true);
		        }
		        else if(diff < 0 && desired_zoom_level > minZoomLevel)
		        {
		        	zoomLevel = desired_zoom_level;
            		c.scale(diff, diff);
            		c.translate(false);
		        }
					
		    	jp.repaint(); 
		    }
		});
		
		wkt_save_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
		    {
				
				JOptionPane.showMessageDialog(null, "Click to start.!", "Save", JOptionPane.INFORMATION_MESSAGE);
				
				String filename = wkt_save_in_file.getText();
				String wkt;

				PrintWriter writer = null;
				
				try {
					writer = new PrintWriter(filename, "UTF-8");
				} catch (FileNotFoundException | UnsupportedEncodingException e) 
				{
					e.printStackTrace();
				}

		    	for(int i = 0; i < geometries.length; i++) 
		    	{
		    		wkt = geometries[i];
		    		writer.println(wkt);
		    	}
				
				writer.close();
		    	
				JOptionPane.showMessageDialog(null, "Saved!", "Save", JOptionPane.INFORMATION_MESSAGE);	
		    }
		});
	}
}