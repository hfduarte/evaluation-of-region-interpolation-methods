package jni_st_mesh;

public enum InterpolationMethod 
{
	COMPATIBLE(1), EQUILATERAL(2), PY_SPATIOTEMPORAL_GEOM(3), LIBRIP(4), NEW(5);
	
	private int value;    

	private InterpolationMethod(int value) {
		this.value = value;
	}

	public int get_value() {
		return value;
	}
}
