package jni_st_mesh;

import java.awt.Color;   
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import javax.swing.JFrame;
import org.jfree.chart.*;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;   
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

public class BarChart extends JFrame
{
	private static final long serialVersionUID = 1328795438858727411L;
	private double[] arr_xy_values;
	private HashMap<String, Double> dic_xy_values;
	private boolean x_axis_leg_angle;
	private String title;
	private String x_leg;
	private String y_leg;
	private ChartPanel chartpanel;
	private String format;
	private Color bar_color;
	private boolean natural_sort;
	
	public BarChart(String title, String x_legend, String y_legend, double[] values, boolean x_axis_leg_angle, String format, Color bar_color)   
	{   
		super("2D Chart");   
		
		this.arr_xy_values = values;
		this.title = title;
		this.x_leg = x_legend;
		this.y_leg = y_legend;
		this.x_axis_leg_angle = x_axis_leg_angle;
		this.format = format;
		this.bar_color = bar_color;
		
	    JFreeChart jfreechart = create_chart(create_dataset_from_arr());   
	    chartpanel = new ChartPanel(jfreechart);   
	    chartpanel.setPreferredSize(new Dimension(800, 600));   
	    setContentPane(chartpanel);   
	    
        addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(WindowEvent e) 
            {
                setVisible(false);
                dispose();
            }
        });
	}
	
	public BarChart(String title, String x_legend, String y_legend, HashMap<String, Double> values, boolean natural_sort, boolean x_axis_leg_angle, String format, Color bar_color)   
	{   
		super("2D Chart");   
		
		this.dic_xy_values = values;
		this.title = title;
		this.x_leg = x_legend;
		this.y_leg = y_legend;
		this.x_axis_leg_angle = x_axis_leg_angle;
		this.format = format;
		this.bar_color = bar_color;
		this.natural_sort = natural_sort;
		
	    JFreeChart jfreechart = create_chart(create_dataset_from_dict());   
	    chartpanel = new ChartPanel(jfreechart);   
	    chartpanel.setPreferredSize(new Dimension(800, 600));   
	    setContentPane(chartpanel);   
	    
        addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(WindowEvent e) 
            {
                setVisible(false);
                dispose();
            }
        });
	}
	
	private CategoryDataset create_dataset_from_dict()   
	{      
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        String seriesKey = title;
        
		List<String> sortedKeys = new ArrayList<>(dic_xy_values.keySet());
		
		if(natural_sort)
			Collections.sort(sortedKeys, new Comparator<String>()
		    {
		      public int compare(String s1, String s2)
		      {
		        return Integer.valueOf(s1).compareTo(Integer.valueOf(s2));
		      }
		    });
		else
			Collections.sort(sortedKeys);
		
		for(String k : sortedKeys)
			dataset.addValue(dic_xy_values.get(k), seriesKey, k);
		
		return dataset;
	}
	
	private CategoryDataset create_dataset_from_arr()   
	{      
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        String seriesKey = title;
        
		for(int i = 0; i < arr_xy_values.length; i++)
			dataset.addValue(arr_xy_values[i], seriesKey, Integer.toString(i + 1));
		
		return dataset;
	}
	
    private JFreeChart create_chart(CategoryDataset dataset) 
    {
        JFreeChart chart = ChartFactory.createBarChart(
               title,
               x_leg,
               y_leg,
               dataset,                     // data
               PlotOrientation.VERTICAL,  	// orientation
               false,                       // include legend
               true,
               false
        );

        chart.setBackgroundPaint(Color.white);   
	           
        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        plot.setRangePannable(true);
        
        plot.setBackgroundPaint(Color.white);   
        plot.setDomainGridlinePaint(Color.black);   
        plot.setRangeGridlinePaint(Color.gray);
	    
        plot.getDomainAxis().setLowerMargin(0.2);
        plot.getDomainAxis().setUpperMargin(0.15);
        
        //"##0.0"
        
        DecimalFormat labelFormat = new DecimalFormat(format);
        
        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setItemLabelAnchorOffset(2.5);
        renderer.setBaseItemLabelsVisible(true);
        renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator("{2}", labelFormat));
        renderer.setBaseItemLabelFont(new Font("arial", Font.BOLD, 12));
        
        // set the color (r,g,b) or (r,g,b,a)
        renderer.setSeriesPaint(0, bar_color);
        
        final CategoryPlot bar_color_setup = chart.getCategoryPlot();
        ((BarRenderer) bar_color_setup.getRenderer()).setBarPainter(new StandardBarPainter());
        
        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        
        if(x_axis_leg_angle) 
        {
            CategoryAxis domainAxis = plot.getDomainAxis();
            domainAxis.setCategoryLabelPositions(CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 4.0));
        }
                
        return chart;
    }
}