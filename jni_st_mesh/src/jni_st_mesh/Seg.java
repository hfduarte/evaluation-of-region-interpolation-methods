package jni_st_mesh;

public class Seg 
{
	public double i_x;
	public double i_y;
	
	public double f_x;
	public double f_y;

	public Seg(double i_x, double i_y, double f_x, double f_y)
	{
		this.i_x = i_x;
		this.i_y = i_y;
		this.f_x = f_x;
		this.f_y = f_y;
	}

	public double dot(double ux, double uy, double vx, double vy)
	{
		return (ux * vx + uy * vy);
	}
	
	public double norm(double vx, double vy)
	{
		return Math.sqrt(dot(vx, vy, vx, vy));
	}
	
	public double d(double ux, double uy, double vx, double vy)
	{
		double wx = ux - vx;
		double wy = uy - vy;

		return norm(wx, wy);
	}
	
	public double distance(double x, double y)
	{	
		double wx, wy, vx, vy;
		
		vx = f_x - i_x;
		vy = f_y - i_y;
		
		wx = x - i_x;
		wy = y - i_y;
		
	    double c1 = dot(wx, wy, vx, vy);
	    
	    if ( c1 <= 0 )
	    	return d(x, y, i_x, i_y);

	    double c2 = dot(vx, vy, vx, vy);
	    
	    if ( c2 <= c1 )
	    	return d(x, y, f_x, f_y);

	    double b = c1 / c2;
	    
	    double px = i_x + b * vx;
	    double py = i_y + b * vy;
	    
	    return d(x, y, px, py);
	}
	
	//.
	
	public double get_ix()
	{
		return i_x;
	}
	
	public double get_iy()
	{
		return i_y;
	}
	
	public double get_fx()
	{
		return f_x;
	}
	
	public double get_fy()
	{
		return f_y;
	}
}
