package jni_st_mesh;

public enum GeometricOperations 
{
	UNION(1), INTERSECTION(2), DIFFERENCE(3);
	
	private int value;    

	private GeometricOperations(int value) {
		this.value = value;
	}

	public int get_value() {
		return value;
	}
}
