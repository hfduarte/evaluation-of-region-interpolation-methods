package jni_st_mesh;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;

import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.geometry.jts.LiteShape;
import org.jfree.ui.RefineryUtilities;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.locationtech.jts.util.GeometricShapeFactory;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JSlider;

import javax.swing.event.ChangeListener;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Font;
import javax.swing.border.BevelBorder;
import java.awt.SystemColor;
import javax.swing.JTextArea;

import java.awt.Toolkit;
import javax.swing.JSeparator;

public class Vce extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField png_sequence_filename_tx;
	private JButton save_interpolation_to_picture;
	private JButton play;
	private JSlider interpolation_slider;
	private JPanel viz_p;
	private Data d;
	private JFrame f;
	private JPanel jp;
	private JLabel status;
	private JPanel tools_p;
	private JButton minus;
	private JButton plus;
	private JButton footprint_bt;
	private JLabel zoom_level;
	private JComboBox<String> metrics;
	
	public static final double DEFAULT_ZOOM_MULTIPLICATION_FACTOR = 0.25;
	public static final double DEFAULT_MIN_ZOOM_LEVEL = 0.25;
	public static final double DEFAULT_MAX_ZOOM_LEVEL = 100;
	
    private double zoomLevel = 1.0;
    private double minZoomLevel = DEFAULT_MIN_ZOOM_LEVEL;
    private double maxZoomLevel = DEFAULT_MAX_ZOOM_LEVEL;
    private double zoomMultiplicationFactor = DEFAULT_ZOOM_MULTIPLICATION_FACTOR;
    
    private double threshold;
    private JTextField rate_tx;
    private int geom_to_show_id = 0;
    private int bbox_to_show_id = 0;
    private GeometryFactory geometryFactory;
    private WKTReader reader; 
    private String[] geometries;
    private String[] bboxes;
    //private String wkt;
    private int max;
    private TimerTask play_task;
	private int w_center;
	private int h_center;
	private double cx = 0;
	private double cy = 0;
    private Timer timer;
    private JTextField granularity_tx;
    private JCheckBox fill;
    private int show_granularity;
    private boolean show_footprint;
    private JSlider zoom_slider;
    private JButton wkt_save_bt;
    private JButton show_metrics_in_chart_bt;
    private JLabel n_samples;
    private String ref_geom_wkt;
    private boolean show_bbox;
    private JTextField wkt_save_in_file;
    private JLabel method_lbl;
    private JLabel method_info_lbl;
    private Geometry geometry;
    private Geometry aabb;
    private JButton are_input_geometries_valid_bt;
    private JButton check_interpolation_validity_bt;
    private JButton visualize_metric_ev_bt;
    private JCheckBox align_geom_cb;
    private boolean show_info_about_interpolation_validity;
    private boolean show_info_about_geometry_validity;
    private boolean is_p_valid;
    private boolean is_q_valid;

    private boolean is_interpolation_valid;
    private int granularity;
    private int num_invalid_geometries;
    private Boolean[] geometries_validity_info;
    private JButton save_curr_to_picture;
    
    private JCheckBox librip_cb;
    private JCheckBox pyspatiotemporalgeom_cb;
    private JCheckBox rigid_interpolation_lib_cb;
    private JButton compare_methods_bt;
    
    private methods.PySpatiotemporalGeom pyspatiotemporalgeom_lib;
    private methods.Librip librip;
    private methods.RigidInterpolation rigid;
    private double db;
    private double de;
    
    private String w_p;
    private String w_q;
    
    private double db_i;
    private double de_i;
    
    private int dn;
    private JTextField zoom_factor_tx;
    private JTextField b_r_tx;
    private JTextField b_g_tx;
    private JTextField b_b_tx;
    private JTextField fill_r_tx;
    private JTextField fill_g_tx;
    private JTextField fill_b_tx;
    private JTextArea ref_geom_wkt_tx;
    private JCheckBox use_ref_geom_cb;
    private JCheckBox use_guides_cb;
    private int horizontal_delta;
    private int vertical_delta;
    private JTextField horz_delta_tx;
    private JTextField vertical_delta_tx;
    private JButton guides_bt;
    private double y;
    private JButton show_points_bt;
    private boolean show_points;
    private JTextField maker_radius_tx;
    private JLabel num_coordinates_lb;
    private JButton pred_persp_eval_bt;
    private JButton char_eval_bt;
    private JLabel exec_status_lb;
    private JButton corr_bt;
    private JLabel corr_lb;
    private JLabel vis_lb;
    private JButton clear_bt;
    
	public Vce(
				Data d, 
				methods.PySpatiotemporalGeom pyspatiotemporalgeom_lib, 
				methods.Librip librip, 
				methods.RigidInterpolation rigid,
				double db, double de, 
				String w_p, String w_q, 
				double db_i, double de_i, 
				int dn
			) 
	{
		this.d = d;
		f = this;
		
		horizontal_delta = 5;
		vertical_delta = 5;
		show_points = false;
		
		this.pyspatiotemporalgeom_lib = pyspatiotemporalgeom_lib;
		this.librip = librip;
		this.rigid = rigid;
		
		this.db = db;
		this.de = de;
		this.w_p = w_p;
		this.w_q = w_q;
		this.db_i = db_i;
		this.de_i = de_i;
		this.dn = dn;
		
		geom_to_show_id = 0;
		//bbox_to_show_id = 0;
		
	    geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
	    reader = new WKTReader(geometryFactory);
		
	    geometries = (String[]) d.get_data();
	    	    
		ref_geom_wkt = this.d.get_ref_obj_wkt();
		
		if(ref_geom_wkt == null || ref_geom_wkt.trim().isEmpty())
			ref_geom_wkt = geometries[0];
	    
		//ref_geom_wkt_tx.setText(ref_geom_wkt);
		
	    //wkt = null;
		
		try {
			geometry = reader.read(geometries[0]);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		y = geometry.getEnvelope().getCoordinates()[0].getY();
		
	    max = geometries.length - 1;
	    
	    show_bbox = false;
	    
	    threshold = d.get_threshold();
	    
		draw_UI();
		
		w_center = (int) (viz_p.getWidth() / 2);
		h_center = (int) (viz_p.getHeight() / 2);
		
	    show_info_about_interpolation_validity = false;
	    show_info_about_geometry_validity = false;
	    is_p_valid = false;
	    is_q_valid = false;
	    is_interpolation_valid = false;
	    granularity = 0;
	    num_invalid_geometries = 0;
		
	    if(geometries.length == 1)
	    	show_metrics_in_chart_bt.setEnabled(false);
	    
		// Show the name of the method being used.
		
		switch(d.get_interpolation_method())
		{
			case COMPATIBLE:
				method_info_lbl.setText("Alexa (Compatible)");
				break;
			case EQUILATERAL:
				method_info_lbl.setText("Alexa (Equilateral)");
				break;
			case PY_SPATIOTEMPORAL_GEOM:
				method_info_lbl.setText("PySpatiotemporalGeom");
				break;
			case LIBRIP:
				method_info_lbl.setText("Librip");
				break;
		default:
			break;
		}
	    
		add_listeners();
		
		draw_geometry();
	}
	
	public void paint(Graphics g) 
	{
	    super.paint(g);
	    jp.repaint();
	}

	public void draw_geometry()
	{
		viz_p.setLayout(new BorderLayout());
		jp = new DrawGraphs();
		viz_p.add(jp, BorderLayout.CENTER);
	}
	
    private class DrawGraphs extends JPanel
    {
    	private int dx = 0;
    	private int dy = 0;
    	
    	private double sx = 1;
    	private double sy = 1; 	
    	   	
    	private int xx = 0;
    	private int yy = 0;
    	
		private static final long serialVersionUID = 3203545490664689791L;
		
		public DrawGraphs() 
		{
			setBackground(Color.white);
			
			setAlignmentY(CENTER_ALIGNMENT);
			setAlignmentX(CENTER_ALIGNMENT);
			
			adjust_screen_position();
			
	        addMouseListener(new MouseAdapter() 
	        {
	            public void mousePressed(MouseEvent e) 
	            {
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });

	        addMouseMotionListener(new MouseAdapter() 
	        {
	            public void mouseDragged(MouseEvent e) 
	            {        	
	            	int ddx = (e.getX() - xx);
	            	int ddy = (e.getY() - yy);
	            	
	            	translate(ddx, ddy);
	            	repaint();
	            	
	            	xx = e.getX();
	            	yy = e.getY();
	            }
	        });
	        
	        addMouseWheelListener(new MouseAdapter()
	        {
	            public void mouseWheelMoved(MouseWheelEvent e) 
	            {
		            int notches = e.getWheelRotation();
					DrawGraphs c = (DrawGraphs) jp;
					
					zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
					
		            if (notches > 0) 
		            {
		            	if (zoomLevel < maxZoomLevel) 
		            	{
		            		zoomLevel += zoomMultiplicationFactor; 
			            	c.scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
			            	c.translate(true);
		            	}
		            } else {
			           	if (zoomLevel > minZoomLevel) 
			           	{
			           		zoomLevel -= zoomMultiplicationFactor;
							c.scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
							c.translate(false);
			            }
		            }

		            jp.repaint();
	            }
	        });       
		};
		
		@Override
        protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);

	        Graphics2D gr = (Graphics2D) g;
	        gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	        
		    gr.setStroke(new BasicStroke(1.0f));	        
		    gr.setPaint(Color.blue);

	        AffineTransform at = new AffineTransform();

	        at.translate(dx, dy);
	        at.scale(sx, -sy);
	        
	        zoom_level.setText("Zoom Level: " + sx);

	        int R = Integer.parseInt(b_r_tx.getText());
	        int G = Integer.parseInt(b_g_tx.getText());
	        int B = Integer.parseInt(b_b_tx.getText());
	        
	        int fill_R = Integer.parseInt(fill_r_tx.getText());
	        int fill_G = Integer.parseInt(fill_g_tx.getText());
	        int fill_B = Integer.parseInt(fill_b_tx.getText());
	        
			horizontal_delta = Integer.parseInt(horz_delta_tx.getText());
			vertical_delta = Integer.parseInt(vertical_delta_tx.getText());
	        
			double marker_radius = Double.parseDouble(maker_radius_tx.getText());
			
	        //Polygon b_box;
	        
			try 
			{
			    if(show_footprint) 
			    {		    	
			    	for(int i = 0; i <= geom_to_show_id; i = i + show_granularity) 
			    	{
			    		geometry = reader.read(geometries[i]);
			    		
			    		//gr.setPaint(Color.black);
			    		gr.setPaint(new Color(R, G, B));
			    		gr.draw(new LiteShape(geometry, at, false));
			    		
	   					if(fill.isSelected())
	   					{
	   						//gr.setPaint(Color.blue);
	   						gr.setPaint(new Color(fill_R, fill_G, fill_B));
	   						
			   		        for (int j = 0; j < geometry.getNumGeometries(); j++)
					   			gr.fill(new LiteShape(geometry.getGeometryN(j), at, false));
	   					}
			    	}
			    }
			    //else if (show_bbox) 
			    //{
			    	/*
		    		if(geometries[geom_to_show_id].toLowerCase().charAt(0) == 'p')
		    			geometry = (Polygon) reader.read(geometries[geom_to_show_id]);
		    		else
		    			geometry = (MultiPolygon) reader.read(geometries[geom_to_show_id]);
				    		    		
		    		b_box = (Polygon) reader.read(bboxes[bbox_to_show_id]);

		    		// Draw the geometry.
			    		
		    		gr.draw(new LiteShape(geometry, at, false));
				    		
		   			if(fill.isSelected())
		   				gr.fill(new LiteShape(geometry, at, false));  	
	    			
		    		// Draw the bbox.
		    			
		   			gr.setPaint(Color.red);
		    		gr.draw(new LiteShape(b_box, at, false));
		    		*/
			    //}
			    else 
			    {		
			    	geometry = reader.read(geometries[geom_to_show_id]);
			    	
			    	//gr.setPaint(Color.black);
			    	gr.setPaint(new Color(R, G, B));
			    	gr.draw(new LiteShape(geometry, at, false));
			    	
   					if(fill.isSelected())
   					{
   						//gr.setPaint(Color.blue);
   						//gr.setPaint(new Color(18, 21, 67));
   						gr.setPaint(new Color(fill_R, fill_G, fill_B));
   						
		   		        for (int j = 0; j < geometry.getNumGeometries(); j++)
				   			gr.fill(new LiteShape(geometry.getGeometryN(j), at, false));
   					}
   					
   					// Draw guides.
   					
   					if(use_guides_cb.isSelected())
   					{
   						aabb = geometry.getEnvelope();
   						
   						if(!aabb.isEmpty())
   						{
   	   						Coordinate[] C = aabb.getCoordinates();
   	   						
   	   						if(C.length > 0)
   	   						{
   	   	   						Coordinate min = new Coordinate();
   	   	   						Coordinate max = new Coordinate();
   	   	   						
   	   	   						min.setCoordinate(C[0]);
   	   	   						max.setCoordinate(C[0]);
   	   	   						
   	   	   						for(int j = 1; j < C.length; j++)
   	   	   						{
   	   	   							if(C[j].getX() < min.getX() && C[j].getY() < min.getY())
   	   	   								min.setCoordinate(C[j]);
   	   	   							
   	   	   							if(C[j].getX() > max.getX() /*&& C[j].getY() < min.getY()*/)
   	   	   								max.setCoordinate(C[j]);
   	   	   						}
   	   	   						
   	   	   						// Draw guides.
   	   	   						
   	   	   						String guide_wkt = "LINESTRING (" + (min.getX() - horizontal_delta) + " " + y + ", " + (min.getX() - horizontal_delta) + " " + (y + vertical_delta) + ")";
   	   	   						
   	   	   						LineString l = (LineString) reader.read(guide_wkt);
   	   	   			    		gr.setPaint(Color.black);
   	   	   			    		gr.draw(new LiteShape(l, at, false));
   	   	   			    		
   	   	   			    		guide_wkt = "LINESTRING (" + (max.getX() + horizontal_delta) + " " + min.getY() + ", " + (max.getX() + horizontal_delta) + " " + (min.getY() + vertical_delta) + ")";
   	   	   						
   	   	   						l = (LineString) reader.read(guide_wkt);
   	   	   			    		gr.setPaint(Color.black);
   	   	   			    		gr.draw(new LiteShape(l, at, false));
   	   						}
   						}
   					}
   					
   					if(show_points)
   					{
   						num_coordinates_lb.setText("N: " + String.valueOf(geometry.getNumPoints()));
   						
   						Coordinate[] C = geometry.getCoordinates();
   						
   						String coordinate_wkt = "LINESTRING (" + C[0].getX() + " " + C[0].getY() + ", " + C[0].getX() + " " + C[0].getY() + ")";
   						
   						LineString l = (LineString) reader.read(coordinate_wkt);
   			    		gr.setPaint(Color.red);
   			    		gr.draw(new LiteShape(l, at, false));
   			    		
   			    		//...
   				    	Coordinate c = l.getCoordinateN(0);
   						
   				    	// Draw marker.
   				    		
   				    	GeometricShapeFactory shape_factory = new GeometricShapeFactory();
   				    	shape_factory.setNumPoints(32);
   				    	shape_factory.setCentre(new Coordinate(c.x, c.y));
   				    	shape_factory.setSize(marker_radius);
   				    	Geometry circle = shape_factory.createCircle();
   				    		
   				    	gr.setPaint(Color.red);
   				    	gr.draw(new LiteShape(circle, at, false));
   				    	//...
   			    		
   						coordinate_wkt = "LINESTRING (" + C[1].getX() + " " + C[1].getY() + ", " + C[1].getX() + " " + C[1].getY() + ")";
   						
   						l = (LineString) reader.read(coordinate_wkt);
   			    		gr.setPaint(Color.red);
   			    		gr.draw(new LiteShape(l, at, false));
   						
   			    		//...
   				    	c = l.getCoordinateN(0);
   						
   				    	// Draw marker.
   				    		
   				    	shape_factory = new GeometricShapeFactory();
   				    	shape_factory.setNumPoints(32);
   				    	shape_factory.setCentre(new Coordinate(c.x, c.y));
   				    	shape_factory.setSize(marker_radius);
   				    	circle = shape_factory.createCircle();
   				    		
   				    	gr.setPaint(Color.black);
   				    	gr.draw(new LiteShape(circle, at, false));
   				    	//...
   				    	
   						for(int j = 2; j < C.length - 1; j++)
   						{
   	   						coordinate_wkt = "LINESTRING (" + C[j].getX() + " " + C[j].getY() + ", " + C[j].getX() + " " + C[j].getY() + ")";
   	   						
   	   						l = (LineString) reader.read(coordinate_wkt);
   	   			    		gr.setPaint(Color.red);
   	   			    		gr.draw(new LiteShape(l, at, false));
   	   			    		
   	   			    		//...
   	   				    	c = l.getCoordinateN(0);
   	   						
   	   				    	// Draw marker.
   	   				    		
   	   				    	shape_factory = new GeometricShapeFactory();
   	   				    	shape_factory.setNumPoints(32);
   	   				    	shape_factory.setCentre(new Coordinate(c.x, c.y));
   	   				    	shape_factory.setSize(marker_radius);
   	   				    	circle = shape_factory.createCircle();
   	   				    		
   	   				    	gr.setPaint(Color.blue);
   	   				    	gr.draw(new LiteShape(circle, at, false));
   	   				    	//...
   						}
   					}
			    }
			    
			    if(show_info_about_geometry_validity)
			    {
					if(is_p_valid && is_q_valid)
						gr.setPaint(new Color(6, 40, 94));
					else
						gr.setPaint(new Color(166, 10, 83));
					
					gr.setFont(new Font("Arial", Font.BOLD, 12));
					gr.drawString("Source Geometry is Valid: [" + is_p_valid + "] :: Target Geometry is Valid: [" + is_q_valid + "]", 20, 30); 				
			    }
			    
			    if(show_info_about_interpolation_validity)
			    {
			    	boolean validity_info = geometries_validity_info[geom_to_show_id];
			    	
			    	if(validity_info)
			    		gr.setPaint(new Color(6, 40, 94));
			    	else
			    		gr.setPaint(new Color(166, 10, 83));
			    	
			    	gr.setFont(new Font("Arial", Font.BOLD, 12));
			    	gr.drawString("Observation " + (geom_to_show_id + 1) + " is Valid: [" + validity_info + "]", 20, 70);
			    	
					if(is_interpolation_valid)
						gr.setPaint(new Color(6, 40, 94));
					else
						gr.setPaint(new Color(166, 10, 83));
					
					gr.setFont(new Font("Arial", Font.BOLD, 12));
					gr.drawString("Interpolation is Valid: [" + is_interpolation_valid + "] :: N� Samples Observed: [" + granularity + "] :: N� Invalid Geometries: [" + num_invalid_geometries + "]", 20, 50);
			    }
			} 
			catch (ParseException e) 
			{
				e.printStackTrace();
			}
        };
		
        public void translate(boolean sign) 
        {
        	zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
        	
        	double cx_star = cx * zoomMultiplicationFactor;
        	double cy_star = cy * zoomMultiplicationFactor;
        	
			if(sign) 
			{
				dx -= (int) cx_star;
				dy += (int) cy_star;				
			}
			else
			{
				dx += (int) cx_star;
				dy -= (int) cy_star;				
			}
        }
        
        public void translate(int x, int y) 
        {
        	dx += x;
        	dy += y;
        }
        
        public void scale(double x, double y) 
        {
        	sx += x;
        	sy += y;
        }
        
        public void adjust_screen_position()
        {
        	if(geometries == null || geometries.length == 0)
        		return;
        	
			try
			{
				//geometry = reader.read(geometries[geom_to_show_id]);
				geometry = reader.read(geometries[0]);
				//Point a = geometry.getCentroid();
				Point a = geometry.getEnvelope().getCentroid();

				//cx = c.getX();
				//cy = c.getY();
				
				
				geometry = reader.read(geometries[geometries.length - 1]);
				//Point b = geometry.getCentroid();
				Point b = geometry.getEnvelope().getCentroid();

				cx = (a.getX() + b.getX()) / 2;
				cy = (a.getY() + b.getY()) / 2;
			} 
			catch (ParseException e) {
				e.printStackTrace();
			}

			w_center = (int) (viz_p.getWidth() / 2);
			h_center = (int) (viz_p.getHeight() / 2);
			
			dx = (int) ((-cx + w_center));
			dy = (int) ((cy + h_center));
        }
    }
	
	public void draw_UI() 
	{
		setTitle("Visualization::Comparison::Evaluation");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	    setSize(1296, 720);
	    setLocationRelativeTo(null);
	    setExtendedState(f.getExtendedState() | JFrame.MAXIMIZED_BOTH);
	    
		contentPane = new JPanel();
		contentPane.setToolTipText("");
		contentPane.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		viz_p = new JPanel();
		viz_p.setLocation(175, 10);
		
		viz_p.setSize(930, 590);
		
		viz_p.setBackground(Color.WHITE);
		viz_p.setBorder(new LineBorder(Color.black, 1));
		contentPane.add(viz_p);
		
		tools_p = new JPanel();
		tools_p.setBorder(new LineBorder(new Color(0, 0, 0)));
		tools_p.setBounds(10, 616, 1260, 36);
		contentPane.add(tools_p);
		
		zoom_factor_tx = new JTextField();
		zoom_factor_tx.setBackground(SystemColor.inactiveCaptionBorder);
		zoom_factor_tx.setForeground(SystemColor.desktop);
		zoom_factor_tx.setText("0.25");
		zoom_factor_tx.setToolTipText("Zoom factor.");
		tools_p.add(zoom_factor_tx);
		zoom_factor_tx.setColumns(3);
		
		minus = new JButton("Zoom Out");
		tools_p.add(minus);
		
		plus = new JButton("Zoom In");
		tools_p.add(plus);
		
		save_curr_to_picture = new JButton("Save Current To PNG");
		tools_p.add(save_curr_to_picture);
	
		save_interpolation_to_picture = new JButton("Save All to PNG");
		save_interpolation_to_picture.setToolTipText("Save the interpolation to a sequence of png pictures.");
		tools_p.add(save_interpolation_to_picture);
		
		use_guides_cb = new JCheckBox("Use Guides");
		tools_p.add(use_guides_cb);
		
		png_sequence_filename_tx = new JTextField();
		png_sequence_filename_tx.setToolTipText("Name of the png pictures.");
		png_sequence_filename_tx.setText("d:\\\\interpolation_viz");
		tools_p.add(png_sequence_filename_tx);
		png_sequence_filename_tx.setColumns(16);
		
		rate_tx = new JTextField();
		rate_tx.setBackground(SystemColor.inactiveCaptionBorder);
		rate_tx.setToolTipText("Rate of the animation of the interpolation.");
		rate_tx.setText("16");
		tools_p.add(rate_tx);
		rate_tx.setColumns(2);
		
		play = new JButton("Interpolate");
		tools_p.add(play);
		
		interpolation_slider = new JSlider();
		interpolation_slider.setValue(0);
		tools_p.add(interpolation_slider);
		
		interpolation_slider.setMinimum(0);
		interpolation_slider.setMaximum(max);
		
		status = new JLabel("");
		tools_p.add(status);
		
		footprint_bt = new JButton("Show Footprint");
		footprint_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
		footprint_bt.setForeground(new Color(0, 0, 0));
		footprint_bt.setBounds(10, 10, 153, 25);
		contentPane.add(footprint_bt);
		
		granularity_tx = new JTextField();
		granularity_tx.setToolTipText("Granularity.");
		granularity_tx.setText("1");
		granularity_tx.setBounds(11, 40, 56, 22);
		contentPane.add(granularity_tx);
		granularity_tx.setColumns(10);
		
		fill = new JCheckBox("Fill");
		fill.setBounds(105, 39, 56, 25);
		contentPane.add(fill);
		
		zoom_level = new JLabel("Zoom Level:");
		zoom_level.setBounds(1115, 10, 155, 14);
		contentPane.add(zoom_level);
		
		zoom_slider = new JSlider();
		zoom_slider.setEnabled(false);
		zoom_slider.setValue(1);
		zoom_slider.setMinimum(1);
		zoom_slider.setBounds(1115, 38, 155, 26);
		contentPane.add(zoom_slider);
		
		n_samples = new JLabel("N\u00BA Samples: ");
		n_samples.setBounds(1115, 73, 155, 14);
		contentPane.add(n_samples);
		
	    //n_samples.setText("N� Samples: " + geometries.length);
	    n_samples.setText("N� Samples: " + geometries.length + " :: " + (geom_to_show_id + 1));
	    
	    wkt_save_in_file = new JTextField();
	    wkt_save_in_file.setForeground(new Color(25, 25, 112));
	    wkt_save_in_file.setBackground(new Color(240, 255, 240));
	    wkt_save_in_file.setText("d:\\\\wkt.txt");
	    wkt_save_in_file.setBounds(11, 580, 155, 20);
	    contentPane.add(wkt_save_in_file);
	    wkt_save_in_file.setColumns(1);
	    
	    wkt_save_bt = new JButton("Save WKT");
	    wkt_save_bt.setToolTipText("Save to file.");
	    wkt_save_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    wkt_save_bt.setBounds(10, 551, 156, 25);
	    contentPane.add(wkt_save_bt);
	    
	    metrics = new JComboBox<>();
	    metrics.setToolTipText("Metrics");
	    metrics.setModel(new DefaultComboBoxModel<>(new String[] {"Area", "Perimeter", "Hausdorff Distance", "Jaccard Index", "Jaccard Distance"}));
	    metrics.setBounds(11, 101, 151, 22);
	    contentPane.add(metrics);
	    
	    JLabel Metrics_lbl = new JLabel("Metrics:");
	    Metrics_lbl.setForeground(new Color(25, 25, 112));
	    Metrics_lbl.setFont(new Font("Tahoma", Font.BOLD, 12));
	    Metrics_lbl.setBounds(11, 78, 153, 16);
	    contentPane.add(Metrics_lbl);
	    
	    show_metrics_in_chart_bt = new JButton("Show Metric in Chart");
	    show_metrics_in_chart_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    show_metrics_in_chart_bt.setForeground(new Color(72, 61, 139));
	    show_metrics_in_chart_bt.setBounds(10, 130, 153, 25);
	    contentPane.add(show_metrics_in_chart_bt);
	    
	    method_lbl = new JLabel("Method:");
	    method_lbl.setForeground(new Color(0, 0, 128));
	    method_lbl.setFont(new Font("Tahoma", Font.BOLD, 12));
	    method_lbl.setBounds(1117, 100, 153, 27);
	    contentPane.add(method_lbl);
	    
	    method_info_lbl = new JLabel("");
	    method_info_lbl.setForeground(new Color(47, 79, 79));
	    method_info_lbl.setFont(new Font("Tahoma", Font.BOLD, 11));
	    method_info_lbl.setBounds(1117, 120, 153, 27);
	    contentPane.add(method_info_lbl);
	    
	    are_input_geometries_valid_bt = new JButton("Input Validity");
	    are_input_geometries_valid_bt.setToolTipText("Check Input Geometries Validity.");
	    are_input_geometries_valid_bt.setForeground(new Color(47, 79, 79));
	    are_input_geometries_valid_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    are_input_geometries_valid_bt.setBounds(10, 303, 153, 23);
	    contentPane.add(are_input_geometries_valid_bt);
	    
	    check_interpolation_validity_bt = new JButton("Interpolation Validity");
	    check_interpolation_validity_bt.setToolTipText("Check Interpolation Validity.");
	    check_interpolation_validity_bt.setForeground(new Color(47, 79, 79));
	    check_interpolation_validity_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    check_interpolation_validity_bt.setBounds(10, 331, 153, 23);
	    contentPane.add(check_interpolation_validity_bt);
	    
	    visualize_metric_ev_bt = new JButton("Visualize Metric");
	    visualize_metric_ev_bt.setForeground(new Color(47, 79, 79));
	    visualize_metric_ev_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    visualize_metric_ev_bt.setBounds(10, 161, 153, 23);
	    contentPane.add(visualize_metric_ev_bt);
	    
	    align_geom_cb = new JCheckBox("Align Geometries");
	    align_geom_cb.setSelected(true);
	    align_geom_cb.setBounds(10, 186, 153, 23);
	    contentPane.add(align_geom_cb);
	    
	    librip_cb = new JCheckBox("Librip");
	    librip_cb.setFont(new Font("Tahoma", Font.ITALIC, 11));
	    librip_cb.setForeground(new Color(25, 25, 112));
	    librip_cb.setSelected(true);
	    librip_cb.setBounds(10, 372, 153, 23);
	    contentPane.add(librip_cb);
	    
	    pyspatiotemporalgeom_cb = new JCheckBox("PySpatiotemporalGeom");
	    pyspatiotemporalgeom_cb.setFont(new Font("Tahoma", Font.ITALIC, 11));
	    pyspatiotemporalgeom_cb.setForeground(new Color(25, 25, 112));
	    pyspatiotemporalgeom_cb.setSelected(true);
	    pyspatiotemporalgeom_cb.setBounds(10, 392, 153, 23);
	    contentPane.add(pyspatiotemporalgeom_cb);
	    
	    rigid_interpolation_lib_cb = new JCheckBox("Morphrip");
	    rigid_interpolation_lib_cb.setFont(new Font("Tahoma", Font.ITALIC, 11));
	    rigid_interpolation_lib_cb.setForeground(new Color(25, 25, 112));
	    rigid_interpolation_lib_cb.setBounds(10, 412, 153, 23);
	    contentPane.add(rigid_interpolation_lib_cb);
	    
	    compare_methods_bt = new JButton("Compare Methods");
	    compare_methods_bt.setForeground(new Color(47, 79, 79));
	    compare_methods_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
	    compare_methods_bt.setBounds(10, 436, 155, 23);
	    contentPane.add(compare_methods_bt);
	    
	    b_r_tx = new JTextField();
	    b_r_tx.setText("0");
	    b_r_tx.setBounds(32, 483, 31, 20);
	    contentPane.add(b_r_tx);
	    b_r_tx.setColumns(10);
	    
	    b_g_tx = new JTextField();
	    b_g_tx.setText("0");
	    b_g_tx.setBounds(73, 483, 31, 20);
	    contentPane.add(b_g_tx);
	    b_g_tx.setColumns(10);
	    
	    b_b_tx = new JTextField();
	    b_b_tx.setText("0");
	    b_b_tx.setBounds(114, 483, 31, 20);
	    contentPane.add(b_b_tx);
	    b_b_tx.setColumns(10);
	    
	    fill_r_tx = new JTextField();
	    fill_r_tx.setText("18");
	    fill_r_tx.setBounds(32, 508, 31, 20);
	    contentPane.add(fill_r_tx);
	    fill_r_tx.setColumns(10);
	    
	    fill_g_tx = new JTextField();
	    fill_g_tx.setText("21");
	    fill_g_tx.setBounds(73, 508, 31, 20);
	    contentPane.add(fill_g_tx);
	    fill_g_tx.setColumns(10);
	    
	    fill_b_tx = new JTextField();
	    fill_b_tx.setText("67");
	    fill_b_tx.setBounds(114, 508, 31, 20);
	    contentPane.add(fill_b_tx);
	    fill_b_tx.setColumns(10);
	    
	    use_ref_geom_cb = new JCheckBox("Use Reference Geometry");
	    use_ref_geom_cb.setSelected(true);
	    use_ref_geom_cb.setBounds(10, 206, 153, 23);
	    contentPane.add(use_ref_geom_cb);
	    
	    ref_geom_wkt_tx = new JTextArea();
	    ref_geom_wkt_tx.setToolTipText("Reference Geometry wkt.");
	    ref_geom_wkt_tx.setBounds(10, 236, 155, 36);
	    contentPane.add(ref_geom_wkt_tx);
	    
		guides_bt = new JButton("Show/Hide Guides");
		guides_bt.setForeground(new Color(47, 79, 79));
		guides_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
		guides_bt.setBounds(1115, 186, 155, 23);
		contentPane.add(guides_bt);
		
		horz_delta_tx = new JTextField();
		horz_delta_tx.setText("10");
		horz_delta_tx.setBounds(1115, 212, 56, 20);
		contentPane.add(horz_delta_tx);
		horz_delta_tx.setColumns(3);
		
		vertical_delta_tx = new JTextField();
		vertical_delta_tx.setText("10");
		vertical_delta_tx.setBounds(1214, 212, 56, 20);
		contentPane.add(vertical_delta_tx);
		vertical_delta_tx.setColumns(3);
		
		show_points_bt = new JButton("Show/Hide Points");
		show_points_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
		show_points_bt.setBounds(1115, 236, 155, 23);
		contentPane.add(show_points_bt);
		
		maker_radius_tx = new JTextField();
		maker_radius_tx.setText("1");
		maker_radius_tx.setBounds(1115, 261, 56, 20);
		contentPane.add(maker_radius_tx);
		maker_radius_tx.setColumns(10);
		
		num_coordinates_lb = new JLabel("N: ");
		num_coordinates_lb.setToolTipText("N\u00BA Points");
		num_coordinates_lb.setBounds(1214, 261, 56, 20);
		contentPane.add(num_coordinates_lb);
		
		pred_persp_eval_bt = new JButton("Prediction");
		pred_persp_eval_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
		pred_persp_eval_bt.setBounds(1115, 446, 155, 23);
		contentPane.add(pred_persp_eval_bt);
		
		char_eval_bt = new JButton("Characterization");
		char_eval_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
		char_eval_bt.setBounds(1115, 422, 155, 23);
		contentPane.add(char_eval_bt);
		
		JLabel exe_status_title_lb = new JLabel("Execution Status:");
		exe_status_title_lb.setForeground(new Color(25, 25, 112));
		exe_status_title_lb.setFont(new Font("Tahoma", Font.BOLD, 12));
		exe_status_title_lb.setToolTipText("Evaluation:");
		exe_status_title_lb.setBounds(1115, 297, 155, 25);
		contentPane.add(exe_status_title_lb);
		
		exec_status_lb = new JLabel("");
		exec_status_lb.setForeground(new Color(47, 79, 79));
		exec_status_lb.setFont(new Font("Tahoma", Font.BOLD, 11));
		exec_status_lb.setBounds(1115, 321, 155, 14);
		contentPane.add(exec_status_lb);
		
		corr_bt = new JButton("Correspondence");
		corr_bt.setToolTipText("Correspondence");
		corr_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
		corr_bt.setBounds(1115, 512, 155, 23);
		contentPane.add(corr_bt);
		
		JLabel eval_lb = new JLabel("Evaluation:");
		eval_lb.setForeground(new Color(0, 51, 102));
		eval_lb.setFont(new Font("Tahoma", Font.BOLD, 12));
		eval_lb.setToolTipText("");
		eval_lb.setBounds(1115, 393, 155, 25);
		contentPane.add(eval_lb);
		
		corr_lb = new JLabel("Correspondence:");
		corr_lb.setForeground(new Color(51, 0, 102));
		corr_lb.setFont(new Font("Tahoma", Font.BOLD, 12));
		corr_lb.setBounds(1115, 486, 155, 22);
		contentPane.add(corr_lb);
		
		vis_lb = new JLabel("Visual Aids:");
		vis_lb.setForeground(new Color(0, 0, 51));
		vis_lb.setFont(new Font("Tahoma", Font.BOLD, 12));
		vis_lb.setBounds(1115, 159, 155, 22);
		contentPane.add(vis_lb);
		
		clear_bt = new JButton("Clear");
		clear_bt.setFont(new Font("Tahoma", Font.BOLD, 11));
		clear_bt.setBounds(1115, 578, 155, 23);
		contentPane.add(clear_bt);
	}

	public void add_listeners()
	{
        addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(WindowEvent e) 
            {
                setVisible(false);
                dispose();
            }
        });
        
		fill.addItemListener(new ItemListener() 
		{
			@Override 
			public void itemStateChanged(ItemEvent e) 
			{
				jp.repaint();
			}
		});
        
        clear_bt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				show_info_about_geometry_validity = false;
				show_info_about_interpolation_validity = false;
				
				jp.repaint();
            }
        });
        
        corr_bt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				Corr c = new Corr(d, pyspatiotemporalgeom_lib, librip, rigid);
				c.setVisible(true);
            }
        });
        
        char_eval_bt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				Characterization c = new Characterization(d, pyspatiotemporalgeom_lib, librip, rigid);
				c.setVisible(true);
				
				/*
				String[] librip_geoms_wkt = null;
				String[] pygeom_geoms_wkt = null;
				String[] rigid_geoms_wkt = null;
				
				if(librip_cb.isSelected())
				{
					exec_status_lb.setText("Using Secondo...");
					contentPane.update(contentPane.getGraphics());
					
					librip_geoms_wkt = librip.during_period(w_p, w_q, db, de, dn);
				}
				
				if(pyspatiotemporalgeom_cb.isSelected())
				{
					exec_status_lb.setText("Using PySptGeom...");
					contentPane.update(contentPane.getGraphics());
					
					pygeom_geoms_wkt = pyspatiotemporalgeom_lib.during_period((int) db, (int) de, dn, w_p, w_q, (int) db, (int) de);
				}

				if(rigid_interpolation_lib_cb.isSelected())
				{
					exec_status_lb.setText("Using Alexa...");
					contentPane.update(contentPane.getGraphics());
				
					rigid_geoms_wkt = rigid.during_period(db, de, w_p, w_q, db, de, dn, InterpolationMethod.COMPATIBLE.get_value(), d.get_cw(), d.get_threshold(), 0.0);
				}
				
				String title = "Method;" + "I Area;" + "F Area;" + "Min. Area;" + "At;" + "Max. Area;" + "At;" + "Dev. Min;" + "Dev. Max;";
				title += "I Per.;" + "F Per.;" + "Min. Per.;" + "At;" + "Max. Per.;" + "At;" + "Dev. Min;" + "Dev. Max;";
				title += "H.;" + "Max. H.;" + "At;" + "Dev.;";
				title += "J.;" + "Max. J.;" + "At;" + "Dev.;" + "MAE A;MAE P;MAE H;MAE J;";
				
				if(librip_geoms_wkt != null)
				{	
					exec_status_lb.setText("Evaluating Librip...");
					contentPane.update(contentPane.getGraphics());
					
					double i_area = 0;
					double f_area = 0;
					
					double i_p = 0;
					double f_p = 0;
					
					double hd = 0;
					double max_hd = 0;
					int max_hd_t = 0;
					
					double jd = 0;
					double max_jd = 0;
					int max_jd_t = 0;
					
					double min_area = Double.MAX_VALUE;
					double max_area = Double.MIN_VALUE;
					
					int min_area_t = 0;
					int max_area_t = 0;
					
					double min_p = Double.MAX_VALUE;
					double max_p = Double.MIN_VALUE;					
					
					int min_p_t = 0;
					int max_p_t = 0;
					
					double A = 0;
					double P = 0;
					double H = 0;
					double J = 0;
					
					Main m = d.get_main();
					
					String ref_g_wkt = librip_geoms_wkt[0];
					
					if(ref_geom_wkt_tx.getText() != null && !ref_geom_wkt_tx.getText().isEmpty())
						ref_g_wkt = ref_geom_wkt_tx.getText();
					
					i_area = m.numerical_metric(librip_geoms_wkt[0], Metrics.AREA.get_value());
					f_area = m.numerical_metric(librip_geoms_wkt[1], Metrics.AREA.get_value());
					
					i_p = m.numerical_metric(librip_geoms_wkt[0], Metrics.PERIMETER.get_value());
					f_p = m.numerical_metric(librip_geoms_wkt[1], Metrics.PERIMETER.get_value());
					
					hd = m.compare_geometries(ref_g_wkt, librip_geoms_wkt[1], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
					jd = m.compare_geometries(ref_g_wkt, librip_geoms_wkt[1], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
					
					double mae_a = 0;
					double mae_p = 0;
					double mae_h = 0;
					double mae_j = 0;
					
					double delta_a = f_area - i_area;
					double delta_p = f_p - i_p;
					double delta_h = hd;
					double delta_j = jd;
					double t = 0;
					int n = dn -1;
					
					for(int j = 0; j < dn; j++)
					{
						t = j / n;
						
						if(j == 0)
						{
							A = i_area;
							P = i_p;
							H = m.compare_geometries(ref_g_wkt, librip_geoms_wkt[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
							J = m.compare_geometries(ref_g_wkt, librip_geoms_wkt[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
						}
						else if(j == librip_geoms_wkt.length - 1)
						{
							A = f_area;
							P = f_p;
							H = hd;
							J = jd;
						}
						else
						{
							A = m.numerical_metric(librip_geoms_wkt[j], Metrics.AREA.get_value());
							P = m.numerical_metric(librip_geoms_wkt[j], Metrics.PERIMETER.get_value());
							H = m.compare_geometries(ref_g_wkt, librip_geoms_wkt[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
							J = m.compare_geometries(ref_g_wkt, librip_geoms_wkt[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
						}
						
						mae_a += Math.abs((i_area + delta_a * t) - A);
						mae_p += Math.abs((i_p + delta_p * t) - P);
						mae_h += Math.abs((delta_h * t) - H);
						mae_j += Math.abs((delta_j * t) - J);
						
						if(min_area > A)
						{
							min_area = A;
							min_area_t = j;
						}
							
						if(max_area < A)
						{
							max_area = A;
							max_area_t = j;
						}
							
						if(min_p > P)
						{
							min_p = P;
							min_p_t = j;
						}
							
						if(max_p < P)
						{
							max_p = P;
							max_p_t = j;
						}
							
						if(max_hd < H)
						{
							max_hd = H;
							max_hd_t = j;
						}
							
						if(max_jd < J)
						{
							max_jd = J;
							max_jd_t = j;
						}
					}
					
					mae_a /= dn;
					mae_p /= dn;
					mae_h /= dn;
					mae_j /= dn;
					
					double min = Math.min(i_area, f_area);
					double max = Math.max(i_area, f_area);
					
					String r = "Librip;" + i_area + ";" + f_area + ";" + min_area + ";" + min_area_t + ";" + max_area + ";" + max_area_t + ";" + ((min_area - min) / min) + ";" + ((max_area - max) / max) + ";";
					
					min = Math.min(i_p, f_p);
					max = Math.max(i_p, f_p);
					
					r += i_p + ";" + f_p + ";" + min_p + ";" + min_p_t + ";" + max_p + ";" + max_p_t + ";" + ((min_p - min) / min) + ";" + ((max_p - max) / max) + ";";
					r += hd + ";" + max_hd + ";" + max_hd_t + ";" + ((max_hd - hd) / hd) + ";";
					r += jd + ";" + max_jd + ";" + max_jd_t + ";" + ((max_jd - jd) / jd) + ";" + mae_a + ";" + mae_p + ";" + mae_h + ";" + mae_j + ";";					
					
					System.out.println(title);
					System.out.println(r);
				}
				
				if(pygeom_geoms_wkt != null)
				{
					exec_status_lb.setText("Evaluating PySprGeom...");
					contentPane.update(contentPane.getGraphics());
					
					double i_area = 0;
					double f_area = 0;
					
					double i_p = 0;
					double f_p = 0;
					
					double hd = 0;
					double max_hd = 0;
					int max_hd_t = 0;
					
					double jd = 0;
					double max_jd = 0;
					int max_jd_t = 0;
					
					double min_area = Double.MAX_VALUE;
					double max_area = Double.MIN_VALUE;
					
					int min_area_t = 0;
					int max_area_t = 0;
					
					double min_p = Double.MAX_VALUE;
					double max_p = Double.MIN_VALUE;					
					
					int min_p_t = 0;
					int max_p_t = 0;
					
					double A = 0;
					double P = 0;
					double H = 0;
					double J = 0;
					
					Main m = d.get_main();
					
					String ref_g_wkt = pygeom_geoms_wkt[0];
					
					if(ref_geom_wkt_tx.getText() != null && !ref_geom_wkt_tx.getText().isEmpty())
						ref_g_wkt = ref_geom_wkt_tx.getText();
					
					i_area = m.numerical_metric(pygeom_geoms_wkt[0], Metrics.AREA.get_value());
					f_area = m.numerical_metric(pygeom_geoms_wkt[1], Metrics.AREA.get_value());
					
					i_p = m.numerical_metric(pygeom_geoms_wkt[0], Metrics.PERIMETER.get_value());
					f_p = m.numerical_metric(pygeom_geoms_wkt[1], Metrics.PERIMETER.get_value());
					
					hd = m.compare_geometries(ref_g_wkt, pygeom_geoms_wkt[1], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
					jd = m.compare_geometries(ref_g_wkt, pygeom_geoms_wkt[1], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
					
					double mae_a = 0;
					double mae_p = 0;
					double mae_h = 0;
					double mae_j = 0;
					
					double delta_a = f_area - i_area;
					double delta_p = f_p - i_p;
					double delta_h = hd;
					double delta_j = jd;
					double t = 0;
					int n = dn -1;
					
					for(int j = 0; j < dn; j++)
					{
						t = j / n;
						
						if(j == 0)
						{
							A = i_area;
							P = i_p;
							H = m.compare_geometries(ref_g_wkt, pygeom_geoms_wkt[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
							J = m.compare_geometries(ref_g_wkt, pygeom_geoms_wkt[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
						}
						else if(j == pygeom_geoms_wkt.length - 1)
						{
							A = f_area;
							P = f_p;
							H = hd;
							J = jd;
						}
						else
						{
							A = m.numerical_metric(pygeom_geoms_wkt[j], Metrics.AREA.get_value());
							P = m.numerical_metric(pygeom_geoms_wkt[j], Metrics.PERIMETER.get_value());
							H = m.compare_geometries(ref_g_wkt, pygeom_geoms_wkt[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
							J = m.compare_geometries(ref_g_wkt, pygeom_geoms_wkt[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
						}
						
						mae_a += Math.abs((i_area + delta_a * t) - A);
						mae_p += Math.abs((i_p + delta_p * t) - P);
						mae_h += Math.abs((delta_h * t) - H);
						mae_j += Math.abs((delta_j * t) - J);
						
						if(min_area > A)
						{
							min_area = A;
							min_area_t = j;
						}
							
						if(max_area < A)
						{
							max_area = A;
							max_area_t = j;
						}
							
						if(min_p > P)
						{
							min_p = P;
							min_p_t = j;
						}
							
						if(max_p < P)
						{
							max_p = P;
							max_p_t = j;
						}
							
						if(max_hd < H)
						{
							max_hd = H;
							max_hd_t = j;
						}							
							
						if(max_jd < J)
						{
							max_jd = J;
							max_jd_t = j;
						}
					}
					
					mae_a /= dn;
					mae_p /= dn;
					mae_h /= dn;
					mae_j /= dn;
					
					double min = Math.min(i_area, f_area);
					double max = Math.max(i_area, f_area);
					
					String r = "PySptGeom;" + i_area + ";" + f_area + ";" + min_area + ";" + min_area_t + ";" + max_area + ";" + max_area_t + ";" + ((min_area - min) / min) + ";" + ((max_area - max) / max) + ";";
					
					min = Math.min(i_p, f_p);
					max = Math.max(i_p, f_p);
					
					r += i_p + ";" + f_p + ";" + min_p + ";" + min_p_t + ";" + max_p + ";" + max_p_t + ";" + ((min_p - min) / min) + ";" + ((max_p - max) / max) + ";";
					r += hd + ";" + max_hd + ";" + max_hd_t + ";" + ((max_hd - hd) / hd) + ";";
					r += jd + ";" + max_jd + ";" + max_jd_t + ";" + ((max_jd - jd) / jd) + ";" + mae_a + ";" + mae_p + ";" + mae_h + ";" + mae_j + ";";

					System.out.println(r);
				}
				
				if(rigid_geoms_wkt != null)
				{
					exec_status_lb.setText("Evaluating Rigid...");
					contentPane.update(contentPane.getGraphics());
					
					double i_area = 0;
					double f_area = 0;
					
					double i_p = 0;
					double f_p = 0;
					
					double hd = 0;
					double max_hd = 0;
					int max_hd_t = 0;
					
					double jd = 0;
					double max_jd = 0;
					int max_jd_t = 0;
					
					double min_area = Double.MAX_VALUE;
					double max_area = Double.MIN_VALUE;
					
					int min_area_t = 0;
					int max_area_t = 0;
					
					double min_p = Double.MAX_VALUE;
					double max_p = Double.MIN_VALUE;					
					
					int min_p_t = 0;
					int max_p_t = 0;
					
					double A = 0;
					double P = 0;
					double H = 0;
					double J = 0;
					
					Main m = d.get_main();
					
					String ref_g_wkt = rigid_geoms_wkt[0];
					
					if(ref_geom_wkt_tx.getText() != null && !ref_geom_wkt_tx.getText().isEmpty())
						ref_g_wkt = ref_geom_wkt_tx.getText();
					
					i_area = m.numerical_metric(rigid_geoms_wkt[0], Metrics.AREA.get_value());
					f_area = m.numerical_metric(rigid_geoms_wkt[1], Metrics.AREA.get_value());
					
					i_p = m.numerical_metric(rigid_geoms_wkt[0], Metrics.PERIMETER.get_value());
					f_p = m.numerical_metric(rigid_geoms_wkt[1], Metrics.PERIMETER.get_value());
					
					hd = m.compare_geometries(ref_g_wkt, rigid_geoms_wkt[1], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
					jd = m.compare_geometries(ref_g_wkt, rigid_geoms_wkt[1], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
					
					double mae_a = 0;
					double mae_p = 0;
					double mae_h = 0;
					double mae_j = 0;
					
					double delta_a = f_area - i_area;
					double delta_p = f_p - i_p;
					double delta_h = hd;
					double delta_j = jd;
					double t = 0;
					int n = dn -1;
					
					for(int j = 0; j < dn; j++)
					{
						t = j / n;
						
						if(j == 0)
						{
							A = i_area;
							P = i_p;
							H = m.compare_geometries(ref_g_wkt, rigid_geoms_wkt[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
							J = m.compare_geometries(ref_g_wkt, rigid_geoms_wkt[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
						}
						else if(j == rigid_geoms_wkt.length - 1)
						{
							A = f_area;
							P = f_p;
							H = hd;
							J = jd;
						}
						else
						{
							A = m.numerical_metric(rigid_geoms_wkt[j], Metrics.AREA.get_value());
							P = m.numerical_metric(rigid_geoms_wkt[j], Metrics.PERIMETER.get_value());
							H = m.compare_geometries(ref_g_wkt, rigid_geoms_wkt[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
							J = m.compare_geometries(ref_g_wkt, rigid_geoms_wkt[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
						}
						
						mae_a += Math.abs((i_area + delta_a * t) - A);
						mae_p += Math.abs((i_p + delta_p * t) - P);
						mae_h += Math.abs((delta_h * t) - H);
						mae_j += Math.abs((delta_j * t) - J);
						
						if(min_area > A)
						{
							min_area = A;
							min_area_t = j;
						}
							
						if(max_area < A)
						{
							max_area = A;
							max_area_t = j;
						}
							
						if(min_p > P)
						{
							min_p = P;
							min_p_t = j;
						}
							
						if(max_p < P)
						{
							max_p = P;
							max_p_t = j;
						}
							
						if(max_hd < H)
						{
							max_hd = H;
							max_hd_t = j;
						}							
							
						if(max_jd < J)
						{
							max_jd = J;
							max_jd_t = j;
						}
					}
					
					mae_a /= dn;
					mae_p /= dn;
					mae_h /= dn;
					mae_j /= dn;
					
					double min = Math.min(i_area, f_area);
					double max = Math.max(i_area, f_area);
					
					String r = "Morphrip;" + i_area + ";" + f_area + ";" + min_area + ";" + min_area_t + ";" + max_area + ";" + max_area_t + ";" + ((min_area - min) / min) + ";" + ((max_area - max) / max) + ";";
					
					min = Math.min(i_p, f_p);
					max = Math.max(i_p, f_p);
					
					r += i_p + ";" + f_p + ";" + min_p + ";" + min_p_t + ";" + max_p + ";" + max_p_t + ";" + ((min_p - min) / min) + ";" + ((max_p - max) / max) + ";";
					r += hd + ";" + max_hd + ";" + max_hd_t + ";" + ((max_hd - hd) / hd) + ";";
					r += jd + ";" + max_jd + ";" + max_jd_t + ";" + ((max_jd - jd) / jd) + ";" + mae_a + ";" + mae_p + ";" + mae_h + ";" + mae_j + ";";

					System.out.println(r);
				}

				exec_status_lb.setText("Done!");
				contentPane.update(contentPane.getGraphics());
				*/
			}
		});
		
        pred_persp_eval_bt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{	
				Prediction pred = new Prediction(d, pyspatiotemporalgeom_lib, librip, rigid);
				pred.setVisible(true);
			}
		});
        
        guides_bt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				if(use_guides_cb.isSelected())
					use_guides_cb.setSelected(false);
				else
					use_guides_cb.setSelected(true);
				
				jp.repaint();
			}
		});
        
        show_points_bt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				if(show_points)
					show_points = false;
				else
					show_points = true;
				
				jp.repaint();
			}
		});
        
        // Show footprint with a granularity.
        
		footprint_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				// If on just turn off.
				
				if(show_footprint) {
					show_footprint = false;
					return;
				}
				
				play.setEnabled(false);
				interpolation_slider.setEnabled(false);
				save_interpolation_to_picture.setEnabled(false);
				
			    show_granularity = Integer.parseInt(granularity_tx.getText());
			    show_footprint = true;
				
				geom_to_show_id = 0;
				
			    play_task = new TimerTask() 
			    {
			        public void run() 
			        {
			        	jp.repaint();
			        	
			    		if(geom_to_show_id < max) 
			    			geom_to_show_id += show_granularity;	    			
			    		else 
			    		{
			    			timer.cancel();
			    			
							interpolation_slider.setValue(interpolation_slider.getMaximum());
							
							play.setEnabled(true);
							interpolation_slider.setEnabled(true);
							save_interpolation_to_picture.setEnabled(true);
			    		}
			        }
			    };
			    
				timer = new Timer(/*true*/);
				timer.scheduleAtFixedRate(play_task, 0, Long.parseLong(rate_tx.getText()));
			}
		});
        
		// Zoom out.
		
    	minus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs c = (DrawGraphs) jp;

				zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
				
	           	if (zoomLevel > minZoomLevel) 
	           	{
	           		zoomLevel -= zoomMultiplicationFactor;
					c.scale(-zoomMultiplicationFactor, -zoomMultiplicationFactor);
					c.translate(false);
	            }

	            jp.repaint();
			}
		});
    	
    	// Zoom in.
    	
    	plus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				DrawGraphs c = (DrawGraphs) jp;

				zoomMultiplicationFactor = Double.parseDouble(zoom_factor_tx.getText());
				
            	if (zoomLevel < maxZoomLevel) 
            	{
            		zoomLevel += zoomMultiplicationFactor; 
	            	c.scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
	            	c.translate(true);
            	}

	            jp.repaint();

			}
		});
        
		save_interpolation_to_picture.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				interpolation_slider.setEnabled(false);
				play.setEnabled(false);
				
				int v = geom_to_show_id;
				int N = geometries.length;
				
				status.setText("Saving!");
				
				//JOptionPane.showMessageDialog(null, "Click to start!", "Save", JOptionPane.INFORMATION_MESSAGE);
				
				try
				{
					show_granularity = Integer.parseInt(granularity_tx.getText());
					
					for(int i = 0; i < N; i = i + show_granularity) 
					{						
						geom_to_show_id = i;
			    		jp.repaint();
			    		
						BufferedImage bi = ScreenImage.createImage(viz_p);
	    				ScreenImage.writeImage(bi, png_sequence_filename_tx.getText() + "_" + (N + i) + ".png");
					}
    			} 
				catch (IOException ex) {
    				ex.printStackTrace();
    			}
				
				geom_to_show_id = v;
				jp.repaint();
				
				JOptionPane.showMessageDialog(null, "Saved!", "Save", JOptionPane.INFORMATION_MESSAGE);	
				
				interpolation_slider.setEnabled(true);
				play.setEnabled(true);
				status.setText("");
			}
		});
		
		save_curr_to_picture.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				interpolation_slider.setEnabled(false);
				play.setEnabled(false);
				
				status.setText("Saving!");
				String filename = png_sequence_filename_tx.getText() + "_" + geom_to_show_id + ".png";
				
				try
				{
					BufferedImage bi = ScreenImage.createImage(viz_p);
	    			ScreenImage.writeImage(bi, filename);
    			} 
				catch (IOException ex) {
    				ex.printStackTrace();
    			}
				
				jp.repaint();
				
				JOptionPane.showMessageDialog(null, "Saved to: " + filename + "!", "Save", JOptionPane.INFORMATION_MESSAGE);	
				
				interpolation_slider.setEnabled(true);
				play.setEnabled(true);
				status.setText("");
			}
		});
		
		play.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				play.setEnabled(false);
				interpolation_slider.setEnabled(false);
				save_interpolation_to_picture.setEnabled(false);
				show_footprint = false;
				
				geom_to_show_id = 0;
				
			    play_task = new TimerTask() 
			    {
			    	int counter = 0;
			    	
			        public void run() 
			        {
			        	jp.repaint();
			        	
			        	// To show the first frame.
			        	if(counter > 2)
			        	{
				    		if(geom_to_show_id < max) 
				    			geom_to_show_id++;	    			
				    		else 
				    		{
				    			timer.cancel();
				    			
								interpolation_slider.setValue(interpolation_slider.getMaximum());
								play.setEnabled(true);
								interpolation_slider.setEnabled(true);
								save_interpolation_to_picture.setEnabled(true);
				    		}			        		
			        	}
			        	
			        	counter++;
			        }
			    };
			    
				timer = new Timer(/*true*/);
				timer.scheduleAtFixedRate(play_task, 0, Long.parseLong(rate_tx.getText()));
			}
		});
		
		are_input_geometries_valid_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Main m = d.get_main();
				is_p_valid = m.is_valid_geometry(geometries[0]);
				is_q_valid = m.is_valid_geometry(geometries[geometries.length - 1]);
				
			    show_info_about_geometry_validity = true;		    
			    jp.repaint();
			}
		});
		
		check_interpolation_validity_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Main m = d.get_main();			
				boolean is_geometry_valid = true;
				is_interpolation_valid = true;
				
				granularity = geometries.length;
				num_invalid_geometries = 0;
				
				geometries_validity_info = new Boolean[granularity];
				
				for(int j = 0; j < geometries.length; j++)
				{
					is_geometry_valid = m.is_valid_geometry(geometries[j]);
					
					geometries_validity_info[j] = is_geometry_valid;
					
					if(!is_geometry_valid)
					{
						is_interpolation_valid = false;
						num_invalid_geometries++;
					}
				}
						
			    show_info_about_interpolation_validity = true;		
			    jp.repaint();
			}
		});
		
		show_metrics_in_chart_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Main functions = d.get_main();
				
				// Metric.
				
				String metric = String.valueOf(metrics.getSelectedItem());
				
				// Metric values.
				
				HashMap<String, Double> v = new HashMap<String, Double>();
				
				// Chart title and axis legends.
				
				String chart_title = "";
				String chart_x_axis_legend = "";
				String chart_y_axis_legend = "";
				
				if(!ref_geom_wkt_tx.getText().isEmpty())
					ref_geom_wkt = ref_geom_wkt_tx.getText();
				else
					ref_geom_wkt = geometries[0];
				
				// Compute metric.
				
				double lower_range = 0;
				double upper_range = 100;
				double step = 0.1;
				boolean set_range = false;
				
				switch(metric)
				{
					case "Area":
						for(int j = 0; j < geometries.length; j++)
							v.put(String.valueOf(j), functions.numerical_metric(geometries[j], Metrics.AREA.get_value()));	
						
						chart_title = "Evolution of the Area";
						chart_x_axis_legend = "Observations";
						chart_y_axis_legend = "Area (Dimensionless)";
						break;
					case "Perimeter":
						for(int j = 0; j < geometries.length; j++)
							v.put(String.valueOf(j), functions.numerical_metric(geometries[j], Metrics.PERIMETER.get_value()));
						
						chart_title = "Evolution of the Perimeter";
						chart_x_axis_legend = "Observations";
						chart_y_axis_legend = "Perimeter (Dimensionless)";
						break;
					case "Hausdorff Distance":
						// Use the same reference geometry.
						if(use_ref_geom_cb.isSelected())
						{
							for(int j = 0; j < geometries.length; j++)
								v.put(String.valueOf(j), functions.compare_geometries(ref_geom_wkt, geometries[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected()));					
						}
						else
						{
							// Put 0?
							v.put(String.valueOf(0), functions.compare_geometries(geometries[0], geometries[0], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected()));
							
							for(int j = 1; j < geometries.length; j++)
								v.put(String.valueOf(j), functions.compare_geometries(geometries[j - 1], geometries[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected()));	
						}

						chart_title = "Evolution of the Hausdorff Distance";
						chart_x_axis_legend = "Observations";
						chart_y_axis_legend = "Hausdorff Distance (Dimensionless)";
						break;
					case "Jaccard Index":
						if(use_ref_geom_cb.isSelected())
						{
							for(int j = 0; j < geometries.length; j++)
								v.put(String.valueOf(j), functions.compare_geometries(ref_geom_wkt, geometries[j], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected()));				
						}
						else
						{
							// Put 1?
							v.put(String.valueOf(0), functions.compare_geometries(geometries[0], geometries[0], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected()));	
							
							for(int j = 1; j < geometries.length; j++)
								v.put(String.valueOf(j), functions.compare_geometries(geometries[j - 1], geometries[j], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected()));	
						}

						chart_title = "Evolution of the Jaccard Index";
						chart_x_axis_legend = "Observations";
						chart_y_axis_legend = "[0, 1]";						
						break;
					case "Jaccard Distance":
						if(use_ref_geom_cb.isSelected())
						{
							for(int j = 0; j < geometries.length; j++)
								v.put(String.valueOf(j), functions.compare_geometries(ref_geom_wkt, geometries[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected()));				
						}
						else
						{
							// Put 0?
							v.put(String.valueOf(0), functions.compare_geometries(geometries[0], geometries[0], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected()));	
							
							for(int j = 1; j < geometries.length; j++)
								v.put(String.valueOf(j), functions.compare_geometries(geometries[j - 1], geometries[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected()));	
						}

						chart_title = "Evolution of the Jaccard Distance";
						chart_x_axis_legend = "Observations";
						chart_y_axis_legend = "[0, 1]";
						
						lower_range = 0;
						upper_range = 1;
						step = 0.05;
						set_range = true;
						break;
				}
							
				// Show the chart.
				
				LineChart line_chart = new LineChart(chart_title, chart_x_axis_legend, chart_y_axis_legend, v, true, lower_range, upper_range, step, set_range);
			    line_chart.pack();
			    
			    RefineryUtilities.centerFrameOnScreen(line_chart);
			    line_chart.setVisible(true);
			}
		});
		
		compare_methods_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Main functions = d.get_main();
				String metric = String.valueOf(metrics.getSelectedItem());
				
				int n_series = 3;
				Double[] metrics_statistics = new Double[dn * n_series];
				String[] keys = new String[n_series];
				boolean[] show = new boolean[n_series];
				
				show[0] = false;
				show[1] = false;
				show[2] = false;
				
				keys[0] = "Librip";
				keys[1] = "PySpatiotemporalGeom";
				keys[2] = "RigidInterpolationLib";
				
				String[] keys_to_show = new String[n_series];
				String[] arr = null;
				int n_series_to_show = 0;
				
				keys_to_show[0] = "";
				keys_to_show[1] = "";
				keys_to_show[2] = "";
				
				if(!ref_geom_wkt_tx.getText().isEmpty())
					ref_geom_wkt = ref_geom_wkt_tx.getText();
				else
					ref_geom_wkt = geometries[0];
				
				int h = 0;
				
				if(librip_cb.isSelected())
				{	
    				arr = librip.during_period(w_p, w_q, db, de, dn, "overlap");
    				
    				if(arr != null && arr.length > 0)
    				{
        				keys_to_show[n_series_to_show] = "Librip";
        				n_series_to_show++;

        				switch(metric)
        				{
        					case "Area":
        						for(int j = 0; j < dn; j++)
        							metrics_statistics[j] = functions.numerical_metric(arr[j], Metrics.AREA.get_value());
        						break;
        					case "Perimeter":
        						for(int j = 0; j < dn; j++)
        							metrics_statistics[j] = functions.numerical_metric(arr[j], Metrics.PERIMETER.get_value());
        						break;
        					case "Hausdorff Distance":
        						if(use_ref_geom_cb.isSelected())
        						{
            						for(int j = 0; j < dn; j++)
            							metrics_statistics[j] = functions.compare_geometries(ref_geom_wkt, arr[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
        						}
        						else
        						{
        							metrics_statistics[0] = functions.compare_geometries(arr[0], arr[0], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
        							
            						for(int j = 1; j < dn; j++)
            							metrics_statistics[j] = functions.compare_geometries(arr[j - 1], arr[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
        						}
        						break;
        					case "Jaccard Index":
        						if(use_ref_geom_cb.isSelected())
        						{
            						for(int j = 0; j < dn; j++)
            							metrics_statistics[j] = functions.compare_geometries(ref_geom_wkt, arr[j], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());
        						}
        						else
        						{
        							metrics_statistics[0] = functions.compare_geometries(arr[0], arr[0], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());
        							
            						for(int j = 1; j < dn; j++)
            							metrics_statistics[j] = functions.compare_geometries(arr[j - 1], arr[j], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());
        						}
        						break;
        					case "Jaccard Distance":
        						if(use_ref_geom_cb.isSelected())
        						{
            						for(int j = 0; j < dn; j++)
            							metrics_statistics[j] = functions.compare_geometries(ref_geom_wkt, arr[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
        						}
        						else
        						{
        							metrics_statistics[0] = functions.compare_geometries(arr[0], arr[0], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
        							
            						for(int j = 1; j < dn; j++)
            							metrics_statistics[j] = functions.compare_geometries(arr[j - 1], arr[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
        						}
        						break;       						
        				}
        				
        				h += dn;
    				}
				}

				if(pyspatiotemporalgeom_cb.isSelected())
				{	
    				arr = pyspatiotemporalgeom_lib.during_period(
    						((Double) db_i).intValue(), 
    						((Double)de_i).intValue(), 
    						dn, 
    						w_p, 
    						w_q, 
    						((Double)db).intValue(), 
    						((Double)de).intValue());
    				
    				if(arr != null && arr.length > 0)
    				{
        				keys_to_show[n_series_to_show] = "PySpatiotemporalGeom";
        				n_series_to_show++;
        				
        				switch(metric)
        				{
        					case "Area":
        						for(int j = 0; j < dn; j++)
        							metrics_statistics[h + j] = functions.numerical_metric(arr[j], Metrics.AREA.get_value());
        						break;
        					case "Perimeter":
        						for(int j = 0; j < dn; j++)
        							metrics_statistics[h + j] = functions.numerical_metric(arr[j], Metrics.PERIMETER.get_value());
        						break;
        					case "Hausdorff Distance":
        						if(use_ref_geom_cb.isSelected())
        						{
            						for(int j = 0; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(ref_geom_wkt, arr[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
            					}
        						else
        						{
        							metrics_statistics[h] = functions.compare_geometries(arr[0], arr[0], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
        							
            						for(int j = 1; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(arr[j - 1], arr[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
        						}
        						break;
        					case "Jaccard Index":
        						if(use_ref_geom_cb.isSelected())
        						{
            						for(int j = 0; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(ref_geom_wkt, arr[j], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());
            					}
        						else
        						{
        							metrics_statistics[h] = functions.compare_geometries(arr[0], arr[0], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());
        							
            						for(int j = 1; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(arr[j - 1], arr[j], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());
            					}
        						break;
        					case "Jaccard Distance":
        						if(use_ref_geom_cb.isSelected())
        						{
            						for(int j = 0; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(ref_geom_wkt, arr[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
            					}
        						else
        						{
        							metrics_statistics[h] = functions.compare_geometries(arr[0], arr[0], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
        							
            						for(int j = 1; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(arr[j - 1], arr[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
            					}
        						break;
        				}
        				
        				h += dn;
    				}
				}
				
				if(rigid_interpolation_lib_cb.isSelected())
				{						
        			arr = rigid.during_period(db, de, w_p, w_q, db_i, de_i, dn, InterpolationMethod.COMPATIBLE.get_value(), d.get_cw(), d.get_threshold(), 0);

    				if(arr != null && arr.length > 0)
    				{
        				keys_to_show[n_series_to_show] = "RigidInterpolationLib";
        				n_series_to_show++;
        				
        				switch(metric)
        				{
        					case "Area":
        						for(int j = 0; j < dn; j++)
        							metrics_statistics[h + j] = functions.numerical_metric(arr[j], Metrics.AREA.get_value());
        						break;
        					case "Perimeter":
        						for(int j = 0; j < dn; j++)
        							metrics_statistics[h + j] = functions.numerical_metric(arr[j], Metrics.PERIMETER.get_value());
        						break;
        					case "Hausdorff Distance":
        						if(use_ref_geom_cb.isSelected())
        						{
            						for(int j = 0; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(ref_geom_wkt, arr[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
        						}
        						else
        						{
        							metrics_statistics[h] = functions.compare_geometries(arr[0], arr[0], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
        							
            						for(int j = 1; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(arr[j - 1], arr[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected());
        						}
        						break;
        					case "Jaccard Index":
        						if(use_ref_geom_cb.isSelected())
        						{
            						for(int j = 0; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(ref_geom_wkt, arr[j], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());        							
        						}
        						else
        						{
        							metrics_statistics[h] = functions.compare_geometries(arr[0], arr[0], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());
        							
            						for(int j = 1; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(arr[j - 1], arr[j], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected());
        						}
        						break;
        					case "Jaccard Distance":
        						if(use_ref_geom_cb.isSelected())
        						{
            						for(int j = 0; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(ref_geom_wkt, arr[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());        							
        						}
        						else
        						{
        							metrics_statistics[h] = functions.compare_geometries(arr[0], arr[0], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
        							
            						for(int j = 1; j < dn; j++)
            							metrics_statistics[h + j] = functions.compare_geometries(arr[j - 1], arr[j], SimilarityMetrics.JACCARD_DISTANCE.get_value(), align_geom_cb.isSelected());
        						}
        						break;
        				}
        				
        				h += dn;
    				}
				}
				
				if(n_series_to_show > 0)
				{
					String chart_title = "";
					String chart_x_axis_legend = "";
					String chart_y_axis_legend = "";
					
					switch(metric)
					{
						case "Area":
							chart_title = "Evolution of the Area";
							chart_x_axis_legend = "Observations";
							chart_y_axis_legend = "Area (Dimensionless)";
							break;
						case "Perimeter":
							chart_title = "Evolution of the Perimeter";
							chart_x_axis_legend = "Observations";
							chart_y_axis_legend = "Perimeter (Dimensionless)";
							break;
						case "Hausdorff Distance":
							chart_title = "Evolution of the Hausdorff Distance";
							chart_x_axis_legend = "Observations";
							chart_y_axis_legend = "Hausdorff Distance (Dimensionless)";
							break;
						case "Jaccard Index":
							chart_title = "Evolution of the Jaccard Index";
							chart_x_axis_legend = "Observations";
							chart_y_axis_legend = "[0, 1]";				
							break;
						case "Jaccard Distance":
							chart_title = "Evolution of the Jaccard Distance";
							chart_x_axis_legend = "Observations";
							chart_y_axis_legend = "[0, 1]";				
							break;
					}
					
					LineChart2 line_chart = new LineChart2(chart_title, chart_x_axis_legend, chart_y_axis_legend, metrics_statistics, keys_to_show, n_series_to_show, dn);   
				    line_chart.pack();
				    
				    RefineryUtilities.centerFrameOnScreen(line_chart);   
				    line_chart.setVisible(true);
				}
				else
					JOptionPane.showMessageDialog(null, "Nothing to compare!", "Metrics", JOptionPane.INFORMATION_MESSAGE);
				
				
				// Metric.
				
				//String metric = String.valueOf(metrics.getSelectedItem());
				
				// Metric values.
				
				//HashMap<String, Double> v = new HashMap<String, Double>();
				
				// Chart title and axis legends.
				
				//String chart_title = "";
				//String chart_x_axis_legend = "";
				//String chart_y_axis_legend = "";
				
				// Compute metric.
				
				/*
				switch(metric)
				{
					case "Area":
						for(int j = 0; j < geometries.length; j++)
							v.put(String.valueOf(j), functions.numerical_metric(geometries[j], Metrics.AREA.get_value()));	
						
						chart_title = "Evolution of the Area";
						chart_x_axis_legend = "Observations";
						chart_y_axis_legend = "Area (Dimensionless)";
						break;
					case "Perimeter":
						for(int j = 0; j < geometries.length; j++)
							v.put(String.valueOf(j), functions.numerical_metric(geometries[j], Metrics.PERIMETER.get_value()));
						
						chart_title = "Evolution of the Perimeter";
						chart_x_axis_legend = "Observations";
						chart_y_axis_legend = "Perimeter (Dimensionless)";
						break;
					case "Hausdorff Distance":				
						for(int j = 0; j < geometries.length; j++)
							v.put(String.valueOf(j), functions.compare_geometries(ref_geom_wkt, geometries[j], SimilarityMetrics.HAUSDORFF_DISTANCE.get_value(), align_geom_cb.isSelected()));
						
						chart_title = "Evolution of the Hausdorff Distance";
						chart_x_axis_legend = "Observations";
						chart_y_axis_legend = "Hausdorff Distance (Dimensionless)";
						break;
					case "Jaccard Index":
						for(int j = 0; j < geometries.length; j++)
							v.put(String.valueOf(j), functions.compare_geometries(ref_geom_wkt, geometries[j], SimilarityMetrics.JACCARD_INDEX.get_value(), align_geom_cb.isSelected()));
						
						chart_title = "Evolution of the Jaccard Index";
						chart_x_axis_legend = "Observations";
						chart_y_axis_legend = "%";						
						break;
				}
				*/
				
				// Show the chart.
				
				/*
				LineChart line_chart = new LineChart(chart_title, chart_x_axis_legend, chart_y_axis_legend, v, true);   
			    line_chart.pack();
			    
			    RefineryUtilities.centerFrameOnScreen(line_chart);   
			    line_chart.setVisible(true);
			    */
			}
		});

		visualize_metric_ev_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Main functions = d.get_main();
				
				// Metric.
				
				String metric = String.valueOf(metrics.getSelectedItem());
				
				String[] arr = new String[geometries.length * 2];
				String[] res;
				String[] metrics = new String[geometries.length];
				
				int n = geometries.length;
				
				//int counter = 0;
				
				SimilarityMetrics geom_sim_metric = SimilarityMetrics.JACCARD_INDEX;
				
				if(metric == "Hausdorff Distance")
					geom_sim_metric = SimilarityMetrics.HAUSDORFF_DISTANCE;
				else if(metric == "Jaccard Distance")
					geom_sim_metric = SimilarityMetrics.JACCARD_DISTANCE;
				
				String[] ref_objects_wkt = null;
				
				// Compute metric.
				
				switch(metric)
				{
					case "Area":
						for(int j = 0; j < n; j++)
							metrics[j] = String.valueOf(functions.numerical_metric(geometries[j], Metrics.AREA.get_value()));
						//arr = geometries;
						break;
					case "Perimeter":
						for(int j = 0; j < n; j++)
							metrics[j] = String.valueOf(functions.numerical_metric(geometries[j], Metrics.PERIMETER.get_value()));
						//arr = geometries;
						break;
					case "Hausdorff Distance":
					case "Jaccard Index":
					case "Jaccard Distance":
						
						if(!ref_geom_wkt_tx.getText().isEmpty())
							ref_geom_wkt = ref_geom_wkt_tx.getText();
						else
							ref_geom_wkt = geometries[0];
						
						
						if(use_ref_geom_cb.isSelected())
						{
							for(int j = 0; j < n; j++)
							{
								res = functions.compare_geometries_2(ref_geom_wkt, geometries[j], geom_sim_metric.get_value(), align_geom_cb.isSelected());
								arr[j] = res[0];
								metrics[j] = res[1];
							}							
						}
						else
						{
							ref_objects_wkt = new String[n];
							
							res = functions.compare_geometries_2(geometries[0], geometries[0], geom_sim_metric.get_value(), align_geom_cb.isSelected());
							arr[0] = res[0];
							metrics[0] = res[1];
							
							ref_objects_wkt[0] = geometries[0];
														
							for(int j = 1; j < n; j++)
							{
								res = functions.compare_geometries_2(geometries[j - 1], geometries[j], geom_sim_metric.get_value(), align_geom_cb.isSelected());
								arr[j] = res[0];
								metrics[j] = res[1];
								
								ref_objects_wkt[j] = geometries[j - 1];
							}
						}
						
						//arr = new String[geometries.length * 2];
						/*
						for(int j = 0; j < n; j++)
						{
							res = functions.compare_geometries_2(ref_geom_wkt, geometries[j], geom_sim_metric.get_value(), align_geom_cb.isSelected());
							arr[j] = res[0];
							metrics[j] = res[1];
						}
						*/
						
						for(int j = 0; j < metrics.length; j++)
							arr[n + j] = metrics[j];
						
						break;
				}
				
				int type = 1;
				String ref_geom = null;
				
				switch(metric)
				{
					case "Perimeter":
						type = 2;
						break;
					case "Hausdorff Distance":
						type = 3;
						ref_geom = ref_geom_wkt;
						break;
					case "Jaccard Index":
						type = 4;
						ref_geom = ref_geom_wkt;
						break;
					case "Jaccard Distance":
						type = 5;
						ref_geom = ref_geom_wkt;
						break;
				}

				if(type < 3)
				{
					for(int j = 0; j < n; j++)
						arr[j] = geometries[j];
					
					for(int j = 0; j < metrics.length; j++)
						arr[n + j] = metrics[j];
				}
				
    			Data _d = new Data(type, arr, ref_geom, d.get_main(), d.get_interpolation_method(), d.get_threshold(), d.get_cw(), ref_objects_wkt); 
    			final Data in = _d;
    			
    			MetricViz mviz = new MetricViz(in);
    			mviz.setVisible(true);
			}
		});
		
		interpolation_slider.addChangeListener(new ChangeListener() 
		{
		    public void stateChanged(ChangeEvent e) 
		    {
		        JSlider s = (JSlider) e.getSource();
		        int i = (int) s.getValue();

		        // Geometries.
		        
		    	if(i < geometries.length)
		    		geom_to_show_id = i;
		    	
		    	n_samples.setText("N� Samples: " + geometries.length + " :: " + (geom_to_show_id + 1));
		    	
		    	// Bboxes.
		    	
		    	//if(bboxes != null && i < bboxes.length)
		    	//	bbox_to_show_id = i;
				
		    	jp.repaint();
		    }
		});
		
		zoom_slider.addChangeListener(new ChangeListener() 
		{
		    public void stateChanged(ChangeEvent e) 
		    {
		        JSlider s = (JSlider) e.getSource();
		        int desired_zoom_level = (int) s.getValue();
		        DrawGraphs c = (DrawGraphs) jp;
		        double diff = desired_zoom_level - zoomLevel;
		        
		        if(diff > 0 && desired_zoom_level < maxZoomLevel)
		        {
		        	zoomLevel = desired_zoom_level;
            		c.scale(diff, diff);
            		c.translate(true);
		        }
		        else if(diff < 0 && desired_zoom_level > minZoomLevel)
		        {
		        	zoomLevel = desired_zoom_level;
            		c.scale(diff, diff);
            		c.translate(false);
		        }
					
		    	jp.repaint(); 
		    }
		});
		
		wkt_save_bt.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
		    {
				
				JOptionPane.showMessageDialog(null, "Click to start!", "Save", JOptionPane.INFORMATION_MESSAGE);
				
				String filename = wkt_save_in_file.getText();
				String wkt;

				PrintWriter writer = null;
				
				try {
					writer = new PrintWriter(filename, "UTF-8");
				} catch (FileNotFoundException | UnsupportedEncodingException e) 
				{
					e.printStackTrace();
				}

		    	for(int i = 0; i < geometries.length; i++) 
		    	{
		    		wkt = geometries[i];
		    		writer.println(wkt);
		    	}
				
				writer.close();
		    	
				JOptionPane.showMessageDialog(null, "Saved!", "Save", JOptionPane.INFORMATION_MESSAGE);	
		    }
		});
	}
}