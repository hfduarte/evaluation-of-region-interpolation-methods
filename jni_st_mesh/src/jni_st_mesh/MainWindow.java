package jni_st_mesh;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextArea;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class MainWindow extends JFrame 
{
	private static final long serialVersionUID = -7003878987581378628L;

	private JPanel contentPane;
	
	// Source and target geometries wkt.
	private JTextArea wkt_p;
	private JTextArea wkt_q;
	
	// Interval.
	private JTextField b_tb;
	private JTextField e_tb;
	
	// Period.
	private JTextField bi_tb;
	private JTextField ei_tb;
	
	// Number of observations.
	private JTextField n_samples_tb;
	
	// Type/format.
	private JComboBox<String> geom_type_cb;
	private JButton interpolate_bt;
	
	// Interpolation method.
	private Main m;
	private JComboBox<String> method_cb;
	private InterpolationMethod interpolation_method;
	
	private JComboBox<String> cw_cb;
	private JTextField coll_threshold_tb;
	private JComboBox<String> f_type_cb;
	private JFrame f;
	
	// Reference geometry wkt (used to compute similarity metrics).
	private JTextArea wkt_ref;
	private JButton sample_wkt_bt;
	private int example_id;
	private JButton load_inv_sample_bt;
	private JButton poly_with_holes_bt;
	private JButton load_multi_poly_bt;
	private methods.PySpatiotemporalGeom pyspatiotemporalgeom_lib;
	private methods.Librip librip;
	private methods.RigidInterpolation rigid;
	private JTextField at_instant_tb;
	private JLabel state_lb;
	private JButton at_bt;
	private String librip_config_file_path;
	private String pyspatiotemporalgeom_config_file_path;
	private String alexa_config_file_path;
	private JComboBox<String> librip_strat;
	
	public MainWindow(Main m) 
	{
		this.m = m;
		f = this;
		example_id = 0;
		draw_UI();
		add_listeners();
		
		// load configuration.
			
		librip_config_file_path = "D:\\java\\jni_st_mesh\\config\\Librip.cfg";
		pyspatiotemporalgeom_config_file_path = "D:\\java\\jni_st_mesh\\config\\PySpatiotemporalGeom.cfg";
		alexa_config_file_path = "D:\\java\\jni_st_mesh\\config\\SpTMorphing.cfg";
		
		librip = new methods.Librip(librip_config_file_path);
		pyspatiotemporalgeom_lib = new methods.PySpatiotemporalGeom(pyspatiotemporalgeom_config_file_path);
		rigid = new methods.RigidInterpolation(alexa_config_file_path, this.m);
	}
	
	public void draw_UI() 
	{
		setTitle("Main Window");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    setSize(830, 700);
	    setLocationRelativeTo(null);
	    setExtendedState(f.getExtendedState() | JFrame.MAXIMIZED_BOTH);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(8, 1, 10, 10));
		
		JLabel lb_p = new JLabel("P");
		lb_p.setToolTipText("Source geometry");
		contentPane.add(lb_p);
		
		wkt_p = new JTextArea();

		wkt_p.setText("POLYGON((949 781, 926 812, 891.6666656434536 796.66666620969772, 857.3333312869072 781.33333241939545, 823 766, 804 736, 807 689, 810 642, 833.6666673719883 612.99999913573265, 857.33333474397659 583.9999982714653, 881 555, 892 572, 909 576, 914 583, 922 580, 928 597, 920 640, 946 640, 968 647.5, 990 655, 949 781))");
		
		wkt_p.setToolTipText("P wkt");
		lb_p.setLabelFor(wkt_p);
		wkt_p.setRows(5);
		wkt_p.setColumns(1);
		contentPane.add(wkt_p);
		
		wkt_p.setLineWrap(true);
		wkt_p.setWrapStyleWord(true);
		
		JScrollPane scroll_p = 
			    new JScrollPane(wkt_p,
			                    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			                    JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		
		scroll_p.setPreferredSize(new Dimension(250, 250));
		contentPane.add(scroll_p);
		
		JLabel lb_q = new JLabel("Q");
		lb_q.setToolTipText("Target geometry");
		contentPane.add(lb_q);
		
		wkt_q = new JTextArea();
		wkt_q.setColumns(1);
		wkt_q.setRows(5);
		
		wkt_q.setText("POLYGON((972 774, 955 806, 922 817, 905 816, 850 782, 822 755, 812 697, 812 656, 835 582, 848 565, 863 562, 876 576, 893 576, 899 583, 909 579, 917 592, 916 637, 962 632, 968 638, 990 640, 972 774))");
		
		wkt_q.setToolTipText("Q wkt");
		lb_q.setLabelFor(wkt_q);
		contentPane.add(wkt_q);
		
		wkt_q.setLineWrap(true);
		wkt_q.setWrapStyleWord(true);
		
		JScrollPane scroll_q = 
			    new JScrollPane(wkt_q,
			                    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			                    JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		
		scroll_q.setPreferredSize(new Dimension(250, 250));
		contentPane.add(scroll_q);

		JPanel interval = new JPanel();
		interval.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		contentPane.add(interval);
		interval.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		b_tb = new JTextField();
		b_tb.setToolTipText("Begin");
		b_tb.setText("1000");
		interval.add(b_tb);
		b_tb.setColumns(6);
		
		e_tb = new JTextField();
		e_tb.setToolTipText("End");
		e_tb.setText("2000");
		interval.add(e_tb);
		e_tb.setColumns(6);
		
		JPanel format = new JPanel();
		format.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		contentPane.add(format);
			
		geom_type_cb = new JComboBox<>();
		geom_type_cb.setToolTipText("Type geometry during interpolation");
		geom_type_cb.setModel(new DefaultComboBoxModel<>(new String[] {"Polygon", "Mesh"}));
		geom_type_cb.setSelectedIndex(0);
		geom_type_cb.setMaximumRowCount(2);
		format.add(geom_type_cb);
		
		f_type_cb = new JComboBox<>();
		f_type_cb.setToolTipText("Functionality required");
		f_type_cb.setModel(new DefaultComboBoxModel(new String[] {"eval"}));
		f_type_cb.setSelectedIndex(0);
		format.add(f_type_cb);
		
		sample_wkt_bt = new JButton("Load Sample WKT");
		format.add(sample_wkt_bt);
		
		load_inv_sample_bt = new JButton("Load Invalid Sample");
		format.add(load_inv_sample_bt);
		
		poly_with_holes_bt = new JButton("Poly With Holes");
		format.add(poly_with_holes_bt);
		
		load_multi_poly_bt = new JButton("MultiPolygon");
		format.add(load_multi_poly_bt);
				
		wkt_ref = new JTextArea();
		wkt_ref.setToolTipText("Wkt of the reference geometry");
		wkt_ref.setWrapStyleWord(true);
		wkt_ref.setLineWrap(true);
		wkt_ref.setColumns(1);
		wkt_ref.setRows(5);
		
		JScrollPane scroll_ref = 
			    new JScrollPane(wkt_ref,
			                    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			                    JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		
		scroll_q.setPreferredSize(new Dimension(250, 250));
		contentPane.add(scroll_ref);
		
		JPanel at_p = new JPanel();
		at_p.setBorder(new LineBorder(new Color(0, 0, 0)));
		contentPane.add(at_p);
		
		coll_threshold_tb = new JTextField();
		coll_threshold_tb.setText("0.005");
		coll_threshold_tb.setToolTipText("Threshold to find collinear points.");
		at_p.add(coll_threshold_tb);
		coll_threshold_tb.setColumns(6);
		
		cw_cb = new JComboBox<>();
		cw_cb.setModel(new DefaultComboBoxModel<>(new String[] {"cw", "ccw"}));
		at_p.add(cw_cb);
		
		method_cb = new JComboBox<>();
		method_cb.setToolTipText("Interpolation Method");
		method_cb.setModel(new DefaultComboBoxModel(new String[] {"Compatible", "Equilateral", "McKenney", "Heinz-Gutting"}));
		method_cb.setSelectedIndex(1);
		at_p.add(method_cb);
		
		librip_strat = new JComboBox<>();
		librip_strat.setModel(new DefaultComboBoxModel<String>(new String[] {"overlap", "simple", "mw", "lowerleft"}));
		at_p.add(librip_strat);
		
		bi_tb = new JTextField();
		bi_tb.setForeground(new Color(0, 0, 128));
		bi_tb.setToolTipText("Begin Instant");
		bi_tb.setText("1000");
		at_p.add(bi_tb);
		bi_tb.setColumns(6);
		
		ei_tb = new JTextField();
		ei_tb.setForeground(new Color(0, 0, 128));
		ei_tb.setToolTipText("End Instant");
		ei_tb.setText("2000");
		at_p.add(ei_tb);
		ei_tb.setColumns(6);
		
		at_instant_tb = new JTextField();
		at_instant_tb.setBackground(new Color(173, 216, 230));
		at_instant_tb.setForeground(new Color(0, 0, 0));
		at_instant_tb.setText("1000");
		at_p.add(at_instant_tb);
		at_instant_tb.setColumns(6);
		
		n_samples_tb = new JTextField();
		n_samples_tb.setBackground(new Color(152, 251, 152));
		n_samples_tb.setToolTipText("N\u00BA Samples");
		n_samples_tb.setText("100");
		at_p.add(n_samples_tb);
		n_samples_tb.setColumns(4);
		
		interpolate_bt = new JButton("Create M. Region");
		at_p.add(interpolate_bt);
		
		at_bt = new JButton("At");
		at_p.add(at_bt);
		
		state_lb = new JLabel("");
		at_p.add(state_lb);
	}

	public void add_listeners()
	{
		sample_wkt_bt.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent event) 
            {   
            	switch(f_type_cb.getSelectedIndex())
            	{
            		// Visualization, comparison and evaluation of interpolation methods.
	            	case 0:
	            		wkt_p.setText("POLYGON((949 781, 926 812, 891.6666656434536 796.66666620969772, 857.3333312869072 781.33333241939545, 823 766, 804 736, 807 689, 810 642, 833.6666673719883 612.99999913573265, 857.33333474397659 583.9999982714653, 881 555, 892 572, 909 576, 914 583, 922 580, 928 597, 920 640, 946 640, 968 647.5, 990 655, 949 781))");
	            		wkt_q.setText("POLYGON((972 774, 955 806, 922 817, 905 816, 850 782, 822 755, 812 697, 812 656, 835 582, 848 565, 863 562, 876 576, 893 576, 899 583, 909 579, 917 592, 916 637, 962 632, 968 638, 990 640, 972 774))");
	            		break;
	            	// Study of interpolation methods for the RIP.
	            	case 1:
	            		wkt_p.setText("POLYGON((1 1, 5 1, 4 3, 1 1))");
	            		wkt_q.setText("POLYGON((22.5 2, 24.25 0, 23.25 3, 22.5 2))");
	            		break;
	            	// Study of approximation curves.
	            	case 2:
	            		
	            		if(example_id == 0)
	            		{
		            		wkt_p.setText("POLYGON((1 1, 5 1, 4 3, 1 1))");
		            		wkt_q.setText("POLYGON((22.5 2, 24.25 0, 23.25 3, 22.5 2))");
		            		example_id = 1;
	            		}
	            		else
	            		{
		            		wkt_p.setText("POLYGON((1 1, 3 2, 4 1, 5 3, 4 5, 4 3, 2 3, 1 1))");
		            		wkt_q.setText("POLYGON((11 1, 13 1.5, 14.5 1, 15 3.5, 13 6, 11 2.5, 9 2.5, 11 1))");
		            		example_id = 0;	            			
	            		}

	            		break;
		            // Study of parameterized trochoids.
		            case 3:
	            		wkt_p.setText("POLYGON((1 1, 5 1, 4 3, 1 1))");
	            		wkt_q.setText("POLYGON((22.5 2, 24.25 0, 23.25 3, 22.5 2))");
		            	break;
		            // Study of bezier curves.
	            	case 4:
	            		
	            		//...
	            		
	            		break;
	            	// The study of bounding boxes.
	            	case 5:
	            		wkt_p.setText("POLYGON((1 1, 4 2, 5 1, 5 4.5, 2 3, 2.5 2.5, 1 1))");
	            		wkt_q.setText("POLYGON((11 11, 14 12, 15 11, 15 14.5, 12 13, 12.5 12.5, 11 11))");            		
	            		break;
	            	// The study of curves sampling.
	            	case 6:
	            		wkt_p.setText("POLYGON((1 1, 5 1, 4 3, 1 1))");
	            		wkt_q.setText("POLYGON((22.5 2, 24.25 0, 23.25 3, 22.5 2))");
	            		break;
	            	// The study of trochoids and ravdoids.
	            	case 7:
	            		wkt_p.setText("POLYGON((1 1, 4 2, 5 1, 5 4.5, 2 3, 2.5 2.5, 1 1))");
	            		wkt_q.setText("POLYGON((11 1, 14 2, 15 1, 15 4.5, 12 3, 12.5 2.5, 11 1))");      
	            		break;
		            // The study of discrete solutions.
		            case 8:
		            	wkt_p.setText("POLYGON((1 1, 5 1, 4 3.5, 1 1))");
		            	wkt_q.setText("POLYGON((22.5 12, 24.25 10, 23.25 13, 22.5 12))");      
		            	break;
			        // The study of interpolation.
			        case 9:
			           	wkt_p.setText("POLYGON((1 1, 5 1, 4 3.5, 1 1))");
			           	wkt_q.setText("POLYGON((22.5 12, 24.25 10, 23.25 13, 22.5 12))");      
			           	break;
			        case 10:
	            		wkt_p.setText("POLYGON((1 1, 5 1, 4 3, 1 1))");
	            		wkt_q.setText("POLYGON((22.5 2, 24.25 0, 23.25 3, 22.5 2))");
			        	break;
	            	default:
	            		
	            		//...
	            		
            	}            	
            }
        });
		
		load_inv_sample_bt.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent event) 
            {   
        		wkt_p.setText("POLYGON((949 781, 926 812, 823 766, 804 736, 810 642, 881 555, 886.5 563.5, 892 572, 909 576, 914 583, 922 580, 924.00000005960464 585.66666683554649, 926.00000011920929 591.33333367109299, 928 597, 924 618.5, 920 640, 928.66666692495346 640, 937.33333384990692 640, 946 640, 990 655, 949 781))");
        		wkt_q.setText("POLYGON((972 774, 955 806, 922 817, 905 816, 850 782, 822 755, 812 697, 812 656, 835 582, 848 565, 863 562, 876 576, 893 576, 899 583, 909 579, 917 592, 916 637, 962 632, 968 638, 990 640, 972 774))");
            }
        });
            
		poly_with_holes_bt.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent event) 
            {   
        		wkt_p.setText("POLYGON ((3 3, 12 7, 4 10, 3 3), (4 6, 6 7, 5 8, 4 6), (7 6, 8 7, 7 8, 7 6))");
        		wkt_q.setText("POLYGON ((17 4, 27 0, 20.5 12, 17 4), (19 5.5, 20 5, 20 7, 19 5.5), (22 4, 24 3, 24 4, 22 4))");
            }
        });
        
		load_multi_poly_bt.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent event) 
            {   
        		wkt_p.setText("MULTIPOLYGON (((3 3, 12 7, 4 10, 3 3), (4 6, 6 7, 5 8, 4 6), (7 6, 8 7, 7 8, 7 6)), ((7 2, 9 2, 7 4, 7 2), (7.5 2.5, 8 2.5, 7.5 3, 7.5 2.5)))");
        		wkt_q.setText("MULTIPOLYGON (((17 4, 27 0, 20.5 12, 17 4), (19 5.5, 20 5, 20 7, 19 5.5), (22 4, 24 3, 24 4, 22 4)), ((27 8, 29 8, 29 10, 27 8), (28 8.5, 28.5 8.5, 28.5 9, 28 8.5)))");
            }
        });
        
		/**
		 * Create a moving region and study the evolution of a geometry during an interval of time.
		 */
		interpolate_bt.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent event) 
            {            	
            	// Values in the GUI.
            	
            	String w_p = wkt_p.getText();
            	String w_q = wkt_q.getText();
            	String w_r = wkt_ref.getText();
            	
            	String strat = (String) librip_strat.getSelectedItem();
            	
            	double db = Double.parseDouble(b_tb.getText());
            	double de = Double.parseDouble(e_tb.getText());
            	
            	double db_i = Double.parseDouble(bi_tb.getText());
            	double de_i = Double.parseDouble(ei_tb.getText());
            	
            	int dn = Integer.parseInt(n_samples_tb.getText());
            	
            	String method = String.valueOf(method_cb.getSelectedItem());
            	
            	String[] arr = null;
            	Data d = null;
            	
            	String type_geometry_during_interpolation = geom_type_cb.getItemAt(geom_type_cb.getSelectedIndex());
            	
            	// Interpolation Method.
            	
            	if(method == "Compatible")
            		interpolation_method = InterpolationMethod.COMPATIBLE;
            	else if(method == "Equilateral")
            		interpolation_method = InterpolationMethod.EQUILATERAL;
            	else if(method == "McKenney")
            		interpolation_method = InterpolationMethod.PY_SPATIOTEMPORAL_GEOM;            
            	else if(method == "Heinz-Gutting")
            		interpolation_method = InterpolationMethod.LIBRIP;
            	//else if(method == "New")
            	//	interpolation_method = InterpolationMethod.NEW;      
            	else
            		interpolation_method = null;
            	          	
            	boolean is_cw = true;
            	
            	String cw_str = String.valueOf(cw_cb.getSelectedItem());
        		
            	if(cw_str == "ccw")
            		is_cw = false;  	

            	double collinearity_threshold = Double.parseDouble(coll_threshold_tb.getText());
            	
            	if((interpolation_method == InterpolationMethod.PY_SPATIOTEMPORAL_GEOM || interpolation_method == InterpolationMethod.LIBRIP) && f_type_cb.getSelectedIndex() != 0)
            	{
            		JOptionPane.showMessageDialog(null, "Not applicable to " + interpolation_method + "'s method.", "Main Window.", JOptionPane.INFORMATION_MESSAGE);
            		return;
            	}
            	
            	if(interpolation_method == InterpolationMethod.NEW && f_type_cb.getSelectedIndex() != 9)
            	{
            		JOptionPane.showMessageDialog(null, "Not applicable to " + interpolation_method + "'s method.", "Main Window.", JOptionPane.INFORMATION_MESSAGE);
            		return;
            	}

            	if(type_geometry_during_interpolation == "Polygon") 
            	{
            		// PySpatiotemporalGeom
            		
            		if(interpolation_method == InterpolationMethod.PY_SPATIOTEMPORAL_GEOM)
            		{
            			methods.Method f = methods.Method.DuringPeriod;

            			// Alternatives:
            			//		1. Integer.parseInt()
            			// 		2. 
            			
            			int t = Integer.parseInt(at_instant_tb.getText());
            			
            			if(f == methods.Method.DuringPeriod)
            				arr = pyspatiotemporalgeom_lib.during_period(
            						Integer.parseInt(bi_tb.getText()), 
            						Integer.parseInt(ei_tb.getText()), 
            						dn, 
            						w_p, 
            						w_q, 
            						Integer.parseInt(b_tb.getText()), 
            						Integer.parseInt(e_tb.getText()));
            			else
            				arr = pyspatiotemporalgeom_lib.at(
            						w_p, 
            						w_q, 
            						Integer.parseInt(b_tb.getText()), 
            						Integer.parseInt(e_tb.getText()), 
            						t);
            		}
            		else if(interpolation_method == InterpolationMethod.LIBRIP)
            		{
            			methods.Method f = methods.Method.DuringPeriod;
            			double t = Double.parseDouble(at_instant_tb.getText());
            			
            			System.out.println(strat);
            			
            			if(f == methods.Method.DuringPeriod)
            				arr = librip.during_period(
            						w_p,
            						w_q,
            						db,
            						de,
            						dn,
            						strat);
            			else
            				arr = librip.at(
            						w_p,
            						w_q,
            						db,
            						de,
            						t,
            						strat);
            		}
            		
            		else if(interpolation_method == InterpolationMethod.NEW)
            		{
        				double[] params = new double[20];
        				params[0] = 0;
        				params[1] = dn;
        				
        				// Command to exec.
        				params[2] = 1;
        				
        				// At instant.
        				params[3] = 0;
        				
        				// Point x, y coordinates.
        				params[4] = 0;
        				params[5] = 0;
        				
        				//...
        				
            			arr = m.study_interpolation_methods(db, de, w_p, w_q, params);
            			d = new Data(2, arr, w_r, m, interpolation_method, collinearity_threshold, is_cw, null);
            		}
            		
            		// Alexa.
            		
            		else
            			arr = rigid.during_period(db, de, w_p, w_q, db_i, de_i, dn, interpolation_method.get_value(), is_cw, collinearity_threshold, 0);
           		
            		d = new Data(2, arr, w_r, m, interpolation_method, collinearity_threshold, is_cw, null); 
            	}
            	
            	// Meshes.
            	
            	else 
            	{
            		if(f_type_cb.getSelectedIndex() == 0 && (interpolation_method == InterpolationMethod.COMPATIBLE || interpolation_method == InterpolationMethod.EQUILATERAL))
            		{
            			arr = rigid.during_period_mesh(db, de, w_p, w_q, db_i, de_i, dn, interpolation_method.get_value(), is_cw, collinearity_threshold, 0);
            			d = new Data(3, arr, w_r, m, interpolation_method, collinearity_threshold, is_cw, null);
            		}
            		else
            			JOptionPane.showMessageDialog(null, "NOT_APPLICABLE_TO_MESHES", "Main Window.", JOptionPane.INFORMATION_MESSAGE);
            	}
            	
            	final Data in = d;
            	
            	// Functionallity required.
            	           	
            	if((interpolation_method == InterpolationMethod.PY_SPATIOTEMPORAL_GEOM || interpolation_method == InterpolationMethod.LIBRIP) && f_type_cb.getSelectedIndex() != 0)
            		JOptionPane.showMessageDialog(null, "Not applicable to " + interpolation_method + "'s method.", "Main Window.", JOptionPane.INFORMATION_MESSAGE);
            	else
            	{
                	switch(f_type_cb.getSelectedIndex())
                	{
                		// Visualization, comparison and evaluation of interpolation methods.
    	            	case 0:
    	            		
    	            		if(arr == null)
    	            		{
    	            			JOptionPane.showMessageDialog(null, "An error occured. No data returned by the method.", "RIP.", JOptionPane.INFORMATION_MESSAGE);
    	            			break;
    	            		}
    	            		
    	            		// Polygons.
    	            		if(type_geometry_during_interpolation == "Polygon") 
    	            		{
        	            		Vce vce = new Vce(in, pyspatiotemporalgeom_lib, librip, rigid, db, de, w_p, w_q, db_i, de_i, dn);
        	            		vce.setVisible(true);
    	            		}
    	            		// Meshes.
    	            		else
    	            		{
    	            			JOptionPane.showMessageDialog(null, "Not available in this application.", "Main Window.", JOptionPane.INFORMATION_MESSAGE);
                        		//VizMesh viz_meshes = new VizMesh(m, in, w_p, w_q, db, de, dn);
                        		//viz_meshes.setVisible(true);
    	            		}
    	            		break;
    	            	default:
    	            		//...   	            		
                	}            		
            	}
            }
        });

		at_bt.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent event) 
            {
            	// Values in the GUI.
            	
            	String w_p = wkt_p.getText();
            	String w_q = wkt_q.getText();
            	String w_r = wkt_ref.getText();
            	
            	String strat = (String) librip_strat.getSelectedItem();
            	
            	if(w_r.isEmpty())
            		w_r = w_p;
            	
            	double db = Double.parseDouble(b_tb.getText());
            	double de = Double.parseDouble(e_tb.getText());
            	            	
            	String method = String.valueOf(method_cb.getSelectedItem());
            	
            	String[] arr = null;
            	Data d = null;
            	
            	String type_geometry_during_interpolation = geom_type_cb.getItemAt(geom_type_cb.getSelectedIndex());
            	
            	// Interpolation Method.
            	
            	if(method == "Compatible")
            		interpolation_method = InterpolationMethod.COMPATIBLE;
            	else if(method == "Equilateral")
            		interpolation_method = InterpolationMethod.EQUILATERAL;
            	else if(method == "McKenney")
            		interpolation_method = InterpolationMethod.PY_SPATIOTEMPORAL_GEOM;            
            	else if(method == "Heinz-Gutting")
            		interpolation_method = InterpolationMethod.LIBRIP;
            	//else if(method == "New")
            	//	interpolation_method = InterpolationMethod.NEW;      
            	else
            		interpolation_method = null;
            	          	
            	boolean is_cw = true;
            	
            	String cw_str = String.valueOf(cw_cb.getSelectedItem());
        		
            	if(cw_str == "ccw")
            		is_cw = false;  	

            	double collinearity_threshold = Double.parseDouble(coll_threshold_tb.getText());
            	
            	if(type_geometry_during_interpolation == "Polygon") 
            	{
            		if(interpolation_method == InterpolationMethod.PY_SPATIOTEMPORAL_GEOM)
            		{
            			int t = Integer.parseInt(at_instant_tb.getText());
           				arr = pyspatiotemporalgeom_lib.at(
           						w_p, 
           						w_q, 
           						Integer.parseInt(b_tb.getText()), 
           						Integer.parseInt(e_tb.getText()), 
           						t);
            		}
            		else if(interpolation_method == InterpolationMethod.LIBRIP)
            		{
            			double t = Double.parseDouble(at_instant_tb.getText());
           				arr = librip.at(w_p, w_q, db, de, t, strat);
            		}
            		
            		// Alexa.
            		
            		else
            		{
            			double t = Double.parseDouble(at_instant_tb.getText());
            			arr = rigid.at(db, de, w_p, w_q, t, interpolation_method.get_value(), is_cw, collinearity_threshold/*, 0*/);
            		}
               		
            		d = new Data(2, arr, w_r, m, interpolation_method, collinearity_threshold, is_cw, null); 
            	}
            	
            	// Meshes.
            	
            	else 
            	{
            		if(f_type_cb.getSelectedIndex() == 0 && (interpolation_method == InterpolationMethod.COMPATIBLE || interpolation_method == InterpolationMethod.EQUILATERAL))
            		{
            			double t = Double.parseDouble(at_instant_tb.getText());
            			arr = rigid.at_mesh(db, de, w_p, w_q, t, interpolation_method.get_value(), is_cw, collinearity_threshold/*, 0*/);
            			d = new Data(3, arr, w_r, m, interpolation_method, collinearity_threshold, is_cw, null); 
            		}
            		else
            			JOptionPane.showMessageDialog(null, "NOT_APPLICABLE_TO_MESHES", "Main Window.", JOptionPane.INFORMATION_MESSAGE);
            	}
            	
            	final Data in = d;
            	
            	if(f_type_cb.getSelectedIndex() == 0)
            	{            		
            		// Polygon.
            		if(type_geometry_during_interpolation == "Polygon") 
            		{
	            		Vce vce = new Vce(in, pyspatiotemporalgeom_lib, librip, rigid, db, de, w_p, w_q, db, de, 1);
	            		vce.setVisible(true);
            		}
            		// Mesh.
            		else
            		{
            			JOptionPane.showMessageDialog(null, "Not available in this application.", "Main Window.", JOptionPane.INFORMATION_MESSAGE);
                		//VizMesh viz_meshes = new VizMesh(m, in, w_p, w_q, db, de, 1);
                		//viz_meshes.setVisible(true);
            		}
            	}
            }
        });

	}	
}