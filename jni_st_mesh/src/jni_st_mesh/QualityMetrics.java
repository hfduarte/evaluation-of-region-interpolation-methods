package jni_st_mesh;

public enum QualityMetrics 
{
	MESH(1);
	
	private int value;    

	private QualityMetrics(int value) {
		this.value = value;
	}

	public int get_value() {
		return value;
	}
}
