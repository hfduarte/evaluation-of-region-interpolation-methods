/**
 * Create the jni interface for c++.
 * "D:\jdk1.8.0_202\bin\javah" -jni -classpath "D:\java\jni_st_mesh\src" jni_st_mesh.Main
 */
package jni_st_mesh;

import java.util.HashMap;
import java.awt.EventQueue;

/**
 * @author jduarte
 * 
 */
public class Main
{
	private static final long serialVersionUID = 4092938070485225093L;
	
	
	native boolean predicates
	(
		String mr,			// moving region representation.
		String obj,			// .
		int op,				// operation to be executed.
		int k,				// .
		double t			// instant.
	);
	
	native String intersect
	(
		String mrA,							// moving region A representation.
		String mp,							// moving point representation.
		String range,
		double temporal_granularity			// instant.
	);
	
	native String intersect2
	(
		String objA,						// .
		String objB,						// .
		String range,
		int op,
		double temporal_granularity			// instant.
	);
	
	native String intersect
	(
		String mrA,						// moving region A representation.
		String r,						// region representation.
		double temporal_granularity
	);
	
	native String geometric_period
	(
		String mr,						// moving region representation.
		String period,
		int op,							// operation to be executed.
		double temporal_granularity
	);
	
	native String geometric_at
	(
		String mr,			// moving region representation.
		int op,				// operation to be executed.
		double t			// instant.
	);
	
	native double real_at
	(
		String mr,			// moving region representation.
		int op,				// operation to be executed.
		double t			// instant.
	);
	
	native String geometric_operations
	(
		String mrA,			// moving region A representation.
		String mrB,			// moving region B representation.
		int op,				// operation to be executed.
		double t			// instant.
	);
	
	native boolean boolean_operations
	(
		String mrA,			// moving region A representation.
		String mrB,			// moving region B representation.
		int op,				// operation to be executed.
		double t			// instant.
	);
	
	native double real_operations
	(
		String mrA,			// moving region A representation.
		String mrB,			// moving region B representation.
		int op,				// operation to be executed.
		double t			// instant.
	);
	
	//Agile
	
	/**
	 * Returns the polygon wkt.
	 */
	public native String at_instant_poly(
		double b,					// begin instant
		double e,					// end instant
		String source_wkt,			// P
		String target_wkt,			// Q
		double t,					// instant
		int triangulation_method,	// triangulation method to use
		boolean cw,					// vertices order
		double threshold			// threshold to find collinear points
	);
	
	/**
	 * Returns an array of Polygon wkts.
	 */
	public native String[] during_period_poly
	(
		double b,					// begin instant
		double e,					// end instant
		String source_wkt,			// P
		String target_wkt,			// Q
		double period_b,			// period begin
		double period_e,			// period end
		int num_samples,			// number of samples
		int triangulation_method,	// triangulation method to use
		boolean cw,					// vertices order
		double threshold,			// threshold to find collinear points
		double add_rotation
	);

	/**
	 * Returns a Multipolygon wkt.
	 */
	public native String at_instant_mesh
	(
		double b,					// begin instant
		double e,					// end instant
		String source_wkt,			// P
		String target_wkt,			// Q
		double t,					// at instant
		int triangulation_method,	// triangulation method to use
		boolean cw,					// vertices order
		double threshold			// threshold to find collinear points
	);
	
	/**
	 * Returns an array of Multipolygon wkts.
	 */
	public native String[] during_period_mesh
	(
		double b,					// begin instant
		double e,					// end instant
		String source_wkt,			// P
		String target_wkt,			// Q
		double period_b,			// period begin
		double period_e,			// period end
		int num_samples,			// number of samples
		int triangulation_method,	// triangulation method to use
		boolean cw,					// vertices order
		double threshold			// threshold to find collinear points
	);
	
	/**
	 * Returns an array of Polygon wkts of the bboxs during interpolation.
	 */
	native String[] during_period_poly_bbox
	(
		double b,					// begin instant
		double e,					// end instant
		String source_wkt,			// P
		String target_wkt,			// Q
		double period_b,			// period begin
		double period_e,			// period end
		int num_samples,			// number of samples
		int triangulation_method,	// triangulation method to use
		boolean cw,					// vertices order
		double threshold			// threshold to find collinear points
	);
		
	/**
	 * Returns a map of ststistics.
	 * Available ststistics are defined in Ststistics.java (Ststistics enum).
	 */
	native HashMap<String, Double> ststistics
	(
		double b,
		double e,
		String source_wkt,
		String target_wkt,
		int num_samples,			// info to create the unit
		int triangulation_method,	// triangulation method to use
		boolean cw,					// vertices order
		double threshold,			// threshold to find collinear points
		int statistic_type			// type of statistic
	);
	
	native Double[] mesh_quality_ststistics
	(
		double b,
		double e,
		String source_wkt,
		String target_wkt,
		int num_samples,			// info to create the unit
		int triangulation_method,	// triangulation method to use
		boolean cw,					// vertices order
		double threshold,			// threshold to find collinear points
		int metric_to_collect		// type of metric
	);
		
	/**
	 * Returns a map of quality measures.
	 */
	native HashMap<String, Double> quality_measures
	(
		String geometry_wkt,		// Geometry wkt
		int triangulation_method,	// triangulation method to use
		boolean cw,					// vertices order
		double threshold,			// threshold to find collinear points
		int options					// for future use
	);
	
	/**
	 * Returns a numerical metric about a geometry.
	 */
	native double numerical_metric
	(
		String geometry_wkt,		// Geometry wkt
		int metric					// metric to use
	);
	
	/**
	 * Compares two geometries using a metric.
	 */
	native double compare_geometries
	(
		String ref_geometry_wkt,	// Reference geometry wkt, i.e. the geometry to compare with
		String geometry_wkt,		// Geometry wkt
		int metric,					// metric to use for comparison
		boolean align_geometries
	);
	
	/**
	 * Compares two geometries using a metric and returns the aligned geometry for visualization.
	 */
	native String[] compare_geometries_2
	(
		String ref_geometry_wkt,	// Reference geometry wkt, i.e. the geometry to compare with
		String geometry_wkt,		// Geometry wkt
		int metric,					// metric to use for comparison
		boolean align_geometries
	);
	
	native String[] trochoids
	(
		double b,
		double e,
		String source_wkt,
		String target_wkt,
		int num_samples,			// info to create the unit
		int triangulation_method,	// triangulation method to use
		boolean cw,					// vertices order
		double threshold			// threshold to find collinear points
	);
	
	native String[] def_trochoids
	(
		double b,
		double e,
		String source_wkt,
		String target_wkt,
		int num_samples,			// info to create the unit
		int triangulation_method,	// triangulation method to use
		boolean cw,					// vertices order
		double threshold,			// threshold to find collinear points
		int options,
		double add_rotation
	);
	
	native String[] curve_approximation
	(
		double b,
		double e,
		String source_wkt,
		String target_wkt,
		int num_samples,			// info to create the unit
		int triangulation_method,	// triangulation method to use
		boolean cw,					// vertices order
		double threshold,			// threshold to find collinear points
		int options,
		double add_rotation,
		int n_samples_per_span,
		int n_segments_per_span,
		double seg_i_x,
		double seg_i_y,
		double seg_e_x,
		double seg_e_y,
		double t
	);
	
	native String[] curve_sampling_approx_footprint
	(
		double b,
		double e,
		String source_wkt,
		String target_wkt,
		int num_samples,			// info to create the unit
		int triangulation_method,	// triangulation method to use
		boolean cw,					// vertices order
		double threshold,			// threshold to find collinear points
		int options,
		double add_rotation,
		int n_samples_per_span,
		int n_segments_per_span,
		double seg_i_x,
		double seg_i_y,
		double seg_e_x,
		double seg_e_y,
		double t
	);
	
	native String[] iterative_method
	(
		double b,
		double e,
		String source_wkt,
		String target_wkt,
		int triangulation_method,	// triangulation method to use
		boolean cw,					// vertices order
		double[] params,			// params
		String region,
		double b2,
		double e2,
		String source_wkt2,
		String target_wkt2		
	);
	
	native String[] study_interpolation_methods
	(
		double b,
		double e,
		String source_wkt,
		String target_wkt,
		double[] params
	);
	
	native String[] out
	(
		double b,
		double e,
		String source_wkt,
		String target_wkt,
		double[] params
	);
	
	native String[] msegments
	(
		double b,
		double e,
		String source_wkt,
		String target_wkt,
		double[] params
	);
	
	native boolean is_valid_geometry
	(
		String wkt
	);
	
	native String[] tests
	(
		int op
	);
	
	/**
	 * Params:
	  		double threshold,			// threshold to find collinear points
			int options,
			double add_rotation,
			int n_samples_per_span,
			int n_segments_per_span,
			double seg_i_x,
			double seg_i_y,
			double seg_e_x,
			double seg_e_y,
			double t
	 */
	
	/**
	 * For the study of bezier curves.
	 */
	native String[] bezier
	(
		int bezier_type,
		int cmd_to_execute,
		int n_samples,
		double t,
		// line segment.
		double seg_i_x,
		double seg_i_y,
		double seg_e_x,
		double seg_e_y,
		// control points
		double cp0_x,
		double cp0_y,
		double cp1_x,
		double cp1_y,
		double cp2_x,
		double cp2_y,
		double cp3_x,
		double cp3_y,
		double[] v,
		int v_size
	);
	
	public Main() 
    {
		// Load the c++ jni interface.
		System.loadLibrary("java_mesh");
    }
	
	public static void main(String[] args) 
    {
        Main ex = new Main();
        
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() {
				try {
					MainWindow main = new MainWindow(ex);
					main.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
     }
}