package jni_st_mesh;

public enum RealOperations 
{
	DISTANCE(1), HAUSDORFF(2);
	
	private int value;    

	private RealOperations(int value) {
		this.value = value;
	}

	public int get_value() {
		return value;
	}
}
