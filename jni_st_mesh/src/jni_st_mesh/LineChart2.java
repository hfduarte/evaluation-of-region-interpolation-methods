package jni_st_mesh;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class LineChart2 extends JFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String title;
	private String x_leg;
	private String y_leg;
	private Double[] metrics_statistics;
	private String[] keys;
	private int n_series;
	private int n_samples_per_series;
	
	public LineChart2(String title, String x_legend, String y_legend, Double[] metrics_statistics, String[] keys, int n_series, int n_samples_per_series) 
	{
		this.title = title;
		this.x_leg = x_legend;
		this.y_leg = y_legend;
		
		this.metrics_statistics = metrics_statistics;
		this.n_series = n_series;
		this.keys = keys;		
		
		this.n_samples_per_series = n_samples_per_series;
		
        initUI();
    }

    private void initUI() {

        XYDataset dataset = createDataset();
        JFreeChart chart = createChart(dataset);
        
        ChartPanel chartPanel = new ChartPanel(chart);
        
        chartPanel.setDomainZoomable(true);
        chartPanel.setRangeZoomable(true);
        
        chartPanel.setPreferredSize(new Dimension(800, 600)); 

        setContentPane(chartPanel);
        setTitle(title);
    }

    private XYDataset createDataset() 
    {
    	XYSeriesCollection dataset = new XYSeriesCollection();
    	
    	XYSeries series;
    	int g = 0;
    	int f = 0;
    	
    	for(int j = 0; j < n_series; j++)
    	{
    		series = new XYSeries(keys[j]);
    		
    		for(int h = g; h < g + n_samples_per_series; h++)
    		{
    			series.add(f, metrics_statistics[h]);
    			f++;
    		}
    		
    		f = 0;
    		g += n_samples_per_series;
    		    		
    		dataset.addSeries(series);
    	}
    	
        return dataset;
    }

    private JFreeChart createChart(final XYDataset dataset) 
    {
    	JFreeChart chart = ChartFactory.createXYLineChart(title, x_leg, y_leg, dataset, PlotOrientation.VERTICAL, true, true, false);  
    	
    	chart.setBackgroundPaint(Color.white);
    	
        XYPlot plot = chart.getXYPlot();
        
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        
        for(int y = 0; y < n_series; y++)
        {
        	if(y == 0)
        		renderer.setSeriesPaint(y, Color.black);
        	else if(y == 1)
        		renderer.setSeriesPaint(y, Color.magenta);
        	else if(y == 2)
        		renderer.setSeriesPaint(y, Color.red);
        	else if(y == 3)
        		renderer.setSeriesPaint(y, Color.green);
        	else if(y == 4)
        		renderer.setSeriesPaint(y, Color.blue);
        	else
        		renderer.setSeriesPaint(y, Color.orange);
            
            renderer.setSeriesStroke(y, new BasicStroke(1.0f));
        }
        
        renderer.setBaseShapesVisible(false);
        renderer.setBaseShapesFilled(false);
        renderer.setDrawSeriesLineAsPath(true);
        
        plot.setRenderer(renderer);
        plot.setBackgroundPaint(Color.white);

	    plot.setDomainGridlinePaint(Color.black);   
	    plot.setRangeGridlinePaint(Color.black);
	    
	    plot.getDomainAxis().setLowerMargin(0.0);
	    plot.getDomainAxis().setUpperMargin(0.0);    

        return chart;
    }
}