package methods;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import jni_st_mesh.InterpolationMethod;
import jni_st_mesh.Main;

public class RigidInterpolation 
{
	private String config_file_path;
	private Main m;
	private double collinearity_threshold;

	public RigidInterpolation(String config_file_path, Main main)
	{
		this.config_file_path = config_file_path;
		this.m = main;
		this.collinearity_threshold = 0;
		
		load_config();
	}
	
	private void load_config()
	{		
		Properties stlib_config = new Properties();
		
		 try 
		 {
			 InputStream in = new FileInputStream(config_file_path);
			 stlib_config.load(in);		
		}
		 catch(Exception eta){
		     eta.printStackTrace();
		}
		
		collinearity_threshold = Double.parseDouble(stlib_config.getProperty("collinearity_threshold"));
	}
	
	public String[] at(
			double b,						// begin instant
			double e,						// end instant
			String source_wkt,				// P
			String target_wkt,				// Q
			double t,						// instant
			int triangulation_method,		// triangulation method to use
			boolean cw,						// vertices order
			double collinearity_threshold	// threshold to find collinear points
		)
	{
		String[] arr = new String[1];
		arr[0] = m.at_instant_poly(
				b,
				e,
				source_wkt,
				target_wkt,
				t,
				triangulation_method,
				cw,
				collinearity_threshold);
		
		return arr;
	}
	
	public String[] at_mesh(
			double b,						// begin instant
			double e,						// end instant
			String source_wkt,				// P
			String target_wkt,				// Q
			double t,						// instant
			int triangulation_method,		// triangulation method to use
			boolean cw,						// vertices order
			double collinearity_threshold	// threshold to find collinear points
		)
	{		
		String[] arr = new String[1];
		arr[0] = m.at_instant_mesh(
				b,
				e,
				source_wkt,
				target_wkt,
				t,
				triangulation_method,
				cw,
				collinearity_threshold);
		
		return arr;
	}
	
	public String[] during_period(
			double b,						// begin instant
			double e,						// end instant
			String source_wkt,				// P
			String target_wkt,				// Q
			double period_b,				// period begin
			double period_e,				// period end
			int num_samples,				// number of samples
			int triangulation_method,		// triangulation method to use
			boolean cw,						// vertices order
			double collinearity_threshold,	// threshold to find collinear points
			double add_rotation				// add extra rotation (use multiples of 2PI)
		)
	{
		return m.during_period_poly(
				b, e, source_wkt, target_wkt, 
				period_b, period_e, 
				num_samples, 
				triangulation_method, 
				cw, 
				collinearity_threshold, 
				add_rotation);
	}
	
	public String[] during_period_mesh(
			double b,						// begin instant
			double e,						// end instant
			String source_wkt,				// P
			String target_wkt,				// Q
			double period_b,				// period begin
			double period_e,				// period end
			int num_samples,				// number of samples
			int triangulation_method,		// triangulation method to use
			boolean cw,						// vertices order
			double collinearity_threshold,	// threshold to find collinear points
			double add_rotation				// add extra rotation (use multiples of 2PI)
		)
	{
		return m.during_period_mesh(
				b, e, 
				source_wkt, target_wkt, 
				period_b, period_e, 
				num_samples, 
				triangulation_method, 
				cw, 
				collinearity_threshold 
				/*add_rotation*/);
	}	
}
