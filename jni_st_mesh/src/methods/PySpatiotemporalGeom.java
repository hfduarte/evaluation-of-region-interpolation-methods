package methods;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * This class acts as a connector between the main application and the PySpatiotemporalGeom library.
 * 
 * @author jduarte
 *
 */

public class PySpatiotemporalGeom 
{
	private String config_file_path;
	private String python_script_path;
	private String[] cmd;
	
	public PySpatiotemporalGeom(String config_file_path)
	{
		this.config_file_path = config_file_path;
		this.cmd = new String[11];
		
		load_config();
	}
	
	private void load_config()
	{		
		Properties librip_config = new Properties();
		
		 try 
		 {
			 InputStream in = new FileInputStream(config_file_path);
			 librip_config.load(in);		
		}
		 catch(Exception eta){
		     eta.printStackTrace();
		}
		
		 python_script_path = librip_config.getProperty("python_script_path");
	}
	
	/**
	 * Resets the command buffer.
	 */
	private void reset_cmd_buffer()
	{
		for(int j = 0; j < cmd.length; j++)
			cmd[j] = "";
	}
	
	/**
	 * Gets an observation at instant instant.
	 * 
	 * @param source_geometry_wkt
	 * @param target_geometry_wkt
	 * @param unit_interval_begin_instant
	 * @param unit_interval_end_instant
	 * @param instant
	 * @return
	 */
	public String[] at(
			String source_geometry_wkt,
			String target_geometry_wkt,
			int unit_interval_begin_instant,
			int unit_interval_end_instant,
			int instant)
	{
		return execute_cmd(
				0, 0, 1,
				source_geometry_wkt,
				target_geometry_wkt,
				unit_interval_begin_instant,
				unit_interval_end_instant,
				instant,
				Method.At);
	}
	
	/**
	 * Gets a number of observations during a specified period of time.
	 * 
	 * @param query_time_begin
	 * @param query_time_end
	 * @param n_samples
	 * @param source_geometry_wkt
	 * @param target_geometry_wkt
	 * @param unit_interval_begin_instant
	 * @param unit_interval_end_instant
	 * @return
	 */
	public String[] during_period(
			int query_time_begin,
			int query_time_end,
			int n_samples,
			String source_geometry_wkt,
			String target_geometry_wkt,
			int unit_interval_begin_instant,
			int unit_interval_end_instant)
	{
		return execute_cmd(
				query_time_begin,
				query_time_end,
				n_samples,
				source_geometry_wkt,
				target_geometry_wkt,
				unit_interval_begin_instant,
				unit_interval_end_instant,
				0,
				Method.DuringPeriod);
	}
	
	/**
	 * Executes a command in the PySpatiotemporalGeom library.
	 * 
	 * @param query_time_begin
	 * @param query_time_end
	 * @param n_samples
	 * @param source_geometry_wkt
	 * @param target_geometry_wkt
	 * @param unit_interval_begin_instant
	 * @param unit_interval_end_instant
	 * @param instant
	 * @param method_to_exec
	 * @return
	 */
	private String[] execute_cmd(
			int query_time_begin,
			int query_time_end,
			int n_samples,
			String source_geometry_wkt,
			String target_geometry_wkt,
			int unit_interval_begin_instant,
			int unit_interval_end_instant,
			int instant,
			Method method_to_exec)
	{
		cmd[0] = "python";
		cmd[1] = python_script_path;
		
		// Parameters.
		
		cmd[2] = Integer.toString(query_time_begin);
		cmd[3] = Integer.toString(query_time_end);
		cmd[4] = Integer.toString(n_samples);
		
		// Geometries.
		
		cmd[5] = source_geometry_wkt;
		cmd[6] = target_geometry_wkt;
		
		// Interval.
		
		cmd[7] = Integer.toString(unit_interval_begin_instant);
		cmd[8] = Integer.toString(unit_interval_end_instant);
		
		cmd[9] = Integer.toString(instant);
		cmd[10] = Integer.toString(method_to_exec.get_value());

		// Execute command.
		
		Runtime rt = Runtime.getRuntime();
		Process pr = null;
		
		// Debug.
		
		try
		{
			pr = rt.exec(cmd);
		}
		catch (IOException e1) 
		{
			e1.printStackTrace();
		}
		
		// Read output.
		
		BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		String line = "";
		
		String[] arr = new String[n_samples];
		int i = 0;
		
		try
		{
			// Build array of geometries in wkt representation.
			
			while((line = bfr.readLine()) != null)
			{
				//System.out.println(line);
				arr[i] = line;
				i++;
			}
		}
		catch (IOException e1)
		{
			e1.printStackTrace();
		}
		
		return arr;
	}
}