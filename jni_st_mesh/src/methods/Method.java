package methods;

public enum Method 
{
	At(0), DuringPeriod(1);
	
	private int value;    

	private Method(int value) {
		this.value = value;
	}

	public int get_value() {
		return value;
	}
}
