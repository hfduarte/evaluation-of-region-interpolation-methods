package methods;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

import javax.swing.JOptionPane;

import sj.lang.ESInterface;
import sj.lang.IntByReference;
import sj.lang.ListExpr;

public class Librip 
{
    private String user;
    private String pwd;
    private String server;
    private int port;
    private String db;
    
    private String config_file_path;
	
	public Librip(String config_file_path)
	{
		this.config_file_path = config_file_path;
		load_config();
	}
	
	private void load_config()
	{		
		Properties librip_config = new Properties();
		
		 try 
		 {
			 InputStream in = new FileInputStream(config_file_path);
			 librip_config.load(in);		
		}
		 catch(Exception eta){
		     eta.printStackTrace();
		}
		
		user = librip_config.getProperty("user");
		pwd = librip_config.getProperty("pwd");
		server = librip_config.getProperty("server");
		port = Integer.parseInt(librip_config.getProperty("port"));
		db = librip_config.getProperty("db");
	}
	
	/**
	 * Converts the wkt of a polygon to a RLIST string representation.
	 */
	public static String polygon_wkt_to_rlist_str(String wkt)
	{
		String rlist_str = "";
		
		String[] tokens = wkt.split("\\(");
		String[] points;
		String[] point;
		
		double x = 0;
		double y = 0;
		
		String token;
		String p;
		
		if(!tokens[0].trim().equals("POLYGON"))
			return "";
		
		// Exterior list.
		
		rlist_str += "((";
		
		for(int j = 1; j < tokens.length; j++)
		{
			token = tokens[j].trim();
			
			if(token.isEmpty())
				continue;				

			token = token.replace(")", "");
			
			points = token.split(",");
			
			rlist_str += "(";
			
			for(int h = 0; h < points.length - 1; h++)
			{
				p = points[h].trim();
				
				if(p.isEmpty())
					continue;
				
				point = p.split(" ");
				
				if(point.length != 2)
					return "";
				
				x = Double.parseDouble(point[0]);
				y = Double.parseDouble(point[1]);
				
				rlist_str += "( " + x + " " + y + " )";
			}
			
			rlist_str += ")";
		}
		
		// Close exterior list.
		
		rlist_str += "))";
		
		return rlist_str;
	}
	
	/**
	 * Converts the wkt of a multipolygon to a RLIST string representation.
	 */
	public static String multi_polygon_wkt_to_rlist_str(String wkt)
	{
		String rlist_str = "";
		
		String[] tokens = wkt.split("\\(");
		String[] points;
		String[] point;
		
		double x = 0;
		double y = 0;
		
		String token;
		String p;
		
		if(!tokens[0].trim().equals("MULTIPOLYGON"))
			return "";
		
		// Exterior list.
		
		rlist_str += "(";
				
		boolean on = false;
		
		for(int j = 1; j < tokens.length; j++)
		{
			token = tokens[j].trim();
			
			if(token.isEmpty())
				continue;				

			token = token.replace("),", "");
			token = token.replace("))", "");
			
			if(!on)
			{
				rlist_str += "(";
				on = true;
			}
			
			points = token.split(",");
			
			rlist_str += "(";
			
			for(int h = 0; h < points.length - 1; h++)
			{
				p = points[h].trim();
				
				if(p.isEmpty())
					continue;
				
				point = p.split(" ");
				
				if(point.length != 2)
					return "";
				
				x = Double.parseDouble(point[0]);
				y = Double.parseDouble(point[1]);
				
				rlist_str += "( " + x + " " + y + " )";
			}
			
			rlist_str += ")";
			
			if(token.endsWith(")"))
			{
				rlist_str += ")";
				on = false;
			}
		}
		
		// Close exterior list.
		
		rlist_str += ")";
		return rlist_str;
	}
	
	/**
	 * Converts a wkt to a RLIST representation.
	 */
	public static String wkt_to_rlist_str(String wkt)
	{
		if(wkt.startsWith("POLYGON"))
			return polygon_wkt_to_rlist_str(wkt);
		else if(wkt.startsWith("MULTIPOLYGON"))
			return multi_polygon_wkt_to_rlist_str(wkt);
		else
			return "";
	}
	
	/**
	 * See:
	 * 		secondo\Javagui\viewer\hoese\LEUtils.java
	 * 
	 * Reads an numeric (a rational or integer number) out of a listexpr and calculates a numeric value as double-value
	 * @param le A ListExpr with a numeric expression
	 * @return A Double object with the numeri value or null if an error occured
	 * @see <a href="LEUtilssrc.html#readNumeric">Source</a>
	 */
	public static Double readNumeric (ListExpr le) 
	{
		if (le.isAtom()) {
			  if (! (le.atomType()==ListExpr.INT_ATOM || le.atomType()==ListExpr.REAL_ATOM)) 
			  {
				  return  null;
			  }
			  
			  if (le.atomType()==ListExpr.INT_ATOM)
				  return  new Double(le.intValue());
			  else
				  return new Double(le.realValue());
	    }
	    else 
	    {
	    	int length = le.listLength();
	    	
	    	if ((length != 5)&& (length != 6))
	    	{
	    		return  null;
	    	}
	    	
	    	if (length==5) 
	    	{
	    		if (
	    				(le.first().atomType() != ListExpr.SYMBOL_ATOM) 	|| 
	    				(le.second().atomType() != ListExpr.INT_ATOM) 		|| 
	    				(le.third().atomType() != ListExpr.INT_ATOM)		|| 
	    				(le.fourth().atomType() != ListExpr.SYMBOL_ATOM) 	|| 
	    				(le.fifth().atomType() != ListExpr.INT_ATOM)) 
	    		{
	    			return  null;
	    		}
	    		
	    		if ((!le.first().symbolValue().equals("rat")) || (!le.fourth().symbolValue().equals("/"))) 
	    		{
	    			return  null;
	    		}
	          
	    		double g = (double)le.second().intValue();
	    		return  new Double((Math.abs(g) + (double)le.third().intValue()/(double)le.fifth().intValue()));
	    	}
	    	else 
	    	{
	    		if (
	    				(le.first().atomType() != ListExpr.SYMBOL_ATOM)		|| 
	    				(le.second().atomType() != ListExpr.SYMBOL_ATOM)	||
	    				(le.third().atomType() != ListExpr.INT_ATOM) 		|| 
	    				(le.fourth().atomType() != ListExpr.INT_ATOM)		|| 
	    				(le.fifth().atomType() != ListExpr.SYMBOL_ATOM) 	|| 
	    				(le.sixth().atomType() != ListExpr.INT_ATOM)) {
	    			return  null;
	    		}
	    		
	    		if (
	        		(!le.first().symbolValue().equals("rat")) ||
	        		(!le.fifth().symbolValue().equals("/")) 	||
	        		!(le.second().symbolValue().equals("-")  	||
	        		le.second().symbolValue().equals("+") )) 
	    		{
	              	return  null;
	    		}
	          
	    		double g = (double)le.third().intValue();
	    		double v=1;
	    		if (le.second().symbolValue().equals("-")) v=-1;
	    		
	    		return  new Double(v*(Math.abs(g) + (double)le.fourth().intValue()/(double)le.sixth().intValue()));
	    	}
	    }
	}
	
	/** 
	 * See 
	 * 		secondo\Javagui\viewer\hoese\algebras\Dsplintimeregion.java
	 * 		secondo\Javagui\viewer\hoese\algebras\Dsplregion.java
	 * 
	 * Convert the Listrepresentation LE to a Area 
	 * */
	public static String ScanValue(ListExpr LE)
	{
		ArrayList<ArrayList<Double>> geom;
		ArrayList<Double> coordinates;
		
		ArrayList<ArrayList<ArrayList<Double>>> geoms = new ArrayList<>(); 
		
	     if(LE==null){
	       return null;
	     }
	     
	     Double x;
	     Double y;
	     
	     while(!LE.isEmpty())
	     {
	    	 ListExpr Face = LE.first();
	    	 LE = LE.rest();
	    	 	    	 
	    	 geom = new ArrayList<>(); 
	    	 
	    	 while(!Face.isEmpty())
	    	 {
	    		 ListExpr Cycle = Face.first();
	    		 Face = Face.rest();
	    		 
	    		 coordinates = new ArrayList<>();
	    		 	    		 
	    		 while(!Cycle.isEmpty())
	    		 {
	    			 ListExpr P = Cycle.first();
	    			 Cycle = Cycle.rest();
	    			 
	    			 x = readNumeric(P.first());
	    			 y = readNumeric(P.second());
		      
	    			 if(x!=null && y!=null)
	    			 {
	    				 coordinates.add(x);
	    				 coordinates.add(y);
	    			 }
	    			 else
	    			 {
	    				 return null;
	    			 }
	    		 }
	    		 
	    		 geom.add(coordinates);
	    	 }
	    	 
	    	 geoms.add(geom);
	     }
	     
	     // Construct wkt.
	     
	     String wkt;
	     
	     if(geoms.size() == 1)
	    	 wkt = "POLYGON ";
	     else if(geoms.size() == 0)
	    	 return "POLYGON EMPTY";
	     else
	    	 wkt = "MULTIPOLYGON (";
	     
	     for(int j = 0; j < geoms.size(); j++)
	     {
	    	 geom = geoms.get(j);
	    	 
	    	 if(j > 0)
	    		 wkt += ", (";
	    	 else
	    		 wkt += "(";
	    	 
		     for(int f = 0; f < geom.size(); f++)
		     {		    	 
		    	 coordinates = geom.get(f);
		    	 
		    	 wkt += "(";
		    	 
		    	 for(int h = 0; h < coordinates.size(); h = h + 2)
		    		 wkt += coordinates.get(h) + " " + coordinates.get(h+1) + ", ";
		    	 
		    	 wkt += coordinates.get(0) + " " + coordinates.get(1) + ")";
		    	 
		    	 if(f < geom.size() - 1)
		    		 wkt += ", ";
		     }
		     
		     wkt += ")";
	     }
	     
	     if(geoms.size() > 1)
	    	 wkt += ")";
	     
	     return wkt;
	}
	
	public static String rlist_to_wkt(ListExpr resultList)
	{
		ListExpr a_list;
		
		if (resultList.listLength() != 2)
			return null;
		
		if (!resultList.first().symbolValue().equals("iregion"))
			return null;
		
		a_list = resultList.second();
		
		return ScanValue(a_list.second());
	}

	public String[] at(
			String source_geom_wkt,
			String target_geom_wkt,
        	double tb,
        	double te,
        	double t,
        	String strategy)
	{
		return exec_cmd(
				source_geom_wkt,
				target_geom_wkt,
				tb,
				te,
				1,
				t,
				strategy,
				Method.At);
	}
	
	public String[] during_period(
			String source_geom_wkt,
			String target_geom_wkt,
        	double tb,
        	double te,
        	int n_samples,
        	String strategy)
	{
		return exec_cmd(
				source_geom_wkt,
				target_geom_wkt,
				tb,
				te,
				n_samples,
				0,
				strategy,
				Method.DuringPeriod);
	}
	
	private String[] exec_cmd(
			String source_geom_wkt,
			String target_geom_wkt,
        	double tb,
        	double te,
        	int n_samples,
        	double t,
        	String strategy,
        	Method cmd_to_exec
			)
	{
	    String source_rlist_str = wkt_to_rlist_str(source_geom_wkt);
	    String target_rlist_str = wkt_to_rlist_str(target_geom_wkt);
	    
	    /*
	    System.out.println(source_geom_wkt);
	    System.out.println(target_geom_wkt);
	    
	    System.out.println(source_rlist_str);
	    System.out.println(target_rlist_str);
	    */
	    
		//String database = database_to_use;
	    
	    ESInterface Secondointerface = new ESInterface();;
	    
	    // Server configuration.
	    
	    Secondointerface.setUserName(user);
	    Secondointerface.setPassWd(pwd);
	    Secondointerface.setHostname(server);
	    Secondointerface.setPort(port);
	    Secondointerface.useBinaryLists(true);
		
	    // Connect to secondo db.
	    
	    //JOptionPane.showMessageDialog(null, "Close to connect to Secondo.", "Secondo.", JOptionPane.INFORMATION_MESSAGE);
	    
	    boolean ok = Secondointerface.connect();
	    
	    // Connected?
	    
	    if(!ok) 
	    {
	    	JOptionPane.showMessageDialog(null, "Connection to Secondo Failled.", "Secondo.", JOptionPane.INFORMATION_MESSAGE);
	    	return null;
	    }
	    
	    ListExpr resultList = new ListExpr();
	    IntByReference error_code = new IntByReference(0);
	    IntByReference error_pos = new IntByReference(0);
	    StringBuffer error_message = new StringBuffer();
	    
	    // Open db.
	    
	    Secondointerface.secondo("open database " + db + ";", resultList, error_code, error_pos, error_message);
	               	        
	    if(error_code.value != 0)
	    {
	    	JOptionPane.showMessageDialog(null, error_message, "Secondo.", JOptionPane.INFORMATION_MESSAGE);
	    	Secondointerface.terminate();
	    	return null;
	    }
	    
	    // Create moving region and store it in the database.
	    
	    String object_name = "mr";
	    String create_m_regiom_cmd = "let " + object_name + " = interpolate2([const region value " + source_rlist_str + "], [const instant value " + tb + "], [const region value " + target_rlist_str + "], [const instant value " + te + "],\"" + strategy + "\");";
	    
	    System.out.println(create_m_regiom_cmd);
	    
	    resultList = new ListExpr();
	    error_code = new IntByReference(0);
	    error_pos = new IntByReference(0);
	    error_message = new StringBuffer();
	           	            
	    Secondointerface.secondo(create_m_regiom_cmd, resultList, error_code, error_pos, error_message);
	    
	    /*
	    System.out.println(error_code.value);
	    System.out.println(resultList);
	    System.out.println(error_pos.value);
		*/
	    
	    if(error_code.value != 0/* && error_pos.value != 0*/)
	    {
	    	JOptionPane.showMessageDialog(null, error_message + ", Err Code: " + error_code.value, "Secondo.", JOptionPane.INFORMATION_MESSAGE);
	    	Secondointerface.terminate();
	    	return null;
	    }
	    
	    //System.out.println("2");
	    	    
		String[] arr = new String[n_samples];
		int i = 0;
		
		String wkt = null;
		String cmd = null;
	    
		double dt = te - tb;
		
		// Get samples at t.
		
	    for(double j = 0; j < n_samples; j++)
	    {
	    	if(n_samples > 1)
	    		t = (j / (n_samples - 1)) * dt + tb;
	    	
	    	cmd = "query " + object_name + " atinstant [const instant value " + t + "];";
	    	
	    	//System.out.println(cmd);
	    	
	        resultList = new ListExpr();
	        error_code = new IntByReference(0);
	        error_pos = new IntByReference(0);
	        error_message = new StringBuffer();
	        
	        //System.out.println(t);
	        //System.out.println(cmd);
	        
	        Secondointerface.secondo(cmd, resultList, error_code, error_pos, error_message);

	        //System.out.println(resultList);
	        //System.out.println(error_code.value);
	        
	        //System.out.println(resultList.listLength());

	        if(resultList.toString().contains("(iregion undefined)") /*|| resultList.toString().contains("()")*/)
	        {
	        	System.out.println(cmd);
	        	JOptionPane.showMessageDialog(null, "Returned: " + resultList.toString(), "Secondo.", JOptionPane.INFORMATION_MESSAGE);
	        	//Secondointerface.terminate();
	        	//return null;
	        	arr = null;
	        	break;
	        }

	        if(error_code.value != 0)
	        {
	        	JOptionPane.showMessageDialog(null, error_message, "Secondo.", JOptionPane.INFORMATION_MESSAGE);
	        	//Secondointerface.terminate();
	        	//return null;
	        	arr = null;
	        	break;
	        }
	        
	        if(resultList.toString().contains("()"))
		        wkt = "POLYGON EMPTY";
	        else
		        wkt = rlist_to_wkt(resultList);
	        
	        //System.out.println(wkt);
	                	            
	        if(wkt == null || error_code.value != 0)
	        {
	        	JOptionPane.showMessageDialog(null, "ERR_PARSING_TO_WKT.", "Secondo.", JOptionPane.INFORMATION_MESSAGE);
	            //Secondointerface.terminate();
	            //return null;
	        	arr = null;
	        	break;
	        }
	        
			arr[i] = wkt;
			i++;
	    }
	    
	    // Clean db.
	    
	    resultList = new ListExpr();
	    error_code = new IntByReference(0);
	    error_pos = new IntByReference(0);
	    error_message = new StringBuffer();
	    
	    Secondointerface.secondo("delete " + object_name + ";", resultList, error_code, error_pos, error_message);
	    
	    if(error_code.value != 0)
	    {
	    	JOptionPane.showMessageDialog(null, error_message, "Secondo.", JOptionPane.INFORMATION_MESSAGE);
	    	Secondointerface.terminate();
	    	return null;
	    }

	    resultList = new ListExpr();
	    error_code = new IntByReference(0);
	    error_pos = new IntByReference(0);
	    error_message = new StringBuffer();
	               	        
	    Secondointerface.secondo("close database;", resultList, error_code, error_pos, error_message);
	               	        
	    if(error_code.value != 0)
	    {
	    	JOptionPane.showMessageDialog(null, error_message, "Secondo.", JOptionPane.INFORMATION_MESSAGE);
	    	Secondointerface.terminate();
	    	return null;
	    }
	    
	    // Disconnect.
	    Secondointerface.terminate();
	    
	    //System.out.println("Disconnected from Server.");
	    //JOptionPane.showMessageDialog(null, "Disconnected from Server.", "Secondo.", JOptionPane.INFORMATION_MESSAGE);
	    return arr;
	}
}
