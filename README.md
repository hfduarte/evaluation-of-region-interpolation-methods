# README #

The application used to evaluate and compare region interpolation methods using the methodology presented in the paper submeted to AGILE 2020 'On the Evaluation and Comparison of Region Interpolation Methods'.

Citation: To appear.


### Overview ###

* Summary


* /data

The data used.

* /jni_st_mesh

An eclipse project with a java interface that comunicates with the region interpolation methods.

* /video

A video presenting the application.

* Version 1.0

### Installation ###

* Installation, Configuration, Dependencies, How to run

See video: To appear

### Contacts ###

* José Duarte: IEETA, University of Aveiro, hfduarte@ua.pt